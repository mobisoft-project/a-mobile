import 'dart:convert';
import 'dart:io';
import 'package:agrimobile/core/utils/connection_status.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/models/app_response_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/notification_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/home_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/models/user_information_model.dart';
import 'package:agrimobile/features/launch/presentation/pages/splash_screen.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'features/common/data/function_utils.dart';
import 'package:firebase_messaging/firebase_messaging.dart';


import 'package:agrimobile/features/launch/presentation/pages/about_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/account_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/conseils/all_conseil_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await allTranslations.init();
  final _appSharedPreferences = AppSharedPreferences();
  runApp(
    MultiProvider(
      providers: [
        Provider.value(value: _appSharedPreferences),
        FutureProvider<ConnectionStatus>(
          create: (_) => _appSharedPreferences.getConnectionState(),
          initialData: ConnectionStatus.disconnected,
        ),
        FutureProvider<CartModel>(
          create: (_) => _appSharedPreferences.getCart(),
          initialData: CartModel.create(allItems: []),
        ),
        ChangeNotifierProvider<UserInformationModel>(
          create: (_) => UserInformationModel.create(id: -1,validationUser:"2",token:"",totalActeurs:"0",totalOffre:"0",totalExperts:"0"),
        ),
        FutureProvider<ConfigModel>(
          create: (_) => _appSharedPreferences.getApkConfig(),
          initialData: ConfigModel.create(slogan1:allTranslations.text('slogant1'),slogan2:allTranslations.text('slogant2'),
              mainColor:"F44336",offreColor:"2196F3", demandeColor:"F44336", bioColor:"4CAF50", cancelColor:"D8322B",devise:DataConstantesUtils.DEVISE,callCenter: DataConstantesUtils.CALLCENTER,logo:""),
        ),
      ],


      child:MyApp(),
    ),
  );
  //runApp(MyApp());
}

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {

  FirebaseMessaging _fcm = FirebaseMessaging();
  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();

  @override
  void initState() {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    DatabaseHelper.initDb();
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {

    });


    if (Platform.isIOS) _iosPermission();

    _fcm.getToken().then((token){
      print("Bernard $token");

      _appSharedPreferences.setRegistrationId(token);
      //sucribe to our topic
      _fcm.subscribeToTopic(FunctionUtils.topicGlobal);

    });


    _fcm.configure(
      onMessage: (Map<String, dynamic> notifInfo) async {

        print("Bernard 1");
        if (notifInfo.containsKey('data')) {
          _registerInfo(notifInfo);
        }
        Widget continueButton = FlatButton(
          child: Text("Fermer",style: new TextStyle(
              fontWeight: FontWeight.bold, fontSize: 14.0),
          ),
          onPressed:  () {
            Navigator.of(context).pop(null);
            if(notifInfo['data']['type_operation'].compareTo("2")==0){
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return HomePage("","-1","0");
                    },
                  ),
                  ModalRoute.withName("/")
              );
            }
          },
        );
        AlertDialog alert = AlertDialog(
          title: Text(notifInfo['notification']['title']),
          content: Text(notifInfo['notification']['body'],style: new TextStyle( fontSize: 15.0),
          ),
          actions: [
            continueButton,
          ],
        );
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return alert;
          },
        );
      },


      onLaunch: (Map<String, dynamic> notifInfo) async {
        print("Bernard 2");
        if (notifInfo.containsKey('data')) {
          _registerInfo(notifInfo);
        }
        // TODO optional
      },
      onResume: (Map<String, dynamic> notifInfo) async {
        print("Bernard 3");
        if (notifInfo.containsKey('data')) {
          _registerInfo(notifInfo);
        }
        // TODO optional
      },
    );


    _getConfigInfo();
    _getServerOffre();
  }

  void _iosPermission() {
    _fcm.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _fcm.onIosSettingsRegistered
        .listen((IosNotificationSettings settings)
    {
      print("Settings registered: $settings");
    });
  }


  _registerInfo(Map<String, dynamic> notifInfo) async {

    print(notifInfo);

    _appSharedPreferences.getUserInformation().then((userInfo) {

      if(userInfo.id>0){

        String status = notifInfo['data']['status'];
        if (status!=null && status.compareTo("000") == 0) {


          String type_operation=notifInfo['data']['type_operation'];
          if(type_operation.compareTo("1")==0){
            //Validation dun compte


            //mise a jour de la validation
            List<NotificationModel> allNotification=[];
            allNotification.add(NotificationModel.create(idNotifcation:int.parse(notifInfo['data']['id']),title:notifInfo['data']['title'],
                description:notifInfo['data']['description'],dateCreation:notifInfo['data']['date_creation'], idAnnonce:int.parse(notifInfo['data']['id_annonce'])));

            FunctionUtils.saveNotifications(allNotification, userInfo.id);

            _appSharedPreferences.setValidation("1");

          }else if(type_operation.compareTo("2")==0){
            //Rejet dun compte

            List<NotificationModel> allNotification=[];
            allNotification.add(NotificationModel.create(idNotifcation:int.parse(notifInfo['data']['id']),title:notifInfo['data']['title'],
                description:notifInfo['data']['description'],dateCreation:notifInfo['data']['date_creation'], idAnnonce:int.parse(notifInfo['data']['id_annonce'])));

            //deconnexion de l'utilisateur du systeme
            _appSharedPreferences.logoutUser(context);

          }else if(type_operation.compareTo("3")==0){
            //Message de traitement du compe
            //mise a jour de la validation
            List<NotificationModel> allNotification=[];
            allNotification.add(NotificationModel.create(idNotifcation:int.parse(notifInfo['data']['id']),title:notifInfo['data']['title'],
                description:notifInfo['data']['description'],dateCreation:notifInfo['data']['date_creation'], idAnnonce:int.parse(notifInfo['data']['id_annonce'])));

            FunctionUtils.saveNotifications(allNotification, userInfo.id);

          }
        }
      }

    });

  }

  _showAlertDialog(String title,String content,String type) {
    // set up the buttons


    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: ListTile(
          title: Text(title),
          subtitle: Text(content),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
      ),
    );

/*
    Widget continueButton = FlatButton(
      child: Text("Fermer",style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        if(type.compareTo("2")==0){
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return HomePage();
                },
              ),
              ModalRoute.withName("/")
          );
        }
      },
    );


    AlertDialog alert = AlertDialog(
      title: Text(title),
      content: Text(content,style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );*/
  }

  _getConfigInfo(){

    Dio dio = new Dio();
    String fullUrl="${DataConstantesUtils.FULLCONFIG_SERVER_URL}?type=1";


    dio.get(fullUrl).then((response) {


      var data = response.toString();

      Map responseMap = jsonDecode(data);
      AppResponseModel infoModel=AppResponseModel.fromMap(responseMap);
      if(infoModel.status.compareTo("ok")==0){

        UserInformationModel information=new UserInformationModel();
        information.totalActeurs=infoModel.totalActeurs;
        information.totalOffre=infoModel.totalOffre;
        information.totalExperts=infoModel.totalExperts;
        _appSharedPreferences.createSession(context,information).then((value){
          _appSharedPreferences.setApkConfig(infoModel.informations);

          //enregistrement des configurations des menu
          FunctionUtils.saveMenu(infoModel.allMenus);

          //enregistrement des assurances
          FunctionUtils.saveAssurance(infoModel.assurance);

          //enregistrement des typeAssurance
          FunctionUtils.saveTypeAssurance(infoModel.typeAssurance);

          //enregistrement des devises
          FunctionUtils.saveDevise(infoModel.devises);

          //enregistrement des unites
          FunctionUtils.saveUnites(infoModel.unites);

          //enregistrement des produits
          FunctionUtils.saveProduits(infoModel.produits);

          //enregistrement des biztypes
          FunctionUtils.saveBiztype(infoModel.biztypes);

          //enregistrement des sous_produits
          FunctionUtils.saveSousProduits(infoModel.sousProduits);

          //enregistrements des pays
          FunctionUtils.saveCountry(infoModel.allCountry);

          //enregistrements des region
          FunctionUtils.saveRegion(infoModel.allRegion);

          //enregistrement des modes de livraisons
          FunctionUtils.saveModeLivraison(infoModel.modeLivraisons);

          //enregistrement des moyens de transports
          FunctionUtils.saveMoyenTransport(infoModel.moyenTransports);

          //enregistrement des types de prêts
          FunctionUtils.saveTypePret(infoModel.typePrets);

          //enregistrement des categories
          FunctionUtils.saveCategories(infoModel.categories);
        });


      }
    }).catchError((error){

    });

  }

  _getServerOffre () async {

    _localAnnonceRepository.allAnnonce(0).then((response) {

      _appSharedPreferences.getLastUpdateAnnonce().then((value){
        _appSharedPreferences.getUserInformation().then((user_info) {
          UserInformationModel info_user = user_info;
        bool continueProcess=true;
        int currentLdate=DateTime.now().millisecondsSinceEpoch;
        if(value.compareTo("0")!=0){
          if( (currentLdate-int.parse(value))<DataConstantesUtils.REQUEST_SECONDE){
            continueProcess=false;
          }
        }

        if(response.length==0 || (response.length>0 && continueProcess==true)){
          Api api = ApiRepository();
          api.getAnnonce(user_info.token).then((value) {
            if (value.isRight()) {
              value.all((a) {
                FunctionUtils.saveAnnonce(a);
                _appSharedPreferences.setLastUpdateAnnonce("$currentLdate");
                return true;
              });
            } else {
              return false;
            }
          });
        }
      });

    });
    });

  }


  @override
  Widget build(BuildContext context) => Consumer3<ConfigModel,CartModel,UserInformationModel>(
    builder: (context, configModel,cartModel,userConnected, child) {
      return MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate
        ],
        supportedLocales: [
          const Locale('en'),
          const Locale('fr')
        ],
        debugShowCheckedModeBanner: false,
        title: allTranslations.text('name_app'),
        initialRoute: '/',
        routes: {
          '/': (context) => SplashScreen(),

        },
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: FunctionUtils.colorFromHex(configModel.mainColor), //Changing this will change the color of the TabBar
          accentColor: FunctionUtils.colorFromHex(configModel.mainColor),

          primaryTextTheme:TextTheme(
            title: TextStyle(color: Colors.white),
            subtitle: TextStyle(color: Colors.white),
            subhead: TextStyle(color: Colors.white),
          ) ,

          appBarTheme: AppBarTheme(
            //color: Colors.white,
              iconTheme: IconThemeData(color: Colors.white),
              actionsIconTheme: IconThemeData(color: Colors.white)),


          floatingActionButtonTheme: FloatingActionButtonThemeData(
            backgroundColor:FunctionUtils.colorFromHex(configModel.mainColor),
          ),


        ),

      );
    },
  );


}
