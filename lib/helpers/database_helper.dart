import 'dart:async';
import 'dart:io' as io;

import 'package:agrimobile/helpers/helpers_utils.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

//lastUpdated: DateTime.now().millisecondsSinceEpoch,
class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  static Future<Database> get database async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  static initDb() async {
    //io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String documentsDirectory = await getDatabasesPath();
    String path = join(documentsDirectory, "b2bagrimobilebase.db");
    var theDb = await openDatabase(path,
        version: 1, onCreate: _onCreate, onUpgrade: _onUpgrade);
    return theDb;
  }

  static void _onCreate(Database db, int version) async {
    try{



      String sqlMeteo='CREATE TABLE $TABLE_NAME_METEO($idMeteo INTEGER PRIMARY KEY AUTOINCREMENT,$dateMeteo TEXT,$descriptionMeteo TEXT,$tempDay FLOAT,$tempMin FLOAT,$tempMax FLOAT'
          ',$tempNight FLOAT,$tempMorn FLOAT,$tempEve FLOAT,$humidity FLOAT,$apiIdMeteo INTEGER,$userIdMeteo INTEGER)';

      String sqlAssurance='CREATE TABLE $TABLE_NAME_ASSURANCE($idAssurance INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdAssurance INTEGER,$denomination TEXT,$logo TEXT,$statuts INTEGER)';

      String sqlTAssurance='CREATE TABLE $TABLE_NAME_TASSURANCE($idTypeAssurance INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdTAssurance INTEGER,$typeassuranceIdTAssurance INTEGER,$typeassuranceNomIdTAssurance TEXT'
          ',$assureurIdTAssurance INTEGER,$assureurNomTAssurance TEXT,$prixTAssurance INTEGER,$validiteTAssurance INTEGER,$isvalideTAssurance INTEGER,$situationTAssurance TEXT,$dateTAssurance TEXT)';

      String sqlDevise='CREATE TABLE $TABLE_NAME_DEVISE($idDevise INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdDevise INTEGER,$nameDevise TEXT,$createdByDevise INTEGER,'
          '$modifiedByDevise INTEGER,$jourDevise TEXT,$heureDevise TEXT)';

      String sqlUnite='CREATE TABLE $TABLE_NAME_UNITE($idUnite INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdUnite INTEGER,$nomUnite TEXT)';

      String sqlProduit='CREATE TABLE $TABLE_NAME_PRODUIT($idProduit INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdProduit INTEGER,$nomCategorieFilleProduit TEXT,$categorieIdProduit INTEGER,'
          '$uniteMesureProduit TEXT,$statutProduit INTEGER,$dureeJourProduit INTEGER)';

      String sqlBiztype='CREATE TABLE $TABLE_NAME_BIZTYPE($idBiztype INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdBiztype INTEGER,$nameBiztype TEXT,$createdByBiztype INTEGER,'
          '$modifiedByBiztype INTEGER,$jourBiztype INTEGER,$heureBiztype INTEGER)';

      String sqlSousProduit='CREATE TABLE $TABLE_NAME_SOUSPRODUIT($idSousProduit INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdSousProduit INTEGER,$nomCategorieFilleSousProduit TEXT,$categorieFilleIdSousProduit INTEGER,'
          '$statutSousProduit INTEGER)';

      String sqlSpeculation='CREATE TABLE $TABLE_NAME_SPECULATION($idSpeculation INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdSpeculation INTEGER,$nomSpeculation TEXT,$isAbonneSpeculation INTEGER,'
          '$quantiteSpeculation INTEGER,$idUniteSpeculation INTEGER,$nomUniteSpeculation TEXT,$userIdSpeculation INTEGER)';


      String sqlCountry='CREATE TABLE $TABLE_NAME_COUNTRY($idCountry INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdCountry INTEGER,$alphaCountry TEXT,$nomFrCountry TEXT,'
          '$indicatifCountry TEXT)';

      String sqlRegion='CREATE TABLE $TABLE_NAME_REGION($idRegion INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdRegion INTEGER,$nomRegion TEXT,$paysId INTEGER)';

      String sqlAnnonce='CREATE TABLE $TABLE_NAME_ANNONCE($idAnnonce INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdAnnonce INTEGER,$titleAnnonce TEXT,$pictureAnnonce TEXT,'
          '$userIdAnnonce INTEGER,$referenceAnnonce TEXT,$categorieIdAnnonce INTEGER,$categorieDureeAnnonce INTEGER,$unitesIdAnnonce INTEGER,$deviseIdAnnonce INTEGER,$prixAnnonce INTEGER,'
          '$latitudeAnnonce TEXT,$longitudeAnnonce TEXT,$lieuAnnonce TEXT,$stockAnnonce INTEGER,$contactAnnonce TEXT,$biztypeIdAnnonce INTEGER,$timeAnnonce INTEGER,$infoAnnonce TEXT,'
          '$expeditionAnnonce TEXT,$identifiantAnnonce TEXT,$autorisationAnnonce INTEGER,$paysAlphaAnnonce TEXT,$paysNomAnnonce TEXT,$disponibiliteAnnonce INTEGER,$indivisibiliteAnnonce INTEGER,'
          '$moisDisponibiliteAnnonce TEXT,$certificateAnnonce INTEGER,$bioAnnonce INTEGER,$noteVendeurAnnonce INTEGER,$uniqueCodeAnnonce TEXT,$scoopId INTEGER, $statutPrive INTEGER, $myApiIdAnnonce INTEGER,$expireTime INTEGER)';

      String sqlPrice='CREATE TABLE $TABLE_NAME_PRICE($idPrice INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdPrice INTEGER,$nomProduitPrice TEXT,$villePrice TEXT,$prixPrice TEXT,$datePrice TEXT,$timestampPrice INTEGER)';

      String sqlUassurance='CREATE TABLE $TABLE_NAME_USERASSURANCE($idUserAssurance  INTEGER PRIMARY KEY AUTOINCREMENT,$idInfoUserAssurance INTEGER,$idUser INTEGER,$idAssureurInfo INTEGER,$montantOperation INTEGER,'
      // '$dureeOperation INTEGER,$dateFin TEXT,$dateOperation TEXT ,$satutOperation INTEGER)';
          '$dureeOperation INTEGER,$dateFin TEXT,$dateOperation TEXT ,$satutOperation INTEGER , $urlPaiementAssurance TEXT, $keyAssurance TEXT)';

      String sqlEop='CREATE TABLE $TABLE_NAME_EOP($idEop  INTEGER PRIMARY KEY AUTOINCREMENT,$idUserEop INTEGER,$idServerEop INTEGER,$etatEop INTEGER,$superficieEop TEXT,'
          '$nameSiteEop TEXT,$nameSpeculationEop TEXT,$interetEop INTEGER ,$dureeEop INTEGER ,$montantTotalEop TEXT ,$montantinteretEop TEXT ,$prixTotalEop TEXT ,$prixSuperficieEop TEXT '
          ',$prixProductionEop TEXT,$descriptionEop TEXT,$fichierEop TEXT,$composantEop TEXT)';

      String sqlScoop='CREATE TABLE $TABLE_NAME_SCOOP($idScoop  INTEGER PRIMARY KEY AUTOINCREMENT,$idUserScoop INTEGER,$idServerScoop INTEGER,$etatScoop INTEGER,$nameScoop TEXT,$statutScoop TEXT,'
          '$cantonScoop TEXT,$adresseScoop TEXT,$descriptionScoop TEXT ,$totalmembreScoop INTEGER ,$superficieScoop TEXT ,$membreScoop TEXT,$memberRole TEXT)';

      String sqlPret='CREATE TABLE $TABLE_NAME_PRET($idPret  INTEGER PRIMARY KEY AUTOINCREMENT,$typePret INTEGER,$idServerPret INTEGER,$idPretScoop TEXT,$idEopScoop TEXT,$superficiePret TEXT,$dateDemande TEXT,$idUserPret INTEGER,'
          '$etatDemandePret INTEGER,$contenuDemandePret TEXT,$titleDemandePret TEXT,$descriptionDemande TEXT,$fichierPret TEXT,$idProjetPret TEXT )';

      String sqlCommande='CREATE TABLE $TABLE_NAME_COMMANDE($idCommande  INTEGER PRIMARY KEY AUTOINCREMENT,$idServerCommande INTEGER,$keyCommande TEXT,$numeroCommande TEXT,$dateCommande TEXT,$amountCommande INTEGER,'
          '$etatCommande INTEGER,$typeCommande INTEGER,$urlPaiementCommande TEXT ,$NomPrenomUserCommande TEXT,$modeLivraisonCommande TEXT,$motCleModeLivraisonCommande TEXT,$moyenTransportCommande TEXT,$transporterCommande TEXT,$emailUserCommande TEXT,$telephoneUserCommande TEXT)';

      String sqlCommandeDetail='CREATE TABLE $TABLE_NAME_COMMANDE_DETAIL($idCommandeDetail  INTEGER PRIMARY KEY AUTOINCREMENT,$keyCommandeDetail TEXT,$nomProduitCommandeDetail TEXT,$qteProduitCommandeDetail TEXT,$prixUnitaireCommandeDetail TEXT,'
          '$prixTotalCommandeDetail TEXT)';


      String sqlConseils='CREATE TABLE $TABLE_NAME_CONSEIL($idConseil INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdConseil INTEGER,$typeConseil TEXT,$titre TEXT,$contenu TEXT,'
          '$speculation TEXT,$dateConseil DATE,$lien TEXT,$fichier TEXT, $userIdConseil INTEGER)';

      String sqlMenus='CREATE TABLE $TABLE_NAME_MENU($idMenu INTEGER PRIMARY KEY AUTOINCREMENT,$typeMenu INTEGER,$visibleMenu INTEGER,$nameMenu TEXT,$codeMenu TEXT,$iconeMenu TEXT)';


      String sqlStatistics='CREATE TABLE $TABLE_NAME_ACCOUNT_STATS($idUserStatistic INTEGER PRIMARY KEY AUTOINCREMENT,$totalOffre INTEGER, $activeOffre INTEGER, $chiffreAffaire INTEGER,$offreSpeculation TEXT, $idUtlsateur INTEGER)';

      String sqlTYpeSouscription = 'CREATE TABLE $TABLE_NAME_TYPE_SOUSCRIPTION($idTypeSouscription INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdType INTEGER,$keyTypeSouscription TEXT,$denoTypeSouscription TEXT,$prixSouscription INTEGER,$dureeSouscription INTEGER, $idUsr INTEGER)';

      String sqlUserSouscription = 'CREATE TABLE $TABLE_NAME_USER_SOUSCRIPTION($idUserSouscription INTEGER PRIMARY KEY AUTOINCREMENT,$id INTEGER,$key TEXT,$numeroSouscription TEXT,$montant INTEGER,$datePaiement TEXT,$dateFinsouscription TEXT,$etat INTEGER,$typeSouscription TEXT, $idUtilisateur INTEGER)';

      String sqlNotification='CREATE TABLE $TABLE_NAME_NOTIFICATION($idNotification INTEGER PRIMARY KEY AUTOINCREMENT,$apiId INTEGER,$title TEXT,$description TEXT,$dateCreation TEXT, $idNAnnonce INTEGER, $userId INTEGER)';

      String sqlUserStatistics = 'CREATE TABLE $TABLE_NAME_USER_STATISTIC($idStatistic INTEGER PRIMARY KEY AUTOINCREMENT,$nbreAchat INTEGER,$montantAchat INTEGER,$endLivraison INTEGER,$waitingLivraison INTEGER,$demandeSpeculation TEXT, $idUtlisateur INTEGER)';

      String sqlActorsProfil = 'CREATE TABLE $TABLE_NAME_ACTORS_PROFIL($idActorProfil INTEGER PRIMARY KEY AUTOINCREMENT,$actorCode TEXT,$username TEXT,$nom TEXT,$prenoms TEXT,$email TEXT, $noteVendeur INTEGER, $noteAcheteur INTEGER)';

      String sqlModeLivraison='CREATE TABLE $TABLE_NAME_MODE_LIVRAISON ($idModeLivraison INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdModeLivraison INTEGER,$libelleModeLivraison TEXT,$etatModeLivraison INTEGER,$keyModeLivraison TEXT,$motCleModeLivraison TEXT)';

      String sqlMoyenTransport='CREATE TABLE $TABLE_NAME_MOYEN_TRANSPORT ($idMoyenTransport INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdMoyenTransport INTEGER,$libelleMoyenTransport TEXT,$etatMoyenTransport INTEGER,$keyMoyenTransport TEXT)';

      String sqlTypePret='CREATE TABLE $TABLE_NAME_TYPE_PRET ($idTypePret INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdTypePret INTEGER,$libelleTypePret TEXT,$etatTypePret INTEGER,$keyTypePret TEXT,$createdByTypePret INTEGER,$dateCreatePret TEXT,$updatedByTypePret INTEGER,$dateUpdateTypePret TEXT)';

      String sqlAutrePret='CREATE TABLE $TABLE_NAME_AUTRE_PRET($idAutrePret  INTEGER PRIMARY KEY AUTOINCREMENT,$typeAutrePretBanqueId INTEGER,$keyAutrePret TEXT,$typeAutrePretBanqueName TEXT,$titreAutrePret TEXT,$descripionAutrePret TEXT,'
          '$codeAutrePret TEXT,$dateSouscriptionAutrePret TEXT,$montantAutrePret TEXT ,$montantInteretAutrePret TEXT,$montantAPayerAutrePret TEXT,$etatAutrePret INTEGER,$tauxAppliqueAutrePret TEXT,$dureeAutrePret TEXT,$dateDebutAutrePret TEXT,$dateEcheanceAutrePret TEXT,$motifAutrePret TEXT)';

      String sqlCategorie='CREATE TABLE $TABLE_NAME_CATEGORIE($idAppCategorieProduit INTEGER PRIMARY KEY AUTOINCREMENT,$apiIdCategorieProduit INTEGER,$nameCategorieProduit TEXT,$statutCategorieProduit INTEGER)';


      //creation des tables
      await db.execute(sqlMeteo);
      await db.execute(sqlAssurance);
      await db.execute(sqlTAssurance);
      await db.execute(sqlDevise);
      await db.execute(sqlUnite);
      await db.execute(sqlProduit);
      await db.execute(sqlBiztype);
      await db.execute(sqlSousProduit);
      await db.execute(sqlSpeculation);
      await db.execute(sqlCountry);
      await db.execute(sqlRegion);
      await db.execute(sqlAnnonce);
      await db.execute(sqlPrice);
      await db.execute(sqlUassurance);
      await db.execute(sqlEop);
      await db.execute(sqlScoop);
      await db.execute(sqlPret);
      await db.execute(sqlCommande);
      await db.execute(sqlCommandeDetail);
      await db.execute(sqlConseils);
      await db.execute(sqlMenus);
      await db.execute(sqlStatistics);
      await db.execute(sqlTYpeSouscription);
      await db.execute(sqlNotification);
      await db.execute(sqlUserStatistics);
      await db.execute(sqlModeLivraison);
      await db.execute(sqlMoyenTransport);
      await db.execute(sqlTypePret);
      await db.execute(sqlAutrePret);
      await db.execute(sqlUserSouscription);
      await db.execute(sqlCategorie);
      await db.execute(sqlActorsProfil);

    } catch (error) {
      print('error during database creation $error');
    }
  }

  static void _onUpgrade(Database db, int oldVersion, int newVersion) async {
    try {

    } catch (error) {}
  }

}
