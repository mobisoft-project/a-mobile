
const String TABLE_NAME_METEO = "AppMeteo";
String idMeteo="idAppMeteo";
String dateMeteo="date";
String descriptionMeteo="description";
String tempDay="temp_day";
String tempMin="temp_min";
String tempMax="temp_max";
String tempNight="temp_night";
String tempMorn="temp_morn";
String tempEve="temp_eve";
String humidity="humidity";
String apiIdMeteo="apiId";
String userIdMeteo="userId";





const String TABLE_NAME_ASSURANCE = "AppAssurance";
String idAssurance="idAppAssurance";
String apiIdAssurance="apiId";
String denomination="denomination";
String logo="logo";
String statuts="statuts";


const String TABLE_NAME_USERASSURANCE = "AppUserAssurance";
String idUserAssurance="idAppUserAssurance";
String idInfoUserAssurance="id_info_userassurance";
String idUser="id_user";
String idAssureurInfo="id_assureur_info";
String montantOperation="montant_operation";
String dureeOperation="duree_operation";
String dateFin="date_fin";
String dateOperation="date_operation";
String satutOperation="satut_operation";
String urlPaiementAssurance="url_paiement";
String keyAssurance="key_assurance";


const String TABLE_NAME_TASSURANCE = "AppTAssurance";
String idTypeAssurance="idAppTAssurance";
String apiIdTAssurance="apiId";
String typeassuranceIdTAssurance="typeassuranceId";
String typeassuranceNomIdTAssurance="typeassuranceNom";
String assureurIdTAssurance="assureurId";
String assureurNomTAssurance="assureurNom";
String prixTAssurance="prix";
String validiteTAssurance="validite";
String isvalideTAssurance="isvalide";
String situationTAssurance="situation";
String dateTAssurance="date";


const String TABLE_NAME_COMMANDE = "AppCommande";
String idCommande="idAppCommande";
String idServerCommande="id_commande";
String keyCommande="keyCommande";
String numeroCommande="numeroCommande";
String dateCommande="dateCommande";
String amountCommande="amountCommande";
String modeLivraisonCommande="modeLivraisonCommande";
String motCleModeLivraisonCommande="motCleModeLivraisonCommande";
String moyenTransportCommande="moyenTransportCommande";
String transporterCommande="transporterCommande";
String etatCommande="etatCommande";
String typeCommande="typeCommande";  //1 commandes envoyées, 2 commades recues
String urlPaiementCommande="urlPaiementCommande";
String NomPrenomUserCommande="NomPrenomUserCommande";
String emailUserCommande="emailUserCommande";
String telephoneUserCommande="telephoneUserCommande";

const String TABLE_NAME_COMMANDE_DETAIL = "AppCommandeDetail";
String idCommandeDetail="idAppCommandeDetail";
String keyCommandeDetail="keyCommande";
String nomProduitCommandeDetail="nomProduitCommandeDetail";
String qteProduitCommandeDetail="qteProduitCommandeDetail";
String prixUnitaireCommandeDetail="prixUnitaireCommandeDetail";
String prixTotalCommandeDetail="prixTotalCommandeDetail";


const String TABLE_NAME_DEVISE = "AppDevise";
String idDevise="idAppDevise";
String apiIdDevise="id";
String nameDevise="name";
String createdByDevise="createdBy";
String modifiedByDevise="modifiedBy";
String jourDevise="jour";
String heureDevise="heure";



const String TABLE_NAME_UNITE = "AppUnite";
String idUnite="idAppUnite";
String apiIdUnite="id";
String nomUnite="nom";


const String TABLE_NAME_PRODUIT = "AppProduit";
String idProduit="idAppProduit";
String apiIdProduit="id";
String nomCategorieFilleProduit="nomCategorie_fille";
String categorieIdProduit="categorieId";
String uniteMesureProduit="unite_mesure";
String statutProduit="statut";
String dureeJourProduit="duree_jour";



const String TABLE_NAME_BIZTYPE = "AppBiztype";
String idBiztype="idAppBiztype";
String apiIdBiztype="id";
String nameBiztype="name";
String createdByBiztype="createdBy";
String modifiedByBiztype="modifiedBy";
String jourBiztype="jour";
String heureBiztype="heure";



const String TABLE_NAME_SOUSPRODUIT = "AppSousProduit";
String idSousProduit="idAppSousProduit";
String apiIdSousProduit="id";
String nomCategorieFilleSousProduit="nomCategorie_fille";
String categorieFilleIdSousProduit="categoriefilleId";
String statutSousProduit="statut";


const String TABLE_NAME_SPECULATION = "AppSpeculation";
String idSpeculation="idAppSpeculation";
String apiIdSpeculation="id";
String nomSpeculation="nom";
String isAbonneSpeculation="is_abonne";
String quantiteSpeculation="quantite";
String idUniteSpeculation="idUnite";
String nomUniteSpeculation="nomUnite";
String userIdSpeculation="userId";

const String TABLE_NAME_COUNTRY = "AppCountry";
String idCountry="idAppCountry";
String apiIdCountry="id";
String alphaCountry="alpha2";
String nomFrCountry="nom_fr_fr";
String indicatifCountry="indicatif";


const String TABLE_NAME_REGION = "AppRegion";
String idRegion="idAppRegion";
String apiIdRegion="id";
String nomRegion="nomregion";
String paysId="paysId";

const String TABLE_NAME_CONSEIL = "AppConseil";
String idConseil ="idAppConseil";
String apiIdConseil="id_conseil";
String typeConseil = "type_conseil";
String titre = "titre";
String contenu = "contenu";
String speculation = "speculation";
String dateConseil="date_conseil";
String lien = "lien";
String fichier = "fichier";
String userIdConseil ="userToken";




const String TABLE_NAME_ANNONCE = "AppAnnonce";
String idAnnonce="idAppAnnonce";
String titleAnnonce="title";
String pictureAnnonce="picture";
String userIdAnnonce="userId";
String referenceAnnonce="reference";
String categorieIdAnnonce="categorieId";
String categorieDureeAnnonce="categorieDuree";
String unitesIdAnnonce="unitesId";
String deviseIdAnnonce="deviseId";
String prixAnnonce="prix";
String latitudeAnnonce="latitude";
String longitudeAnnonce="longitude";
String lieuAnnonce="lieu";
String stockAnnonce="stock";
String contactAnnonce="contact";
String biztypeIdAnnonce="biztypeId";
String timeAnnonce="time";
String infoAnnonce="info";
String expeditionAnnonce="exp";
String identifiantAnnonce="identifiant";
String autorisationAnnonce="autorisation";
String paysAlphaAnnonce="paysAlpha";
String paysNomAnnonce="paysNom";
String indivisibiliteAnnonce="indivisibilite";
String disponibiliteAnnonce="disponibilite";
String moisDisponibiliteAnnonce="moisDisponibilite";
String certificateAnnonce="certificate";
String bioAnnonce="bio";
String noteVendeurAnnonce="noteVendeur";
String uniqueCodeAnnonce="uniqueCode";
String scoopId="scoopId";
String statutPrive="statutPrive";
String myApiIdAnnonce="myApiId";
String apiIdAnnonce="apiId";
String expireTime="expireTime";

const String TABLE_NAME_PRICE = "AppPrice";
String idPrice="idAppPrice";
String apiIdPrice="apiId";
String nomProduitPrice="nom_produit";
String villePrice="ville";
String prixPrice="prix";
String datePrice="datePrice";
String timestampPrice="timestamp";

const String TABLE_NAME_SCOOP = "AppScoop";
String idScoop="idAppScoop";
String idUserScoop="id_user";
String etatScoop="etat_scoop";
String idServerScoop="id_association";
String nameScoop="name_association";
String cantonScoop="canton_association";
String adresseScoop="adresse_association";
String descriptionScoop="description_association";
String totalmembreScoop="total_membre";
String superficieScoop="total_superficie";
String membreScoop="member_association";
String statutScoop="statut_publication";
String memberRole="role_membre";


const String TABLE_NAME_EOP = "AppEop";
String idEop="idAppEop";
String idUserEop="id_user";
String etatEop="etat_eop";
String idServerEop="key_eop";
String superficieEop="superficie";
String nameSiteEop="name_site";
String nameSpeculationEop="name_speculation";
String interetEop="interet_eop";
String dureeEop="duree_eop";
String montantTotalEop="montant_total";
String montantinteretEop="montant_interet";
String prixTotalEop="projet_interet";
String prixSuperficieEop="montant_surperficie";
String prixProductionEop="montant_production";
String descriptionEop="description_eop";
String fichierEop="fichier_eop";
String composantEop="composant_eop";


const String TABLE_NAME_PRET = "AppPret";
String idPret="idAppPret";
String typePret="typePret";
String idServerPret="idServerPret";
String idPretScoop="idScoop";
String idEopScoop="idEop";
String superficiePret="superficie";
String dateDemande="dateDemande";
String idUserPret="idUser";
String etatDemandePret="etatDemande";
String contenuDemandePret="contenuDemande";
String titleDemandePret="titleDemande";
String descriptionDemande="descriptionDemande";
String fichierPret="all_fichier";
String idProjetPret="key_projet";

const String TABLE_NAME_MENU = "AppMenu";
String idMenu="idAppMenu";
String typeMenu="type_menu";
String nameMenu="name_menu";
String codeMenu="code_menu";
String iconeMenu="icone_menu";
String visibleMenu="visible_menu";

const String TABLE_NAME_ACTORS_PROFIL = "AppActorsProfil";
String idActorProfil = "idAppActorsProfil";
String actorCode = "actor_code";
String username = "username";
String nom = "nom";
String prenoms = "prenoms";
String email = "email";
String noteVendeur = "note_vendeur";
String noteAcheteur = "note_acheteur";

const String TABLE_NAME_ACCOUNT_STATS =  "AppAccountStatistics";
String idUserStatistic = "idAppAccountStatistics";
String totalOffre = "total_offre";
String activeOffre = "active_offre";
String chiffreAffaire = "chiffre_affaire";
String offreSpeculation = "offre_speculation";
String idUtlsateur = "id_user";

const String TABLE_NAME_TYPE_SOUSCRIPTION = "AppTypeSouscription";
String idTypeSouscription = "idAppTypeSouscription";
String apiIdType = "id_type_souscription";
String keyTypeSouscription = "key_type_souscription";
String denoTypeSouscription = "deno_type_souscription";
String prixSouscription = "prix_souscription";
String dureeSouscription = "duree_souscription";
String idUsr = "user_id";

const String TABLE_NAME_USER_SOUSCRIPTION = "AppUserSouscription";
String idUserSouscription = "idAppUserSouscription";
String id = "id";
String key = "key";
String numeroSouscription = "numero_souscription";
String montant = "montant";
String datePaiement = "date_paiement";
String dateFinsouscription = "date_finsouscription";
String etat = "etat";
String typeSouscription = "type_souscription";
String idUtilisateur = "id_user";

const String TABLE_NAME_NOTIFICATION = "AppNotification";
String idNotification ="idAppNotification";
String apiId="id";
String title = "title";
String description = "description";
String dateCreation = "date_creation";
String idNAnnonce = "id_annonce";
String userId ="user_id";


const String TABLE_NAME_USER_STATISTIC = "AppUserStatistics";
String idStatistic = "idAppUserStatistics";
String nbreAchat = "nbre_achat";
String montantAchat = "montant_achat";
String endLivraison = "end_livraison";
String waitingLivraison = "waiting_livraison";
String demandeSpeculation = "demande_speculation";
String idUtlisateur = "id_user";

const String TABLE_NAME_MODE_LIVRAISON = "AppModeLivraison";
String idModeLivraison="idAppModeLivraison";
String apiIdModeLivraison="id";
String libelleModeLivraison="libelle";
String etatModeLivraison="etat";
String keyModeLivraison="mode_livraison_key";
String motCleModeLivraison="mot_cle";

const String TABLE_NAME_MOYEN_TRANSPORT = "AppMoyenTransport";
String idMoyenTransport="idAppMoyenTransport";
String apiIdMoyenTransport="id";
String libelleMoyenTransport="libelle";
String etatMoyenTransport="etat";
String keyMoyenTransport="moyen_transport_key";


const String TABLE_NAME_TYPE_PRET = "AppTypePret";
String idTypePret="idAppTypePret";
String apiIdTypePret="id";
String libelleTypePret="libelle";
String etatTypePret="etat";
String keyTypePret="type_pret_key";
String createdByTypePret="created_by";
String dateCreatePret="date_create";
String updatedByTypePret="updated_by";
String dateUpdateTypePret="date_update";

const String TABLE_NAME_AUTRE_PRET = "AppAutrePret";
String idAutrePret="idAppAutrePret";
String keyAutrePret="key_autre_pret";
String typeAutrePretBanqueId="type_pret_banqueId";
String typeAutrePretBanqueName="type_pret_banqueName";
String titreAutrePret="titre";
String descripionAutrePret="description";
String codeAutrePret="codePret";
String dateSouscriptionAutrePret="date_pret";
String montantAutrePret="montant_pret";
String montantInteretAutrePret="montant_interet";
String montantAPayerAutrePret="montant_a_payer";
String etatAutrePret="etat";
String tauxAppliqueAutrePret="taux_applique";
String dureeAutrePret="duree";
String dateDebutAutrePret="date_debut";
String dateEcheanceAutrePret="date_echeance";
String motifAutrePret="motif_rejet";

const String TABLE_NAME_CATEGORIE = "AppCategorieProduit";
String idAppCategorieProduit="idAppCategorieProduit";
String apiIdCategorieProduit="id";
String nameCategorieProduit="name";
String statutCategorieProduit="statut";
