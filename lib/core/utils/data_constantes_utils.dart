
import 'package:agrimobile/core/utils/core_constantes.dart';

class DataConstantesUtils{


  static String DEVISE = "F CFA";
  static String CALLCENTER = "4242";
  static String simulationFolder = "Agrimobile/simulation";
  static String API_TOKEN = "ddfd544deRHOFle1TrJGW4MHrTo8Cf8et";

  static String PROJET_ID = "https://play.google.com/store/apps/details?id=com.clinsarlu.wiagri";
  static String TOKEN_SERVER = "CvdvXlAmzRbYmOPjfKnjCnBVWpxlJUwM";
  static String IMAGE_ANNONCE_URL = "$SERVER_URL/themes/site/agro_img/annonces/";
  static String IMAGE_MENU_URL = "$SERVER_URL/admin/images/icon/";
  static String SHARE_ANNONCE_URL = "$SERVER_URL/annonce/detail?reference=";

  static String LOGIN_SERVER_URL = "/api/web/flutterv1/users/login_user";
  static String RLOCATION_SERVER_URL = "/api/web/flutterv1/users/getchampclient";
  static String UPASSWORD_SERVER_URL = "/api/web/flutterv1/users/update_mdp";
  static String RPROFIL_SERVER_URL = "/api/web/flutterv1/users/update_user_infos";
  static String FREGISTER_SERVER_URL = "/api/web/flutterv1/users/suscribeclientconfirmation";
  static String REGISTER_SERVER_URL = "/api/web/flutterv1/users/suscribeclient";
  static String FORGET_SERVER_URL = "/api/web/flutterv1/users/resetmdp";
  static String RESET_SERVER_URL = "/api/web/flutterv1/users/resetconfirm";
  static String COUNTRY_SERVER_URL = "/api/web/flutterv1/pays/all";
  static String REGION_SERVER_URL = "/api/web/flutterv1/regions/allfirst";
  static String CONTACT_SERVER_URL = "/api/web/flutterv1/annonces/get_contact";
  static String PRICE_SERVER_URL = "/api/web/flutterv1/post-agents/all_price";
  static String ASPECULATION_SERVER_URL = "/api/web/flutterv1/alertes/abonnement_speculation";
  static String DSPECULATION_SERVER_URL = "/api/web/flutterv1/alertes/desabonnement_speculation";
  static String AMETEO_SERVER_URL = "/api/web/flutterv1/meteos/get_meteo";
  static String ASSURANCE_SERVER_URL = "/api/web/flutterv1/assurances/suscribe_info";
  static String eopServerUrl = "/api/web/flutterv1/prets/geteop";
  static String scoopServerUrl = "/api/web/flutterv1/prets/getscoop";
  static String usimulationServerUrl = "/api/web/flutterv1/prets/simulation_userpret";
  static String ssimulationServerUrl = "/api/web/flutterv1/prets/simulation_scooppret";
  static String upretServerUrl = "/api/web/flutterv1/prets/demande_userpret";
  static String spretServerUrl = "/api/web/flutterv1/prets/demande_scooppret";
  static String imagepretServerUrl = "/api/web/flutterv1/prets/image_pret";
  static String listepretServerUrl = "/api/web/flutterv1/prets/all_pret";

  static String COMMANDE_SERVER_URL = "/api/web/flutterv1/commandes/sendcommande";
  static String LISTE_COMMANDE_SERVER_URL = "/api/web/flutterv1/commandes/getcommande";
  static String DELETE_COMMANDE_SERVER_URL = "/api/web/flutterv1/commandes/deletecommande";

  static String CONSEIL_SERVER_URL = "$SERVER_URL/api/web/flutterv1/conseils/getconseil";
  static String DELETE_CONSEIL_URL = "$SERVER_URL/api/web/flutterv1/conseils/deleteconseil";



  static String ANNONCE_SERVER_URL = "/api/web/flutterv1/annonces/all";
  static String PANNONCE_SERVER_URL = "/api/web/flutterv1/annonces/send_annonce";

  static String FULLCONDITION_SERVER_URL = "$SERVER_URL/api/web/flutterv1/users/get_cgu";
  static String FULLABOUT_SERVER_URL = "$SERVER_URL/api/web/flutterv1/users/get_apropos";
  static String FULLPARTENAIRE_SERVER_URL = "$SERVER_URL/api/web/flutterv1/partenaires/all";
  static String FULLCONFIG_SERVER_URL = "$SERVER_URL/api/web/flutterv1/configs/all";

  static String UserDocument_send = "/api/web/flutterv1/accounts/send_document";
  static String scoopStatutServerUrl = "/api/web/flutterv1/prets/change_statut";

  static int APP_SERVER_PORT =  0;
  static int REQUEST_SECONDE =  5*60*60*1000;

  static String bTokenServer = "ddfd544deRHOFle1TrJGW4MHrTo8Cf8et";

  static String UPDATE_ASSURANCE_SERVER_URL = "$SERVER_URL/api/web/flutterv1/assurances/get_user_assurance";
  static String GET_MY_STATISTICS_SERVER_URL = "$SERVER_URL/api/web/flutterv1/accounts/get_statistique";
  static String GET_NOTIFICATION_SERVER_URL = "$SERVER_URL/api/web/flutterv1/accounts/get_notification";
  static String DELETE_NOTIFICATION_SERVER_URL = "$SERVER_URL/api/web/flutterv1/accounts/delete_notification";
  static String GET_SOUSCRIPTION_SERVER_URL = "$SERVER_URL/api/web/flutterv1/accounts/get_souscription";
  static String SOUSCRIPTION_PAIEMENT_SERVER_URL = "$SERVER_URL/api/web/flutterv1/accounts/send_souscription";
  static String GET_USER_STATISTICS = "$SERVER_URL/api/web/flutterv1/accounts/get_ustatistique";
  static String GET_USER_SOUSCRIPTION = "$SERVER_URL/api/web/flutterv1/accounts/get_usersouscription";
  static String GET_ACTOR_PROFIL = "$SERVER_URL/api/web/flutterv1/users/get_note_acheteur";


  static String DEFAULT_LOGO = "assets/img/longlogo.png";

  static String LISTE_TRANSPORTER_SERVER_URL = "$SERVER_URL/api/web/flutterv1/commandes/get_liste_transporteur";
  static String LISTE_TYPE_PRET_BANQUE_SERVER_URL = "$SERVER_URL/api/web/flutterv1/autre-prets/get_info_pret";
  static String SEND_AUTRE_PRET_SERVER_URL = "$SERVER_URL/api/web/flutterv1/autre-prets/send_pret";

  /***************ANNONCE PAGE CODE*****************/
  static String ANNONCE_MARCHE = "0";
  static String ANNONCE_MES_CONTACTS = "-10";
  static String ANNONCE_SCOOP_PAGE = "-20";
  static String ANNONCE_ACHAT = "-30";
  static String ANNONCE_VENDEUR = "-40";


}
