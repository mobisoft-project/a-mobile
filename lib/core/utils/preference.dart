import 'dart:convert';

import 'package:agrimobile/core/utils/connection_status.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/user_information_model.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/launch/presentation/pages/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';

class AppSharedPreferences {

  final String lastUpdateRegion = "updateRegion";
  final String lastUpdateCountry = "updateCountry";
  final String lastUpdateAnnonce = "updateAnnonce";


  final String userLogin = "isLoggin";
  final String registrationId= "userRegistration";
  final String validationUser= "validationUser";
  final String userId = "userId";
  final String userPhone = "userPhone";
  final String userLastName = "userLastName";
  final String userFirstname = "userFirstname";
  final String dateCreated = "dateCreated";
  final String userEmail = "userEmail";
  final String userToken = "userToken";
  final String userSolde = "userSolde";
  final String latitude = "latitude";
  final String longitude = "longitude";
  final String paysAlpha = "paysAlpha";
  final String paysNom = "paysNom";
  final String document = "documentUser";
  final String noteVendeur = "noteVendeur";
  final String isfournisseur = "isfournisseur";
  final String totalActeurs = "total_acteurs";
  final String totalOffre = "total_offres";
  final String totalExperts = "total_experts";
  final String routeConnected = "routeConnected";
  final String routeAccess = "routeAccess";
  final String routeAnnonce = "routeAnnonce";

  final String slogan1 = "slogan1";
  final String slogan2 = "slogan2";
  final String mainColor = "main_color";
  final String offreColor = "offre_color";
  final String demandeColor = "demande_color";
  final String bioColor = "bio_color";
  final String cancelColor = "cancel_color";
  final String devise = "devise";
  final String callCenter = "centre_appel";
  final String defaultFile = "user_document";
  final String logo = "logo";




  Future<String> createLoginSession(BuildContext context,UserInformationModel userInfo) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    print("ConnectedBernard ${userInfo.validationUser}");
    prefs.setBool(userLogin, true);
    prefs.setInt(userId, userInfo.id);
    prefs.setString(userLastName, userInfo.nom);
    prefs.setString(userFirstname, userInfo.prenom);
    prefs.setString(userPhone, userInfo.username);
    prefs.setString(userEmail, userInfo.email);
    prefs.setString(userToken, userInfo.token);
    prefs.setString(dateCreated, userInfo.dateCreated);
    prefs.setInt(userSolde, userInfo.solde);
    prefs.setString(latitude, userInfo.latitude);
    prefs.setString(longitude, userInfo.longitude);
    prefs.setString(paysAlpha, userInfo.paysAlpha);
    prefs.setString(paysNom, userInfo.paysNom);
    prefs.setInt(noteVendeur, userInfo.noteVendeur);
    prefs.setInt(isfournisseur, userInfo.isfournisseur);
    prefs.setString(totalActeurs, userInfo.totalActeurs);
    prefs.setString(totalOffre, userInfo.totalOffre);
    prefs.setString(totalExperts, userInfo.totalExperts);
    prefs.setString(document, userInfo.document);
    prefs.setString(validationUser, userInfo.validationUser);

    Provider.of<UserInformationModel>(context, listen: false).signalUpdated(userInfo);
    return prefs.getString(routeConnected) ?? "";
  }

  Future<bool> createSession(BuildContext context,UserInformationModel userInfo) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString(totalActeurs, userInfo.totalActeurs);
    prefs.setString(totalOffre, userInfo.totalOffre);
    prefs.setString(totalExperts, userInfo.totalExperts);


    userInfo.id=prefs.getInt(userId) ?? -1;
    userInfo.token=prefs.getString(userToken) ?? "";
    userInfo.validationUser=prefs.getString(validationUser) ?? "0";
    Provider.of<UserInformationModel>(context, listen: false).signalUpdated(userInfo);

    return true;
  }

  Future<UserInformationModel> getUserInformation() async {

    final SharedPreferences prefs = await SharedPreferences.getInstance();

    UserInformationModel information=new UserInformationModel();
    information.id=prefs.getInt(userId) ?? -1;
    information.nom=prefs.getString(userLastName) ?? "";
    information.prenom=prefs.getString(userFirstname) ?? "";
    information.email=prefs.getString(userEmail) ?? "";
    information.username=prefs.getString(userPhone) ?? "";
    information.token=prefs.getString(userToken) ?? "";
    information.dateCreated=prefs.getString(dateCreated) ?? "";
    information.solde=prefs.getInt(userSolde) ?? 0;
    information.latitude=prefs.getString(latitude) ?? "";
    information.longitude=prefs.getString(longitude) ?? "";
    information.paysAlpha=prefs.getString(paysAlpha) ?? "";
    information.paysNom=prefs.getString(paysNom) ?? "";
    information.document=prefs.getString(document) ?? "";
    information.noteVendeur=prefs.getInt(noteVendeur) ?? 0;
    information.isfournisseur=prefs.getInt(isfournisseur) ?? 0;
    information.totalActeurs=prefs.getString(totalActeurs) ?? "0";
    information.totalOffre=prefs.getString(totalOffre) ?? "0";
    information.totalExperts=prefs.getString(totalExperts) ?? "0";
    information.validationUser=prefs.getString(validationUser) ?? "0";

    return information ;
  }

  Future<String> getRegistrationId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(registrationId) ?? "";
  }
  Future<String> getRouteAccess() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(routeAccess) ?? "1";
  }
  Future<int> getRouteAnnonce() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getInt(routeAnnonce) ?? 0;
  }

  Future<bool> setRegistrationId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(registrationId, value);
  }

  Future<bool> setValidation(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(validationUser, value);
  }

  Future<bool> setRoute(String lastRoute,String validation,int idAnnonce) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(routeConnected, lastRoute);
    prefs.setString(routeAccess, validation);
    prefs.setInt(routeAnnonce, idAnnonce);
    return true;
  }


  Future<String> getLastUpdateAnnonce() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(lastUpdateAnnonce) ?? "0";
  }

  Future<bool> setLastUpdateAnnonce(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(lastUpdateAnnonce, value);
  }

  Future<String> getLastUpdateRegion() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(lastUpdateRegion) ?? "0";
  }

  Future<bool> setLastUpdateRegion(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(lastUpdateRegion, value);
  }

  Future<String> getLastUpdateCountry() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(lastUpdateCountry) ?? "0";
  }

  Future<bool> setLastUpdateCountry(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(lastUpdateCountry, value);
  }

  logoutUser(BuildContext context,) async {

    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(userId, -1);
    prefs.setString(userLastName, "");
    prefs.setString(userFirstname, "");
    prefs.setString(userEmail, "");
    prefs.setString(userPhone, "");
    prefs.setString(userToken, "");
    prefs.setString(dateCreated, "");
    prefs.setString(latitude, "");
    prefs.setString(longitude, "");
    prefs.setString(paysAlpha, "");
    prefs.setString(paysNom, "");
    prefs.setString(document, "");
    prefs.setInt(noteVendeur, 0);
    prefs.setInt(isfournisseur, 0);
    prefs.setInt(userSolde, 0);
    prefs.setString(validationUser, "0");


    String acteurs=prefs.getString(totalActeurs) ?? "0";
    String offre=prefs.getString(totalOffre) ?? "0";
    String expert=prefs.getString(totalExperts) ?? "0";

    UserInformationModel userInfo=UserInformationModel.create(id: -1,validationUser:"0",token:"",totalActeurs:acteurs,totalOffre:offre,totalExperts:expert);
    Provider.of<UserInformationModel>(context, listen: false).signalUpdated(userInfo);

  }

  Future<ConnectionStatus> getConnectionState() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    bool isLogin=prefs.getBool(userLogin)?? false;

    if(isLogin==false){
      return ConnectionStatus.disconnected;
    }else{
      return ConnectionStatus.connected;
    }

  }
  Future<ConfigModel> getApkConfig() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    ConfigModel newConfig=new ConfigModel();
    newConfig.bioColor=prefs.getString(bioColor) ?? "4AB253";
    newConfig.slogan1=prefs.getString(slogan1) ?? allTranslations.text('slogant1');
    newConfig.slogan2=prefs.getString(slogan2) ?? allTranslations.text('slogant2');
    newConfig.cancelColor=prefs.getString(cancelColor) ?? "E9456A";
    newConfig.mainColor=prefs.getString(mainColor) ?? "146C54";
    newConfig.offreColor=prefs.getString(offreColor) ?? "2196F6";
    newConfig.demandeColor=prefs.getString(demandeColor) ?? "FB4034";
    newConfig.devise=prefs.getString(devise) ?? DataConstantesUtils.DEVISE;
    newConfig.callCenter=prefs.getString(callCenter) ?? DataConstantesUtils.CALLCENTER;
    newConfig.logo=prefs.getString(logo) ?? "assets/img/longlogo.png";
    newConfig.defaultFile=prefs.getString(defaultFile) ?? "";
    return newConfig ;
  }

  Future<String> getDefaultFile() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(defaultFile) ?? "";
  }


  Future<bool> setApkConfig(ConfigModel configModel) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(bioColor, configModel.bioColor);
    prefs.setString(slogan1, configModel.slogan1);
    prefs.setString(slogan2, configModel.slogan2);
    prefs.setString(mainColor, configModel.mainColor);
    prefs.setString(cancelColor, configModel.cancelColor);
    prefs.setString(offreColor, configModel.offreColor);
    prefs.setString(demandeColor, configModel.demandeColor);
    prefs.setString(devise, configModel.devise);
    prefs.setString(callCenter, configModel.callCenter);
    prefs.setString(logo, configModel.logo);
    prefs.setString(defaultFile, configModel.defaultFile);
    return true;
  }


  /*Future<bool> setCart(CartModel cart) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String cartobject = json.encode(cart);
    prefs.setString('cart_items', cartobject);
    return true;
  }*/

  Future<bool> setCart(Map<String, dynamic> cart) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    print(json.encode(cart));
    prefs.setString('cart',json.encode(cart));
    return true;
  }

  Future<CartModel> getCart() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    print(json.decode('${prefs.getString('cart')}'));
    CartModel cart =  CartModel.fromMap(json.decode('${prefs.getString('cart')}'));
    return cart;
  }

}
