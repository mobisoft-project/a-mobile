import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/core/utils/colors.dart';

abstract class FeedBackMeassage{

  BuildContext _buildContext;
  String _message;


  FeedBackMeassage(this._buildContext, this._message);

  void showErrorDialog() {

  }

  void showSuccessDialog() {

  }

  void actionToDo();

}