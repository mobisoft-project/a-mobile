import 'dart:async';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/actor_profil_dto.dart';
import 'package:agrimobile/features/common/data/models/actor_profil_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_actor_profil_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_actor_profil_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';


class ProfilagePage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  _ProfilagePageState createState() => _ProfilagePageState();
}

class _ProfilagePageState extends State<ProfilagePage> with SingleTickerProviderStateMixin{
  LocalActorProfilRepository localActorProfilRepository = LocalActorProfilRepository();
  List<LocalActorProfilEntity> localActorProfilEntity;
  ActorProfilModel actorProfilModel;
  AppSharedPreferences appSharedPreferences = AppSharedPreferences();
  Api api = ApiRepository();
  bool isLoading = false;
  bool displayBtn = false;
  String inputValue = "";
  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child){
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title: Align(
            child: Container(
              child: Text("Profilage"),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  // UrlLauncher.launch('tel:+${configModel.callCenter}');
                },
                child: Icon(Icons.phone,color: Colors.white,),
              )
            ),
          ],
          backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
          //bottom: searchBarView(configModel),
        ),
        backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
        body: Builder(builder: (BuildContext myContext){
            return SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: 60.0),
                  headerSearchPage(),
                  SizedBox(height: 50.0),
                  searchBarView(configModel),
                  SizedBox(height: 10.0),
                  searchButton(configModel),
                  actorProfilModel != null ? 
                  searchResultView(actorProfilModel) : 
                  Text("")
                ],
              ),
            );
        })
      );
    }
  );

  searchResultView(ActorProfilModel actorProfilModel){
    if(actorProfilModel.status.compareTo("ok") == 0){
      return Container(
        child: Center(
          child: Table(
            border: TableBorder.all(), // Allows to add a border decoration around your table
            children: [ 
              TableRow(children :[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'Nom', 
                      textAlign: TextAlign.start,
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      )
                    ),
                  )
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      actorProfilModel.nom,
                      textAlign: TextAlign.center
                    ),
                  )
                ),
              ]),
              TableRow(children :[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'Prenom(s)',
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                      textAlign: TextAlign.center
                    ),
                  )
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      actorProfilModel.prenoms,
                      textAlign: TextAlign.center
                    ),
                  )
                ),
              ]),
              TableRow(children :[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'Telephone',
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                      textAlign: TextAlign.center
                    ),
                  )
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      actorProfilModel.username,
                      textAlign: TextAlign.center
                    ),
                  )
                ),
              ]),
              TableRow(
                children :[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'Email',
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                      textAlign: TextAlign.center
                    ),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      actorProfilModel.email,
                      textAlign: TextAlign.center
                    ),
                  ),
                ),
              ]),
              TableRow(children :[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'Moyenne des notes reçu comme vendeur', 
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ), 
                      textAlign: TextAlign.center
                    ),
                  )
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: RatingBar(
                      initialRating: actorProfilModel.noteVendeur * 1.0,
                      direction: Axis.horizontal,
                      itemSize: 25.0,
                      allowHalfRating: true,
                      itemCount: 5,
                      ratingWidget: RatingWidget(
                        full: Icon(
                          Icons.star, 
                          color: Color(0xFF146C54),
                        ),
                        half: Icon(
                          Icons.star_half,
                          color: Color(0xFF146C54),
                        ),
                        empty: Icon(
                          Icons.star_outline,
                          color: Color(0xFF146C54),
                        )
                      )
                    ),
                  )
                )
              ]),
              TableRow(children :[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'Moyenne des notes reçu comme acheteur', 
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                      textAlign: TextAlign.center
                    ),
                  )
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: RatingBar(
                      initialRating: actorProfilModel.noteAcheteur * 1.0,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemSize: 25.0,
                      itemCount: 5,
                      ratingWidget: RatingWidget(
                        full: Icon(Icons.star, color: Colors.orange),
                        half: Icon(
                          Icons.star_half,
                          color: Colors.orange,
                        ),
                        empty: Icon(
                          Icons.star_outline,
                          color: Colors.orange,
                        )
                      )
                    ),
                  )
                ),
              ]),
            ]
          ),
        ),
        height: 120.0,
        margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 60.0),
      );
    }else if(actorProfilModel.status.compareTo("ko") == 0){
      return  Center(
      child: Column(
        children: [
          SizedBox(
            height: 20.0
          ),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          SizedBox(height: 0,),
          Container(
            width:300,
            child: Text(
              "Désolé, cet acteur n'existe pas!",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.bold,
                fontSize: 15.0,
              ),
            ),
          )
        ],
      ),
    );
    }else{
      return  Center(
        child: Column(
          children: [
            Text("Erreur !")
          ],
        ),
      );
    }
  }

  searchProccessing(String codeActeur, ConfigModel configModel) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.all(12),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Vérification en cours, veuillez patienté...",
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(
                  FunctionUtils.colorFromHex(configModel.mainColor)
                ),
              ),
            ],
          )
        );
      }
    );

    appSharedPreferences.getUserInformation().then((utilisateur){
      ActorProfilDto actorProfilDto = ActorProfilDto.create(
        accessToken: utilisateur.token,//DataConstantesUtils.API_TOKEN,
        code: codeActeur
      );
      api.getActorProfil(actorProfilDto).then((value) {
        if(value.isRight()){
          value.all((actorProfil) {
            if(actorProfil != null && actorProfil.status.compareTo("ok") == 0){
              setState(() {
                actorProfilModel = actorProfil;
              });
              Navigator.of(context).pop(null);
              return true;
            }else if(actorProfil != null && actorProfil.status.compareTo("ko") == 0){
              setState(() {
                  actorProfilModel = actorProfil;
              });
              Navigator.of(context).pop(null);
              return true;
            }else{
              Navigator.of(context).pop(null);
              return false;
            }
          });
        }
      });
    });
  }

  loaderView(){
    return  Center(
        child: Column(
          children: [
            Center(
                child: Column(
                  children: [
                    SizedBox(
                      height: 100,
                    ),
                    Material(
                      shape: CircleBorder(),
                      clipBehavior: Clip.hardEdge,
                      color: Colors.transparent,
                      child: Image.asset('assets/img/loading.gif', height: 100),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                  ],
                ),
            )
          ],
        ),
      );
  }

  headerSearchPage(){
    return  Center(
      child: Column(
        children: [
          SizedBox(
            height: 10.0
          ),
          Container(
            width:300,
            child: Text(
              "Vérification de profil",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
          ),
        ],
      ),
    );
  }
          
  searchBarView(ConfigModel configModel) {
    return  PreferredSize(
      preferredSize: Size(double.infinity, 60.0),
      child: Card(
        margin: EdgeInsets.only(left: 30.0, right: 30.0, bottom: 10.0),
        elevation: 2.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(2.0)),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                child: Icon(Icons.search,
                color: FunctionUtils.colorFromHex("DDDDDD")),
              ),
              Expanded(
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText:   "  N° AgriStore de l'acteur",
                  ),
                  onChanged: (String numAgristore) async {
                    print(numAgristore);
                    inputValue = numAgristore;
                    //searchProccessing(numAgristore);
                  },
                  onSubmitted: (String numAgristore) async {
                    if(
                      numAgristore.isNotEmpty && 
                      numAgristore.replaceAll(' ', '').compareTo("") != 0) 
                    {
                      searchProccessing(numAgristore.replaceAll(' ', ''), configModel);
                    }else{
                      _displaySnackBar(
                        context, 
                        "Le numéro agristore de l'acteur ne doit pas être vide"
                      );
                    }
                    //print(numAgristore);
                  },
                )
              ),
            ],
          ),
        ),
      )
    );
  }

  searchButton(ConfigModel configModel){
    return GestureDetector(
      onTap: (){
        FocusScope.of(context).requestFocus(new FocusNode());
        if(inputValue.isNotEmpty && inputValue.replaceAll(' ', '').compareTo("") != 0) {
          searchProccessing(inputValue.replaceAll(' ', ''), configModel);
        }else{
          _displaySnackBar(
            context, 
            "Le numéro agristore de l'acteur ne doit pas être vide"
          );
        }
      },
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width/1.2,
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                FunctionUtils.colorFromHex(configModel.mainColor),
                FunctionUtils.colorFromHex(configModel.mainColor),
              ],
            ),
        ),
        child: Center(
          child: Text("Rechercher".toUpperCase(),
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
    );
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(
      content: Text(
          message,
          textAlign: TextAlign.center,
      ),
      //backgroundColor: Colors.redAccent,
    );
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }
}