
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/connection_dto.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/add_udocument_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/liste_udocument_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/mes_statistiques/mes_statistiques_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/liste_scoop_page.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/launch/presentation/pages/first_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/home_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/update_password_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/update_profil_page.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:provider/provider.dart';

import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'notification/notification_page.dart';

class AccountPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  String type;
  AccountPage(this.type);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState(this.type);
  }



}



class PageState extends State<AccountPage> {

  String _type;
  PageState(this._type);

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  ConnectionDto _connectionDto = ConnectionDto();
  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();


  final controller = ScrollController();
  double offset = 0;
  String _fullName="";
  String _dateCreate="";
  String _phone="";
  String _email="";
  String _location="";
  String _solde="";
  String _accessToken="";

  final List<PopupMenuEntry> menuEntries = [
    PopupMenuItem(
      value: 1,
      child: Text(allTranslations.text('update_profil')),
    ),
    PopupMenuItem(
      value: 2,
      child: Text(allTranslations.text('update_password')),
    ),
    PopupMenuItem(
      value: 3,
      child: Text(allTranslations.text('update_location')),
    ),

    PopupMenuItem(
      value: 4,
      child: Text(allTranslations.text('log_out')),
    ),

  ];




  @override
  void initState() {


    // TODO: implement initState

    super.initState();
    controller.addListener(onScroll);

    _getUserInformation();
    WidgetsBinding.instance.addPostFrameCallback((_) =>_showMessage() );
  }

  void _showMessage(){
    if(_type.compareTo("1")==0){
      _displaySnackBar(context, allTranslations.text('emptyLocation'));
    }
  }

  @override
  void dispose() {


    // TODO: implement dispose

    controller.dispose();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (controller.hasClients) ? controller.offset : 0;
    });
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      final hr = Divider(
        thickness: 1.0,
        color: Colors.black12,
      );

      return Scaffold(
        key:widget._scaffoldKey,
        body: SingleChildScrollView(
          controller: controller,
          child: Column(
            children: <Widget>[

              ClipPath(
                clipper: MyClipper(),
                child: Container(
                  padding: EdgeInsets.only(left: 0, top: 20, right: 0),
                  height: 320,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        FunctionUtils.colorFromHex(configModel.mainColor),
                        FunctionUtils.colorFromHex(configModel.mainColor)
                        //Color(0xFF11249F),
                      ],
                    ),
                  ),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only( left: 20.0, top: 20.0,right: 20.0),
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop(null);
                              },
                              child: Icon(Icons.arrow_back,size: 22.0,color: Colors.white),
                            ),
                            Expanded( child: Text("")),
                            GestureDetector(
                              onTap: () {
                                final RenderBox popupButtonObject = widget._scaffoldKey.currentContext.findRenderObject();
                                final RenderBox overlay = Overlay.of(context).context.findRenderObject();
                                final RelativeRect position = RelativeRect.fromRect(
                                    Rect.fromPoints(
                                      popupButtonObject.localToGlobal(Offset.zero, ancestor: overlay),
                                      popupButtonObject.localToGlobal(popupButtonObject.size.bottomRight(Offset.zero), ancestor: overlay),
                                    ),
                                    new Rect.fromLTWH(-100.0, -55.0, overlay.size.width, overlay.size.height)
                                );

                                // Finally, show the menu.
                                showMenu(
                                  context: context,
                                  elevation: 8.0, // default value
                                  items: menuEntries,
                                  initialValue: null,
                                  position: position,
                                ).then((res) {
                                  if(res==1){

                                    _update_profile();

                                  }else if(res==2){

                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => UpdatePasswordPage()),
                                    ).then((value){
                                      if(value!=null && value.compareTo("update")==0){

                                        _displaySnackBar(context, allTranslations.text('success_update'));

                                      }
                                    });

                                  }else if(res==3){
                                    _showLocationDialog(context,configModel);
                                  }
                                  else if(res==4){
                                    _showAlertDialog(context);
                                  }

                                });
                              },
                              child: Container(
                                padding: EdgeInsets.only(top:0),
                                child: SvgPicture.asset("assets/img/menu.svg"),
                              ),
                            ),
                          ],
                        ),
                      ),

                      SizedBox(height: 0),
                      Material(
                        shape: CircleBorder(),
                        clipBehavior: Clip.hardEdge,
                        color: Colors.white,
                        child: Image.asset(
                          "assets/img/firsthome.png",
                          width: 130,
                          height: 130,
                          fit: BoxFit.fitWidth,
                          alignment: Alignment.topCenter,
                        ),
                      ),
                      SizedBox(height: 15),
                      Text(
                        _fullName,
                        textAlign: TextAlign.center,
                        style: kHeadingTextStyle.copyWith(
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        //  "${allTranslations.text('created_at')} $_dateCreate",
                          "$_phone",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Colors.white70,
                              fontSize: 14
                          )
                      ),

                    ],
                  ),


                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[


                    MaterialButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          //recuperer les informations de lutilisateur

                        },
                        child:Container(
                          padding: EdgeInsets.only( bottom: 20.0, top: 20.0),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.monetization_on,size: 18.0,color: Colors.black45),
                              SizedBox(width: 10),
                              Expanded(
                                child: Text(
                                  allTranslations.text('solde') ,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black45
                                  )
                                  ,
                                ),
                              ),
                              Text(
                                _solde ,
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black
                                ),
                              ),
                            ],
                          ),
                        )
                    ),
                    hr,
                    MaterialButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          //recuperer les informations de lutilisateur
                          _showLocationDialog(context,configModel);
                        },
                        child:Container(
                          padding: EdgeInsets.only( bottom: 20.0, top: 20.0),
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.map,size: 18.0,color: Colors.black45),
                              SizedBox(width: 10),
                              Expanded(
                                child: Text(
                                  allTranslations.text('location') ,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black45
                                  )
                                  ,
                                ),
                              ),
                              Text(
                                _location ,
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black
                                ),
                              ),
                            ],
                          ),
                        )
                    ),
                    hr,
                    SizedBox(height: 50),

                   /*  MaterialButton(
                      color: Colors.white,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: FunctionUtils.colorFromHex("E6EBEF"))
                      ),
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ListeUserDocumentPage()),
                        );
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.insert_drive_file,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                            size: 25,
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            child: Text(
                              allTranslations.text('user_documents'),
                              style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black, fontSize: 16.0),
                            ),
                          )
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 15),

                    ),
                    SizedBox(height: 10,), */

                    MaterialButton(
                      color: Colors.white,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: FunctionUtils.colorFromHex("E6EBEF"))
                      ),
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ListeScoopPage()),
                        );
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.people,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                            size: 25,
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            child: Text(
                              allTranslations.text('menu_listescoop'),
                              style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black, fontSize: 16.0),
                            ),
                          )
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                    ),
                    SizedBox(height: 10,),
                    MaterialButton(
                      color: Colors.white,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: FunctionUtils.colorFromHex("E6EBEF"))
                      ),
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => NotificationPage()),
                        );
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.add_alert,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                            size: 25,
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            child: Text(
                              allTranslations.text('mes_notification'),
                              style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black, fontSize: 16.0),
                            ),
                          )
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 15),

                    ),
                    SizedBox(height: 10,),

                    MaterialButton(
                      color: Colors.white,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: FunctionUtils.colorFromHex("E6EBEF"))
                      ),
                      textColor: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MesStatistiquesPage()),
                        );
                      },
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.show_chart,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                            size: 25,
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                            child: Text(
                              allTranslations.text('mes_statistics'),
                              style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black, fontSize: 16.0),
                            ),
                          )
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(10, 15, 10, 15),

                    ),
                    SizedBox(height: 10,),





                    SizedBox(height: 100,),
                    MaterialButton(
                      color: FunctionUtils.colorFromHex(configModel.mainColor),
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: FunctionUtils.colorFromHex("E6EBEF"))
                      ),
                      textColor: Colors.white,
                      onPressed: () {
                        _showAlertDialog(context);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(
                            allTranslations.text('log_out'),
                            style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white, fontSize: 15.0),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(10, 17, 10, 17),

                    ),

                    SizedBox(height: 50),
                  ],
                ),
              ),


            ],
          ),
        ),
      );
    },
  );



  _getUserInformation(){
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _fullName="${value.nom} ${value.prenom}";
        _dateCreate=FunctionUtils.convertDate(value.dateCreated);
        _phone=value.username;
        _email=value.email;
        _location="${value.latitude} , ${value.longitude}";
        _solde="${value.solde}";
        _accessToken=value.token;
      });

    });


  }

  _update_profile(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => UpdateProfilPage()),
    ).then((value){
      if(value!=null && value.compareTo("update")==0){

        _getUserInformation();
        _displaySnackBar(context, allTranslations.text('success_update'));
      }
    });
  }

  _showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        _confirmeLogout();
      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(allTranslations.text('log_out_text'),style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _showLocationDialog(BuildContext context,ConfigModel configModel) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        _getLocation(configModel);
      },
    );

    String full_location="";
    if(_location.trim().compareTo(",")!=0){
      full_location="\n\n${allTranslations.text('location')}: $_location";
    }

    AlertDialog alert = AlertDialog(
      content: Text("${allTranslations.text('location_text')} $full_location",style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _confirmeLogout(){

    _appSharedPreferences.logoutUser(context);
    _getServerOffre();
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) =>
        // FirstPage()
        HomePage("","-1","0")
        ),
        ModalRoute.withName("/"));
  }

  _getServerOffre () async {
    print("get public annonce");
    _localAnnonceRepository.allAnnonce(0).then((response) {
      Api api = ApiRepository();
      api.getAnnonce("").then((value) {
        if (value.isRight()) {
          value.all((a) {
            FunctionUtils.saveAnnonce(a);
            return true;
          });
        } else {
          Navigator.of(context).pop(null);
          return false;
        }
      });
    });
  }

  _getLocation(ConfigModel configModel){
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('location_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    FunctionUtils.getCurrentLocation().then((location){
      Navigator.of(context).pop(null);
      if(location!=null){
        _confirmSendLocation(context,"${location.latitude}","${location.longitude}",configModel);
      }else{
        _displaySnackBar(context, allTranslations.text('location_error'));
      }

    });
  }

  _confirmSendLocation(BuildContext context,String lat,String long,ConfigModel configModel) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('location_send'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        _connectionDto.accessToken = _accessToken;
        _connectionDto.latitude = lat;
        _connectionDto.longitude = long;
        _resetLocation(configModel);
      },
    );

    String full_location="\n\n${allTranslations.text('newlocation')}: $lat,$long,";

    AlertDialog alert = AlertDialog(
      content: Text("${allTranslations.text('location_text')} $full_location",style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _resetLocation(ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('slocation_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.refreshLocation(_connectionDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if(a.status.compareTo("ok")==0){

            //mise a jour de la preference
            _appSharedPreferences.getUserInformation().then((value) {

              value.latitude=_connectionDto.latitude;
              value.longitude=_connectionDto.longitude;

              _appSharedPreferences.createLoginSession(context,value).then((value) {
                _getUserInformation();
                Navigator.of(context).pop(null);
                _displaySnackBar(context, a.message);
              });

            });
            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context,a.message);
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context,allTranslations.text('error_process'));
        return false;
      }
    });

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }
}


class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height - 80);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 80);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}






