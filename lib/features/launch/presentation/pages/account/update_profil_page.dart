import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/dto/inscription_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:provider/provider.dart';



class UpdateProfilPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState();
  }
}



class PageState extends State<UpdateProfilPage> {

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  TextEditingController _emailController=TextEditingController();
  TextEditingController _nomController=TextEditingController();
  TextEditingController _prenomController=TextEditingController();
  TextEditingController _phoneController=TextEditingController();
  InscriptionDto _inscriptionDto = InscriptionDto();
  String _accessToken="";


  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }



  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:  Align(
            // alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(right: 50.0),
              alignment: Alignment.center,
              child: Text(allTranslations.text('title_profil'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,

                ),
              ),
            ),
          ),
          backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        body: Stack(

            children: <Widget>[
              ListView(
                  padding: EdgeInsets.all( 20.0),
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width/1.2,
                      height: 45,
                      margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          top: 4,left: 16, right: 16, bottom: 4
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5
                            )
                          ]
                      ),
                      child: TextField(
                        keyboardType: TextInputType.text,
                        controller: _nomController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          icon: Icon(Icons.account_circle,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                          ),
                          hintText: allTranslations.text('enter_nom'),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width/1.2,
                      height: 45,
                      margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          top: 4,left: 16, right: 16, bottom: 4
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5
                            )
                          ]
                      ),
                      child: TextField(
                        keyboardType: TextInputType.text,
                        controller: _prenomController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          icon: Icon(Icons.account_circle,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                          ),
                          hintText: allTranslations.text('enter_firstname'),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width/1.2,
                      height: 45,
                      margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          top: 4,left: 16, right: 16, bottom: 4
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5
                            )
                          ]
                      ),
                      child: TextField(
                        keyboardType: TextInputType.phone,
                        controller: _phoneController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          icon: Icon(Icons.phone,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                          ),
                          hintText: allTranslations.text('enter_number'),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width/1.2,
                      height: 45,
                      margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          top: 4,left: 16, right: 16, bottom: 4
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5
                            )
                          ]
                      ),
                      child: TextField(
                        keyboardType: TextInputType.emailAddress,
                        controller: _emailController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          icon: Icon(Icons.mail,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                          ),

                          hintText: allTranslations.text('enter_email'),
                        ),
                      ),
                    ),


                    SizedBox(height: 50),
                    MaterialButton(
                      color:  FunctionUtils.colorFromHex(configModel.mainColor),
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color:  FunctionUtils.colorFromHex(configModel.mainColor))
                      ),
                      textColor: Colors.white,
                      onPressed: () {

                        if(_emailController.text.compareTo("")!=0 && _nomController.text.compareTo("")!=0 && _prenomController.text.compareTo("")!=0 && _phoneController.text.compareTo("")!=0) {

                          _inscriptionDto.accessToken = _accessToken;
                          _inscriptionDto.telephone = _phoneController.text;
                          _inscriptionDto.email = _emailController.text;
                          _inscriptionDto.nom = _nomController.text;
                          _inscriptionDto.prenoms = _prenomController.text;
                          _updateProfil(configModel);

                        }else{
                          _displaySnackBar(context, allTranslations.text('emptyField'));
                        }



                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(
                            allTranslations.text('update'),
                            style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white, fontSize: 16.0),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),

                    ),
                  ])
            ]
        ),
      );
    },
  );



  _getUserInformation(){
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _accessToken=value.token;
        _emailController.text=value.email;
        _nomController.text=value.nom;
        _prenomController.text=value.prenom;
        _phoneController.text=value.username;

      });

    });


  }

  _updateProfil(ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('profil_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.updateProfil(_inscriptionDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if(a.status.compareTo("ok")==0){

            //mise a jour de la preference
            _appSharedPreferences.getUserInformation().then((value) {

              value.email=_emailController.text;
              value.nom=_nomController.text;
              value.prenom=_prenomController.text;
              value.username=_phoneController.text;

              _appSharedPreferences.createLoginSession(context,value).then((value) {

                Navigator.of(context).pop(null);
                Navigator.pop(context,"update");

              });

            });


            return true;

          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}

