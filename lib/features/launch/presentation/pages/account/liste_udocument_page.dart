
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:agrimobile/core/utils/core_constantes.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/composanteop_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/detaileop_model.dart';
import 'package:agrimobile/features/common/data/models/eop_model.dart';
import 'package:agrimobile/features/common/data/models/simulation_model.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/add_udocument_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/add_document_page.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/pdf.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:share/share.dart';

import 'package:dio/dio.dart';
import 'package:progress_dialog/progress_dialog.dart';


class ListeUserDocumentPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  ListeUserDocumentPage();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}



class PageState extends State<ListeUserDocumentPage> {


  ConfigModel _myConfigModel;
  List<String> _allFichier=[];


  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );


  @override
  void initState() {
    super.initState();

    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      _myConfigModel=configModel;
      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 0.0)),

          appBar: new AppBar(
            title:  Text(allTranslations.text('user_documents'),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,

              ),
            ),
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>
                    AddUserDocumentPage()),
              ).then((value){
                if(value!=null){
                  setState(() {
                    if(value.compareTo("")!=0){
                      _allFichier=value.split(";");
                    }
                  });
                  _displaySnackBar(context, allTranslations.text('success_document'));
                }
              });
            },
            child: Icon(Icons.insert_drive_file,color: Colors.white),
            backgroundColor: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return Stack(
                  children: <Widget>[
                    ListView(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      children: <Widget>[
                        SizedBox(height: 30,),
                        _getDocumentListview(myContext),
                        if(_allFichier!=null && _allFichier.length>0)SizedBox(height: 70.0),
                      ],
                    )

                  ],
                );

              }
          )
      );
    },
  );

  Future<bool> _requestPermissions() async {
    var permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);
    }

    return permission == PermissionStatus.granted;
  }

  Future<void> _getPermissionPath(BuildContext context,String filename,int type) async {


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('download_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });




    Directory _appDocDir= await getExternalStorageDirectory();

    String mypath=_appDocDir.path;
    if (Platform.isAndroid) {
      mypath=mypath.split("Android")[0];
    }


    final Directory _appDocDirFolder = Directory(
        '$mypath/${DataConstantesUtils.simulationFolder}/');

    if (await _appDocDirFolder.exists()) {
      String filePath = '${_appDocDirFolder.path}$filename';

      _verifyFile(context,filename, filePath);
    } else {
      Future<Directory> _appDocDirNewFolder = _appDocDirFolder.create(
          recursive: true);
      _appDocDirNewFolder.then((response) {
        String filePath = '${response.path}$filename';
        _verifyFile(context,filename, filePath);
      });
    }
  }

  _verifyFile(BuildContext context,String fileName, String filePath) async {
    Navigator.of(context).pop(null);
    final File file = File(filePath);

    if (await file.exists()) {
      OpenFile.open(filePath);
    }else{
      String serverPath="${SERVER_URL}/admin/images/user_document/$fileName";
      _startdownload(serverPath,filePath);
    }
  }

  Future<void> _startdownload(uri, savePath) async {

    String  progress="0";
    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,type: ProgressDialogType.Download, isDismissible: false, showLogs: false);
    pr.style(
        message: allTranslations.text('download_loading'),
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    pr.show();



    Dio dio = Dio();

    dio.download(
      uri,
      savePath,
      onReceiveProgress: (rcv, total) {
        progress = ((rcv / total) * 100).toStringAsFixed(0);

        if(int.parse(progress)>0) {
          pr.update(
            progress: int.parse(progress) * 1.0,
            message: allTranslations.text('download_loading'),
            progressWidget: Container(
                padding: EdgeInsets.all(8.0),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
            ),
            maxProgress: 100.0,
            progressTextStyle: TextStyle(
                color: Colors.black,
                fontSize: 13.0,
                fontWeight: FontWeight.w400),
            messageTextStyle: TextStyle(
                color: Colors.black,
                fontSize: 19.0,
                fontWeight: FontWeight.w600),
          );
        }
      },
      deleteOnError: true,
    ).then((_) {

      pr.hide().then((isHidden) {

        OpenFile.open(savePath);
      });

    });
  }

  _getUserInformation(){

    _appSharedPreferences.getUserInformation().then((value) {

      setState(() {
        if(value.document.compareTo("")!=0){
          _allFichier=value.document.split(";");
        }
      });
    });





  }

  _getDocumentListview(BuildContext selectContext) {

    if(_allFichier!=null && _allFichier.length>0){

      var listview= ListView.builder(
          itemCount: _allFichier.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            String fileUrl=_allFichier[position];
            String name_file=_allFichier[position];
            name_file=name_file.split("__")[0];
            name_file=name_file.replaceAll('_', ' ');
            name_file=name_file.split(".")[0];

            return Container(
              padding: EdgeInsets.only(left:10,right:10),
              child: Column(
                children: [

                  MaterialButton(

                    elevation: 0.0,
                    onPressed: ()  async {
                      final isPermissionStatusGranted = await _requestPermissions();
                      if (isPermissionStatusGranted) {
                        _getPermissionPath(context,fileUrl,1);
                      }else{
                        _displaySnackBar(context,allTranslations.text('reject_permission'));
                      }
                    },
                    child: Container(
                      padding: EdgeInsets.fromLTRB(10, 3, 10, 3),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color:FunctionUtils.colorFromHex("DEDEDE"),
                              style: BorderStyle.solid,
                              width: 1.0
                          ),
                          borderRadius: BorderRadius.circular(5.0)
                      ),
                      child: Row(
                        children: [
                          CircleAvatar(
                              radius: 15,
                              backgroundColor: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
                              child:CircleAvatar(
                                radius: 14.0,
                                backgroundColor:Colors.white,
                                child: Icon(Icons.attach_file,
                                  color: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
                                  size: 20,
                                ) ,
                              )
                          ),
                          SizedBox(width: 5,),
                          Expanded(
                            child:  Text("${name_file}",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                            ),
                          ),
                          Icon(Icons.cloud_download, color: FunctionUtils.colorFromHex(_myConfigModel.mainColor),size: 20,),
                        ],
                      ),
                    ),
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  ),
                ],
              ),
            );

          });

      return listview;

    }else{

      return  Center(
        child: Column(
          children: [
            SizedBox(height: 100,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_dempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );

    }
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}

