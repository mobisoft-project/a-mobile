import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/dto/connection_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:provider/provider.dart';



class UpdatePasswordPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState();
  }
}



class PageState extends State<UpdatePasswordPage> {

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  TextEditingController _oldController=TextEditingController();
  TextEditingController _passwordController=TextEditingController();
  TextEditingController _passwordConfirmController=TextEditingController();
  ConnectionDto _connectionDto = ConnectionDto();
  String _accessToken="";


  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }



  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:  Align(
            // alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(right: 50.0),
              alignment: Alignment.center,
              child: Text(allTranslations.text('title_pwd'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,

                ),
              ),
            ),
          ),
          backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        body: Stack(

            children: <Widget>[
              ListView(
                  padding: EdgeInsets.all( 20.0),
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width/1.2,
                      height: 45,
                      margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          top: 4,left: 16, right: 16, bottom: 4
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5
                            )
                          ]
                      ),
                      child: TextField(
                        controller: _oldController,
                        obscureText: true,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          icon: Icon(Icons.vpn_key,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                          ),
                          hintText:  allTranslations.text('old_pwd'),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width/1.2,
                      height: 45,
                      margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          top: 4,left: 16, right: 16, bottom: 4
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5
                            )
                          ]
                      ),
                      child: TextField(
                        controller: _passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          icon: Icon(Icons.vpn_key,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                          ),
                          hintText:  allTranslations.text('new_pwd'),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width/1.2,
                      height: 45,
                      margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          top: 4,left: 16, right: 16, bottom: 4
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5
                            )
                          ]
                      ),
                      child: TextField(
                        controller: _passwordConfirmController,
                        obscureText: true,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          icon: Icon(Icons.vpn_key,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                          ),
                          hintText:  allTranslations.text('confirm_password'),
                        ),
                      ),
                    ),
                    SizedBox(height: 50),
                    MaterialButton(
                      color:  FunctionUtils.colorFromHex(configModel.mainColor),
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color:  FunctionUtils.colorFromHex(configModel.mainColor))
                      ),
                      textColor: Colors.white,
                      onPressed: () {
                        if(_oldController.text.compareTo("")!=0 && _passwordController.text.compareTo("")!=0 && _passwordConfirmController.text.compareTo("")!=0) {

                          if(_oldController.text.compareTo(_passwordController.text)!=0 ) {

                            if(_passwordController.text.compareTo(_passwordConfirmController.text)==0 ) {

                              _connectionDto.accessToken = _accessToken;
                              _connectionDto.newPassword = _passwordController.text;
                              _connectionDto.oldPassword = _oldController.text;
                              _updatePassword(configModel);


                            }else{
                              _displaySnackBar(context, allTranslations.text('error_confirm_pwd'));
                            }
                          }else{
                            _displaySnackBar(context, allTranslations.text('oldmewPassword'));
                          }
                        }else{
                          _displaySnackBar(context, allTranslations.text('emptyField'));
                        }

                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(
                            allTranslations.text('update'),
                            style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white, fontSize: 16.0),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),

                    ),
                  ])
            ]
        ),
      );
    },
  );

  
  _getUserInformation(){
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _accessToken=value.token;
      });

    });


  }

  _updatePassword(ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('spassword_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.updatePassword(_connectionDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if(a.status.compareTo("ok")==0){

            Navigator.of(context).pop(null);
            Navigator.pop(context,"update");
            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}

