import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/notification_dto.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_notification_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_notification_repository.dart';


class NotificationPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final VoidCallback onRefresh;
  NotificationPage({
    this.onRefresh
  });

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage>{
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalNotificationRepository _localNotificationRepository = LocalNotificationRepository();
  List<LocalNotificationEntity> _localNotificationEntity;
  
  bool _haveNotification = true;
  List notifications;
  String _userToken = "";
  String _userId = "";
  Api api = ApiRepository();
  
  @override
  void initState(){
    super.initState();
    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child){
        return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:  Align(
            child: Container(
              child: Text(allTranslations.text('mes_notification'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  _getServerNotification(configModel);
                  setState(() {
                    _haveNotification = false;
                  });
                },
                child: Icon(
                  Icons.refresh,
                  size: 26.0,
                ),
              )
            ),
          ],
          backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        backgroundColor: FunctionUtils.colorFromHex("ffffff"),
        body: Builder(builder: (BuildContext myContext){
          return _getNotificationListView();
        })
      );
    }
  );

  _refreshNotificationList() async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.all(12),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Supperssion en cours..."),
              SizedBox(height: 20),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(
                  Colors.red
                ),
              ),
            ],
          )
        );
      }
    );

    _localNotificationRepository
    .listNotification(int.parse(_userId))
    .then((res){
      setState(() {
        _localNotificationEntity = res;
      });
    });
  }

  _getUserInformation() {
      _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userToken = "${value.token}";
        _userId = "${value.id}";
      });
      _getLocalNotifcations();
    });
  }

  _getLocalNotifcations() async {
    _localNotificationRepository
    .listNotification(int.parse(_userId))
    .then((List<LocalNotificationEntity> items) {
      setState(() {
        _haveNotification = false;
        _localNotificationEntity = items;
      });
      if(_localNotificationEntity.isEmpty){
        _getRemoteNotification(0);
        return;
      }
    });
  }

  _getRemoteNotification(int caller) {
    NotificationDto _notificationDto = NotificationDto.create(
        accessToken: DataConstantesUtils.API_TOKEN,
        userToken: _userToken
    );

    api.getNotification(_notificationDto).then((value) {
      if (value.isRight()) {
        value.all((notificationResponse) {
          if (notificationResponse != null &&
              notificationResponse.status.compareTo("000") == 0) {
            if (notificationResponse.allNotification.isEmpty) {
              
              if(caller == 1){
                Navigator.of(context).pop(null);
              }
              return false;
            }
            _haveNotification = true;
            FunctionUtils.saveNotifications(notificationResponse.allNotification, int.parse(_userId));
            Timer(Duration(seconds: 2),(){
              _localNotificationRepository.listNotification(int.parse(_userId)).then((res){
                if(res.length > 0){
                  setState(() {
                    _haveNotification = false;
                    _localNotificationEntity = res;
                  });
                }
              });
              if(caller == 1){
                Navigator.of(context).pop(null);
              }
            });
            return true;
          } else {
            if(caller == 1){
              Navigator.of(context).pop(null);
            }
            return false;
          }
        });
      } else {
        if(caller == 1){
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });
  }

  _getServerNotification(ConfigModel configModel) async{
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.all(12),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(allTranslations.text('refresh_processing')),
              SizedBox(height: 20),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(
                  FunctionUtils.colorFromHex(configModel.mainColor)
                ),
              ),
            ],
          )
        );
      }
    );
    _getRemoteNotification(1);
  }
  _removeNotif(LocalNotificationEntity notification){
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.all(12),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Suppression en cours..."),
              SizedBox(height: 20),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(
                  Colors.red
                ),
              ),
            ],
          )
        );
      }
    );
    _deleteNotification(notification);
  }

  _alertDialogbox(BuildContext context, LocalNotificationEntity notification) {
    Widget cancelButton = FlatButton(
      child: Text(
        allTranslations.text('cancel'),
        style: new TextStyle(
          fontWeight: FontWeight.bold, 
          fontSize: 14.0
        ),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(
        allTranslations.text('confirm'),
        style: new TextStyle(
          fontWeight: FontWeight.bold, 
          fontSize: 14.0
        ),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        _removeNotif(notification);
      },
    );


    AlertDialog alert = AlertDialog(
      content: Text(
        allTranslations.text('delete_notification_text'),
        style: new TextStyle( fontSize: 15.0),
      ),
      actions: [cancelButton,continueButton],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _deleteNotification(LocalNotificationEntity notification){
    NotificationDto notificationDto = NotificationDto();
    notificationDto.accessToken = DataConstantesUtils.API_TOKEN;
    notificationDto.userToken = _userToken;
    notificationDto.notification_ids = "${notification.apiId}";

    Api api = ApiRepository();
    
    print('--Id notification--');
    print(notificationDto.notification_ids.toString());
    api.deleteNotification(notificationDto).then((val) {
      if(val.isRight()){
        val.all((res){
          print('---**//Message/----55');
          print(res.message.toString());
          if(res.status.compareTo("000") == 0){
            _localNotificationRepository.delete(
              notification.idNotification, 
              "idAppNotification"
            ).then((va){
              print('---**/888/----55');
              print(va.toString());
              _getLocalNotifcations();
            });

            
            Navigator.of(context).pop(null);
            return true;
          }else{
            Navigator.of(context).pop(null);
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        return false;
      }
    });
  }

  _getNotificationListView(){
    if(_haveNotification == true){
      return Center(
          child: Column(
            children: [
              SizedBox(
                height: 100,
              ),
              Material(
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                color: Colors.transparent,
                child: Image.asset('assets/img/loading.gif', height: 100),
              ),
              SizedBox(
                height: 50,
              ),
            ],
          ),
        );
    }else if(_localNotificationEntity == null || 
    _localNotificationEntity.length == 0){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 100),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0),
            Container(
              width: 300,
              child: Text(
                "Aucune notification n'est disponible pour vous!",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0
                ),
              ),
            )
          ],
        ),
      );
    }else{
      return Container(
        child: ListView.separated(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemCount: _localNotificationEntity.length,
          itemBuilder: (BuildContext context, int index) {
            return _getNotificationTile(_localNotificationEntity[index]);
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
        ),
      );
    }
  }

  _getNotificationTile(LocalNotificationEntity item){
    return Container(
      padding: EdgeInsets.all(12.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: CircleAvatar(
              radius: 25.0,
              child: Text(
                "WIA",
                style: TextStyle(color: Colors.orangeAccent),
              ),
              backgroundColor: Color(0xFF146C54)
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  InkWell(
                    child: Text(
                      item?.title ?? "",
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context){
                          return _getCustomDialogBox(item);
                        }
                      );
                    },
                  ),
                  InkWell(
                    child: Text(
                      item?.description ?? "",
                      maxLines: 2,
                      style: TextStyle(fontSize: 16.0, color: Colors.grey),
                    ),
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context){
                          return _getCustomDialogBox(item);
                        }
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(item?.dateCreation ?? ""),
              //DateFormat.jm().format(DateTime.now())
              IconButton(
                icon: Icon(
                  Icons.delete_rounded, 
                  color: Colors.red
                ),
                onPressed: () {
                  _alertDialogbox(context, item);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  _getCustomDialogBox(LocalNotificationEntity data){
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20)
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Stack(
      children: [
        Container(
          padding: EdgeInsets.only(left: 20, top: 65, right: 20, bottom: 20),
          margin: EdgeInsets.only(top: 40),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                offset: Offset(0,10), 
                blurRadius: 10
              ),
            ]
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                data.title,
                style: TextStyle(
                  fontSize: 18, 
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 15),
              Text(
                data.description,
                style: TextStyle(fontSize: 14),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  /* Align(
                    alignment: Alignment.bottomRight,
                    child: FlatButton(
                        onPressed: () async {
                          _removeNotif(data);
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Supprimer", 
                          style: TextStyle(
                            fontSize: 13,
                            color: Colors.red
                          )
                        )
                    ),
                  ), */
                  Center(
                    child: FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        "Fermer",
                        style: TextStyle(
                          fontSize: 13,
                          color: Colors.lightBlue
                        )
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ],
      )
    );
  }
}
