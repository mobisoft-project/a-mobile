import 'package:agrimobile/features/launch/presentation/pages/account/notification/custom_dialog_box.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/features/common/data/models/notification_model.dart';

class NotificationListTile extends StatelessWidget {
  const NotificationListTile({
    Key key,
    @required this.item,
  }) : super(key: key);

  final NotificationModel item;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: CircleAvatar(
              radius: 25.0,
              child: Text(
                "WIA",
                style: TextStyle(color: Colors.orangeAccent),
              ),
              backgroundColor: Color(0xFF146C54)
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  InkWell(
                    child: Text(
                      item?.title ?? "",
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context){
                          return CustomDialogBox(
                            data: item
                          );
                        }
                      );
                    },
                  ),
                  InkWell(
                    child: Text(
                      item?.description ?? "",
                      maxLines: 2,
                      style: TextStyle(fontSize: 16.0, color: Colors.grey),
                    ),
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context){
                          return CustomDialogBox(
                            data: item
                          );
                        }
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(item?.dateCreation ?? ""),
              //DateFormat.jm().format(DateTime.now())
              IconButton(
                icon: Icon(Icons.delete_rounded, color: Colors.red),
                onPressed: () {},
              ),
            ],
          ),
        ],
      ),
    );
  }
}