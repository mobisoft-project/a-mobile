import 'dart:async';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/my_statistics_dto.dart';
import 'package:agrimobile/features/common/data/dto/user_statistics_dto.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_account_statistics_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_user_statistics_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_account_statistics_repository.dart.dart';
import 'package:agrimobile/features/common/domain/repositories/local_user_statistics_repository.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';


class MesStatistiquesPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  _MesStatistiquesPageState createState() => _MesStatistiquesPageState();
}

class _MesStatistiquesPageState extends State<MesStatistiquesPage> with SingleTickerProviderStateMixin{
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalMyStatisticsRepository _localOffreStatisticsRepository = LocalMyStatisticsRepository();
  LocalUserStatisticRepository _localDemandeStatisticsRepository = LocalUserStatisticRepository();
  
  List<LocalMyStatisticsEntity> localOffreStatisticsData;
  List<LocalUserStatisticEntity> localDemandeStatisticsData;

  String _userToken="";
  String offreSpeculation = "";
  String demandeSpeculation = "";
  String _userId="";
  Api api = ApiRepository();
  

  TabController _myController;
  int _mySelectedIndex = 0;

  List<Widget> my_list = [
    Tab(text: "Mes Demandes"),
    Tab(text: "Mes Offres"),
  ];
  @override
  void initState() {
    super.initState();
    _myController = TabController(length: my_list.length, vsync: this);
    _myController.addListener(() {
      setState(() {
        _mySelectedIndex = _myController.index;
      });
    });
    _getUserInformations();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child){
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title: Align(
            child: Container(
              child: Text(allTranslations.text('mes_statistics')),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  _getFromServer(configModel);
                  
                },
                child: Icon(
                  Icons.refresh,
                  size: 26.0,
                ),
              )
            )
          ],
          backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
        body: Builder(builder: (BuildContext myContext){
          if((localOffreStatisticsData == null || localOffreStatisticsData.length == 0) 
          || (localDemandeStatisticsData == null || localDemandeStatisticsData.length == 0)){
            return Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  Material(
                    shape: CircleBorder(),
                    clipBehavior: Clip.hardEdge,
                    color: Colors.transparent,
                    child: Image.asset('assets/img/loading.gif', height: 100),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              ),
            );
          }else{
            return Column(
              children: [
                Container(
                  child: TabBar(
                    tabs: my_list,
                    controller: _myController,
                    labelColor: FunctionUtils.colorFromHex("020000"),
                    unselectedLabelColor: FunctionUtils.colorFromHex("737373"),
                    indicatorColor: FunctionUtils.colorFromHex(configModel.mainColor)
                  ),
                  color: FunctionUtils.colorFromHex("dfdfdf"),
                ),
                Expanded(
                  child: TabBarView(
                  controller: _myController,
                  children: [
                    _mesDemandesView(),
                    _mesOffresView()
                  ],
                  ),
                )
              ],
            );
          }
          
        })
      );
    }
  );
  //Traitements
  _getUserInformations(){
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userToken = "${value.token}";
        _userId="${value.id}";
      });
      _getUserDemandeStatistics();
      _getUserOffresStatistics();
    });
  }

  _getUserOffresStatistics() async {
    _localOffreStatisticsRepository
    .userStatistics(int.parse(_userId))
    .then((List<LocalMyStatisticsEntity> value){
      setState(() { localOffreStatisticsData = value;});

      if (localOffreStatisticsData.isEmpty) {
        _getRometOffresStatistics();
        return;
      }
    });
  }

  _getUserDemandeStatistics() async {
    _localDemandeStatisticsRepository
    .userStatisticData(int.parse(_userId))
    .then((List<LocalUserStatisticEntity> value){
      setState(() { localDemandeStatisticsData = value;});

      if (localDemandeStatisticsData.isEmpty) {
        _getRometDemandeStatistics(0);
        return;
      }
    });
  }

  _getRometOffresStatistics(){
      MyStatisticsDto _myStatDto = MyStatisticsDto.create(
        accessToken: DataConstantesUtils.API_TOKEN,
        userToken: _userToken
      );
      api.getMyStatistics(_myStatDto).then((value) {
      if (value.isRight()) {
        value.all((userRemoteStat) {
          if (userRemoteStat != null && userRemoteStat.status.compareTo("000") == 0){
            FunctionUtils.saveMyStatistics(userRemoteStat.totalOffre, userRemoteStat.activeOffre, userRemoteStat.chiffreAffaire, userRemoteStat.offreSpeculation, int.parse(_userId));
            
            Timer(Duration(seconds: 2),(){
              _localOffreStatisticsRepository.userStatistics(int.parse(_userId)).then((res){
                if(res.length > 0){
                  setState(() {
                    localOffreStatisticsData = res;
                  });
                }
              });
              //Navigator.of(context).pop(null);
            });
            return true;
          }else{
            //Navigator.of(context).pop(null);
            return false;
          }
        });
      }else{
        //Navigator.of(context).pop(null);
        return false;
      }
    });
  }

  _getRometDemandeStatistics(int caller){
      UserStatisticsDto _myDto = UserStatisticsDto.create(
        accessToken: DataConstantesUtils.API_TOKEN,
        userToken: _userToken
      );
      api.getUserStatistics(_myDto).then((value) {
      if (value.isRight()) {
        value.all((userRemoteStat) {
          if (userRemoteStat != null && userRemoteStat.status.compareTo("000") == 0){
            FunctionUtils.saveUserStatistic(
              userRemoteStat.nbreAchat, userRemoteStat.montantAchat, 
              userRemoteStat.endLivraison, userRemoteStat.waitingLivraison, 
              userRemoteStat.demandeSpeculation, int.parse(_userId));
            Timer(Duration(seconds: 2),(){
              _localDemandeStatisticsRepository.userStatisticData(int.parse(_userId)).then((res){
                if(res.length > 0){
                  setState(() {
                    localDemandeStatisticsData = res;
                  });
                }
              });
              if(caller == 1){
                Navigator.of(context).pop(null);
              }
            });
            return true;
          }else{
            if(caller == 1){
                Navigator.of(context).pop(null);
            }
            return false;
          }
        });
      }else{
        if(caller == 1){
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });
  }

  _getFromServer(ConfigModel configModel) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.all(12),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(allTranslations.text('refresh_processing')),
              SizedBox(height: 20),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(
                  FunctionUtils.colorFromHex(configModel.mainColor)
                ),
              ),
            ],
          )
        );
      }
    );
    _getRometOffresStatistics();
    _getRometDemandeStatistics(1);
  }

  //Traitements
  _mesOffresView(){
    return CustomScrollView(
      physics: ClampingScrollPhysics(),
      slivers: [
        SliverPadding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          sliver: SliverToBoxAdapter(
            child: _mesOffresGrid(),
          ),
        ),
        SliverPadding(
          padding: const EdgeInsets.only(top: 20.0),
          sliver: SliverToBoxAdapter(
            child: _mesOffresBarChart(),
          ),
        ),
      ],
    );
  }

  _formatPrice(int number){
    double nmberToDouble = number * 1.0;
    return FunctionUtils.separateur(nmberToDouble) + " F CFA";
  }

  _mesOffresGrid(){
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      child: Column(
        children: [
          Flexible(
            child: Row(
              children: [
                _buildStatCard(
                  'Chiffre d\'affaire', 
                  _formatPrice(localOffreStatisticsData[0]?.chiffreAffaire),
                  Colors.blue
                ),
              ],
            ),
          ),
          Flexible(
            child: Row(
              children: [
                _buildStatCard(
                  'Total offre',
                  localOffreStatisticsData[0]?.totalOffre.toString() ?? "", 
                  Colors.blueGrey
                ),
                _buildStatCard(
                  'Offre active', 
                  localOffreStatisticsData[0]?.activeOffre.toString() ?? "", 
                  Colors.green
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _mesOffresBarChart(){
    List _offreSpeculation;
    List nameSpeculation = [];
    List numberSpeculation = [];
    offreSpeculation = localOffreStatisticsData[0].offreSpeculation;
    _offreSpeculation = jsonDecode(offreSpeculation);
    for(var i = 0; i < _offreSpeculation.length; i++){
      if(_offreSpeculation[i]['nbre'].toInt() > 0){
        nameSpeculation.add(_offreSpeculation[i]['name'].toString());
        numberSpeculation.add(_offreSpeculation[i]['nbre'].toDouble());
      }
    }
    if(numberSpeculation == null || numberSpeculation.length == 0){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 10),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0),
            Container(
              width: 300,
              child: Text(
                "Désolé, il n'y a aucune donnée à affiché ici",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 13.0
                ),
              ),
            ),
            SizedBox(height: 40),
          ],
        ),
      );
    }else{
      return Container(
      height: MediaQuery.of(context).size.width * 1.10,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(20.0),
            alignment: Alignment.centerLeft,
            child: Text(
              'Par spéculation',
              style: const TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.85,
            child: BarChart(
              BarChartData(
                alignment: BarChartAlignment.spaceAround,
                maxY: 16.0,
                barTouchData: BarTouchData(enabled: false),
                titlesData: FlTitlesData(
                  show: true,
                  bottomTitles: SideTitles(
                    margin: 10.0,
                    showTitles: true,
                    rotateAngle: 35.0,
                    getTitles: (value){
                      for(var i = 0; i < nameSpeculation.length; i++){
                        if(value.toInt() == i){
                          return nameSpeculation[i]; 
                        }
                      }
                    },
                  ),
                  leftTitles: SideTitles(
                      margin: 10.0,
                      showTitles: true,
                      getTitles: (value) {
                        if (value == 0) {
                          return '0';
                        } else if (value % 3 == 0) {
                          return '${value ~/ 3 * 5}';
                        }
                        return '';
                      }),
                ),
                gridData: FlGridData(
                  show: true,
                  checkToShowHorizontalLine: (value) => value % 3 == 0,
                  getDrawingHorizontalLine: (value) => FlLine(
                    color: Colors.black12,
                    strokeWidth: 1.0,
                    dashArray: [5],
                  ),
                ),
                borderData: FlBorderData(
                  show: false,
                ),
                barGroups: numberSpeculation
                    .asMap()
                    .map((key, value) => MapEntry(
                        key,
                        BarChartGroupData(
                          x: key,
                          barRods: [
                            BarChartRodData(
                              y: value,
                              colors: [
                                Colors.purple,
                              ],
                            ),
                          ],
                        )))
                    .values
                    .toList(),
              ),
            ),
          ),
        ],
      ),
      );
    }
  }

  _mesDemandesView(){
    return CustomScrollView(
      physics: ClampingScrollPhysics(),
      slivers: [
        SliverPadding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          sliver: SliverToBoxAdapter(
            child: _mesDemandesGird(),
          ),
        ),
        SliverPadding(
          padding: const EdgeInsets.only(top: 20.0),
          sliver: SliverToBoxAdapter(
            child: _mesDemandesBarChart(),
          ),
        ),
      ],
    );
  }

  _mesDemandesGird(){
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      child: Column(
        children: [
          Flexible(
            child: Row(
              children: [
                _buildStatCard(
                  "Nombre d'achat", 
                  localDemandeStatisticsData[0]?.nbreAchat.toString() ?? "", 
                  Colors.blue),
                _buildStatCard(
                  "Montant",
                  _formatPrice(localDemandeStatisticsData[0]?.montantAchat),
                  Colors.blueGrey),
              ],
            ),
          ),
          Flexible(
            child: Row(
              children: [
                 _buildStatCard(
                   'En cours de livraison', 
                   _formatPrice(localDemandeStatisticsData[0]?.waitingLivraison),
                   Colors.green),
                 _buildStatCard(
                   'Livrée(s)', 
                   localDemandeStatisticsData[0]?.endLivraison.toString() ?? "", 
                 Colors.red),
              ],
            ),
          ),
        ],
      )
    );
  }

  _mesDemandesBarChart(){
    List _demandeSpeculation;
    List nameSpeculation = [];
    List numberSpeculation = [];
    demandeSpeculation = localDemandeStatisticsData[0].demandeSpeculation;
    
    _demandeSpeculation = jsonDecode(demandeSpeculation);
    
    for(var i = 0; i < _demandeSpeculation.length; i++){
      if(_demandeSpeculation[i]['nbre'].toInt() > 0){
        nameSpeculation.add(_demandeSpeculation[i]['name'].toString());
        numberSpeculation.add(_demandeSpeculation[i]['nbre'].toDouble());
      }
    }

    if(numberSpeculation == null || numberSpeculation.length == 0){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 10),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0),
            Container(
              width: 300,
              child: Text(
                "Désolé, il n'y a aucune donnée à affiché ici",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 13.0
                ),
              ),
            ),
            SizedBox(height: 40),
          ],
        ),
      );
    }else{
      return Container(
      height: MediaQuery.of(context).size.width * 1.10,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
      ),
      child: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(20.0),
            alignment: Alignment.centerLeft,
            child: Text('Par spéculation',
              style: const TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.85,
            child: BarChart(
              BarChartData(alignment: BarChartAlignment.spaceAround, maxY: 16.0,
                barTouchData: BarTouchData(enabled: false),
                titlesData: FlTitlesData(show: true,
                  bottomTitles: SideTitles(
                    margin: 10.0, 
                    showTitles: true, 
                    rotateAngle: 35.0,
                    getTitles: (value){
                      for(var i = 0; i < nameSpeculation.length; i++){
                        if(value.toInt() == i){
                          return nameSpeculation[i]; 
                        }
                      }
                    },
                  ),
                  leftTitles: SideTitles(
                      margin: 10.0,
                      showTitles: true,
                      getTitles: (value) {
                        if (value == 0) {
                          return '0';
                        } else if (value % 3 == 0) {
                          return '${value ~/ 3 * 5}';
                        }
                        return '';
                      }),
                ),
                gridData: FlGridData(
                  show: true,
                  checkToShowHorizontalLine: (value) => value % 3 == 0,
                  getDrawingHorizontalLine: (value) => FlLine(
                    color: Colors.black12,
                    strokeWidth: 1.0,
                    dashArray: [5],
                  ),
                ),
                borderData: FlBorderData(show: false),
                barGroups: numberSpeculation.asMap().map(
                  (key, value) => MapEntry(
                        key,
                        BarChartGroupData(
                          x: key,
                          barRods: [
                            BarChartRodData(
                              y: value, colors: [Colors.purple,],
                            ),
                          ],
                        ))).values.toList(),
              ),
            ),
          ),
        ],
      ),
    );
    }
  }

  Expanded _buildStatCard(String title, count,color){
    return Expanded(
      child: Container(
        margin: const EdgeInsets.all(8.0),
        padding: const EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 12.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            Text(
              count,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 14.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}