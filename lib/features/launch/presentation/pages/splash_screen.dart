import 'dart:async';

import 'package:agrimobile/core/utils/connection_status.dart';
import 'package:agrimobile/features/common/data/models/user_information_model.dart';
import 'package:agrimobile/features/launch/presentation/pages/first_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';


class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 5),
            () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => Consumer2<ConnectionStatus,UserInformationModel>(
              builder: (context, connectionStatus,userConnected, child) {
                /*if (connectionStatus == ConnectionStatus.connected) {
                      // return AllAssurancePage();
                       return HomePage();
                      //return AddAnnoncePage("0");
                    } else {
                      return FirstPage();
                    }*/
                return HomePage("","${userConnected.id}","${userConnected.validationUser}");
              },
            ))));
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {

      print('logo +++++++++'+configModel.logo);
      return Scaffold(
        //backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
        body: Container(
          /*decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/img/splash.jpg"),
              fit: BoxFit.cover,
            ),
          ),*/
          child:   Container(
            //height: MediaQuery.of(context).size.height/2,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 62),
            child:   Center(
              child:CachedNetworkImage(
                imageUrl: configModel.logo,
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                height: 150,
                placeholder: (context, url) => Image.asset('assets/img/loading.gif',width: 150,height: 150,),
                errorWidget: (context, url, error) => Container(),
                // errorWidget: (context, url, error) => new Image.asset('assets/img/longlogo.png',height:150),
                // errorWidget: (context, url, error) => new Icon(Icons.error),
              )
              /* Image(
                image: AssetImage("assets/img/longlogo.png"),
                height: 150,
              ),*/
            ),
          ),

        ),
      );
    },
  );


}
