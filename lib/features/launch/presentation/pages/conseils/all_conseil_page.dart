import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/launch/presentation/pages/conseils/chewielist.dart';
import 'package:dio/dio.dart';


import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:url_launcher/url_launcher.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/domain/entities/local_conseil_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_conseil_repository.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/dto/conseil_dto.dart';
import 'package:video_player/video_player.dart';


class AllConseilPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  List<LocalConseilEntity> allconseils;
  GlobalKey<ScaffoldState> scaffoldKey;
  String userId;
  String userToken;
  int idSearch;
  final VoidCallback onrefresh;

  AllConseilPage({
    this.onrefresh,
    this.allconseils,
    this.idSearch,
    this.scaffoldKey,
    this.userId,
    this.userToken,
  });

  @override
  AllConseilPageState createState() => AllConseilPageState();
}

class AllConseilPageState extends State<AllConseilPage>
    with SingleTickerProviderStateMixin {
  VideoPlayerController _controller;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TabController _tabController;
  List<Tab> _myTabs;
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalConseilRepository _localConseilRepository = LocalConseilRepository();

  List<LocalConseilEntity> _localConseilEntity;
  VideoPlayerController _videoPlayerController;
  final imgUrl = "http://devussdmtn.agristore.ci/b2b/backend/web/uploads/-08qCo15Uvc3.csv";
  bool _isLoading = true;
  bool _hasError = false;
  bool fullscreen = false;
  String _userToken = "";
  String _userId = "";
  int idConseil;
  bool isPlaying = false;
  bool isStarted = false;
  Duration duration;
  int time;
  bool _visible = false;

  //to download file



  String timeLeft = "";
  // double progress = 0.0;

  var div = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  Api api = ApiRepository();

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(length: 4, vsync: this);
    _videoPlayerController;

    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);
    _getUserInformation();
  }

  @override
  void dispose() async {
    SystemChrome.restoreSystemUIOverlays();
    _controller?.dispose();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) =>
      Consumer<ConfigModel>(builder: (context, configModel, child) {
        _myTabs = <Tab>[
          new Tab(text: "${allTranslations.text('menu_audio')}"),
          new Tab(text: "${allTranslations.text('menu_videos')}"),
          new Tab(text: "${allTranslations.text('menu_csv')}"),
          new Tab(text: "${allTranslations.text('menu_sms')}"),


        ];

        return Scaffold(
            key: _scaffoldKey,
            appBar: fullscreen == false? new AppBar(
              title: Align(
                child: Container(
                  child: Text(
                    allTranslations.text('description_conseil'),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ),
              actions: <Widget>[
                Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        _getLocalConseils();
                        print("Raffraissement de la liste en cours...");
                      },
                      child: Icon(
                        Icons.refresh,
                        size: 26.0,
                      ),
                    )),
                /*  Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        showSearch(
                          context: context,
                          delegate: Search(_localConseilEntity, _userToken,
                              widget._scaffoldKey, _userId, onrefresh),
                        );
                      },
                      child: Icon(
                        Icons.search,
                        size: 26.0,
                      ),
                    ))*/
                Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        UrlLauncher.launch('tel:+${configModel.callCenter}');
                      },
                      child: Icon(
                        Icons.phone,
                        color: Colors.white,
                      ),
                    )),
              ],
              backgroundColor:
              FunctionUtils.colorFromHex(configModel.mainColor),
              elevation: 0.0,
            ):null,
            backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
            body: Builder(builder: (BuildContext myContext) {
              return Column(
                children: [
                  Container(
                    color: FunctionUtils.colorFromHex("dfdfdf"),
                    // Tab Bar color change
                    child: TabBar(
                      // TabBar
                      controller: _tabController,
                      unselectedLabelColor:
                      FunctionUtils.colorFromHex("737373"),
                      labelColor: FunctionUtils.colorFromHex("020000"),
                      indicatorWeight: 3,
                      indicatorColor:
                      FunctionUtils.colorFromHex(configModel.mainColor),
                      tabs: _myTabs,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: TabBarView(
                      // Tab Bar View
                      physics: BouncingScrollPhysics(),
                      controller: _tabController,
                      children: <Widget>[
                        _audioScreen(configModel),
                        _videosScreen(configModel),
                        _fichierScreen(configModel),
                        _smsScreen(configModel),
                      ],
                    ),
                  ),
                ],
              );
              //return _videosScreen(configModel);
            }));
      });

  onrefresh() {
    //_localConseilRepository.listConseils(int.parse(_userId)).then((response) {
    _localConseilRepository.listConseils().then((response) {
      _localConseilEntity = response;
      setState(() {});
    });
  }

  _audioScreen(ConfigModel configModel) {
    if (_isLoading == true) {
      return Center(
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif', height: 150),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              width: 400,
              child: Text(
                allTranslations.text('list_conseil_success'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    } else if (_hasError) {
      return Center(
        child: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg', height: 250),
            ),
            SizedBox(
              height: 0,
            ),
            Container(
              width: 300,
              child: Text(
                allTranslations.text('list_conseil_success'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
      //} else if (conseilRecuperationModel != null) {
    } else if (_localConseilEntity != null) {

      // TODO B.1 - Revision de ce bloc pour afficher la liste de videos. Regarde bien
      var audiofile = _localConseilEntity.where((element) => element.typeConseil==('audio')).toList();
      if (audiofile.isEmpty) {

        return Center(
          child: Column(
            children: [
              SizedBox(height: 100),
              Material(
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                color: Colors.transparent,
                child: Image.asset('assets/img/splash.jpg', height: 250),
              ),
              SizedBox(
                height: 0,
              ),
              Container(
                width: 300,
                child: Text(
                  allTranslations.text('list_conseil_audio_success'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                  ),
                ),
              )
            ],
          ),
        );
      }

      var audioConseils = _localConseilEntity.where((element) {
        print("lien ${element.typeConseil}");
        return element.typeConseil== ('audio');
      }).toList();

      return ListView.builder(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          itemCount: audioConseils.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          // ignore: missing_return
          itemBuilder: (BuildContext context, int position) {
            LocalConseilEntity conseilModel = audioConseils[position];

            if(conseilModel.lien.contains('www.youtube.com')){
              return Container(
                margin: new EdgeInsets.only(bottom: 20.0),
                child: new Card(
                    elevation: 5,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              conseilModel.speculation,
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Titre :",
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                            Text(
                              conseilModel.titre,
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                          ],
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Publié le :",style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12)),
                            SizedBox(width: 3,),
                            Text(conseilModel.dateConseil,style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),)
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () async{
                                if(conseilModel.lien.isEmpty){
                                  Fluttertoast.showToast(
                                      msg: "Impossible de lancer le fichier car il est inexistant",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.grey,
                                      textColor: Colors.white,
                                      fontSize: 16.0
                                  );
                                }else if (await canLaunch(conseilModel.lien)) {
                                  await launch(conseilModel.lien, forceSafariVC: false);
                                } else {
                                  throw 'Could not launch ${conseilModel.lien}';
                                }
                              },
                              child: Container(
                                width: 300,
                                height: 150,
                                margin: const EdgeInsets.all(1.0),
                                padding: const EdgeInsets.all(3.0),
                                decoration: BoxDecoration(image: DecorationImage(
                                    image: new ExactAssetImage('assets/img/youtube.jpg'),
                                    fit: BoxFit.cover
                                )),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [

                                    Text("Cliquez ici pour consulter le lien",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),

                                  ],
                                ),

                              ),
                            )
                          ],
                        ),
                        /*ChewieListItem(
                          key: UniqueKey(),
                          videoPlayerController:
                          VideoPlayerController.network(conseilModel.lien),
                          looping: false,
                        ),*/
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [

                            GestureDetector(
                              onTap: () async{
                                if (await canLaunch(conseilModel.fichier)) {
                                  await launch(conseilModel.fichier, forceSafariVC: false);
                                } else {
                                  throw 'Could not launch ${conseilModel.fichier}';
                                }
                              },
                              child: Container(
                                width: 300,
                                height: 45,
                                margin: const EdgeInsets.all(1.0),
                                padding: const EdgeInsets.all(3.0),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey[500])),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text("Cliquez ici pour consulter le lien",
                                        style: TextStyle(
                                            color: Color.fromRGBO(49, 39, 79, 1),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),
                                    SizedBox( width: 30,),
                                    Icon(Icons.arrow_forward_ios_outlined,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                  ],
                                ),

                              ),
                            )
                          ],
                        ),

                        GestureDetector(
                            onTap: () {

                              showAlertDialog(context, conseilModel, configModel);
                            },
                            child: Container(
                              width: 100,
                              height: 45,
                              margin: const EdgeInsets.all(15.0),
                              padding: const EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromRGBO(49, 39, 79, 1))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Retirer",
                                      style: TextStyle(
                                          color: Color.fromRGBO(49, 39, 79, 1),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                  Icon(Icons.delete_outline,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                ],
                              ),


                            )

                        ),
                      ],
                    )),
              );
            }
            else  return Container(
              margin: new EdgeInsets.only(bottom: 20.0),
              child: new Card(
                  elevation: 5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            conseilModel.speculation,
                            style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Titre :",
                            style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                          Text(
                            conseilModel.titre,
                            style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Publié le :",style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 12)),
                          SizedBox(width: 3,),
                          Text(conseilModel.dateConseil,style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 12),)
                        ],
                      ),
                      ChewieListItem(
                        key: UniqueKey(),
                        videoPlayerController:
                        VideoPlayerController.network(conseilModel.lien),
                        looping: false,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                          GestureDetector(
                            onTap: () async{
                              if (await canLaunch(conseilModel.fichier)) {
                                await launch(conseilModel.fichier, forceSafariVC: false);
                              } else {
                                throw 'Could not launch ${conseilModel.fichier}';
                              }
                            },
                            child: Container(
                              width: 300,
                              height: 45,
                              margin: const EdgeInsets.all(1.0),
                              padding: const EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey[500])),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text("Cliquez ici pour consulter le lien",
                                      style: TextStyle(
                                          color: Color.fromRGBO(49, 39, 79, 1),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                  SizedBox( width: 30,),
                                  Icon(Icons.arrow_forward_ios_outlined,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                ],
                              ),

                            ),
                          )
                        ],
                      ),

                      GestureDetector(
                          onTap: () {

                            showAlertDialog(context, conseilModel, configModel);
                          },
                          child: Container(
                            width: 100,
                            height: 45,
                            margin: const EdgeInsets.all(15.0),
                            padding: const EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                                border: Border.all(color: Color.fromRGBO(49, 39, 79, 1))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Retirer",
                                    style: TextStyle(
                                        color: Color.fromRGBO(49, 39, 79, 1),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16)),
                                Icon(Icons.delete_outline,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                              ],
                            ),


                          )

                      ),
                    ],
                  )),
            );
          });
    }
  }

  _videosScreen(ConfigModel configModel) {
    if (_isLoading == true) {
      return Center(
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif', height: 150),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              width: 400,
              child: Text(
                allTranslations.text('list_conseil_success'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    } else if (_hasError) {
      return Center(
        child: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg', height: 250),
            ),
            SizedBox(
              height: 0,
            ),
            Container(
              width: 300,
              child: Text(
                allTranslations.text('list_conseil_success'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    } else if (_localConseilEntity != null) {

      var videofile = _localConseilEntity.where((element) => element.typeConseil==('video')).toList();// TODO B.2 - Revision de ce bloc pour afficher la liste de videos. Regarde bien
      if (videofile.isEmpty) {
        return Center(
          child: Column(
            children: [
              SizedBox(
                height: 100,
              ),
              Material(
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                color: Colors.transparent,
                child: Image.asset('assets/img/splash.jpg', height: 250),
              ),
              SizedBox(
                height: 0,
              ),
              Container(
                width: 300,
                child: Text(
                  allTranslations.text('list_conseil_success'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                  ),
                ),
              )
            ],
          ),
        );
      }

      var localVideos = _localConseilEntity.where((element) => element.typeConseil==('video')).toList();

      return ListView.builder(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          itemCount: localVideos.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {
            LocalConseilEntity conseilModel = localVideos[position];

            if(conseilModel.lien.contains('www.youtube.com')){
              return Container(
                margin: new EdgeInsets.only(bottom: 20.0),
                child: new Card(
                    elevation: 5,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              conseilModel.speculation,
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Titre :",
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                            Text(
                              conseilModel.titre,
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                          ],
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Publié le :",style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12)),
                            SizedBox(width: 3,),
                            Text(conseilModel.dateConseil,style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),)
                          ],
                        ),SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () async{
                                if(conseilModel.lien.isEmpty){
                                  Fluttertoast.showToast(
                                      msg: "Impossible de lancer le fichier car il est inexistant",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.grey,
                                      textColor: Colors.white,
                                      fontSize: 16.0
                                  );
                                }else if (await canLaunch(conseilModel.lien)) {
                                  await launch(conseilModel.lien, forceSafariVC: false);
                                } else {
                                  throw 'Could not launch ${conseilModel.lien}';
                                }
                              },
                              child: Container(
                                width: 300,
                                height: 150,
                                margin: const EdgeInsets.all(1.0),
                                padding: const EdgeInsets.all(3.0),
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: new ExactAssetImage('assets/img/youtube.jpg'),
                                        fit: BoxFit.cover
                                    )
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [

                                    Text("Cliquez ici pour consulter le lien",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),

                                  ],
                                ),

                              ),
                            )
                          ],
                        ),

                        /*ChewieListItem(
                          key: UniqueKey(),
                          videoPlayerController:
                          VideoPlayerController.network(conseilModel.lien),
                          looping: true,
                        ),*/
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () async{
                                if(conseilModel.fichier.isEmpty){
                                  Fluttertoast.showToast(
                                      msg: "Impossible de lancer le fichier car il est inexistant",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.grey,
                                      textColor: Colors.white,
                                      fontSize: 16.0
                                  );
                                }
                                else if (await canLaunch(conseilModel.fichier)) {
                                  await launch(conseilModel.fichier, forceSafariVC: false);
                                } else {
                                  throw 'Could not launch ${conseilModel.fichier}';
                                }
                              },
                              child: Container(
                                width: 300,
                                height: 45,
                                margin: const EdgeInsets.all(1.0),
                                padding: const EdgeInsets.all(3.0),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey[500])),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text("Cliquez ici pour consulter le lien",
                                        style: TextStyle(
                                            color: Color.fromRGBO(49, 39, 79, 1),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),
                                    SizedBox( width: 30,),
                                    Icon(Icons.arrow_forward_ios_outlined,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                  ],
                                ),

                              ),
                            )
                          ],
                        ),
                        GestureDetector(
                            onTap: () {
                              showAlertDialog(context, conseilModel, configModel);
                            },
                            child:
                            Container(
                              width: 100,
                              height: 45,
                              margin: const EdgeInsets.all(15.0),
                              padding: const EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromRGBO(49, 39, 79, 1))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Retirer",
                                      style: TextStyle(
                                          color: Color.fromRGBO(49, 39, 79, 1),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                  Icon(Icons.delete_outline,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                ],
                              ),


                            )),
                      ],
                    )),
              );
            }
            else return Container(
              margin: new EdgeInsets.only(bottom: 20.0),
              child: new Card(
                  elevation: 5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            conseilModel.speculation,
                            style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Titre :",
                            style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                          Text(
                            conseilModel.titre,
                            style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Publié le :",style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 12)),
                          SizedBox(width: 3,),
                          Text(conseilModel.dateConseil,style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 12),)
                        ],
                      ),

                      ChewieListItem(
                        key: UniqueKey(),
                        videoPlayerController:
                        VideoPlayerController.network(conseilModel.lien),
                        looping: true,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () async{
                              if(conseilModel.fichier.isEmpty){
                                Fluttertoast.showToast(
                                    msg: "Impossible de lancer le fichier car il est inexistant",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor: Colors.grey,
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }
                              else if (await canLaunch(conseilModel.fichier)) {
                                await launch(conseilModel.fichier, forceSafariVC: false);
                              } else {
                                throw 'Could not launch ${conseilModel.fichier}';
                              }
                            },
                            child: Container(
                              width: 300,
                              height: 45,
                              margin: const EdgeInsets.all(1.0),
                              padding: const EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey[500])),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text("Cliquez ici pour consulter le lien",
                                      style: TextStyle(
                                          color: Color.fromRGBO(49, 39, 79, 1),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                  SizedBox( width: 30,),
                                  Icon(Icons.arrow_forward_ios_outlined,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                ],
                              ),

                            ),
                          )
                        ],
                      ),
                      GestureDetector(
                          onTap: () {
                            showAlertDialog(context, conseilModel, configModel);
                          },
                          child:
                          Container(
                            width: 100,
                            height: 45,
                            margin: const EdgeInsets.all(15.0),
                            padding: const EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                                border: Border.all(color: Color.fromRGBO(49, 39, 79, 1))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Retirer",
                                    style: TextStyle(
                                        color: Color.fromRGBO(49, 39, 79, 1),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16)),
                                Icon(Icons.delete_outline,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                              ],
                            ),


                          )),
                    ],
                  )),
            );
          });
    }
  }

  _fichierScreen(ConfigModel configModel){
    if (_isLoading == true) {
      return Center(
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif', height: 150),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              width: 400,
              child: Text(
                allTranslations.text('Recherche_list_conseil'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    } else if (_hasError) {
      return Center(
        child: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg', height: 250),
            ),
            SizedBox(
              height: 0,
            ),
            Container(
              width: 300,
              child: Text(
                allTranslations.text('list_conseil_doc_succes'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    } else if (_localConseilEntity != null) {
      var documentfile = _localConseilEntity.where((element) => element.typeConseil==('document')).toList();
      // TODO B.2 - Revision de ce bloc pour afficher la liste de videos. Regarde bien
      if (documentfile.isEmpty) {
        return Center(
          child: Column(
            children: [
              SizedBox(
                height: 100,
              ),
              Material(
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                color: Colors.transparent,
                child: Image.asset('assets/img/splash.jpg', height: 250),
              ),
              SizedBox(
                height: 0,
              ),
              Container(
                width: 300,
                child: Text(
                  allTranslations.text('list_conseil_doc_succes'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                  ),
                ),
              )
            ],
          ),
        );
      }

      var localdoc = _localConseilEntity.where((element) => element.typeConseil==('document')).toList();

      return ListView.builder(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          itemCount: localdoc.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {
            LocalConseilEntity conseilModel = localdoc[position];

            if(conseilModel.lien.contains('www.youtube.com')){
              return Container(
                margin: new EdgeInsets.only(bottom: 20.0),
                child: new Card(
                    elevation: 5,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              conseilModel.speculation,
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Titre :",
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                            Text(
                              conseilModel.titre,
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                          ],
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Publié le :",style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12)),
                            SizedBox(width: 3,),
                            Text(conseilModel.dateConseil,style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),)
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () async{
                                if(conseilModel.lien.isEmpty){
                                  Fluttertoast.showToast(
                                      msg: "Impossible de lancer le fichier car il est inexistant",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.grey,
                                      textColor: Colors.white,
                                      fontSize: 16.0
                                  );
                                }else if (await canLaunch(conseilModel.lien)) {
                                  await launch(conseilModel.lien, forceSafariVC: false);
                                } else {
                                  throw 'Could not launch ${conseilModel.lien}';
                                }
                              },
                              child: Container(
                                width: 300,
                                height: 150,
                                margin: const EdgeInsets.all(1.0),
                                padding: const EdgeInsets.all(3.0),
                                decoration: BoxDecoration(image: DecorationImage(
                                    image: new ExactAssetImage('assets/img/youtube.jpg'),
                                    fit: BoxFit.cover
                                )),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [

                                    Text("Cliquez ici pour consulter le lien",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),

                                  ],
                                ),

                              ),
                            )
                          ],
                        ),
                        /*  ChewieListItem(
                         key: UniqueKey(),
                         videoPlayerController:
                         VideoPlayerController.network(conseilModel.lien),
                         looping: true,
                       ),*/
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () async{
                                if(conseilModel.fichier.isEmpty){
                                  Fluttertoast.showToast(
                                      msg: "Impossible de lancer le fichier car il est inexistant",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.grey,
                                      textColor: Colors.white,
                                      fontSize: 16.0
                                  );
                                }else if (await canLaunch(conseilModel.fichier)) {
                                  await launch(conseilModel.fichier, forceSafariVC: false);
                                } else {
                                  throw 'Could not launch ${conseilModel.fichier}';
                                }
                              },
                              child: Container(
                                width: 300,
                                height: 45,
                                margin: const EdgeInsets.all(1.0),
                                padding: const EdgeInsets.all(3.0),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey[500])),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text("Cliquez ici pour consulter le lien",
                                        style: TextStyle(
                                            color: Color.fromRGBO(49, 39, 79, 1),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),
                                    SizedBox( width: 30,),
                                    Icon(Icons.arrow_forward_ios_outlined,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                  ],
                                ),

                              ),
                            )
                          ],
                        ),

                        GestureDetector(
                            onTap: () {
                              showAlertDialog(context, conseilModel, configModel);
                            },
                            child: Container(
                              width: 100,
                              height: 45,
                              margin: const EdgeInsets.all(15.0),
                              padding: const EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromRGBO(49, 39, 79, 1))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Retirer",
                                      style: TextStyle(
                                          color: Color.fromRGBO(49, 39, 79, 1),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                  Icon(Icons.delete_outline,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                ],
                              ),


                            )),
                      ],
                    )),
              );
            }
            else return Container(
              margin: new EdgeInsets.only(bottom: 20.0),
              child: new Card(
                  elevation: 5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            conseilModel.speculation,
                            style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Titre :",
                            style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                          Text(
                            conseilModel.titre,
                            style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                          ),
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Publié le :",style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 12)),
                          SizedBox(width: 3,),
                          Text(conseilModel.dateConseil,style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 12),)
                        ],
                      ),
                      ChewieListItem(
                        key: UniqueKey(),
                        videoPlayerController:
                        VideoPlayerController.network(conseilModel.lien),
                        looping: true,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () async{
                              if(conseilModel.fichier.isEmpty){
                                Fluttertoast.showToast(
                                    msg: "Impossible de lancer le fichier car il est inexistant",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1,
                                    backgroundColor: Colors.grey,
                                    textColor: Colors.white,
                                    fontSize: 16.0
                                );
                              }else if (await canLaunch(conseilModel.fichier)) {
                                await launch(conseilModel.fichier, forceSafariVC: false);
                              } else {
                                throw 'Could not launch ${conseilModel.fichier}';
                              }
                            },
                            child: Container(
                              width: 300,
                              height: 45,
                              margin: const EdgeInsets.all(1.0),
                              padding: const EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey[500])),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text("Cliquez ici pour consulter le lien",
                                      style: TextStyle(
                                          color: Color.fromRGBO(49, 39, 79, 1),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                  SizedBox( width: 30,),
                                  Icon(Icons.arrow_forward_ios_outlined,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                ],
                              ),

                            ),
                          )
                        ],
                      ),

                      GestureDetector(
                          onTap: () {
                            showAlertDialog(context, conseilModel, configModel);
                          },
                          child: Container(
                            width: 100,
                            height: 45,
                            margin: const EdgeInsets.all(15.0),
                            padding: const EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                                border: Border.all(color: Color.fromRGBO(49, 39, 79, 1))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Retirer",
                                    style: TextStyle(
                                        color: Color.fromRGBO(49, 39, 79, 1),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16)),
                                Icon(Icons.delete_outline,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                              ],
                            ),


                          )),
                    ],
                  )),
            );
          });
    }
  }

  _smsScreen(ConfigModel configModel){
    if (_isLoading == true) {
      return Center(
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif', height: 150),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              width: 400,
              child: Text(
                allTranslations.text('Recherche_list_conseil'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    } else if (_hasError) {
      return Center(
        child: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg', height: 250),
            ),
            SizedBox(
              height: 0,
            ),
            Container(
              width: 300,
              child: Text(
                allTranslations.text('list_conseil_sms_success'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );

    } else if (_localConseilEntity != null) {
      var smsfile = _localConseilEntity.where((element) => element.typeConseil==('Sms')).toList();
      // TODO B.1 - Revision de ce bloc pour afficher la liste de videos. Regarde bien

      if (smsfile.isEmpty) {
        return Center(
          child: Column(
            children: [
              SizedBox(height: 100),
              Material(
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                color: Colors.transparent,
                child: Image.asset('assets/img/splash.jpg', height: 250),
              ),
              SizedBox(
                height: 0,
              ),
              Container(
                width: 300,
                child: Text(
                  allTranslations.text('list_conseil_sms_success'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                  ),
                ),
              )
            ],
          ),
        );
      }



      var smsConseils = _localConseilEntity.where((element){
        print("typeConseil ${element.typeConseil}");
        return element.typeConseil== ('Sms');}).toList();

      return ListView.builder(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          itemCount: smsConseils.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {
            LocalConseilEntity conseilModel = smsConseils[position];

            if(conseilModel.lien.contains('www.youtube.com')){
              return Container(
                margin: new EdgeInsets.only(bottom: 20.0),
                child: new Card(
                    elevation: 3,
                    child: ListTile(
                        leading: const Icon(Icons.account_circle,size: 50.0,),
                        title: Row(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  conseilModel.speculation,
                                  style: TextStyle(
                                      color: Color.fromRGBO(49, 39, 79, 1),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12),
                                ),
                              ],
                            ),

                            SizedBox(
                              width: 16.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Publié le :",style: TextStyle(
                                    color: Color.fromRGBO(49, 39, 79, 1),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12)),
                                SizedBox(width: 3,),
                                Text(conseilModel.dateConseil,style: TextStyle(
                                    color: Color.fromRGBO(49, 39, 79, 1),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12),)
                              ],
                            )],
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                            Text(conseilModel.titre,style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 16)),

                            Text(conseilModel.contenu,style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,fontSize: 16)),
                          ],
                        ),
                        onTap: () => {showAlertDialog(context, conseilModel, configModel)}))
                        );

            } else return Container(
              margin: new EdgeInsets.only(bottom: 20.0),
              child: new Card(
                  elevation: 3,
                  child: ListTile(
                      leading: const Icon(Icons.account_circle,size: 50.0,),
                      title: Row(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                conseilModel.speculation,
                                style: TextStyle(
                                    color: Color.fromRGBO(49, 39, 79, 1),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12),
                              ),
                            ],
                          ),

                          SizedBox(
                            width: 16.0,
                          ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Publié le :",style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 12)),
                          SizedBox(width: 3,),
                          Text(conseilModel.dateConseil,style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 12),)
                        ],
                      )],
                      ),
                      subtitle: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          Text(conseilModel.titre,style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 16)),

                          Text(conseilModel.contenu,style: TextStyle(
                              color: Color.fromRGBO(49, 39, 79, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 16)),
                        ],
                      ),

                      onTap: () => {showAlertDialog(context, conseilModel, configModel)})),
            );
          });
    }

  }



  _getUserInformation() {
    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userToken = "${value.token}";
        _userId = "${value.id}";
      });
      //_getListConseils(value.id);
      _getLocalConseils();
    });
  }

  _getLocalConseils() async {
    print("get local conseils");

    setState(() => _hasError = false);

    _localConseilRepository
        .listConseils()
        .then((List<LocalConseilEntity> items) {
      setState(() {
        _localConseilEntity = items;
      });

      if (_localConseilEntity.isEmpty) {
        print("get remote");
        _getRemoteConseils();

        return;
      }

      print("dont save again");

      setState(() => _isLoading = false);
    });
  }

  void _getRemoteConseils() {
    print("get remote conseils");

    ConseilDto _conseilDto = ConseilDto.create(
        accessToken: "ddfd544deRHOFle1TrJGW4MHrTo8Cf8et",
        userToken: _userToken);

    api.getConseil(_conseilDto).then((value) {
      if (value.isRight()) {
        value.all((conseilResponse) {
          if (conseilResponse != null &&
              conseilResponse.status.compareTo("000") == 0) {
            if (conseilResponse.allConseil.isEmpty) {
              setState(() => _isLoading = false);
              return false;
            }

            FunctionUtils.saveConseils(
                conseilResponse.allConseil, int.parse(_userId))
                .then((saved) => _getLocalConseils());

            return true;
          } else {
            // Navigator.of(context).pop(null);
            // _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      } else {
        //  Navigator.of(context).pop(null);
        // _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
  }

  showAlertDialog(BuildContext context, LocalConseilEntity infosC,
      ConfigModel configModel) {
    // set up the button
    Widget okButton = FlatButton(
      child:Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text(allTranslations.text('delete_conseil_text'), style: new TextStyle( fontSize: 15.0),),
      actions: [
        okButton,
        FlatButton(
          child: Text(allTranslations.text('confirm'),style: new TextStyle(
              fontWeight: FontWeight.bold, fontSize: 14.0),),
          onPressed: () {
            _deleteAllConseils(infosC, configModel);
            //  _getLocalConseils();
          },
        ),
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _deleteAllConseils(
      LocalConseilEntity infoConseils, ConfigModel configModel) async {
    print(_userToken);
    setState(() => _hasError = false);
    ConseilDto conseilDto = ConseilDto();
    conseilDto.accessToken = "ddfd544deRHOFle1TrJGW4MHrTo8Cf8et";
    conseilDto.userToken = _userToken;
    conseilDto.conseil_ids = "${infoConseils.apiId}";

    print(infoConseils.toDatabase());

    Api api = ApiRepository();
    api.deleteConseil(conseilDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if (a.status.compareTo("ok") == 0) {
            _localConseilRepository
                .delete(infoConseils.idConseil, "idAppConseil")
                .then((value) {
              widget.onrefresh();
            });

            Navigator.of(context).pop(null);
            // _displaySnackBar(
            //context, allTranslations.text('delete_conseil_success'));
            return true;
          } else {
            Navigator.of(context).pop(null);
            //  _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      } else {
        Navigator.of(context).pop(null);
        // _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }
}

/*class Search extends SearchDelegate {
  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container(
      child: Center(
        child: Text(""),
      ),
    );
  }

  // LocalConseilRepository _localConseilRepository = LocalConseilRepository();

  List<LocalConseilEntity> listExample;
  List<LocalConseilEntity> suggestionList;
  GlobalKey<ScaffoldState> scaffoldKey;
  String userToken;
  String userId;
  final VoidCallback onrefresh;


  Search(this.listExample, this.userToken, this.scaffoldKey, this.userId,
      this.onrefresh)
      : super(searchFieldLabel: allTranslations.text('search'));

  @override
  Widget buildSuggestions(BuildContext context) {
    ConseilRecuperationModel conseilRecuperationModel;
    suggestionList = [];
    String search = query.toLowerCase();

    search.isEmpty
        ? suggestionList = listExample //In the true case
        : suggestionList.addAll(listExample.where((element) =>
        element.titre.toLowerCase().contains(search.toLowerCase())));
    print(listExample);
    if (suggestionList == null || suggestionList.length == 0) {
      return Center(
        child: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg', height: 250),
            ),
            SizedBox(
              height: 0,
            ),
            Container(
              width: 300,
              child: Text(
                allTranslations.text('list_conseil_success'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    } else {
      return Container(
        decoration: BoxDecoration(color: FunctionUtils.colorFromHex("DDDDDD")),
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
        child: ListView.builder(
            padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
            itemCount: suggestionList.length,
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            // ignore: missing_return
            itemBuilder: (BuildContext context, int position) {
              LocalConseilEntity conseilModel = suggestionList[position];
              return Container(
                margin: new EdgeInsets.only(bottom: 20.0),
                child: new Card(
                    elevation: 5,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              conseilModel.speculation,
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Titre :",
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                            Text(
                              conseilModel.titre,
                              style: TextStyle(
                                  color: Color.fromRGBO(49, 39, 79, 1),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12),
                            ),
                          ],
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Publié le :",style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12)),
                            SizedBox(width: 3,),
                            Text(conseilModel.dateConseil,style: TextStyle(
                                color: Color.fromRGBO(49, 39, 79, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),)
                          ],
                        ),
                        ChewieListItem(
                          key: UniqueKey(),
                          videoPlayerController:
                          VideoPlayerController.network(conseilModel.lien),
                          looping: true,
                        ),
                        GestureDetector(
                          onTap: () {
                            /* showAlertDialog(
                                    context, conseilModel, configModel);*/
                          },
                          child: FadeAnimation(
                            4,
                            Container(
                              width: 100,
                              height: 45,
                              margin: const EdgeInsets.all(15.0),
                              padding: const EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Color.fromRGBO(49, 39, 79, 1))),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Retirer",
                                      style: TextStyle(
                                          color: Color.fromRGBO(49, 39, 79, 1),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16)),
                                  Icon(Icons.delete_outline,size: 20.0,color: Color.fromRGBO(49, 39, 79, 1))
                                ],
                              ),


                            ),
                          ),
                        ),
                      ],
                    )),
              );
            }),
      );
    }
  }
}*/
