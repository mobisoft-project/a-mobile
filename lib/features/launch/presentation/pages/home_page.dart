
import 'dart:convert';

import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/app_response_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/user_information_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_menu_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_menu_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/add_annonce_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/liste_annonce_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/prix_marche_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/abonnement_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/assurance/all_assurance_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/commande/liste_commande_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/conseils/all_conseil_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/meteo/meteo_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/pret_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/home_pret_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/profilage/profilage_page.dart';
import 'package:agrimobile/features/launch/presentation/widgets/menu_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/features/launch/presentation/widgets/counter.dart';
import 'package:agrimobile/features/launch/presentation/widgets/my_header.dart';
import 'package:agrimobile/features/launch/presentation/pages/about_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/account_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/detail_annonce_page.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';

import 'package:agrimobile/features/launch/presentation/pages/souscription/souscription_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/messagerie/messagerie_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/statistiques/statistique_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/geolocalisation/geolocalisation_page.dart';

import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/features/launch/presentation/pages/commande/checkout_final_page.dart';
class HomePage extends StatefulWidget {


  String defaultCode ;
  String userLogin ;
  String userValidate ;
  HomePage(this.defaultCode,this.userLogin,this.userValidate);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState(this.defaultCode,this.userLogin,this.userValidate);
  }
}



class PageState extends State<HomePage> {

  String codeMenu;
  String userLogin;
  String userValidate;
  PageState(this.codeMenu,this.userLogin,this.userValidate);

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  LocalMenuRepository _localMenuRepository = LocalMenuRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  List<LocalAnnonceEntity> _allLocalAnnonceEntity;
  List<LocalMenuModel> defaultMenu=[];
  List<LocalMenuModel> tamponMenu=[];
  List<LocalMenuModel> _allMenu=[];
  List<Widget> _showAnnonce;
  final  popupButtonKey = GlobalKey<State>();
  final dataKey = new GlobalKey();

  final controller = ScrollController();
  double offset = 0;
  String _nbreActeurs="0";
  String _nbreOffres="0";
  String _nbreExperts="0";
  String _userKey="";
  bool _haveAnnonce=false;

  String menu1=allTranslations.text('menu_marches');
  String menu2=allTranslations.text('menu_mesannonces');
  String menu3=allTranslations.text('menu_prixmarches');
  String menu4=allTranslations.text('menu_compte');
  List<String>choices;
  List<String> areaListData = <String>[];
  double _menuHeight=550;




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller.addListener(onScroll);

    choices=<String>[menu1,menu2,menu3,menu4];


    defaultMenu = LocalMenuModel.allMenu(userLogin, 0);
    _getConfigInfo();
    _getUserInformation();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _appSharedPreferences.getRouteAnnonce().then((value) {




          if(codeMenu.compareTo("")!=0){
          //recuperer le menu en question
           List<LocalMenuModel> myDefaultMenu = LocalMenuModel.allMenu(userLogin, value);
          List<LocalMenuModel> suggestionList=[];

          suggestionList=[];
          suggestionList.addAll(myDefaultMenu.where((element) => element.code.compareTo(codeMenu)==0));

          if(suggestionList.length==1){

            //verifier getRouteAccess
            _appSharedPreferences.getRouteAccess().then((value){

              if( (value.compareTo("2")==0 && userValidate.compareTo("1")==0) || (value.compareTo("2")!=0)){
                //message de waiting validation
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => suggestionList[0].widget),
                );
              }else {
                _displaySnackBar(context, allTranslations.text('waiting_validation'));
              }
            });

          }
        }

      });


    });

  }

  @override
  void dispose() {
    // TODO: implement dispose
    controller.dispose();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (controller.hasClients) ? controller.offset : 0;
    });
  }

  @override
  Widget build(BuildContext context) => Consumer2<ConfigModel,UserInformationModel>(
    builder: (context, configModel,userConnected, child){

      userConnected.addListener(() {
        setState(() {
          
        });
      });


      _userKey="${userConnected.id}";
      _nbreActeurs=userConnected.totalActeurs;
      _nbreOffres=userConnected.totalOffre;
      _nbreExperts=userConnected.totalExperts;

      return Scaffold(
        key: _scaffoldKey,
        /*floatingActionButton: FloatingActionButton(
          onPressed: () {
            FunctionUtils.pressedMenu(context,"${userConnected.id}",userConnected.validationUser,"M005",AddAnnoncePage("-1",1),_scaffoldKey,0);
          },
          child: Icon(Icons.add,color: Colors.white),
          backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
        ),*/

        body: SingleChildScrollView(
          controller: controller,
          child: Column(
            children: <Widget>[
              MyHeader(key:popupButtonKey,
                userKey: "${userConnected.id}",
                userValidation: "${userConnected.validationUser}",
                image: "assets/img/firsthome.png",
                textTop: configModel.slogan1,
                textBottom: configModel.slogan2,
                offset: offset,
                pKey: popupButtonKey,
                scaffoldKey: _scaffoldKey,
              ),

              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 4),
                            blurRadius: 30,
                            color: kShadowColor,
                          ),
                        ],
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Counter(
                              color: kInfectedColor,
                              number: int.parse(_nbreActeurs),
                              title: allTranslations.text('acteurs')
                          ),
                          Counter(
                              color: kDeathColor,
                              number: int.parse(_nbreOffres),
                              title: allTranslations.text('offres')
                          ),
                          Counter(
                              color: kRecovercolor,
                              number: int.parse(_nbreExperts),
                              title: allTranslations.text('experts')
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          GestureDetector(
                              onTap: () {
                                int venteBiztypeId = 1;
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => MyAnnoncePage(DataConstantesUtils.ANNONCE_MARCHE,0,0,venteBiztypeId)),
                                );
                              },
                            child: Container(
                              padding: EdgeInsets.only(top:0),
                              child: Text(
                                allTranslations.text('last_offre'),
                                style: kTitleTextstyle,
                              ),
                            ),
                          ),

                          SizedBox(height: 10),
                          _lastAnnonce(),
                        ],
                      ),
                      visible: _haveAnnonce,
                    ),
                    SizedBox(key: dataKey,height: 30),
                    GestureDetector(

                      onTap: () {
                        Scrollable.ensureVisible(dataKey.currentContext);
                      },
                      child: Container(
                        padding: EdgeInsets.only(top:0),
                        child: Text(allTranslations.text('acces_rapide'), style: kTitleTextstyle),
                      ),
                    ),
                    Container(
                        height: _menuHeight,
                        child: _MenuScreen(configModel,userConnected)
                    ),

                  ],
                ),
              ),


            ],
          ),
        ),
      );
    },
  );

  _lastAnnonce(){

    if(_allLocalAnnonceEntity!=null && _allLocalAnnonceEntity.length>0) {

      return Container(
        height: 145,
        child: ListView.builder(
          itemCount: _allLocalAnnonceEntity.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, position) {

            return  LastAnnonce(
              image: _allLocalAnnonceEntity[position].picture,
              title: "${_allLocalAnnonceEntity[position].nomCategorieFilleProduit}",
              isActive: position==0?true:false,
              idAnnonce: _allLocalAnnonceEntity[position].apiId,
            );
          },
        ),
      );
    }else{
      return Container();
    }
  }

  _getUserInformation(){

    if(defaultMenu.length>0){


      for(int i=0; i<defaultMenu.length; i++){


        if(defaultMenu[i].homeMenu.compareTo(1)==0){
          _localMenuRepository.existMenu(defaultMenu[i].code).then((response){

            if(response!=null && response.length>0 && response[0].visibleMenu.compareTo(1)==0){

              defaultMenu[i].typeMenu=response[0].typeMenu;
              defaultMenu[i].iconeMenu=response[0].iconeMenu;
              tamponMenu.add(defaultMenu[i]);

              setState(() {
                _menuHeight=138.0*((tamponMenu.length/3).ceil());
                _allMenu=tamponMenu ;
              });

            }

          });
        }
      }
    }

    _localAnnonceRepository.allAnnonce(5).then((response) {
      if (response.length>0){
        setState(() {
          _allLocalAnnonceEntity = response;
          _haveAnnonce = true;
        });
      }
    });

  }

  _MenuScreen(ConfigModel configModel,UserInformationModel userConnected){


    if(_allMenu.length<=0){
      return Container(
        alignment: FractionalOffset.topCenter,
        margin: EdgeInsets.only(top: 30),
        child: CircularProgressIndicator(
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            strokeWidth:2.0
        ),
      );
    }else {

      return Padding(
        padding: const EdgeInsets.only(left: 0.0, right: 0.0),
        child: GridView(
          padding: const EdgeInsets.only(
              left: 5, right: 5, top:16, bottom: 16),
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          children: List<Widget>.generate(
            _allMenu.length,
                (int index) {
              return MenuWidget(
                imagepath: _allMenu[index].image,
                titleMenu: _allMenu[index].title,
                widgetMenu: _allMenu[index].widget,
                typeMenu: _allMenu[index].typeMenu,
                iconeMenu: _allMenu[index].iconeMenu,
                connectedUser: "${userConnected.id}",
                validationUser: "${userConnected.validationUser}",
                codeMenu: _allMenu[index].code,
                scaffoldKey: _scaffoldKey,

              );
            },
          ),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3,
            mainAxisSpacing: 12.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 1.0,
          ),
        ),
      );
    }


  }

  _getConfigInfo(){

    Dio dio = new Dio();
    dio.get("${DataConstantesUtils.FULLCONFIG_SERVER_URL}?type=2").then((response) {
      var data = response.toString();
      Map responseMap = jsonDecode(data);
      AppResponseModel infoModel=AppResponseModel.fromMap(responseMap);
      if(infoModel.status.compareTo("ok")==0){
        _appSharedPreferences.setApkConfig(infoModel.informations);

        //enregistrement des configurations des menu
        FunctionUtils.saveMenu(infoModel.allMenus);
      }
    }).catchError((error){

    });

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}

class LastAnnonce extends StatelessWidget {
  final String image;
  final String title;
  final bool isActive;
  final int idAnnonce;
  const LastAnnonce({
    Key key,
    this.image,
    this.title,
    this.isActive = false,
    this.idAnnonce,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LocalScoopEntity _infoScoop = LocalScoopEntity.create(idScoop:0,roleMember: "1",typeAssociation: "1",idAssociation: 1,nameAssociation: "",cantonAssociation: "",adresseAssociation: "");

    return Container(
        padding: EdgeInsets.only(top: 10,left: 10,right: 10),
        margin: EdgeInsets.only(top: 10,bottom: 10,right: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: [
            isActive
                ? BoxShadow(
              offset: Offset(0, 10),
              blurRadius: 20,
              color: kActiveShadowColor,
            )
                : BoxShadow(
              offset: Offset(0, 3),
              blurRadius: 6,
              color: kShadowColor,
            ),
          ],
        ),
        child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => DetailAnnoncePage(idAnnonce,_infoScoop,"0",1)),
              );
            },
          child:  Column(
            children: <Widget>[
              image.compareTo("")==0
                  ?Image.asset(image, height: 90)
                  :ClipRRect(
                borderRadius: BorderRadius.circular(4.0),
                child:CachedNetworkImage(
                  imageUrl: '${DataConstantesUtils.IMAGE_ANNONCE_URL}$image',
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  height: 90,
                  width: 90,
                  //placeholder: (context, url) => new CircularProgressIndicator(),
                  placeholder: (context, url) => new Image.asset('assets/img/loading.gif',height:20),
                  errorWidget: (context, url, error) => new Icon(Icons.error),
                ),
              ),

              SizedBox(height:4),
              Text(
                title,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
        )
    );
  }


}

class LocalMenuModel {
  String code;
  String title;
  String image;
  String description;
  String iconeMenu;
  int typeMenu;
  int homeMenu;
  Widget widget;


  LocalMenuModel();

  LocalMenuModel.create({this.code,this.title, this.image, this.description, this.widget, this.typeMenu,this.iconeMenu,this.homeMenu});



  static List<LocalMenuModel> allMenu(String userKey,int idAnnonce)  {
    List<LocalMenuModel> CreateMenu=[];
    int venteBiztypeId =1;
    int achatBiztypeId = 2;
    LocalScoopEntity _infoScoop = LocalScoopEntity.create(idScoop:0,roleMember: "1",typeAssociation: "1",idAssociation: 1,nameAssociation: "",cantonAssociation: "",adresseAssociation: "");

    //enregistrement de tous les menus
    CreateMenu.add(LocalMenuModel.create(code:"M001",title: allTranslations.text('menu_marche'),image: "assets/img/marche.png",widget:MyAnnoncePage(DataConstantesUtils.ANNONCE_MARCHE,0,0,venteBiztypeId),homeMenu:1));
    //CreateMenu.add(LocalMenuModel.create(code:"M002",title: allTranslations.text('menu_commande'),image: "assets/img/marche.png",widget: ListeCommandePage(userKey),homeMenu:1));
    // CreateMenu.add(LocalMenuModel.create(code:"M003",title: allTranslations.text('menu_pret'),image: "assets/img/marche.png",widget:PretPage(),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M003",title: allTranslations.text('menu_pret'),image: "assets/img/marche.png",widget:HomePretPage(),homeMenu:1));
    //CreateMenu.add(LocalMenuModel.create(code:"M004",title: allTranslations.text('menu_vendeurs'),image: "assets/img/marche.png",widget:AbonnementPage(),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M005",title: allTranslations.text('menu_vendeurs'),image: "assets/img/marche.png",widget: MyAnnoncePage(DataConstantesUtils.ANNONCE_VENDEUR,int.parse(userKey),0,venteBiztypeId),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M006",title: allTranslations.text('menu_contact'),image: "assets/img/marche.png",widget:MyAnnoncePage(DataConstantesUtils.ANNONCE_MES_CONTACTS,0,0,venteBiztypeId),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M007",title: allTranslations.text('menu_assurance'),image: "assets/img/marche.png",widget:AllAssurancePage(),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M008",title: allTranslations.text('menu_meteo'),image: "assets/img/marche.png",widget:MeteoPage(),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M009",title: allTranslations.text('menu_mprice'),image: "assets/img/marche.png",widget: PrixMarchePage(),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M010",title: allTranslations.text('menu_account'),image: "assets/img/marche.png",widget: AccountPage("0"),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M011",title: allTranslations.text('description_conseil'),image: "assets/img/marche.png",widget: AllConseilPage(),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M016",title: allTranslations.text('menu_profilage'),image: "assets/img/marche.png",widget:ProfilagePage(),homeMenu:1));
    CreateMenu.add(LocalMenuModel.create(code:"M012",title: allTranslations.text('menu_about'),image: "assets/img/marche.png",widget:AboutPage(),homeMenu:1));
    
    //CreateMenu.add(LocalMenuModel.create(code:"M013",title: allTranslations.text('menu_souscription'),image: "assets/img/marche.png",widget:SouscriptionPage(),homeMenu:1));
    //CreateMenu.add(LocalMenuModel.create(code:"M014",title: allTranslations.text('menu_statistique_marchand'),image: "assets/img/marche.png",widget:SatistiqueMarchandPage(),homeMenu:1));
    //defaultMenu.add(LocalMenuModel.create(code:"M015",title: allTranslations.text('menu_messagerie_personnelle'),image: "assets/img/marche.png",widget:MessageriePersonnellePage(),homeMenu:1));
    //CreateMenu.add(LocalMenuModel.create(code:"M015",title: allTranslations.text('menu_geolocalisation'),image: "assets/img/marche.png",widget:GeolocalisationPage(),homeMenu:1));
    //CreateMenu.add(LocalMenuModel.create(code:"M080",title: allTranslations.text('menu_geolocalisation'),image: "assets/img/marche.png",widget:CheckoutFinalPage(),homeMenu:0));
    //CreateMenu.add(LocalMenuModel.create(code:"M081",title: allTranslations.text('menu_geolocalisation'),image: "assets/img/marche.png",widget:DetailAnnoncePage(idAnnonce,_infoScoop, DataConstantesUtils.ANNONCE_MARCHE,venteBiztypeId),homeMenu:0));

    return CreateMenu ;
  }

}
