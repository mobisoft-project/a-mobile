import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_type_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_assurance_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_type_assurance_repository.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import 'package:agrimobile/features/launch/presentation/pages/assurance/assurance_user_page.dart';



class AllAssureurPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  LocalAssuranceEntity infoAssurance;
  AllAssureurPage(this.infoAssurance);
  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState(this.infoAssurance);
  }
}



class PageState extends State<AllAssureurPage> {

  LocalAssuranceEntity _infoAssurance;
  PageState(this._infoAssurance);

  LocalTypeAssuranceRepository _localAssureurRepository = LocalTypeAssuranceRepository();
  List<LocalTypeAssuranceEntity> _localAssureurEntity;
  bool _getAssureur=true;

  var div= Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
    _getAssureurList();
  }




  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:   Align(
            child: Container(
              child: Text(_infoAssurance.denomination,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
            ),
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    UrlLauncher.launch('tel:+${configModel.callCenter}');
                  },
                  child: Icon(Icons.phone,color: Colors.white,),
                )
            ),
          ],
          backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        body: Stack(
            children: <Widget>[
              _getAssuranceListview()
            ]
        ),
      );
    },
  );

  
  _getAssureurList(){
    //recuperation des prix disponible sur le marche
    _localAssureurRepository.allTypeAssurance("${_infoAssurance.apiId}").then((response) {
      if(response.length>0 ){

        setState(() {
          _getAssureur=false;
          _localAssureurEntity=response;
        });

      }else{
        setState(() {
          _getAssureur=false;
        });
      }

    });
  }

  _getAssuranceListview(){

    if (_getAssureur==true){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif',height:150),
            ),
            SizedBox(height: 30,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_loading'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else if (_localAssureurEntity==null || _localAssureurEntity.length==0 ){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_asempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else{
      return ListView.builder(
          padding: EdgeInsets.all( 20.0),
          itemCount:_localAssureurEntity.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            LocalTypeAssuranceEntity infoAssureur= _localAssureurEntity[position];

            String nbre="An";
           /* String nbre="jours";
            if(infoAssureur.validite>1){
              nbre="${infoAssureur.validite} jours";
            }*/

            return Column(
                children: [
                  Container(
                    height: 80,
                    child:  MaterialButton(
                      elevation: 0.0,
                      child: Row(
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            child:CachedNetworkImage(
                              imageUrl: _infoAssurance.logo,
                              imageBuilder: (context, imageProvider) => Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              height: 70,
                              // placeholder: (context, url) => new CircularProgressIndicator(),
                              placeholder: (context, url) => new Image.asset('assets/img/loading.gif',height:30),
                              errorWidget: (context, url, error) => new Icon(Icons.error),
                            )
                          ),
                          SizedBox(width: 10,),
                          Expanded(child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                "${infoAssureur.assureurNom}",
                                style: TextStyle(
                                  color: Colors.black ,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                              SizedBox(height:2.5),
                              Text(
                                "${infoAssureur.prix} FCFA/$nbre",
                                style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16.0,
                                ),
                              ),
                              SizedBox(height:2.5),
                              Text(
                                "${infoAssureur.situation}",
                                style: TextStyle(
                                  color: infoAssureur.isvalide==0?Colors.red:Colors.green,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0,
                                ),
                              ),
                              SizedBox(height:2.5),
                              if(infoAssureur.isvalide==1)Text(
                                "${allTranslations.text('date_fin')} ${infoAssureur.date}",
                                style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0,
                                ),
                              ),
                            ],
                          )),
                        ],
                      ),
                      //textColor: Colors.white,
                      onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AssuranceUserPage(infoAssureur)),
                        ).then((value){
                             _getAssureurList();
                        });

                      },
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    ),
                  ),
                  div,
                ]
            );

          });
    }

  }



}

