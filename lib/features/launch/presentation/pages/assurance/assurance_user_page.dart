import 'dart:async';

import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/assurance_dto.dart';
import 'package:agrimobile/features/common/data/dto/update_assurance_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_type_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_user_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_assurance_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_type_assurance_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_user_assurance_repository.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;


class AssuranceUserPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  LocalTypeAssuranceEntity infoAssureur;
  AssuranceUserPage(this.infoAssureur);
  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState(this.infoAssureur);
  }
}



class PageState extends State<AssuranceUserPage> {

  LocalTypeAssuranceEntity _infoAssureur;
  PageState(this._infoAssureur);

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalUserAssuranceRepository _localUserAssuranceRepository = LocalUserAssuranceRepository();
  LocalTypeAssuranceRepository _localAssureurRepository = LocalTypeAssuranceRepository();
  List<LocalUserAssuranceEntity> _localUserAssureurEntity;

  String _accessToken="";
  String _userId="";

  var div= Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
    _getUserInformation();

  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
          key: widget._scaffoldKey,
          appBar: new AppBar(
            title:   Align(
              child: Container(
                child: Text(allTranslations.text('detail_assurance'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      UrlLauncher.launch('tel:+${configModel.callCenter}');
                    },
                    child: Icon(Icons.phone,color: Colors.white,),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return ListView(

                    children: <Widget>[
                      SizedBox(height: 30,),
                      Text(allTranslations.text('detail_listeassurance'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black ,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      Text.rich(
                        TextSpan(

                          text: "${_infoAssureur.typeassuranceNom}",
                          style: TextStyle(fontSize: 14, color: Colors.green,fontWeight: FontWeight.bold,),
                          children:
                          <TextSpan>[
                            TextSpan(
                              text: " chez ${_infoAssureur.assureurNom}",
                              style: TextStyle(
                                decoration: TextDecoration.none,
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        textAlign: TextAlign.center,
                      ),
                      if(_infoAssureur.isvalide==0)Container(
                        height:60,
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                          color: Colors.red,

                        ),
                        child: Text(allTranslations.text('empty_assurance'),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white70 ,
                            fontWeight: FontWeight.w600,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                      Container(
                        height:40,
                        margin: EdgeInsets.all(50),
                        decoration: BoxDecoration(
                          color: FunctionUtils.colorFromHex(configModel.mainColor),
                          borderRadius: new BorderRadius.circular(5.0),
                        ),
                        child: MaterialButton(
                          elevation: 0.0,
                          child: Text(_infoAssureur.isvalide==0?allTranslations.text('souscrire'):allTranslations.text('prolonger'),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white ,
                              fontWeight: FontWeight.w600,
                              fontSize: 15.0,
                            ),
                          ),
                          //textColor: Colors.white,
                          onPressed: (){
                            _showAlertDialog(myContext,configModel);
                          },
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        ),
                      ),
                      _getAssuranceListview(configModel),
                    ]
                );

              }
          )
      );
    },
  );

  

  _getAssuranceListview(ConfigModel configModel){
    if (_localUserAssureurEntity!=null && _localUserAssureurEntity.length>0 ){
      return ListView.builder(
          //padding: EdgeInsets.all( 20.0),
          itemCount:_localUserAssureurEntity.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            LocalUserAssuranceEntity infoUassurance= _localUserAssureurEntity[position];

            return Container(
              decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(width: 2.0, color: FunctionUtils.colorFromHex("AE682D")),
                  ),
                  // borderRadius: BorderRadius.all(
                  //     Radius.circular(5)
                  // ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        blurRadius: 5
                    )
                  ]
              ),
              margin:EdgeInsets.only(bottom: 20),
              padding:EdgeInsets.only(left:20,top:10,bottom:10),
              height: 116,
              child: GestureDetector(
                onTap: () {
                  if(infoUassurance.satutOperation == 0){
                    _confirmPay(context, infoUassurance.urlPaiement,configModel);
                  }
                },
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "${allTranslations.text('date_operation')} ${infoUassurance.dateOperation}",
                      style: TextStyle(
                        color: Colors.black ,
                        //fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                      ),
                    ),

                    SizedBox(height:2.5),
                    Text(
                      "${allTranslations.text('montant_assurance')} ${infoUassurance.montantOperation} FCFA",
                      style: TextStyle(
                        color: Colors.black,
                        //fontWeight: FontWeight.w500,
                        fontSize: 15.0,
                      ),
                    ),
                    SizedBox(height:2.5),
                    Text(
                      "${allTranslations.text('duree_assurance')} ${infoUassurance.dureeOperation} an(s)",
                      style: TextStyle(
                        color: Colors.black,
                        // fontWeight: FontWeight.w500,
                        fontSize: 14.0,
                      ),
                    ),
                    SizedBox(height:2.5),
                    Text(
                      "${allTranslations.text('date_fincontrat')} ${infoUassurance.dateFin}",
                      style: TextStyle(
                        color: Colors.black,
                        //fontWeight: FontWeight.w500,
                        fontSize: 14.0,
                      ),
                    ),
                    SizedBox(height:2.5),
                    if(infoUassurance.satutOperation == 0)Text(
                     "${allTranslations.text('statut_assurance')} : ${allTranslations.text('pending_pay_assurance')}",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  ],
                )
              )
            );

          });
    }else{
      return Container();
    }
  }

  _getUserInformation(){
    _appSharedPreferences.getUserInformation().then((value) {
        setState(() {
          _accessToken=value.token;
          _userId="${value.id}";
        });
        _getAssureurList();
      });
  }

  _getAssureurList(){
    print(1313);
    //recuperation des prix disponible sur le marche
    _localUserAssuranceRepository.allUAssurance(int.parse(_userId),_infoAssureur.apiId).then((response) {
      print(response.toString());
      if(response.length>0 ){
        setState(() {
          print(1414);
          _localUserAssureurEntity=response;
        });

      }
    });

  }

  _showAlertDialog(BuildContext context,ConfigModel configModel) {

    TextEditingController _yearsController=TextEditingController();

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        if(_yearsController.text.compareTo("")!=0 && _yearsController.text.compareTo("0")!=0){

            Navigator.of(context).pop(null);
            _confirmeAssurance(_yearsController.text,configModel);

        }else{
          _displaySnackBar(context, allTranslations.text('empty_years'));
        }

      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content:Container(
        height: 135,
        child: Column(
          children: [
            Text(allTranslations.text('souscription_assurance'), style: new TextStyle( fontSize: 17.0,fontWeight: FontWeight.bold),),
            SizedBox(height: 20,),
            Text("${allTranslations.text('nbre_years')}", style: new TextStyle( fontSize: 13.0),),
            Container(
              width: MediaQuery.of(context).size.width/1.2,
              height: 45,
              margin: EdgeInsets.only(top: 12),
              padding: EdgeInsets.only(
                  top: 4,left: 16, right: 16, bottom: 4
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                      Radius.circular(50)
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        blurRadius: 5
                    )
                  ]
              ),
              child: TextField(
                controller: _yearsController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: InputBorder.none,

                  //hintText:  allTranslations.text('qte_minimum'),
                ),
              ),
            ),
          ],
        ) ,
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _confirmeAssurance(String nbre, ConfigModel configModel) async {

    AssuranceDto _assuranceDto = AssuranceDto();

    _assuranceDto.accessToken=_accessToken;
    _assuranceDto.idAssuranceinfo="${_infoAssureur.apiId}";
    _assuranceDto.nbreAnnee=nbre;
    _assuranceDto.prixUnitaire="${_infoAssureur.prix}";


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('assurance_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        }
      );

    Api api = ApiRepository();
    api.askAbonnement(_assuranceDto).then((value) {

      if (value!=null && value.isRight()) {

        value.all((a) {

          if(a.status.compareTo("ok")==0){

          /*  setState(() {
              if(a.assurances[0].satutOperation.compareTo(1)== 0){
                _infoAssureur.isvalide=1;
                _infoAssureur.situation="EN COURS";
                _infoAssureur.date=a.assurances[0].dateFin;
              }
            });

            _localAssureurRepository.update(_infoAssureur, _infoAssureur.idTypeAssurance);

            //enregistrement des speculations
            FunctionUtils.saveUserAssurance(a.assurances);*/

            //pay
            _launchURL(a.assurances[0].urlPaiement);
            _update_assurance(configModel);
            Timer(Duration(seconds: 3),(){
              _getAssureurList();
                Navigator.of(context).pop(null);
                _displaySnackBar(context, allTranslations.text('assurance_success'));
                return true;
            });
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
  }


  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _confirmPay(BuildContext context,String url_paiement,ConfigModel configModel) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        _launchURL(url_paiement);
        _update_assurance(configModel);
      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(
        allTranslations.text('pay_assurance_text'),
        style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  _launchURL(url) async {
    if (await UrlLauncher.canLaunch(url)) {
      await UrlLauncher.launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _update_assurance(ConfigModel configModel) async {

    UpdateAssuranceDto _update_assuranceDto = UpdateAssuranceDto();

    _update_assuranceDto.accessToken=_accessToken;
    _update_assuranceDto.idAssuranceinfo="${_infoAssureur.apiId}";

    Api api = ApiRepository();
    api.updateAssurance(_update_assuranceDto).then((value) {

      if (value!=null && value.isRight()) {

        value.all((a) {

          if(a.status.compareTo("ok")==0){

            setState(() {
              print(555);
              _infoAssureur.isvalide= a.isvalide;
              _infoAssureur.situation= a.situation;
              _infoAssureur.date=a.date;
            });

            _localAssureurRepository.update(_infoAssureur, _infoAssureur.idTypeAssurance);
            //enregistrement des speculations
            FunctionUtils.saveUserAssurance(a.datas);
          }else{
           // Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }

        });
      }else{
        //Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
  }



/*
  _getAssuranceListview(){

    if (_getAssureur==true){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif',height:150),
            ),
            SizedBox(height: 30,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_loading'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else if (_localAssureurEntity==null || _localAssureurEntity.length==0 ){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_asempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else{
      return ListView.builder(
          padding: EdgeInsets.all( 20.0),
          itemCount:_localAssureurEntity.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            LocalTypeAssuranceEntity infoAssureur= _localAssureurEntity[position];

            String nbre="An";
           /* String nbre="jours";
            if(infoAssureur.validite>1){
              nbre="${infoAssureur.validite} jours";
            }*/

            return Column(
                children: [
                  Container(
                    height: 70,
                    child:  MaterialButton(
                      elevation: 0.0,
                      child: Row(
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            child:CachedNetworkImage(
                              imageUrl: _infoAssurance.logo,
                              imageBuilder: (context, imageProvider) => Container(
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              height: 70,
                              // placeholder: (context, url) => new CircularProgressIndicator(),
                              placeholder: (context, url) => new Image.asset('assets/img/loading.gif',height:30),
                              errorWidget: (context, url, error) => new Icon(Icons.error),
                            )
                          ),
                          SizedBox(width: 10,),
                          Expanded(child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                "${infoAssureur.assureurNom}",
                                style: TextStyle(
                                  color: Colors.black ,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                              SizedBox(height:2.5),
                              Text(
                                "${infoAssureur.prix} FCFA/$nbre",
                                style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16.0,
                                ),
                              ),
                              SizedBox(height:2.5),
                              Text(
                                "${infoAssureur.situation}",
                                style: TextStyle(
                                  color: infoAssureur.isvalide==0?Colors.red:Colors.green,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0,
                                ),
                              ),
                              SizedBox(height:2.5),
                              if(infoAssureur.isvalide==1)Text(
                                "${allTranslations.text('date_fin')} ${infoAssureur.date}",
                                style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0,
                                ),
                              ),
                            ],
                          )),
                        ],
                      ),
                      //textColor: Colors.white,
                      onPressed: (){


                      },
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                    ),
                  ),
                  div,
                ]
            );

          });
    }

  }
  */
}

