import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_assurance_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/assurance/all_assureur_page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;



class AllAssurancePage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState();
  }
}



class PageState extends State<AllAssurancePage> {


  LocalAssuranceRepository _localAssuranceRepository = LocalAssuranceRepository();
  List<LocalAssuranceEntity> _localAssuranceEntity;
  bool _getAssurance=true;

  @override
  void initState() {
    super.initState();
    _getAssuranceList();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:   Align(
            child: Container(
              child: Text(allTranslations.text('menu_assurance'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
            ),
          ),
          actions: <Widget>[
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    UrlLauncher.launch('tel:+${configModel.callCenter}');
                  },
                  child: Icon(Icons.phone,color: Colors.white,),
                )
            ),
          ],
          backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        body: Stack(
            children: <Widget>[
              _getAssuranceListview()
            ]
        ),
      );
    },
  );

  _getAssuranceList(){
    //recuperation des prix disponible sur le marche
    _localAssuranceRepository.allAssurance().then((response) {
      if(response.length>0 ){

        setState(() {
          _getAssurance=false;
          _localAssuranceEntity=response;
        });

      }else{
        setState(() {
          _getAssurance=false;
        });
      }

    });
  }

  _getAssuranceListview(){

    if (_getAssurance==true){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif',height:150),
            ),
            SizedBox(height: 30,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_loading'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else if (_localAssuranceEntity==null || _localAssuranceEntity.length==0 ){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_asempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else{
      return Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 8),
        child: GridView(
          padding: const EdgeInsets.only(left: 10, right: 10, top: 16, bottom: 16),
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          children: List<Widget>.generate(
            _localAssuranceEntity.length,
                (int index) {
              return AreaView( assurance: _localAssuranceEntity[index] );
            },
          ),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 15.0,
            crossAxisSpacing: 15.0,
            childAspectRatio: 1.0,
          ),
        ),
      );
    }

  }

}



class AreaView extends StatelessWidget {
  const AreaView({
    Key key,
    this.assurance,
  }) : super(key: key);

  final LocalAssuranceEntity assurance;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(8.0),
            bottomLeft: Radius.circular(8.0),
            bottomRight: Radius.circular(8.0),
            topRight: Radius.circular(8.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey.withOpacity(0.4),
              offset: const Offset(1.1, 1.1),
              blurRadius: 10.0),
        ],
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          focusColor: Colors.transparent,
          highlightColor: Colors.transparent,
          hoverColor: Colors.transparent,
          borderRadius: const BorderRadius.all(Radius.circular(8.0)),
          splashColor: Color(0xFF2633C5).withOpacity(0.2),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AllAssureurPage(assurance)),
            );
          },
          child: Column(
            children: <Widget>[
              Padding(
                padding:
                const EdgeInsets.only(top: 5, left: 5, right: 5),
                child: CachedNetworkImage(
                    imageUrl: assurance.logo,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    height: 125,
                   // placeholder: (context, url) => new CircularProgressIndicator(),
                    placeholder: (context, url) => new Image.asset('assets/img/loading.gif',height:30),
                    errorWidget: (context, url, error) => new Icon(Icons.error),
                  ),
              ),
              Text(
                "${assurance.denomination}",
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}



