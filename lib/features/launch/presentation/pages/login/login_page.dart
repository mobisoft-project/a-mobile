import 'dart:async';

import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/connection_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/user_information_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/registration_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/forget_password_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/home_page.dart';
import 'package:provider/provider.dart';


class LoginPage extends StatefulWidget{
  final _scaffoldKey = GlobalKey<ScaffoldState>();






  String type;
  LoginPage(this.type);

  @override
  LoginPageState createState() => LoginPageState(this.type);
}

class LoginPageState extends State<LoginPage> {
  String _type;
  LoginPageState(this._type);

  ConnectionDto _connectionDto = ConnectionDto();
  TextEditingController _passwordController=TextEditingController();
  TextEditingController _phoneController=TextEditingController();
  String _defaultIndicatif="+225";
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  String _registrationId = "";

  @override
  void initState() {
    super.initState();

    _getUserOtherInfo();
    WidgetsBinding.instance.addPostFrameCallback((_) =>_showMessage() );
  }

  void _showMessage(){
    if(_type.compareTo("1")==0){
      _displaySnackBar(context, allTranslations.text('reset_success'));
    }
  }



  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        key: widget._scaffoldKey,
        body: Container(
          child: ListView(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/3.5,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.white70,
                          Colors.white70
                        ],
                      ),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(90)
                      ),
                      border: Border.all(color: FunctionUtils.colorFromHex(configModel.mainColor),width: 2.0)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      //SizedBox(height: 0,),
                      Align(
                        alignment: Alignment.center,
                        child: Image(
                          image: AssetImage("assets/img/longlogo.png"),
                          height: 150,
                        ),
                      ),
                      //Spacer(),

                      Align(
                        alignment: Alignment.bottomRight,
                        child: Padding(
                          padding: const EdgeInsets.only(
                            //bottom: 32,
                              right: 32
                          ),
                          child: Text(allTranslations.text('connexion'),
                            style: TextStyle(
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                                fontSize: 20
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height/2,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(top: 62),
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 45,
                        padding: EdgeInsets.only(
                            top: 4,left: 16, right: 16, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 5
                              )
                            ]
                        ),
                        child: TextField(
                          keyboardType: TextInputType.phone,
                          controller: _phoneController,
                          maxLength: 10,
                          // maxLengthEnforced: true,
                          decoration: InputDecoration(
                            counterText: '',
                            border: InputBorder.none,
                            icon: Icon(Icons.person,
                              color: FunctionUtils.colorFromHex(configModel.mainColor),
                            ),
                            prefixIcon: Padding(padding: EdgeInsets.only(top:14), child: Text(_defaultIndicatif,style: TextStyle(
                                fontSize: 16
                            ),)),
                            hintText: allTranslations.text('enter_number'),
                          ),
                        ),
                      ),

                      Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 45,
                        margin: EdgeInsets.only(top: 32),
                        padding: EdgeInsets.only(
                            top: 4,left: 16, right: 16, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 5
                              )
                            ]
                        ),
                        child: TextField(
                          keyboardType: TextInputType.visiblePassword,
                          controller: _passwordController,
                          obscureText: true,
                          decoration: InputDecoration(
                            isDense: true,
                            border: InputBorder.none,
                            icon: Icon(Icons.vpn_key,
                              color: FunctionUtils.colorFromHex(configModel.mainColor),
                            ),
                            hintText:  allTranslations.text('enter_pwd'),
                          ),

                        ),
                      ),

                      Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 16, right: 32
                          ),
                          child:
                          GestureDetector(
                            onTap: () {

                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(builder: (context) => ForgetPasswordPage()),
                              );
                            },
                            child:
                            Text(allTranslations.text('forget_pwd'),
                              style: TextStyle(
                                  color: Colors.grey
                              ),
                            ),
                          ),
                        ),
                      ),
                      Spacer(),

                      GestureDetector(
                        onTap: (){

                          if(_phoneController.text.compareTo("")!=0 && _passwordController.text.compareTo("")!=0) {
                            _connectionDto.pwd = _passwordController.text;
                            // _connectionDto.telephone = _phoneController.text;
                            _connectionDto.telephone = "${_defaultIndicatif.replaceAll("+", "")}${_phoneController.text}";;
                            _connectionDto.registrationId = _registrationId;

                            _connexion(configModel);
                          }else{
                            _displaySnackBar(context, allTranslations.text('emptyField'));
                          }
                        },
                        child: Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width/1.2,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  FunctionUtils.colorFromHex(configModel.mainColor),
                                  FunctionUtils.colorFromHex(configModel.mainColor),
                                ],
                              ),
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              )
                          ),
                          child: Center(
                            child: Text(allTranslations.text('login').toUpperCase(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 50,
                ),

                /*Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(allTranslations.text('no_compte')),
                      SizedBox(width:5),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => RegistrationPage()),
                          );
                        },
                        child:Text(allTranslations.text('no_compte_signup'),style: TextStyle(color: FunctionUtils.colorFromHex(configModel.mainColor))),
                      )
                    ]
                ),*/
              ]
          ),
        ),
      );
    },
  );

  Future _getUserOtherInfo() async {
    _registrationId = await  _appSharedPreferences.getRegistrationId();

  }

  _connexion(ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('login_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.login(_connectionDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if(a!=null && a.status.compareTo("ok")==0){




            //enregistrement des informations de l'utilisateur dans la session
            UserInformationModel information=new UserInformationModel();
            information.id=a.id ?? 0;
            information.nom=a.nom;
            information.prenom=a.prenom;
            information.email=a.email;
            information.token=a.token;
            information.solde=a.solde;
            information.dateCreated=a.dateCreated;
            information.username=a.username;
            information.latitude=a.latitude;
            information.longitude=a.longitude;
            information.paysAlpha=a.paysAlpha;
            information.paysNom=a.paysNom;
            information.noteVendeur=a.noteVendeur;
            information.isfournisseur=a.isfournisseur;
            information.totalActeurs=a.totalActeurs;
            information.totalOffre=a.totalOffre;
            information.totalExperts=a.totalExperts;
            information.document=a.document;
            information.validationUser=a.validate;
            _appSharedPreferences.createLoginSession(context,information).then((value){

              //enregistrement des meteos
              FunctionUtils.saveMeteo(a.meteos,information.id);

              //enregistrement des assurances
              FunctionUtils.saveAssurance(a.assurance);

              //enregistrement des assurances de lutilisateur
              FunctionUtils.saveUserAssurance(a.mesAssurances);

              //enregistrement des typeAssurance
              FunctionUtils.saveTypeAssurance(a.typeAssurance);

              //enregistrement des devises
              FunctionUtils.saveDevise(a.devises);

              //enregistrement des unites
              FunctionUtils.saveUnites(a.unites);

              //enregistrement des produits
              FunctionUtils.saveProduits(a.produits);

              //enregistrement des biztypes
              FunctionUtils.saveBiztype(a.biztypes);

              //enregistrement des sous_produits
              FunctionUtils.saveSousProduits(a.sousProduits);

              //enregistrement des speculations
              FunctionUtils.saveSpeculations(a.speculations,information.id);

              //enregistrement des modes de livraisons
              FunctionUtils.saveModeLivraison(a.modeLivraisons);

              //enregistrement des moyens de transports
              FunctionUtils.saveMoyenTransport(a.moyenTransports);

              //enregistrement des types de prêts
              FunctionUtils.saveTypePret(a.typePrets);

              //enregistrement des categories
              FunctionUtils.saveCategories(a.categories);

              Navigator.of(context).pop(null);


              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage(value,"${a.id}","${a.validate}")),
                  ModalRoute.withName("/")
              );



            });



            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}
