import 'dart:async';

import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/core/utils/connection_status.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/connection_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/user_information_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/launch/presentation/pages/home_page.dart';


class RegistrationFinishPage extends StatefulWidget{
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String code;
  String telephone;
  RegistrationFinishPage(this.code,this.telephone);

  @override
  RegistrationFinishPageState createState() => RegistrationFinishPageState(this.code,this.telephone);
}

class RegistrationFinishPageState extends State<RegistrationFinishPage> {
  String _code;
  String _telephone;
  RegistrationFinishPageState(this._code,this._telephone);


  ConnectionDto _connectionDto = ConnectionDto();
  TextEditingController _codeController=TextEditingController();

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  String _registrationId = "";


  @override
  void initState() {
    super.initState();

    _getUserOtherInfo();
    WidgetsBinding.instance.addPostFrameCallback((_) =>_showMessage() );
  }

  void _showMessage(){
    _displaySnackBar(context, allTranslations.text('firstregistration_success'));
  }

  Future _getUserOtherInfo() async {
    _registrationId = await  _appSharedPreferences.getRegistrationId();
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        key: widget._scaffoldKey,
        body: Container(
          child: ListView(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/3.5,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.white70,
                          Colors.white70
                        ],
                      ),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(90)
                      ),
                      border: Border.all(color: FunctionUtils.colorFromHex(configModel.mainColor),width: 2.0)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                      Align(
                        alignment: Alignment.center,
                        child: Image(
                          image: AssetImage("assets/img/longlogo.png"),
                          height: 150,
                        ),
                      ),


                      Align(
                        alignment: Alignment.bottomRight,
                        child: Padding(
                          padding: const EdgeInsets.only(
                            //  bottom: 32,
                              right: 32
                          ),
                          child: Text(allTranslations.text('confirmationtitle'),
                            style: TextStyle(
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                                fontSize: 20
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height/2,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(top: 62),
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 45,
                        padding: EdgeInsets.only(
                            top: 4,left: 16, right: 16, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 5
                              )
                            ]
                        ),
                        child: TextField(
                          keyboardType: TextInputType.phone,
                          controller: _codeController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(Icons.lock,
                              color: FunctionUtils.colorFromHex(configModel.mainColor),
                            ),
                            hintText: allTranslations.text('confirmation_code'),
                          ),
                        ),
                      ),

                      Spacer(),

                      GestureDetector(
                        onTap: (){

                          if(_codeController.text.compareTo("")!=0 ) {


                            if(_code.compareTo(_codeController.text)==0){
                              _connectionDto.telephone = _telephone;
                              _connectionDto.secretCode = _codeController.text;
                              _connectionDto.registrationId = _registrationId;
                              _finishRegistration(configModel);
                            }else{
                              _displaySnackBar(context, allTranslations.text('error_confirm_code'));
                            }

                          }else{
                            _displaySnackBar(context, allTranslations.text('emptyField'));
                          }
                        },
                        child: Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width/1.2,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  FunctionUtils.colorFromHex(configModel.mainColor),
                                  FunctionUtils.colorFromHex(configModel.mainColor),
                                ],
                              ),
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              )
                          ),
                          child: Center(
                            child: Text(allTranslations.text('comfirm').toUpperCase(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
              ]
          ),
        ),
      );
    },
  );



  _finishRegistration(ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('fregister_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.confirmRegistration(_connectionDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if(a.status.compareTo("ok")==0){


            //enregistrement des informations de l'utilisateur dans la session
            UserInformationModel information=new UserInformationModel();
            information.id=a.id ?? 0;
            information.nom=a.nom;
            information.prenom=a.prenom;
            information.dateCreated=a.dateCreated;
            information.username=a.username;
            information.email=a.email;
            information.token=a.token;
            information.solde=a.solde;
            information.latitude=a.latitude;
            information.longitude=a.longitude;
            information.paysAlpha=a.paysAlpha;
            information.paysNom=a.paysNom;
            information.noteVendeur=a.noteVendeur;
            information.isfournisseur=a.isfournisseur;
            information.totalActeurs=a.totalActeurs;
            information.totalOffre=a.totalOffre;
            information.totalExperts=a.totalExperts;
            information.document=a.document;
            information.validationUser=a.validate;
            _appSharedPreferences.createLoginSession(context,information).then((value){

              //enregistrement des meteos
              FunctionUtils.saveMeteo(a.meteos,information.id);

              //enregistrement des assurances
              FunctionUtils.saveAssurance(a.assurance);

              //enregistrement des typeAssurance
              FunctionUtils.saveTypeAssurance(a.typeAssurance);

              //enregistrement des devises
              FunctionUtils.saveDevise(a.devises);

              //enregistrement des unites
              FunctionUtils.saveUnites(a.unites);

              //enregistrement des produits
              FunctionUtils.saveProduits(a.produits);

              //enregistrement des biztypes
              FunctionUtils.saveBiztype(a.biztypes);

              //enregistrement des sous_produits
              FunctionUtils.saveSousProduits(a.sousProduits);

              //enregistrement des speculations
              FunctionUtils.saveSpeculations(a.speculations,information.id);

              //enregistrement des modes de livraisons
              FunctionUtils.saveModeLivraison(a.modeLivraisons);

              //enregistrement des moyens de transports
              FunctionUtils.saveMoyenTransport(a.moyenTransports);

              //enregistrement des types de prêts
              FunctionUtils.saveTypePret(a.typePrets);

              //enregistrement des categories
              FunctionUtils.saveCategories(a.categories);

              Navigator.of(context).pop(null);

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => HomePage(value,"${a.id}","${a.validate}")),
                  ModalRoute.withName("/")
              );



            });


            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}
