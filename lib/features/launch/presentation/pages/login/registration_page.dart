import 'dart:async';
import 'dart:io';

import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_country_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_region_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_country_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_region_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/condition_user_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/registration_finish_page.dart';

import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/features/common/data/dto/inscription_dto.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';



class RegistrationPage extends StatefulWidget{
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  RegistrationPageState createState() => RegistrationPageState();
}

class RegistrationPageState extends State<RegistrationPage> {

  // ProgressDialog progressDialog;
  TextEditingController _regionController = TextEditingController();
  TextEditingController _phoneController=TextEditingController();
  TextEditingController _nameController=TextEditingController();
  TextEditingController _lastnameController=TextEditingController();
  TextEditingController _emailController=TextEditingController();
  TextEditingController _passwordController=TextEditingController();
  TextEditingController _passwordConfirmController=TextEditingController();

  InscriptionDto _inscriptionDto = InscriptionDto();

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalCountryRepository _localCountryRepository = LocalCountryRepository();
  LocalRegionRepository _localRegionRepository = LocalRegionRepository();

  ConfigModel _myConfigModel ;
  List<File>  _image;
  List<String> _allfile =[];

  bool _newRegion=false;
  String _defaultIndicatif="";
  bool   _defaultCondition=false;
  String _countryValue="";
  String _regionValue1="";


  List<DropdownMenuItem<String>> _dropdownRegionItems=[];


  var countryKey = GlobalKey<FindDropdownState>();
  var regionKey = GlobalKey<FindDropdownState>();

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {

      _myConfigModel=configModel;

      if(configModel.defaultFile.compareTo("")==0){

        _appSharedPreferences.getDefaultFile().then((value) {

          if(value.compareTo("")!=0){

            setState(() {
              _allfile = value.split(",");
              if(_image==null) {
                _image = new List(_allfile.length);
              }
            });

          }
        });

      }else{
        _allfile = configModel.defaultFile.split(",");
        if(_image==null) {
          _image = new List(_allfile.length);
        }
      }

      return Scaffold(
        key: widget._scaffoldKey,
        body: Stack(

            children: <Widget>[
              ListView(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height/3.5,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            Colors.white70,
                            Colors.white70
                          ],
                        ),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(90)
                        ),
                        border: Border.all(color: FunctionUtils.colorFromHex(configModel.mainColor),width: 2.0)
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        // Spacer(),
                        Align(
                          alignment: Alignment.center,
                          child: Image(
                            image: AssetImage("assets/img/longlogo.png"),
                            height: 150,
                          ),
                        ),
                        // Spacer(),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Padding(
                            padding: const EdgeInsets.only(
                              //bottom: 32,
                                right: 32
                            ),
                            child: Text(allTranslations.text('registration'),
                              style: TextStyle(
                                  color: FunctionUtils.colorFromHex(configModel.mainColor),
                                  fontSize: 20
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(

                    padding: EdgeInsets.only(left: 30.0,right:30.0,top:62.0),
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[

                        Text(allTranslations.text('select_country'),style: TextStyle(
                            color:Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                        ),),
                        SizedBox(height: 5),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.only(
                              top: 0,left: 16, right: 16, bottom: 0
                          ),
                          margin: EdgeInsets.only(bottom: 20),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5
                                )
                              ]
                          ),
                          child: FindDropdown<LocalCountryEntity>(
                            key: countryKey,
                            onFind: (String filter) => getCountry(filter),
                            searchBoxDecoration: InputDecoration(
                              hintText: allTranslations.text('search'),
                              border: OutlineInputBorder(),
                            ),
                            emptyBuilder: (BuildContext context){
                              return  Container(
                                decoration: new BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                      color: Colors.red,
                                    ),
                                    color: Colors.red),
                                height: 50,
                                margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                child: Center(
                                  child: Text("${allTranslations.text('liste_empty')}",
                                    style: new TextStyle(
                                        color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                  ),
                                ),
                              );
                            },
                            onChanged: (LocalCountryEntity value) {
                              setState(() {
                                _defaultIndicatif="+${value.indicatif}";
                              });
                              _countryValue="${value.apiId}";
                              _regionController.text="";
                            },
                          ),
                        ),


                        Text(allTranslations.text('select_region'),style: TextStyle(
                            color:Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                        ),),
                        SizedBox(height: 5),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.only(
                              top: 0,left: 16, right: 16, bottom: 0
                          ),
                          margin: EdgeInsets.only(bottom: 20),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5
                                )
                              ]
                          ),
                          child: FindDropdown<LocalRegionEntity>(
                            key: regionKey,
                            onFind: (String filter) => getRegion(filter),
                            searchBoxDecoration: InputDecoration(
                              hintText: allTranslations.text('search'),
                              border: OutlineInputBorder(),
                            ),
                            emptyBuilder: (BuildContext context){
                              return  Container(
                                decoration: new BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                      color: Colors.red,
                                    ),
                                    color: Colors.red),
                                height: 50,
                                margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                child: Center(
                                  child: Text("${allTranslations.text('liste_empty')}",
                                    style: new TextStyle(
                                        color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                  ),
                                ),
                              );
                            },
                            onChanged: (LocalRegionEntity value) {


                              _regionValue1="${value.apiId}";
                              if(_regionValue1.compareTo("0")==0){
                                setState(() {
                                  _newRegion=true;
                                });
                              }else{
                                setState(() {
                                  _newRegion=false;
                                });
                              }



                            },
                          ),
                        ),



                        Visibility(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(allTranslations.text('enter_region'),style: TextStyle(
                                  color:Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500
                              ),),
                              SizedBox(height: 5),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                height: 45,
                                margin: EdgeInsets.only(bottom: 20),
                                padding: EdgeInsets.only(
                                    top: 0,left: 16, right: 16, bottom: 0
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(50)
                                    ),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black12,
                                          blurRadius: 5
                                      )
                                    ]
                                ),
                                child: TextField(
                                  controller: _regionController,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    icon: Icon(Icons.location_city,
                                      color: FunctionUtils.colorFromHex(configModel.mainColor),
                                    ),

                                  ),
                                ),
                              )
                            ],
                          ),
                          visible: _newRegion,
                        ),


                        Text(allTranslations.text('enter_name'),style: TextStyle(
                            color:Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                        ),),
                        SizedBox(height: 5),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 45,
                          margin: EdgeInsets.only(bottom: 20),
                          padding: EdgeInsets.only(
                              top: 0,left: 16, right: 16, bottom: 0
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5
                                )
                              ]
                          ),
                          child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _nameController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              icon: Icon(Icons.person,
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                              ),

                              //hintText: allTranslations.text('enter_number'),
                            ),
                          ),
                        ),

                        Text(allTranslations.text('enter_lastname'),style: TextStyle(
                            color:Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                        ),),
                        SizedBox(height: 5),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 45,
                          margin: EdgeInsets.only(bottom: 20),
                          padding: EdgeInsets.only(
                              top: 0,left: 16, right: 16, bottom: 0
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5
                                )
                              ]
                          ),
                          child: TextField(
                            keyboardType: TextInputType.text,
                            controller: _lastnameController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              icon: Icon(Icons.person,
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                              ),

                              //hintText: allTranslations.text('enter_number'),
                            ),
                          ),
                        ),

                        Text(allTranslations.text('enter_number'),style: TextStyle(
                            color:Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                        ),),
                        SizedBox(height: 5),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 45,
                          margin: EdgeInsets.only(bottom: 20),
                          padding: EdgeInsets.only(
                              top: 0,left: 16, right: 16, bottom: 0
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5
                                )
                              ]
                          ),
                          child: TextField(
                            keyboardType: TextInputType.phone,
                            controller: _phoneController,
                            maxLength: 10,
                            decoration: InputDecoration(
                              counterText: '',
                              border: InputBorder.none,
                              icon: Icon(Icons.phone,
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                              ),
                              prefixIcon: Padding(padding: EdgeInsets.only(top:14), child: Text(_defaultIndicatif,style: TextStyle(
                                  fontSize: 16
                              ),)),
                              //hintText: allTranslations.text('enter_number'),
                            ),
                          ),
                        ),

                        Text(allTranslations.text('enter_email'),style: TextStyle(
                            color:Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                        ),),
                        SizedBox(height: 5),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 45,
                          margin: EdgeInsets.only(bottom: 20),
                          padding: EdgeInsets.only(
                              top: 0,left: 16, right: 16, bottom: 0
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5
                                )
                              ]
                          ),
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            controller: _emailController,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              icon: Icon(Icons.mail,
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                              ),

                              // hintText: allTranslations.text('enter_email'),
                            ),
                          ),
                        ),

                        Text(allTranslations.text('enter_pwd'),style: TextStyle(
                            color:Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                        ),),
                        SizedBox(height: 5),
                        Container(
                          width: MediaQuery.of(context).size.width/1.2,
                          height: 45,
                          margin: EdgeInsets.only(bottom: 20),
                          padding: EdgeInsets.only(
                              top: 0,left: 16, right: 16, bottom: 0
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5
                                )
                              ]
                          ),
                          child: TextField(
                            controller: _passwordController,
                            obscureText: true,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              icon: Icon(Icons.vpn_key,
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                              ),
                              // hintText:  allTranslations.text('enter_pwd'),
                            ),
                          ),
                        ),

                        Text(allTranslations.text('confirm_password'),style: TextStyle(
                            color:Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.w500
                        ),),
                        SizedBox(height: 5),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 45,
                          //margin: EdgeInsets.only(bottom: 20),
                          padding: EdgeInsets.only(
                              top: 0,left: 16, right: 16, bottom: 0
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              ),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    blurRadius: 5
                                )
                              ]
                          ),
                          child: TextField(
                            controller: _passwordConfirmController,
                            obscureText: true,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              icon: Icon(Icons.vpn_key,
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                              ),
                              // hintText:  allTranslations.text('confirm_password'),
                            ),
                          ),
                        ),

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: _getProjetFile(),
                        ),

                        SizedBox(height: 20),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Checkbox(
                                  value:_defaultCondition,
                                  activeColor: FunctionUtils.colorFromHex(configModel.mainColor)  ,
                                  onChanged:(bool newValue) {
                                    setState(() {
                                      _defaultCondition=newValue;
                                    });
                                  }
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => ConditionUsePage()),
                                  );
                                },
                                child:Text(allTranslations.text('use_condition'),style: TextStyle(color: FunctionUtils.colorFromHex(configModel.mainColor))),
                              )
                            ]
                        ),
                        SizedBox(
                          height: 20,
                        ),


                        InkWell(
                          onTap: (){
                            if(_defaultCondition){

                              if(_countryValue.compareTo("")!=0 && _regionValue1.compareTo("")!=0 && _nameController.text.compareTo("")!=0   && _lastnameController.text.compareTo("")!=0   && _phoneController.text.compareTo("")!=0   && _passwordController.text.compareTo("")!=0 && _passwordConfirmController.text.compareTo("")!=0 ) {

                                if ( _regionValue1.compareTo("0")!=0 || (_regionValue1.compareTo("0")==0 && _regionController.text.compareTo("")!=0)){

                                  if ( _passwordController.text.compareTo( _passwordConfirmController.text)==0){

                                    if( (_emailController.text.compareTo("")==0  && _defaultIndicatif.compareTo("+225")==0) || _emailController.text.compareTo("")!=0 ){


                                      bool verifcationFile=true;
                                      if(_image!=null && _image.length>0){
                                        for(int u=0;u<_image.length;u++){

                                          if(_image[u]==null || _image[u].path.compareTo("")==0){
                                            verifcationFile=false;
                                          }

                                        }
                                      }
                                      if(verifcationFile==true){
                                        _inscriptionDto.pwd = _passwordController.text;
                                        _inscriptionDto.telephone = "${_defaultIndicatif.replaceAll("+", "")}${_phoneController.text}";
                                        _inscriptionDto.email = _emailController.text;
                                        _inscriptionDto.nom = _nameController.text;
                                        _inscriptionDto.prenoms = _lastnameController.text;
                                        _inscriptionDto.pays = _countryValue;
                                        _inscriptionDto.region = _regionValue1;
                                        _inscriptionDto.addregion = _regionController.text;
                                        _inscriptionDto.fileProjet=_image;

                                        if(_regionValue1.compareTo("0")==0){
                                          _inscriptionDto.region = "ko";
                                        }

                                        _register(configModel);
                                      }else{
                                        _displaySnackBar(context, allTranslations.text('empty_fichier'));
                                      }




                                    }else{
                                      _displaySnackBar(context, allTranslations.text('error_email'));
                                    }


                                  }else{
                                    _displaySnackBar(context, allTranslations.text('error_confirm_pwd'));
                                  }
                                }else{
                                  _displaySnackBar(context, allTranslations.text('emptyRegion'));
                                }

                              }else{
                                _displaySnackBar(context, allTranslations.text('emptyField'));
                              }
                            }else{
                              _displaySnackBar(context, allTranslations.text('emptyConditionUse'));
                            }

                          },
                          child: Container(
                            height: 45,
                            width: MediaQuery.of(context).size.width/1.2,
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  colors: [
                                    FunctionUtils.colorFromHex(configModel.mainColor),
                                    FunctionUtils.colorFromHex(configModel.mainColor),
                                  ],
                                ),
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                )
                            ),
                            child: Center(
                              child: Text(allTranslations.text('register').toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold
                                ),
                              ),
                            ),
                          ),
                        ),

                        SizedBox(
                          height: 10,
                        ),

                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(allTranslations.text('have_compte')),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(builder: (context) => LoginPage("0")),
                                  );
                                },
                                child:Text(allTranslations.text('login'),style: TextStyle(color: FunctionUtils.colorFromHex(configModel.mainColor))),
                              )
                            ]
                        ),

                        SizedBox(
                          height: 50,
                        ),

                      ],
                    ),

                  ),
                ],
              ),
            ]
        ),
      );
    },
  );

  Future<List<LocalCountryEntity>> getCountry(String filter){
    return  _localCountryRepository.filterCountry(filter);
  }

  Future<List<LocalRegionEntity>> getRegion(String filter){
    return  _localRegionRepository.filterRegion(_countryValue,filter);
  }

  _register(ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('register_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.registration(_inscriptionDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if(a.status.compareTo("ok")==0){

            Navigator.of(context).pop(null);

            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => RegistrationFinishPage("${a.code}","${_defaultIndicatif.replaceAll("+", "")}${_phoneController.text}")),
            );
            return true;

          }else{
            print(a.message);
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });

  }

  _getProjetFile(){

    List<Widget> allWidget =[];

    if(_allfile!=null &&  _allfile.length>0) {

      allWidget.add( SizedBox(height: 20));
      allWidget.add( Text("${allTranslations.text('add_document')}",style: TextStyle(
          color:Colors.red,
          fontSize: 15,
          fontWeight: FontWeight.bold
      ),),);
      for(int u=0;u<_allfile.length;u++){

        String filename="";
        if(_image[u]!=null){
          List<String> pathInfo=_image[u].path.split("/");
          filename=pathInfo[pathInfo.length-1];
        }

        Widget fileContent= Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20),
            Text("${_allfile[u]} ",style: TextStyle(
                color:Colors.black,
                fontSize: 13,
                fontWeight: FontWeight.w500
            ),),
            SizedBox(height: 5),
            RawMaterialButton(
              onPressed: () {
                //_showChoiceDialog(context,configModel);
                _openGallery(u);
              },
              child: new Icon( Icons.insert_drive_file, color: FunctionUtils.colorFromHex(_myConfigModel.mainColor), size: 25.0, ),
              shape: new CircleBorder(),
              elevation: 2.0,
              fillColor: Colors.white,
              padding: const EdgeInsets.all(10.0),
            ),
            if(filename.compareTo("")!=0)Text("${filename} ",style: TextStyle(
                color:Colors.red,
                fontSize: 12,
                fontWeight: FontWeight.w500
            ),),

          ],
        );

        allWidget.add(fileContent);

      }

    }

    return allWidget ;

  }

  Future _openGallery(int position) async {

    FilePickerCross myFile = await FilePickerCross.importFromStorage(
      type: FileTypeCross.any,       // Available: `any`, `audio`, `image`, `video`, `custom`. Note: not available using FDE
      //fileExtension: 'txt, md, pdf'     // Only if FileTypeCross.custom . May be any file extension like `dot`, `ppt,pptx,odp`
    );

    if(myFile != null) {
      setState(() {
        _image[position] = File(myFile.path);

      });
    }

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }
}

