import 'dart:async';

import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/core/utils/connection_status.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/connection_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/user_information_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';


class ResetPasswordPage extends StatefulWidget{
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String code;
  String telephone;
  ResetPasswordPage(this.code,this.telephone);

  @override
  ResetPasswordPageState createState() => ResetPasswordPageState(this.code,this.telephone);
}

class ResetPasswordPageState extends State<ResetPasswordPage> {
  String _code;
  String _telephone;
  ResetPasswordPageState(this._code,this._telephone);


  ConnectionDto _connectionDto = ConnectionDto();
  TextEditingController _codeController=TextEditingController();
  TextEditingController _password1Controller=TextEditingController();
  TextEditingController _password2Controller=TextEditingController();


  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        key: widget._scaffoldKey,
        body: Container(
          child: ListView(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height/3.5,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.white70,
                          Colors.white70
                        ],
                      ),
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(90)
                      ),
                      border: Border.all(color: FunctionUtils.colorFromHex(configModel.mainColor),width: 2.0)
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      //Spacer(),
                      Align(
                        alignment: Alignment.center,
                        child: Image(
                          image: AssetImage("assets/img/longlogo.png"),
                          height: 150,
                        ),
                      ),
                     // Spacer(),

                      Align(
                        alignment: Alignment.bottomRight,
                        child: Padding(
                          padding: const EdgeInsets.only(
                             // bottom: 32,
                              right: 32
                          ),
                          child: Text(allTranslations.text('reset_pwd'),
                            style: TextStyle(
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                                fontSize: 20
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height/2,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(top: 62),
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 45,
                        padding: EdgeInsets.only(
                            top: 4,left: 16, right: 16, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 5
                              )
                            ]
                        ),
                        child: TextField(
                          keyboardType: TextInputType.phone,
                          controller: _codeController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(Icons.lock,
                              color: FunctionUtils.colorFromHex(configModel.mainColor),
                            ),
                            hintText: allTranslations.text('confirmation_code'),
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 45,
                        margin: EdgeInsets.only(top: 32),
                        padding: EdgeInsets.only(
                            top: 4,left: 16, right: 16, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 5
                              )
                            ]
                        ),
                        child: TextField(
                          keyboardType: TextInputType.visiblePassword,
                          controller: _password1Controller,
                          obscureText: true,
                          decoration: InputDecoration(
                            isDense: true,
                            border: InputBorder.none,
                            icon: Icon(Icons.vpn_key,
                              color: FunctionUtils.colorFromHex(configModel.mainColor),
                            ),
                            hintText:  allTranslations.text('enter_pwd'),
                          ),

                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width/1.2,
                        height: 45,
                        margin: EdgeInsets.only(top: 32),
                        padding: EdgeInsets.only(
                            top: 4,left: 16, right: 16, bottom: 4
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(50)
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 5
                              )
                            ]
                        ),
                        child: TextField(
                          keyboardType: TextInputType.visiblePassword,
                          controller: _password2Controller,
                          obscureText: true,
                          decoration: InputDecoration(
                            isDense: true,
                            border: InputBorder.none,
                            icon: Icon(Icons.vpn_key,
                              color: FunctionUtils.colorFromHex(configModel.mainColor),
                            ),
                            hintText:  allTranslations.text('enter_pwd'),
                          ),

                        ),
                      ),
                      Spacer(),

                      GestureDetector(
                        onTap: (){

                          if(_codeController.text.compareTo("")!=0 && _password1Controller.text.compareTo("")!=0 && _password2Controller.text.compareTo("")!=0) {

                            if(_password1Controller.text.compareTo(_password2Controller.text)==0) {

                              if(_code.compareTo(_codeController.text)==0){

                                _connectionDto.telephone = _telephone;
                                _connectionDto.code = _codeController.text;
                                _connectionDto.pwd = _password1Controller.text;

                                _resetPassword(configModel);
                              }else{
                                _displaySnackBar(context, allTranslations.text('error_confirm_code'));
                              }
                            }else{
                              _displaySnackBar(context, allTranslations.text('error_confirm_pwd'));
                            }
                          }else{
                            _displaySnackBar(context, allTranslations.text('emptyField'));
                          }
                        },
                        child: Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width/1.2,
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  FunctionUtils.colorFromHex(configModel.mainColor),
                                  FunctionUtils.colorFromHex(configModel.mainColor),
                                ],
                              ),
                              borderRadius: BorderRadius.all(
                                  Radius.circular(50)
                              )
                          ),
                          child: Center(
                            child: Text(allTranslations.text('recover').toUpperCase(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 50,
                ),

                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(allTranslations.text('know_password')),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => LoginPage("0")),
                          );
                        },
                        child:Text(allTranslations.text('login'),style: TextStyle(color: FunctionUtils.colorFromHex(configModel.mainColor))),
                      )
                    ]
                ),
              ]
          ),
        ),
      );
    },
  );
  
  _resetPassword(ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('spassword_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.resetPassword(_connectionDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if(a.status.compareTo("ok")==0){

            Navigator.of(context).pop(null);

            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => LoginPage("1")),
            );
            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}
