
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/annonce_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/annonce_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_biztype_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_produit_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_sous_produit_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_unite_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_biztype_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_produit_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_sous_produit_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_unite_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/liste_annonce_page.dart';

import 'package:agrimobile/features/common/data/dto/scoop_dto.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_scoop_repository.dart';

  import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'dart:io';
import 'dart:async';

import 'package:provider/provider.dart';

class AddAnnoncePage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String type;
  int biztype;
  AddAnnoncePage(this.type,this.biztype);

  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState(this.type,this.biztype);
  }
}



class PageState extends State<AddAnnoncePage> {

  String _type;
  int _biztypeId;
  PageState(this._type,this._biztypeId);

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  TextEditingController _pprixController=TextEditingController();
  TextEditingController _qteController=TextEditingController();
  TextEditingController _lieuController=TextEditingController();
  TextEditingController _infoController=TextEditingController();
  TextEditingController _conditionController=TextEditingController();
  TextEditingController _dateDispoController=TextEditingController();


  String _apiId="0";
  String _userId="";
  bool _visibleDate=false;
  bool _visibleScoop=false;
  String _userToken="";
  bool _haveEop=false;
  String _keyScoop="";

  final scoopKey = GlobalKey<FindDropdownState>();

  LocalScoopRepository _localScoopRepository = LocalScoopRepository();
  List<LocalScoopEntity> _allLocalScoopEntity;

  LocalBiztypeRepository _localBiztypeRepository=LocalBiztypeRepository();
  LocalProduitRepository _localProduitRepository=LocalProduitRepository();
  LocalSousProduitRepository _localSousProduitRepository=LocalSousProduitRepository();
  LocalUniteRepository _localUniteRepository=LocalUniteRepository();
  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();



  final format= DateFormat("dd/MM/yyyy");
  int years=2020;
  int id=0;


  String _titleAnnonce="" ;
  String _pictureName="" ;
  String _categorieAnnonce="" ;
  String _uniteAnnonce="" ;
  String _typeAnnonce="" ;
  String _dispoAnnonce="" ;
  String _indivisibiliteAnnonce="" ;

  File _image;
  final _picker = ImagePicker();
  String _serverImage="" ;
  String _assetImageUrl="assets/img/annonceregister.png" ;
  AnnonceDto _annonceDto = AnnonceDto();



  var bizKey = GlobalKey<FindDropdownState>();
  var disponibilityKey = GlobalKey<FindDropdownState>();
  var indivisibiliteKey = GlobalKey<FindDropdownState>();
  var produitKey = GlobalKey<FindDropdownState>();
  var sousproduitKey = GlobalKey<FindDropdownState>();
  var uniteproduitKey = GlobalKey<FindDropdownState>();
  String _uniteMesure="";

  @override
  void initState() {
    super.initState();
    var now =new DateTime.now();
    var formatter =new DateFormat("yyyy");
    years=int.parse(formatter.format(now))+5;

    id=int.parse("${(DateTime.now().millisecondsSinceEpoch/1000).toInt()}");
    _titleAnnonce="" ;
    _pictureName="" ;
    _categorieAnnonce="" ;
    _uniteAnnonce="" ;
    _typeAnnonce="" ;
    _dispoAnnonce="" ;
    _getUserInformation();
  }



  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      final accountCard=RawMaterialButton(
        onPressed: () {
          _showChoiceDialog(context,configModel);
        },
        child: new Icon( Icons.camera_alt, color: FunctionUtils.colorFromHex(configModel.mainColor), size: 25.0, ),
        shape: new CircleBorder(),
        elevation: 2.0,
        fillColor: Colors.white,
        padding: const EdgeInsets.all(10.0),
      );
      
      
      print("biztype ++"+widget.biztype.toString());

      String biztype = widget.biztype.compareTo(1)==0?allTranslations.text('vente'):allTranslations.text('achat');

      String before_title = _apiId.compareTo("0")==0?allTranslations.text('title_add_annonce'):allTranslations.text('title_update_annonce');

      String title = before_title;
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:  Align(
            child: Container(
              margin: EdgeInsets.only(right: 50),
              child: Text(title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,

                ),
              ),
            ),
          ),
          backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        body: Stack(

            children: <Widget>[
              ListView(

                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        _showChoiceDialog(context,configModel);
                      },
                      child:
                      _serverImage.compareTo("")!=0 &&  _image==null?Container(
                          constraints: new BoxConstraints.expand(
                            height: 300.0,
                          ),
                          padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                          decoration: new BoxDecoration(
                            image: new DecorationImage(
                              image: NetworkImage(_serverImage),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: new Stack(
                            children: <Widget>[

                              new Positioned(
                                right: 0.0,
                                bottom: 0.0,
                                child: accountCard,
                              ),
                            ],
                          )
                      ):
                      _image == null ?Image.asset(_assetImageUrl, color:FunctionUtils.colorFromHex(configModel.mainColor),width:100,height:100)
                          :Container(
                          constraints: new BoxConstraints.expand(
                            height: 300.0,
                          ),
                          padding: new EdgeInsets.only(left: 16.0, bottom: 8.0, right: 16.0),
                          decoration: new BoxDecoration(
                            image: new DecorationImage(
                              image: FileImage(File("${_image.path}")),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: new Stack(
                            children: <Widget>[

                              new Positioned(
                                right: 0.0,
                                bottom: 0.0,
                                child: accountCard,
                              ),
                            ],
                          )
                      ),

                    ),

                    Container(
                      padding: EdgeInsets.only(left: 30.0,right:30.0,top:20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height:20),
                         Text(allTranslations.text('select_typeBusy'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child: FindDropdown<LocalBiztypeEntity>(
                              key: bizKey,
                              onFind: (String filter) => getBiztype(filter),
                              searchBoxDecoration: InputDecoration(
                                hintText: allTranslations.text('search'),
                                border: OutlineInputBorder(),
                              ),
                              emptyBuilder: (BuildContext context){
                                return  Container(
                                  decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.0),
                                      border: Border.all(
                                        color: Colors.red,
                                      ),
                                      color: Colors.red),
                                  height: 50,
                                  margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                  child: Center(
                                    child: Text("${allTranslations.text('liste_empty')}",
                                      style: new TextStyle(
                                          color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                );
                              },
                              onChanged: (LocalBiztypeEntity value) {

                                _typeAnnonce="";
                                if(value!=null){
                                  _typeAnnonce="${value.apiId}";
                                }

                              },
                            ),
                          ),

                          Visibility(
                            child:  Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(allTranslations.text('selectScoop'),style: TextStyle(
                                    color:Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500
                                ),),
                                SizedBox(height: 5),

                                Container(
                                  margin: EdgeInsets.only(bottom: 20),
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.only(
                                      top: 0,left: 16, right: 16, bottom: 0
                                  ),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50)
                                      ),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black12,
                                            blurRadius: 5
                                        )
                                      ]
                                  ),
                                  child:   FindDropdown <LocalScoopEntity>(
                                    key: scoopKey,
                                    onFind: (String filter) => getScoop(filter),
                                    searchBoxDecoration: InputDecoration(
                                      hintText: allTranslations.text('search'),
                                      border: OutlineInputBorder(),
                                    ),
                                    dropdownBuilder: (BuildContext context, LocalScoopEntity item) {
                                      return Container(
                                        width: MediaQuery.of(context).size.width,
                                        padding: EdgeInsets.all(10),
                                        height: 40,
                                        decoration: BoxDecoration(
                                          border: Border.all(color: Theme.of(context).dividerColor),
                                          borderRadius: BorderRadius.circular(5),
                                          color: Colors.white,
                                        ),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: item==null?Text(""):Text("${item.nameAssociation} (${item.cantonAssociation})"),
                                            ),
                                            Icon(
                                              Icons.arrow_drop_down,
                                              color: Colors.grey,
                                              size: 20,
                                            ),
                                          ],
                                        ),
                                      );
                                    },
                                    dropdownItemBuilder:(BuildContext context, LocalScoopEntity item, bool isSelected) {
                                      return Container(
                                        decoration: !isSelected ? null : BoxDecoration(
                                          color: Colors.white,
                                        ),
                                        child: ListTile(
                                          selected: isSelected,
                                          title: item==null?Text(""):Text("${item.nameAssociation} (${item.cantonAssociation})"),
                                        ),
                                      );
                                    },
                                    emptyBuilder: (BuildContext context){
                                      return  Container(
                                        decoration: new BoxDecoration(
                                            borderRadius: BorderRadius.circular(4.0),
                                            border: Border.all(
                                              color: Colors.red,
                                            ),
                                            color: Colors.red),
                                        height: 50,
                                        margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                        child: Center(
                                          child: Text("${allTranslations.text('liste_empty')}",
                                            style: new TextStyle(
                                                color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      );
                                    },

                                    onChanged: (LocalScoopEntity data) {

                                      _keyScoop="";
                                      // _allComposantScoop=[];

                                      if(data!=null ){
                                        _keyScoop="${data.idAssociation}";

                                        /*List<dynamic> responseMap = jsonDecode(data.memberAssociation.toString());
                                      _allComposantScoop=responseMap.map((i) => DetailScoopModel.fromMap(i)).toList();*/
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                             visible: _visibleScoop,
                            //visible: true,
                          ),

                          Text(allTranslations.text('select_disponibilite'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child:  FindDropdown(
                              key: disponibilityKey,
                              items: ["${allTranslations.text('disponibilite1')}","${allTranslations.text('disponibilite2')}"],
                              searchBoxDecoration: InputDecoration(
                                hintText: allTranslations.text('search'),
                                border: OutlineInputBorder(),
                              ),
                              emptyBuilder: (BuildContext context){
                                return  Container(
                                  decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.0),
                                      border: Border.all(
                                        color: Colors.red,
                                      ),
                                      color: Colors.red),
                                  height: 50,
                                  margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                  child: Center(
                                    child: Text("${allTranslations.text('liste_empty')}",
                                      style: new TextStyle(
                                          color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                );
                              },
                              onChanged: (String item){
                                _dispoAnnonce="";
                                setState(() {
                                  _visibleDate=false;
                                });
                                if(item!=null){
                                  _dispoAnnonce="1";
                                  if(item.compareTo("${allTranslations.text('disponibilite2')}")==0){
                                    _dispoAnnonce="2";
                                    setState(() {
                                      _visibleDate=true;
                                    });
                                  }
                                }
                              },

                            ),
                          ),

                          Visibility(
                            child:  Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(allTranslations.text('datedispo'),style: TextStyle(
                                    color:Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500
                                ),),
                                SizedBox(height: 5),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 45,
                                  margin: EdgeInsets.only(bottom: 20),
                                  padding: EdgeInsets.only(
                                      top: 0,left: 16, right: 16, bottom: 0
                                  ),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50)
                                      ),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black12,
                                            blurRadius: 5
                                        )
                                      ]
                                  ),
                                  child:  DateTimeField(
                                    format: format,
                                    controller: _dateDispoController,
                                    onShowPicker: (context, currentValue) {
                                      return showDatePicker(
                                          locale : const Locale("fr","FR"),
                                          context: context,
                                          firstDate: DateTime.now(),
                                          initialDate: DateTime.now(),
                                          lastDate: DateTime(years)
                                      );
                                    },
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      icon: Icon(Icons.date_range,
                                        color: FunctionUtils.colorFromHex(configModel.mainColor),
                                      ),
                                      // hintText:  allTranslations.text('datedispo'),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            visible: _visibleDate,
                          ),

                          Text(allTranslations.text('select_produit'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child:  FindDropdown<LocalProduitEntity>(
                              key: produitKey,
                              onFind: (String filter) => getProduit(filter),
                              searchBoxDecoration: InputDecoration(
                                hintText: allTranslations.text('search'),
                                border: OutlineInputBorder(),
                              ),
                              emptyBuilder: (BuildContext context){
                                return  Container(
                                  decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.0),
                                      border: Border.all(
                                        color: Colors.red,
                                      ),
                                      color: Colors.red),
                                  height: 50,
                                  margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                  child: Center(
                                    child: Text("${allTranslations.text('liste_empty')}",
                                      style: new TextStyle(
                                          color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                );
                              },
                              onChanged: (LocalProduitEntity value) {

                                _categorieAnnonce="0";
                                _uniteMesure="";
                                if(value!=null) {
                                  _categorieAnnonce="${value.apiId}";
                                  _uniteMesure="${value.uniteMesure}";
                                }

                              },
                            ),
                          ),


                          Text(allTranslations.text('nom_produit'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child:  FindDropdown<LocalSousProduitEntity>(
                              key: sousproduitKey,
                              onFind: (String filter) => getNameProduit(filter),
                              searchBoxDecoration: InputDecoration(
                                hintText: allTranslations.text('search'),
                                border: OutlineInputBorder(),
                              ),
                              emptyBuilder: (BuildContext context){
                                return  Container(
                                  decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.0),
                                      border: Border.all(
                                        color: Colors.red,
                                      ),
                                      color: Colors.red),
                                  height: 50,
                                  margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                  child: Center(
                                    child: Text("${allTranslations.text('liste_empty')}",
                                      style: new TextStyle(
                                          color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                );
                              },
                              onChanged: (LocalSousProduitEntity value) {
                                _titleAnnonce=value.nomCategorieFille;
                              },
                            ),
                          ),


                          Text("${allTranslations.text('produit_prix')} (${configModel.devise})",style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 45,
                            margin: EdgeInsets.only(bottom: 20),
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child: TextField(
                              controller: _pprixController,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),



                          Text(allTranslations.text('select_unite'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child:  FindDropdown<LocalUniteEntity>(
                              key: uniteproduitKey,
                              onFind: (String filter) => getUniteProduit(filter),
                              searchBoxDecoration: InputDecoration(
                                hintText: allTranslations.text('search'),
                                border: OutlineInputBorder(),
                              ),
                              emptyBuilder: (BuildContext context){
                                return  Container(
                                  decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.0),
                                      border: Border.all(
                                        color: Colors.red,
                                      ),
                                      color: Colors.red),
                                  height: 50,
                                  margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                  child: Center(
                                    child: Text("${allTranslations.text('liste_empty')}",
                                      style: new TextStyle(
                                          color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                );
                              },
                              onChanged: (LocalUniteEntity value) {
                                _uniteAnnonce="${value.apiId}";
                              },
                            ),
                          ),


                          Text(allTranslations.text('quantite'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 45,
                            margin: EdgeInsets.only(bottom: 20),
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child: TextField(
                              controller: _qteController,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),

                          Text(allTranslations.text('lieu'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 45,
                            margin: EdgeInsets.only(bottom: 20),
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child: TextField(
                              controller: _lieuController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),


                          Text(allTranslations.text('select_indivisibilite'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            margin: EdgeInsets.only(bottom: 20),
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child:  FindDropdown(
                              key: indivisibiliteKey,
                              items: ["${allTranslations.text('indivisibilite1')}","${allTranslations.text('indivisibilite2')}"],
                              searchBoxDecoration: InputDecoration(
                                hintText: allTranslations.text('search'),
                                border: OutlineInputBorder(),
                              ),
                              emptyBuilder: (BuildContext context){
                                return  Container(
                                  decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.circular(4.0),
                                      border: Border.all(
                                        color: Colors.red,
                                      ),
                                      color: Colors.red),
                                  height: 50,
                                  margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                  child: Center(
                                    child: Text("${allTranslations.text('liste_empty')}",
                                      style: new TextStyle(
                                          color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                );
                              },
                              onChanged: (String item){
                                _indivisibiliteAnnonce="";
                                if(item!=null){
                                  _indivisibiliteAnnonce="1";
                                  if(item.compareTo("${allTranslations.text('indivisibilite2')}")==0){
                                    _indivisibiliteAnnonce="2";
                                  }
                                }
                              },

                            ),
                          ),


                          Text(allTranslations.text('infoSupp'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(bottom: 20),
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child: TextFormField(
                              minLines: 1,
                              maxLines: 5,
                              keyboardType: TextInputType.multiline,
                              controller: _infoController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),

                          Text(allTranslations.text('conditionExpedition'),style: TextStyle(
                              color:Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),),
                          SizedBox(height: 5),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(bottom: 20),
                            padding: EdgeInsets.only(
                                top: 0,left: 16, right: 16, bottom: 0
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(50)
                                ),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black12,
                                      blurRadius: 5
                                  )
                                ]
                            ),
                            child: TextFormField(
                              minLines: 1,
                              maxLines: 5,
                              keyboardType: TextInputType.multiline,
                              controller: _conditionController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),

                          SizedBox(height: 50),
                          MaterialButton(
                            color:  FunctionUtils.colorFromHex(configModel.mainColor),
                            elevation: 0.0,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color:  FunctionUtils.colorFromHex(configModel.mainColor))
                            ),
                            textColor: Colors.white,
                            onPressed: () {
                              bool continues = true;
                              //_typeAnnonce ="${widget.biztype}";

                              if(_typeAnnonce.compareTo("") != 0 && 
                                _categorieAnnonce.compareTo("") != 0 && 
                                _uniteAnnonce.compareTo("") != 0 && 
                                _pprixController.text.compareTo("") != 0 && 
                                _lieuController.text.compareTo("") != 0 && 
                                _qteController.text.compareTo("") != 0 && 
                                _dispoAnnonce.compareTo("") != 0 && 
                                _indivisibiliteAnnonce.compareTo("") != 0) {

                                if( _visibleScoop == true && _keyScoop.compareTo("") == 0) continues = false;
                                if(continues == true){

                              if (_dispoAnnonce.compareTo("1") == 0 ||(_dispoAnnonce.compareTo("2") == 0 && _dateDispoController.text.compareTo("") != 0)) {
                                String disponibilite = "";
                                if (_dispoAnnonce.compareTo("2") == 0) {
                                  disponibilite =
                                      FunctionUtils.disponibiliteDate(_dateDispoController.text);
                                }
                                
                                
                                print(_keyScoop);

                                _annonceDto.id = id;
                                _annonceDto.userId = _userId;
                                _annonceDto.title = _titleAnnonce;
                                _annonceDto.picture = _pictureName;
                                _annonceDto.categorieId = _categorieAnnonce;
                                _annonceDto.biztypeId = _typeAnnonce;
                                _annonceDto.unitesId = _uniteAnnonce;
                                _annonceDto.prix = _pprixController.text;
                                _annonceDto.lieu = _lieuController.text;
                                _annonceDto.stock = _qteController.text;
                                _annonceDto.info = _infoController.text;
                                _annonceDto.exp = _conditionController.text;
                                _annonceDto.disponibilite = _dispoAnnonce;
                                _annonceDto.indivisibilite = _indivisibiliteAnnonce;
                                _annonceDto.moisDisponibilite = disponibilite;
                                _annonceDto.scoopId = _keyScoop;
                                _annonceDto.apiId = _apiId;
                                _annonceDto.etat = "1";
                                _annonceDto.photo = "0";
                                if (_image != null) {
                                  _annonceDto.photo = "${_image.path}";
                                }

                                _publishAnnonce(configModel);
                              } else {
                                _displaySnackBar(context, allTranslations.text('emptyDisponibilite'));
                              }
                              }else{
                                _displaySnackBar(context, allTranslations.text('selectScoop'));
                              }
                              }else{
                                _displaySnackBar(context, allTranslations.text('emptyField'));
                              }

                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Text(
                                  _apiId.compareTo("0")==0?allTranslations.text('publish'):allTranslations.text('update'),
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white, fontSize: 16.0),
                                ),
                              ],
                            ),
                            padding: EdgeInsets.fromLTRB(0, 15, 0, 15),

                          ),
                          SizedBox(height: 50),
                        ],
                      ),
                    ),

                  ]
              )
            ]
        ),
      );
    },
  );

  Future<List<LocalScoopEntity>> getScoop(String filter){
    Future<List<LocalScoopEntity>> localScoopEntity =  _localScoopRepository.filterScoop(int.parse(_userId),filter);

    return localScoopEntity;
  }


  Future<List<LocalBiztypeEntity>> getBiztype(String filter){
    return  _localBiztypeRepository.filterBisy(filter);
  }

  Future<List<LocalProduitEntity>> getProduit(String filter){
    return  _localProduitRepository.filterProduit(filter);
  }

  Future<List<LocalSousProduitEntity>> getNameProduit(String filter){
    print("11111");
    int categorieId = 0;
    if(_categorieAnnonce.compareTo("")!=0) {
      categorieId = int.parse(_categorieAnnonce);
    }

    print(categorieId);
    Future<List<LocalSousProduitEntity>> response =  _localSousProduitRepository.filterSousProduit(categorieId,filter);
    print(response);
    return response;

  }

  Future<List<LocalUniteEntity>> getUniteProduit(String filter){

    print(_uniteMesure);
    String allId=_uniteMesure;
    allId=allId.replaceAll("[", "(");
    allId=allId.replaceAll("]", ")");

    return  _localUniteRepository.filterUniteProduit(allId,filter);
  }


  Future _openGallery() async {
    PickedFile pickedFile = await _picker.getImage(source: ImageSource.gallery, imageQuality: 80,maxHeight: 300,maxWidth: 300);

    setState(() {
      _image = File(pickedFile.path);

    });
  }

  Future getImageFromCamera() async {

    PickedFile pickedFile = await _picker.getImage(source: ImageSource.camera, imageQuality: 80,maxHeight: 300,maxWidth: 300);
    setState(() {
      _image = File(pickedFile.path);
    });
  }

  Future<void> _showChoiceDialog(BuildContext context,ConfigModel configModel) {

    Dialog fancyDialog = Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
        ),
        height: 190,
        child: Stack(
          children: <Widget>[
            Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 60.0,bottom: 10.0,left:10.0,right:5.0),

                decoration: BoxDecoration(
                  color: Colors.grey[100],
                  borderRadius: BorderRadius.circular(12.0),
                ),
                child:SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      Divider(
                        thickness: 1.0,
                        color: Colors.black12,
                      ),
                      SizedBox(height: 10),
                      GestureDetector(
                        child: Text(allTranslations.text('galerie_select')),
                        onTap: () {
                          Navigator.pop(context, true);
                          _openGallery();
                        },
                      ),
                      SizedBox(height: 10),
                      Divider(
                        thickness: 1.0,
                        color: Colors.black12,
                      ),
                      SizedBox(height: 10),
                      GestureDetector(
                        child: Text(allTranslations.text('camera_select')),
                        onTap: () {
                          Navigator.pop(context, true);
                          getImageFromCamera();
                        },
                      ),
                      SizedBox(height: 10),
                      Divider(
                        thickness: 1.0,
                        color: Colors.black12,
                      ),
                    ],
                  ),
                )
            ),
            Container(
              width: double.infinity,
              height: 50,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: FunctionUtils.colorFromHex(configModel.mainColor),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12),
                  topRight: Radius.circular(12),
                ),
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(allTranslations.text('type_select'),
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17.0,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),


          ],
        ),
      ),
    );
    showDialog(
        context: context, builder: (BuildContext context) => fancyDialog);
  }

  _getScoopList(int userId){

    //recuperation des produits de la personne
    _localScoopRepository.listScoop(userId).then((response) {

      int taille = response.length;
     //taille = 0;
      if(taille > 0 ){
        setState(() {
          _haveEop = true;
          _visibleScoop=true;
          _allLocalScoopEntity = response;
        });
      }else{
        _getServerScoop (null,0);
      }
    });

  }

  _getUserInformation(){

    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userId="${value.id}";
        _userToken="${value.token}";
      });
      _getScoopList(value.id);
    });

    if(_type.compareTo("0")!=0 && _type.compareTo("-1")!=0){
        //verifier si cette annonce existe deja
        _localAnnonceRepository.existAnnonce(int.parse((_type))).then((infoAnnonce) {

          if(infoAnnonce.length>0){

            _pprixController.text="${infoAnnonce[0].prix}";
            _qteController.text="${infoAnnonce[0].stock}";
            _lieuController.text="${infoAnnonce[0].lieu}";
            _infoController.text="${infoAnnonce[0].info}";
            _conditionController.text="${infoAnnonce[0].expedition}";

            _categorieAnnonce="${infoAnnonce[0].categorieId}";
            _uniteAnnonce="${infoAnnonce[0].unitesId}";
            _typeAnnonce="${infoAnnonce[0].biztypeId}";
            _dispoAnnonce="${infoAnnonce[0].disponibilite}";
            _indivisibiliteAnnonce="${infoAnnonce[0].indivisibilite}";
            _titleAnnonce="${infoAnnonce[0].title}";

            setState(() {
              _apiId="${infoAnnonce[0].apiId}";
            });
            if(infoAnnonce[0].disponibilite.compareTo(2)==0){
              setState(() {
                _visibleDate=true;
              });
            }

            _localBiztypeRepository.oneBiztype(_typeAnnonce).then((response){
              bizKey.currentState.setSelectedItem(response);
            });

            String namedispo="${allTranslations.text('disponibilite$_dispoAnnonce')}";
            disponibilityKey.currentState.setSelectedItem(namedispo);

            String nameindivi="${allTranslations.text('indivisibilite$_indivisibiliteAnnonce')}";
            indivisibiliteKey.currentState.setSelectedItem(nameindivi);

            _localProduitRepository.oneProduit(_categorieAnnonce).then((response){
              produitKey.currentState.setSelectedItem(response);
            });

            _localSousProduitRepository.oneSousProduit("${infoAnnonce[0].categorieId}",_titleAnnonce).then((response){
              sousproduitKey.currentState.setSelectedItem(response);
            });

            _localUniteRepository.oneUniteProduit(_uniteAnnonce).then((response){
              uniteproduitKey.currentState.setSelectedItem(response);
            });

            if(infoAnnonce[0].picture.compareTo("")!=0){
              setState(() {
                _serverImage =DataConstantesUtils.IMAGE_ANNONCE_URL + "" + infoAnnonce[0].picture;
              });

            }
          }


        });
    }


  }


  _publishAnnonce(ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(_apiId.compareTo("0")==0?allTranslations.text('publish_loading'):allTranslations.text('update_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });
    Api api = ApiRepository();
    api.sendAnnonce(_annonceDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          if(a.status.compareTo("ok")==0){


              List<AnnonceModel> allAnnonce=[];
              allAnnonce.add(a.annonce);
              FunctionUtils.saveAnnonce(allAnnonce).then((value){

              Timer(Duration(seconds: 2),(){
                Navigator.of(context).pop(null);
                if(_type.compareTo("-1")==0){
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => MyAnnoncePage("-1",0,0,_biztypeId)),
                  );
                }else if(_type.compareTo("0")==0){
                    Navigator.of(context).pop("added");
                }else {
                    Navigator.of(context).pop("updated");
                }
              });


            });

            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });

  }

  _getServerScoop (ConfigModel configModel,int typeRequest) async {

    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    ScoopDto scoopDto=ScoopDto();
    scoopDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getScoop(scoopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          FunctionUtils.saveScoop(a.mesScoop,int.parse(_userId));

          Timer(Duration(seconds: 2),(){

            _localScoopRepository.listScoop(int.parse(_userId)).then((response) {
              int taille = response.length;
              //taille = 0;
              if(taille >0 ){
                setState(() {
                  _haveEop=true;
                  _visibleScoop=true;
                  _allLocalScoopEntity = response;
                });

              }else{
                setState(() {
                  _haveEop=true;
                  _visibleScoop=false;
                });
              }

            });

            if(typeRequest.compareTo(1)==0) {
              Navigator.of(context).pop(null);
            }
          });

          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        setState(() {
          _haveEop=true;
        });
        return false;
      }
    });


  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}

