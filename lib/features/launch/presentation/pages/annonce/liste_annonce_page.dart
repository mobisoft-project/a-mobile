
import 'dart:async';

import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/add_annonce_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/filter_annonce_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/minimal_annonce_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/commande/checkout_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/commande/cart_page.dart';
import 'package:agrimobile/features/launch/presentation/widgets/liste_annonce_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:badges/badges.dart';
import 'package:agrimobile/features/common/domain/repositories/local_scoop_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';

import 'package:agrimobile/features/launch/presentation/pages/pret/liste_scoop_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';
import 'package:agrimobile/features/common/domain/repositories/local_menu_repository.dart';
import 'package:agrimobile/features/common/data/models/filter_product_model.dart';

import 'package:agrimobile/features/common/data/models/user_information_model.dart';

import 'package:agrimobile/features/common/data/models/categorie_produit_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_categorie_produit_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_categorie_produit_entity.dart';

class MyAnnoncePage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String type_page;
  int id_user;
  int id_scoop;
  int biztypeId;

  MyAnnoncePage(this.type_page,this.id_user, this.id_scoop,this.biztypeId);

  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState(this.type_page,this.id_user,this.id_scoop,this.biztypeId);
  }
}



class PageState extends State<MyAnnoncePage> with SingleTickerProviderStateMixin {

  String _typePage;
  int _idUser;
  int _idScoop;
  int _biztypeId;
  String token="";

  PageState(this._typePage,this._idUser,this._idScoop,this._biztypeId);

  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  LocalScoopRepository _localScoopRepository = LocalScoopRepository();
  LocalCategorieProduitRepository _localCategorieProduitRepository = LocalCategorieProduitRepository();

  LocalMenuRepository _localMenuRepository = LocalMenuRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  List<LocalAnnonceEntity> _allLocalAnnonceEntity=[];
  List<LocalAnnonceEntity> _allLocalAnnonceEntityAchat=[];
  List<LocalCategorieProduitEntity> _listCategorieEntity=[];

  ResponseFilterModel responseFilterModel;

  // List<int> selectedProducts = new List<int>();
  // List<int> selectedSousProducts= new List<int>();

  List<FilterProduct> selectedCategories = new List<FilterProduct>();
  List<FilterProduct> selectedProducts = new List<FilterProduct>();
  List<FilterProduct> selectedSousProducts= new List<FilterProduct>();

  ScrollController scrollController = ScrollController();
  int currentCategoryIndex = 0;
  ScrollController headerScrollController = ScrollController();

  List<ShopModel> shopList = [];


  double lowerValue=50;
  double upperValue=350;
  int disponibilite = 0;
  bool filterPrix = false;
  bool isFilter = false;
  double max = 100.0;
  double min = 50;

  LocalScoopEntity _infoScoop = LocalScoopEntity.create(idScoop:0,roleMember: "1",typeAssociation: "1",idAssociation: 1,nameAssociation: "",cantonAssociation: "",adresseAssociation: "");


  //static const venteBiztypeId = 1;
  //static const achatBiztypeId = 2;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TabController _tabController;
  List<Tab> _myTabs;

  List<String> areaListData = <String>[];

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  bool _haveAnnonce=false;
  bool _haveAnnonceAchat=false;
  String _userKey="";
  String _idSearch="0";
  int _nbreCart = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);


    _getUserInformation();
    WidgetsBinding.instance.addPostFrameCallback((_) =>_showMessage() );


  }



  double oneItemHeight = 0;


  double getShopListPosition(double val, int index){
    // val * (shopList[index].products.length / ShopHelper.GRID_COLUMN_VALUE) + shopList[index - 1].position;
  }


  void _showMessage(){
    if(_typePage.compareTo("-1")==0){
      _displaySnackBar(context, allTranslations.text('annonce_success'));
    }
  }


  @override
  Widget build(BuildContext context) => Consumer3<ConfigModel, CartModel,UserInformationModel>(
    builder: (context, configModel, cartModel,userConnected, child) {

      _myTabs = <Tab>[
        new Tab(
            text: "${allTranslations.text('menu_vente')}"
        ),
        new Tab(text: "${allTranslations.text('menu_achat')}"),
      ];
        _nbreCart = cartModel.getCount();
      return Scaffold(
          key: widget._scaffoldKey,
          // floatingActionButton: widget.type_page.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE)!=0 &&   widget.type_page.compareTo(DataConstantesUtils.ANNONCE_MES_CONTACTS)!=0?FloatingActionButton(
          //floatingActionButton: widget.type_page.compareTo(DataConstantesUtils.ANNONCE_VENDEUR)==0 || widget.type_page.compareTo(DataConstantesUtils.ANNONCE_ACHAT)==0?FloatingActionButton(
          floatingActionButton:FloatingActionButton(
            onPressed: () {


              LocalMenuRepository _localMenuRepository = LocalMenuRepository();
              _localMenuRepository.existMenu("M005").then((response){

                if(response!=null && response.length>0) {

                  AppSharedPreferences appSharedPreferences = AppSharedPreferences();
                  if(userConnected.id.compareTo(-1)==0) {
                    //recuperation du menu et comparer son type
                    if(response[0].typeMenu.compareTo(2)==0 || response[0].typeMenu.compareTo(3)==0){

                      appSharedPreferences.setRoute("M005","${response[0].typeMenu}",0).then((value){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LoginPage("0")),
                        );
                      });

                    }else{
                      _runMenu();
                    }

                  }else{
                    if(userConnected.validationUser.compareTo("1")==0) {
                      _runMenu();
                    }else{
                      if(response[0].typeMenu.compareTo(2)==0){
                        //message de waiting validation
                        _displaySnackBar(context, allTranslations.text('waiting_validation'));
                      }else {
                        _runMenu();
                      }
                    }
                  }
                }

              });
            },
            child: Icon(Icons.add,color: Colors.white),
            backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
          ),
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 80.0)),

          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(
                  _idSearch.compareTo(DataConstantesUtils.ANNONCE_MES_CONTACTS)==0?
                  allTranslations.text('menu_contact'):_idSearch.compareTo(DataConstantesUtils.ANNONCE_MARCHE)==0?
                  allTranslations.text('menu_marche'):_idSearch.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE)==0?
                  "${_infoScoop.nameAssociation}: ${_infoScoop.cantonAssociation}":_idSearch.compareTo(DataConstantesUtils.ANNONCE_ACHAT)==0?
                  allTranslations.text('menu_achat'): allTranslations.text('menu_vendeurs'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,

                  ),
                ),
              ),
            ),
            actions: <Widget>[
              isFilter==true?Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _resetFilter("10");
                    },
                    child: Icon(
                      Icons.close,
                      size: 26.0,
                    ),
                  )
              ):Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _getServerOffre (configModel);
                    },
                    child: Icon(
                      Icons.refresh,
                      size: 26.0,
                    ),
                  )
              ),
              /* _nbreCart > 0 ? Padding(
                  padding: EdgeInsets.only(right: 15.0),
                  child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => CheckoutPage()),
                        ).then((value) => {
                            setState(() {
                              _nbreCart = cartModel.getCount();
                            })
                        });
                      },
                      child: Badge(
                        position: BadgePosition.topEnd(top: 5, end: -5),
                        badgeContent: Text(_nbreCart.toString(), style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                        child: Icon(Icons.shopping_cart_outlined),
                        )
                  )
              ): */Padding(
                  padding: EdgeInsets.only(right: 5.0),
                  child: GestureDetector(
                      onTap: () {
                        UrlLauncher.launch('tel:+${configModel.callCenter}');
                      },
                      child:Icon(Icons.phone,color: Colors.white,),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,

            bottom: _getSearchView(configModel,_allLocalAnnonceEntity,_userKey,widget._scaffoldKey,_idSearch,_infoScoop,widget.biztypeId,_typePage),

          ),
          backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        height: 50,
                        child: ListView.builder(
                          itemCount: shopList.length,
                          controller: headerScrollController,
                          padding: EdgeInsets.all(10),
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) => buildPaddingHeaderCard(configModel,index),
                        )
                    ),
                    Divider(
                      height: 0,
                    ),
                    Expanded(
                        child:  _getAnnonceListview(cartModel,configModel,widget.biztypeId)
                    )
                  ],
                );
              }
          )
      );
    },
  );

  Padding buildPaddingHeaderCard(ConfigModel configModel,int index) {

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: RaisedButton(
        elevation: 0.4,
        textColor: shopList[index].selected==true ?Colors.white:FunctionUtils.colorFromHex(configModel.mainColor),
        color: shopList[index].selected==true ?FunctionUtils.colorFromHex(configModel.mainColor):Colors.white ,
        onPressed: () => headerListChangePosition(index),
        child: Text("${shopList[index].name}"),
        shape: StadiumBorder(),
      ),
    );
  }

  void headerListChangePosition(int index) {
    // scrollController.animateTo(shopList[index].position,duration: Duration(seconds: 1), curve: Curves.ease);

    _resetFilter("20");
        setState(() {

          if(shopList[index].apiId.compareTo(0)==0){
            shopList.forEach((element) =>element.selected = false);
          }else{
            shopList[0].selected = false;
          }
          shopList[index].selected = !shopList[index].selected;
          // selectedCategories[index].isCheck = !selectedCategories[index].isCheck;
        });

    _updateSelectedCategories();
    _getFilterByCategoryList();
  }

  _updateSelectedCategories(){
    selectedCategories=[];
    for(int i=0; i < shopList.length; i++){

      if(shopList[i].selected==true){

        if(shopList[i].apiId.compareTo(0)==1){
          FilterProduct filterProduct = FilterProduct(shopList[i].name, false,shopList[i].apiId);
          setState(() {
             selectedCategories.add(filterProduct);
          });
        }
      }
    }
  }

  _getFilterByCategoryList(){
    String categories = _getSelectedCategoriesId();

    _localAnnonceRepository.listAnnonceCategories(_typePage,"${_idUser}",_idScoop,widget.biztypeId,categories).then((response) {
          setState(() {
            _haveAnnonce = true;
            _allLocalAnnonceEntity = response;
            _allLocalAnnonceEntityAchat = response;
          });
      });
  }

  _resetFilter(String type) {
      setState(() {
        lowerValue = min*2;
        upperValue = max/2;
        disponibilite = 0;
        isFilter=false;
        selectedCategories.clear();
        selectedProducts.clear();
        selectedSousProducts.clear();

        if(type.compareTo("10")==0) {
          shopList.forEach((element) => element.selected = false);
          shopList[0].selected = true;

          _getAnnonceList(_typePage, _idSearch, _idScoop);
        }
      });
  }

  _getSearchView(ConfigModel configModel ,List<LocalAnnonceEntity> allLocalAnnonceEntity,String _userKey, GlobalKey<ScaffoldState> scaffoldKey, String _idSearch, LocalScoopEntity infoScoop, int biztype, String type){
    return  PreferredSize(
              preferredSize: Size(double.infinity, 60.0),
              child: Card(
                margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                elevation: 2.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(2.0)),
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                          child: TextField(
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText:   allTranslations.text('search'),
                            ),
                            onChanged: (String place) {
                              print(place);
                              _showSearch(place);
                              //return Search(allLocalAnnonceEntity, _userKey, _scaffoldKey, _idSearch, infoScoop, biztype, type,place);
                            },
                            onSubmitted: (String place) {},
                          )),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => FilterAnnoncePage(selectedCategories,selectedProducts,selectedSousProducts,lowerValue,upperValue,disponibilite,min,max)),
                          ).then((value) => {
                          if(value!=null){
                            setState(() {
                              isFilter = true;
                              responseFilterModel = value;
                              selectedCategories = responseFilterModel.selectedCategories;
                              selectedProducts = responseFilterModel.selectedProducts;
                              selectedSousProducts = responseFilterModel.selectedSousProducts;
                              lowerValue = responseFilterModel.lowerValue;
                              upperValue = responseFilterModel.upperValue;
                              disponibilite = responseFilterModel.disponibilite;
                              filterPrix = responseFilterModel.filterPrix;


                              print(selectedCategories.toString());
                              print(selectedProducts.toString());
                              print(selectedSousProducts.toString());
                              print(upperValue);
                              print(lowerValue);
                              print(disponibilite);
                              print(filterPrix);

                              _updateCategorieListDatas();
                              _getFilterDatas();
                            })
                          }
                          });
                        },
                        child: Container(
                          child: Icon( Icons.filter_list,
                            color: FunctionUtils.colorFromHex(configModel.mainColor)),
                        ),
                      )
                    ],
                  ),
                ),
              )
    );
  }

  _updateCategorieListDatas(){
    if(selectedCategories.length > 0){
      setState(() {
        shopList.forEach((element) =>element.selected = false);
      });

      List<int> filterCategories = _getSelectedFilterCategoriesId(selectedCategories);
      for(int i=0; i < shopList.length; i++){
          if(filterCategories.contains(shopList[i].apiId)){
              setState(() {
                shopList[i].selected=true;
              });
          }
      }

    }

  }

  _getFilterDatas(){

    String categories = _getSelectedProductsId(selectedCategories);
    String produits = _getSelectedProductsId(selectedProducts);
    String sousproduits = _getSelectedProductsName(selectedSousProducts);

    _localAnnonceRepository.userfilterlistAnnonce(categories,produits,sousproduits,lowerValue,upperValue,disponibilite, widget.biztypeId,filterPrix).then((response) {
      // if(response.length>0 ){
        setState(() {
          _allLocalAnnonceEntity = response;
        });
      // }
    });
  }

  _getSelectedCategoriesId(){
    String response="";

    for(int i=0; i < shopList.length; i++){

      if(shopList[i].selected == true) {
          if (response.compareTo("") == 1) response += ",";
            response += shopList[i].apiId.toString();
        }
    }
    if(response.compareTo("")==0){
      setState(() {
        shopList[0].selected = true;
      });
    }
    return response;
  }


  List<int> _getSelectedFilterCategoriesId(List<FilterProduct> tab){

    List<int> response=[];
    for(int i=0; i < tab.length; i++){
      response.add(tab[i].apiId);
    }
    return response;
  }

  _getSelectedProductsId(List<FilterProduct> tab){

    String response="";
    for(int i=0; i < tab.length; i++){
      if(response.compareTo("")==1)response+=",";
      response+=tab[i].apiId.toString();
    }
    return response;
  }

  _getSelectedProductsName(List<FilterProduct> tab){
    String response="";
    for(int i=0; i < tab.length; i++){
      if(response.compareTo("")==1)response+=",";
      response+= "'"+tab[i].name+"'";
    }
   return response;
  }


  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
          setState(() {
            _userKey="${value.id}";
            token="${value.token}";
          });
    });

    String tamponId=_typePage;
    //recuperation de l'id de la personne
    if(tamponId.compareTo("-1")==0 || tamponId.compareTo("0")==0){
      tamponId="0";
    }
    setState(() {
      _idSearch=tamponId;
    });

    if(_typePage.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE)==0) {
      _localScoopRepository.existScoop(_idScoop).then((response) {
        //calcul de la date expiration
        if (response.length > 0) {
          setState(() {
            _infoScoop = response[0];
          });
        }
      });
    }
    _getAnnonceList(_typePage,"${_idUser}",_idScoop);
    _getCategorieList();

  }

  _getCategorieList(){
    shopList=[];
    _localCategorieProduitRepository.allCategories().then((categories) {

        ShopModel defaultShopModel = ShopModel(apiId:0,name:"Toutes", selected: true);
        setState(() {
          shopList.add(defaultShopModel);
        });
        if(categories.length> 0 ){
              for(int i=0; i < categories.length; i++){

                ShopModel shopModel = ShopModel(apiId: categories[i].apiId,name: categories[i].name, selected: false);

                setState(() {
                  shopList.add(shopModel);
                });
              }
        }
     });
  }


  _getAnnonceList(String typePage,String userId,int idScoop){
    //1. vente
    //2. achat
    //recuperation des produits de la personne

    _localAnnonceRepository.listAnnonce(typePage,userId,idScoop,widget.biztypeId).then((response) {
      if(response.length>0 ){
        print("vente");
        setState(() {
          _haveAnnonce = true;
          _allLocalAnnonceEntity = response;
          _allLocalAnnonceEntityAchat = response;
        });
      }
    });

    _localAnnonceRepository.getMaxPrixAnnonce(userId,idScoop,widget.biztypeId).then((response) {
      if(response.length>0 ){
        setState(() {
          int value = int.parse(response[0].prix.toString());
          print(value);
          max = 1.0*value;
          lowerValue = min*2;
          upperValue = max/2;
          print("upperValue");
          print(max);
        });
      }
    });

    /* _localAnnonceRepository.listAnnonce(typePage,userId,idScoop,widget.biztypeId).then((response) {
       if(response.length>0 ){
         print("achat");
         setState(() {
           _haveAnnonceAchat = true;
           _allLocalAnnonceEntityAchat = response;
         });
       }
    }); */

  }

  _getEmptyView(){
    return Center(
      child: Column(
        children: [
         // SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          SizedBox(height: 0,),
          Container(
            width:300,
            child: Text(
              allTranslations.text('searching_aempty'),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.bold,
                fontSize: 22.0,
              ),
            ),
          )
        ],
      ),
    );
  }

  _getMinAnnonce(ConfigModel configModel, LocalScoopEntity infoScoop, String type){
    if(_allLocalAnnonceEntity!=null && _allLocalAnnonceEntity.length>0) {
      var size = MediaQuery.of(context).size;

      /*24 is for notification bar on Android*/
      final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
      final double itemWidth = size.width / 2;
      print(itemWidth / itemHeight);

      return Padding(
        padding: const EdgeInsets.only(left: 0.0, right: 0.0),
        child: GridView(

          padding: const EdgeInsets.only(
              left: 10, right: 10, top:16, bottom: 16),
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          children: List<Widget>.generate(
            _allLocalAnnonceEntity.length,
                (int index) {
              LocalAnnonceEntity infoAnnonce = _allLocalAnnonceEntity[index];
              return MinimalAnnoncePage(
                image: infoAnnonce.picture,
                infoAnnonce: infoAnnonce,
                infoScoop:infoScoop,
                typePage: type,
                biztypeId: widget.biztypeId,
                onrefresh: (){
                  _localAnnonceRepository.listAnnonce(_typePage,"${_idUser}",_idScoop,widget.biztypeId).then((response) {
                    setState(() {
                      _allLocalAnnonceEntity = response;
                    });
                  });
                },
              );
            },
          ),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 12.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 0.70,

          ),
        ),
      );

    }else{
      return Container();
    }
  }

  _getListView(CartModel cartModel, int biztype){
    return Container(
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
        child: ListeAnnonceWidget(
          allAnnonce: biztype.compareTo(widget.biztypeId)==0?_allLocalAnnonceEntity:_allLocalAnnonceEntityAchat,
          scaffoldKey:  widget._scaffoldKey,
          userKey: _userKey,
          idSearch: _idSearch,
          scoop: _infoScoop,
          typePage: _typePage,
          biztype: biztype,
          onrefresh: (){
            setState(() {
              _nbreCart = cartModel.getCount();
            });
          },
        )
    );
  }

  _getAnnonceListview(CartModel cartModel, ConfigModel configModel, int biztype){


      if (_allLocalAnnonceEntity.length==0 || _haveAnnonce==false){
         return _getEmptyView();
      }else{
          //return _getListView(cartModel,biztype);
          return _getMinAnnonce(configModel, _infoScoop,_typePage);
      }

  }

  _runMenu() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AddAnnoncePage("0",widget.biztypeId)),
    ).then((value){
      if(value!=null){

        _localAnnonceRepository.listAnnonce(_typePage,"${_idUser}",_idScoop,widget.biztypeId).then((response) {
          setState(() {
            _haveAnnonce=true;
            _allLocalAnnonceEntity = response;
          });
          if(value.compareTo("updated")==0){
            _displaySnackBar(context, allTranslations.text('uannonce_success'));
          }else{
            _displaySnackBar(context, allTranslations.text('annonce_success'));
          }
        });
      }
    });
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }


  _getServerOffre (ConfigModel configModel) async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('refresh_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    _localAnnonceRepository.allAnnonce(0).then((response) {

          Api api = ApiRepository();
          api.getAnnonce(token).then((value) {
            if (value.isRight()) {
              value.all((a) {
                FunctionUtils.saveAnnonce(a);

                Timer(Duration(seconds: 2),(){
                  _localAnnonceRepository.listAnnonce(_typePage,"${_idUser}",_idScoop,widget.biztypeId).then((response) {
                    if(response.length>0 ){
                      setState(() {
                        _haveAnnonce=true;
                        _allLocalAnnonceEntity = response;
                      }); 
                    }
                  });
                  Navigator.of(context).pop(null);
                });

                return true;
              });
            } else {
              Navigator.of(context).pop(null);
              return false;
            }
          });
    });
  }


  _showSearch(String query){
    List<LocalAnnonceEntity> suggestionListAll;
    List<LocalAnnonceEntity> suggestionList;
    GlobalKey<ScaffoldState> scaffoldKey;
    String _userKey;
    String _idSearch;
    LocalScoopEntity _localScoopEntity;
    int _biztype;
    String _type;


    suggestionListAll=_allLocalAnnonceEntityAchat;
    suggestionList=[];
    String search=query.toLowerCase();
    print(search);
    search.isEmpty
        ? suggestionList = suggestionListAll //In the true case
        : suggestionList.addAll(suggestionListAll.where(
          (element) => element.nomCategorieFilleProduit.toLowerCase().contains(search) || element.title.toLowerCase().contains(search) ||  element.lieu.toLowerCase().contains(search) || "${element.prix}".toLowerCase().contains(search)
          || "${element.stock}".toLowerCase().contains(search) || element.uniqueCode.toLowerCase().contains(search),
    ));

    print(suggestionList.toString());
    setState(() {
      _allLocalAnnonceEntity=suggestionList;
    });
  }

}


class Search extends SearchDelegate {

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container(
      child: Center(
        child: Text(""),
      ),
    );
  }

  final List<LocalAnnonceEntity> listExample;
  List<LocalAnnonceEntity> suggestionList;
  GlobalKey<ScaffoldState> scaffoldKey;
  String _userKey;
  String _idSearch;
  LocalScoopEntity _localScoopEntity;
  int _biztype;
  String _type;
  String query;

  Search(this.listExample,this._userKey,this.scaffoldKey,this._idSearch,this._localScoopEntity,this._biztype,this._type,this.query): super(searchFieldLabel: allTranslations.text('search'));


  @override
  Widget buildSuggestions(BuildContext context) {

    suggestionList=[];
    String search=query.toLowerCase();
    print(search);
    search.isEmpty
        ? suggestionList = listExample //In the true case
        : suggestionList.addAll(listExample.where(
          (element) => element.nomCategorieFilleProduit.toLowerCase().contains(search) || element.title.toLowerCase().contains(search) ||  element.lieu.toLowerCase().contains(search) || "${element.prix}".toLowerCase().contains(search)
              || "${element.stock}".toLowerCase().contains(search) || element.uniqueCode.toLowerCase().contains(search),
    ));
    print("12345");
    print(search);
    print(suggestionList.toString());
    if (suggestionList==null || suggestionList.length==0){
      return  Center(
        child: Column(
          children: [
            SizedBox(height: 100,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_aempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else {

        var size = MediaQuery.of(context).size;

      /*24 is for notification bar on Android*/
      final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
      final double itemWidth = size.width / 2;
      print(itemWidth / itemHeight);

      return Padding(
        padding: const EdgeInsets.only(left: 0.0, right: 0.0),
        child: GridView(

          padding: const EdgeInsets.only(
              left: 10, right: 10, top:16, bottom: 16),
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          children: List<Widget>.generate(
            suggestionList.length,
                (int index) {
              LocalAnnonceEntity infoAnnonce = suggestionList[index];
              return MinimalAnnoncePage(
                image: infoAnnonce.picture,
                infoAnnonce: infoAnnonce,
                infoScoop:_localScoopEntity,
                typePage: _type,
                biztypeId: _biztype,
              );
            },
          ),
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 12.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 0.70,

          ),
        ),
      )

      /*return Container(
          decoration: BoxDecoration(
            color: FunctionUtils.colorFromHex("DDDDDD")
          ),
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          child: ListeAnnonceWidget(
            allAnnonce:suggestionList,
            userKey: _userKey,
            scaffoldKey:  scaffoldKey,
            idSearch: _idSearch,
            scoop: _localScoopEntity,
            typePage: _type,
            biztype: _biztype,
          )
      )*/
      ;

    }
  }

}

class ShopModel {
   int apiId;
   String name;
   bool selected;

  ShopModel({this.apiId,this.name, this.selected});
}



