
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/launch/presentation/widgets/annonce_widget.dart';
import 'package:agrimobile/features/launch/presentation/pages/commande/checkout_page.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:badges/badges.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';

class DetailAnnoncePage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final int idAnnonce;
  final int biztypeId;
  final LocalScoopEntity infoScoop;
  final String typePage;

  DetailAnnoncePage(this.idAnnonce,this.infoScoop, this.typePage,this.biztypeId);

  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState(this.idAnnonce);
  }
}


class PageState extends State<DetailAnnoncePage> {

  int _idAnnonce;
  PageState(this._idAnnonce);

  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalAnnonceEntity _localAnnonceEntity;
  bool _haveAnnonce=false;
  String _userKey="";
  int _nbreCart = 0;

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }


  @override
  Widget build(BuildContext context) => Consumer2<ConfigModel, CartModel>(
    builder: (context, configModel, cartModel, child) {
      _nbreCart = cartModel.getCount();
      return Scaffold(
          key: widget._scaffoldKey,

          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_marche_details'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,

                  ),
                ),
              ),
            ),
            actions: <Widget>[
              /* _nbreCart > 0 ? Padding(
                  padding: EdgeInsets.only(right: 15.0),
                  child: GestureDetector(
                      /* onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => CheckoutPage()),
                        ).then((value) => {
                          setState(() {
                            _nbreCart = cartModel.getCount();
                          })
                        });
                      }, */
                      /* child: Badge(
                        position: BadgePosition.topEnd(top: 5, end: -5),
                        badgeContent: Text(_nbreCart.toString(), style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                        //child: Icon(Icons.shopping_cart_outlined),
                      ) */
                  )
              ): */Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      UrlLauncher.launch('tel:+${configModel.callCenter}');
                    },
                    child: Icon(Icons.phone,color: Colors.white,),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return  _showDetail();
              }
          )
      );
    },
  );

  

  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
          setState(() {
            _userKey="${value.id}";
          });
    });

    //recuperation des produits de la personne
    _localAnnonceRepository.existAnnonce(_idAnnonce).then((response) {

      if(response.length>0 ){
        setState(() {
          _haveAnnonce = true;
          _localAnnonceEntity = response[0];
        });

      }else{
        Navigator.of(context).pop(null);
      }

    });


  }


  _showDetail(){

    if (_localAnnonceEntity==null  || _haveAnnonce==false){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 100,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_aempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else{


      return Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          height: 550,
          child: AnnonceWidget(
            infoAnnonce:_localAnnonceEntity,
            scaffoldKey:  widget._scaffoldKey,
            userKey: _userKey,
            typeAssociation: widget.infoScoop.typeAssociation,
            typePage: widget.typePage,
            userScoopRole:widget.infoScoop.roleMember,
            biztypeId:widget.biztypeId,
            onrefresh: (){
              _localAnnonceRepository.existAnnonce(_idAnnonce).then((response) {

                if(response.length>0 ){
                  setState(() {
                    _haveAnnonce = true;
                    _localAnnonceEntity = response[0];
                  });

                }else{
                  Navigator.of(context).pop(null);
                }

              });
            },
          )
      );



    }
  }




}
