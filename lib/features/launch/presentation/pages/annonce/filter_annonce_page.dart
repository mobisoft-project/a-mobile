import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:agrimobile/features/common/data/models/cart_item_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/filter_product_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_sous_produit_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_produit_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;
import 'package:agrimobile/features/launch/presentation/pages/annonce/liste_data_page.dart';
import 'package:agrimobile/features/common/data/models/filter_product_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_sous_produit_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_produit_repository.dart';

class FilterAnnoncePage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  //
  // List<int> selectedProducts = new List<int>();
  // List<int> selectedSousProducts= new List<int>();

  List<FilterProduct> selectedCategories;
  List<FilterProduct> selectedProducts;
  List<FilterProduct> selectedSousProducts;
  double lowerValue;
  double upperValue;
  int disponibilite;
  double max;
  double min;

  FilterAnnoncePage(this.selectedCategories,this.selectedProducts, this.selectedSousProducts, this.lowerValue, this.upperValue, this.disponibilite, this.min,this.max);

  List<CartItemModel> allItems;
  double total = 0;

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}

class PageState extends State<FilterAnnoncePage> {
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalProduitRepository _localProduitRepository=LocalProduitRepository();
  LocalSousProduitRepository _localSousProduitRepository=LocalSousProduitRepository();
  String _userKey = "";
  // List<RangeSliderData> rangeSliders;

  //
  // int disponibilite = 0;

  double _lowerValue;
  double _upperValue;
  bool showValueIndicator = true;
  bool filterPrix = false;
  int valueIndicatorMaxDecimals;
  bool forceValueIndicator = false;
  Color overlayColor;
  Color activeTrackColor;
  Color inactiveTrackColor;
  Color thumbColor;
  Color valueIndicatorColor;
  Color activeTickMarkColor;



  String selectedCategoriesName="";
  String selectedProductsName="";
  String selectedSousProductsName="";

  List<RadioModel> disponibilites = new List<RadioModel>();
  List<FilterProduct> selectedDatas = new List<FilterProduct>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("sampleData.length");
    disponibilites.add(
        new RadioModel(1,false, "${allTranslations.text('disponibilite1')}"));
    disponibilites.add(  new RadioModel(2,false, "${allTranslations.text('disponibilite2')}"));

    _getSelectedProductsName("0",widget.selectedCategories);
    _getSelectedProductsName("10",widget.selectedProducts);
    _getSelectedProductsName("20",widget.selectedSousProducts);

    if(widget.disponibilite.compareTo(0)!=0){
      disponibilites.forEach((element) {
        element.isSelected=false;
        if(element.id == widget.disponibilite)element.isSelected=true;
      });
    }



    _lowerValue =  widget.lowerValue;
    _upperValue = widget.upperValue;


    print(widget.max);
    print(widget.min);
    print(widget.upperValue);
    print(_lowerValue);
    print(_upperValue);

  }

  @override
  Widget build(BuildContext context) => Consumer2<ConfigModel, CartModel>(
          builder: (context, configModel, cartModel, child) {
        widget.allItems = cartModel.allItems;
        widget.total = cartModel.total;

        return Scaffold(
            key: widget._scaffoldKey,
            appBar: new AppBar(
              centerTitle: true,
              title: Text(
                allTranslations.text('filter'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
                actions: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(right: 20.0),
                      child: GestureDetector(
                        onTap: () {
                          _refreshData();
                        },
                        child: Icon(
                          Icons.clear,
                          size: 26.0,
                        ),
                      )
                  ),
                ],
              backgroundColor:
                  FunctionUtils.colorFromHex(configModel.mainColor),
              elevation: 0.0,
            ),
            backgroundColor: FunctionUtils.colorFromHex("#F2F2F2"),
            body: Builder(builder: (BuildContext myContext) {
              RangeValues _currentRangeValues = const RangeValues(40, 80);

              return Column(children: <Widget>[
                Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    children: [
                      ListTile(
                        title: Text(
                          "${allTranslations.text('prixunitaire')} ( ${configModel.devise} )",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0,
                              color: Colors.black87),
                        ),
                      ),
                      Card(
                          elevation: 1.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                          ),
                          margin: EdgeInsets.zero,
                          child: Padding(
                              padding: EdgeInsets.all(20.0),
                              child: Container(
                                width: double.infinity,
                                child: new Row(
                                  children: <Widget>[
                                    new Container(
                                      constraints: new BoxConstraints(
                                        minWidth: 40.0,
                                        maxWidth: 80.0,
                                      ),
                                      child: new Text(get_value(_lowerValue),
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16.0,
                                              color: Colors.black54)),
                                    ),
                                    new Expanded(
                                      child: SliderTheme(
                                        // Customization of the SliderTheme
                                        data: SliderTheme.of(context).copyWith(
                                          overlayColor: FunctionUtils.colorFromHex(
                                              configModel.mainColor),
                                          activeTickMarkColor: activeTickMarkColor,
                                          activeTrackColor:
                                          FunctionUtils.colorFromHex(
                                              configModel.mainColor),
                                          // inactiveTrackColor: inactiveTrackColor,
                                          inactiveTrackColor:
                                          FunctionUtils.colorFromHex("BABABA"),
                                          trackHeight: 3.0,
                                          thumbColor: FunctionUtils.colorFromHex(
                                              configModel.mainColor),
                                          valueIndicatorColor:
                                          FunctionUtils.colorFromHex(
                                              configModel.mainColor),
                                        ),
                                        child: frs.RangeSlider(
                                          min: widget.min,
                                          max: widget.max,
                                          lowerValue:   _lowerValue,
                                          upperValue: _upperValue,
                                          divisions: 20,
                                          showValueIndicator: true,

                                          /*valueIndicatorFormatter: (int index, double value) {
                                      String twoDecimals = value.toStringAsFixed(2);
                                      return '$twoDecimals mm';
                                    },*/
                                          valueIndicatorMaxDecimals: 0,
                                          onChanged: (double newLowerValue,
                                              double newUpperValue) {
                                            setState(() {
                                              _lowerValue = newLowerValue;
                                              _upperValue = newUpperValue;
                                              filterPrix=true;
                                            });
                                          },
                                          onChangeStart: (double startLowerValue,
                                              double startUpperValue) {
                                            print(
                                                'Started with values: $startLowerValue and $startUpperValue');
                                          },
                                          onChangeEnd: (double newLowerValue,
                                              double newUpperValue) {
                                            print(
                                                'Ended with values: $newLowerValue and $newUpperValue');
                                          },
                                        ),
                                      ),
                                    ),
                                    new Container(
                                      constraints: new BoxConstraints(
                                        minWidth: 40.0,
                                        maxWidth: 80.0,
                                      ),
                                      child: new Text(get_value(_upperValue),
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16.0,
                                              color: Colors.black54)),
                                    ),
                                  ],
                                ),
                              ))),
                      ListTile(
                        title: Text(allTranslations.text('disponibilite_filter'),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0,
                                color: Colors.black87)),
                      ),
                      Card(
                          elevation: 1.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                          ),
                          margin: EdgeInsets.zero,
                          child: Padding(
                            padding: EdgeInsets.all(5.0),
                            child: GridView(
                              // padding: const EdgeInsets.only(left: 10, right: 10),
                              physics: const BouncingScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              children: List<Widget>.generate(
                                disponibilites.length,
                                    (int index) {
                                  RadioModel _item =  disponibilites[index];
                                  return Container(
                                      padding: EdgeInsets.all(3.0),
                                      child:Container(
                                        margin: new EdgeInsets.all(15.0),
                                        child: new Row(
                                          mainAxisSize: MainAxisSize.max,
                                          children: <Widget>[
                                            new InkWell(
                                              //highlightColor: Colors.red,
                                              splashColor: Colors.transparent,
                                              onTap: () {
                                                setState(() {
                                                  disponibilites.forEach((element) =>
                                                  element.isSelected = false);
                                                  _item.isSelected = true;
                                                  widget.disponibilite = _item.id;
                                                });
                                              },
                                              child: Container(
                                                height: 40.0,
                                                width: 110.0,
                                                child: new Center(
                                                  child: new Text(_item.buttonText,
                                                      style: new TextStyle(
                                                          color: _item.isSelected ? Colors.white : FunctionUtils.colorFromHex(configModel.mainColor),
                                                          //fontWeight: FontWeight.bold,
                                                          fontSize: 14.0,
                                                          fontWeight: FontWeight.bold)),
                                                ),
                                                decoration: new BoxDecoration(
                                                  color: _item.isSelected
                                                      ? FunctionUtils.colorFromHex(configModel.mainColor)
                                                      : Colors.transparent,
                                                  border: new Border.all(
                                                      width: 1.0,
                                                      color: _item.isSelected
                                                          ? FunctionUtils.colorFromHex(configModel.mainColor)
                                                          :  FunctionUtils.colorFromHex(configModel.mainColor)),
                                                  borderRadius:
                                                  const BorderRadius.all(const Radius.circular(3.0)),
                                                ),
                                              ),
                                              // child: new Text(sampleData[index].buttonText),
                                            ),
                                          ],
                                        ),
                                      )
                                  );
                                },
                              ),
                              gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisSpacing: 0.5,
                                crossAxisSpacing: 0.5,
                                childAspectRatio: 2.5,
                              ),
                            ),
                          )
                      ),
                      ListTile(
                          title: Text(allTranslations.text('categorie_filter'),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0,
                                  color: Colors.black87)),
                          subtitle: Text(selectedCategoriesName,
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black54)),
                          trailing:  Icon(Icons.navigate_next, color:Colors.black,size: 20,),
                          onTap: () {
                            String type ="0";
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => ListeDataPage(type,widget.selectedCategories,widget.selectedProducts,widget.selectedSousProducts)),
                            ).then((value){
                              if(value!=null){
                                _constructSelectedData(type,value);
                              }
                            });
                          }
                      ),
                      ListTile(
                          title: Text(allTranslations.text('produit_filter'),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0,
                                  color: Colors.black87)),
                          subtitle: Text(selectedProductsName,
                              style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.black54)),
                          trailing:  Icon(Icons.navigate_next, color:Colors.black,size: 20,),
                          onTap: () {
                            String type ="10";
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => ListeDataPage(type,widget.selectedCategories,widget.selectedProducts,widget.selectedSousProducts)),
                            ).then((value){
                              if(value!=null){
                                _constructSelectedData(type,value);
                              }
                            });
                          }
                      ),
                      ListTile(
                        title: Text(allTranslations.text('sousproduit_filter'),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0,
                                color: Colors.black87)),
                        subtitle: Text(selectedSousProductsName,
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black54)),
                        trailing:  Icon(Icons.navigate_next, color: Colors.black,size: 20,),
                          onTap: () {
                            String type ="20";
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => ListeDataPage(type,widget.selectedCategories,widget.selectedProducts,widget.selectedSousProducts)),
                            ).then((value){
                              if(value!=null){
                                  _constructSelectedData(type,value);
                                }
                            });
                          }
                      ),
                    ],
                  ),
                ),
                Divider(),
                Padding(
                  padding: const EdgeInsets.only(left: 15,right: 15),
                  child:   Row(
                    children: <Widget>[
                      Expanded(
                        child:  Container(
                          height: 40.0,
                          width: 150.0,
                          decoration: new BoxDecoration(
                              border: new Border.all(
                              width: 1.5,
                              color: Colors.black54),
                              borderRadius:
                              const BorderRadius.all(const Radius.circular(60.0)),
                          ),
                          child:  GestureDetector(
                              onTap: () {
                                setState(() {
                                  print('annuler');
                                  Navigator.of(context).pop(null);
                                });
                              },
                              child: Center(
                                child: Text(
                                  allTranslations.text('cancel'),
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                      fontSize: 14.0,
                                      color:Colors.black54),
                                ),
                              ),
                            ),
                        ),
                      ),
                      SizedBox(width: 20.0,),
                      Expanded(
                        child: Container(
                          // height: 50,
                          child: Container(
                            height: 40.0,
                            width: 150.0,
                            child: Material(
                              borderRadius: BorderRadius.circular(60.0),
                              color: FunctionUtils.colorFromHex(configModel.mainColor),
                              //elevation: 7.0,
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    print('validate');
                                   print(widget.selectedCategories.toString());
                                   print(widget.selectedProducts.toString());
                                   print(widget.selectedSousProducts.toString());
                                   print(_upperValue);
                                   print(_lowerValue);
                                   print(widget.disponibilite);
                                    ResponseFilterModel _responseFilterModel = new ResponseFilterModel(widget.selectedCategories,widget.selectedProducts,widget.selectedSousProducts,_lowerValue,_upperValue,widget.disponibilite,filterPrix);
                                      Navigator.of(context).pop(_responseFilterModel);
                                  });
                                },
                                child: Center(
                                  child: Text(
                                    allTranslations.text('validate'),
                                    style: TextStyle(
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 5.0,)
              ]);
            }));
      });

  _refreshData() {
      setState(() {
       _lowerValue = widget.min*2;
       _upperValue = widget.max/2;
       widget.disponibilite = 0;
       filterPrix=false;
       disponibilites.forEach((element) => element.isSelected = false);
       widget.selectedCategories.clear();
       widget.selectedProducts.clear();
       widget.selectedSousProducts.clear();
       _getSelectedProductsName("0",widget.selectedCategories);
       _getSelectedProductsName("10",widget.selectedProducts);
       _getSelectedProductsName("20",widget.selectedSousProducts);
      });
  }

  get_value(value) {
    // int result = value.toStringAsFixed(0);
    return FunctionUtils.separateur(value);
  }

  _constructSelectedData(String type, List<FilterProduct> selectDatas) {

      setState(() {
          String response="";

          if(type.compareTo("0")==0){

            widget.selectedCategories.clear();
            widget.selectedCategories = selectDatas;

            //traiter produits, enlever les produits qui n'ont plus de parent selectionnés

            //liste des produits
            if( widget.selectedProducts.length > 0) {

              //recuperer les id des categories selctionnées
              String filter = _getIdString(widget.selectedCategories);
              print(filter);

              //recuperer les produits des catégories selectionnées
              _localProduitRepository.filterCategorieProduitList(filter).then((produitEntity) {
                  print(produitEntity.length);
                if (produitEntity.length > 0) {

                  //recuperer les id des produits recupérés
                  List<int> produitIds = _getIdProducts(produitEntity);
                  List<int> intToRemove = new  List<int>();

                  //loop produits list
                  for (int k = 0; k < widget.selectedProducts.length; k++) {

                    // si pour un produit etant selectionné mais qui n'existe pas dans la liste des produits recupérées, save son id
                    if (!produitIds.contains(widget.selectedProducts[k].apiId)) {
                      intToRemove.add(widget.selectedProducts[k].apiId);
                    }
                  }

                  //enlever les id
                  _removeProduct(intToRemove);

                  //trainer les noms
                  _getSelectedProductsName("10", widget.selectedProducts);
                  print(selectedProductsName);

                  if( widget.selectedProducts.length > 0) {
                      if( widget.selectedSousProducts.length > 0) {
                    print("is selectedSousProducts");
                    print(widget.selectedProducts.length);

                          String filter = _getIdString(widget.selectedProducts);
                          print("categorie +++++++++" + filter);

                          _localSousProduitRepository.filterSousProduitList(filter).then((produitEntity) {
                            if (produitEntity.length > 0) {

                              List<int> sousProduitIds = _getIdSousProducts(produitEntity);
                              List<int> intToRemove = new  List<int>();

                              //loop sousproduct list
                              for (int k = 0; k < widget.selectedSousProducts.length; k++) {

                                if (!sousProduitIds.contains(widget.selectedSousProducts[k].apiId)) {
                                  intToRemove.add(widget.selectedSousProducts[k].apiId);
                                }
                              }

                              print(intToRemove.toString());
                              print("before _removeSousProduct");
                              _removeSousProduct(intToRemove);
                              _getSelectedProductsName("20", widget.selectedSousProducts);

                            } else {
                              widget.selectedSousProducts.clear();
                              selectedSousProductsName = response;
                            }
                          });
                    }
                  } else {
                    widget.selectedSousProducts.clear();
                    selectedSousProductsName = "";
                  }

                } else {
                  widget.selectedProducts.clear();
                  selectedProductsName = response;
                }
              });
            }

          }else if(type.compareTo("10")==0){

            widget.selectedProducts.clear();
            widget.selectedProducts = selectDatas;

            //traiter sous produits, enlever les sous produits qui n'ont plus de parent selectionnés
            if( widget.selectedSousProducts.length > 0) {
              String filter = _getIdString(widget.selectedProducts);
              print("categorie +++++++++" + filter);

              _localSousProduitRepository.filterSousProduitList(filter).then((produitEntity) {
                if (produitEntity.length > 0) {

                  List<int> sousProduitIds = _getIdSousProducts(produitEntity);
                  List<int> intToRemove = new  List<int>();

                  //loop sousproduct list
                  for (int k = 0; k < widget.selectedSousProducts.length; k++) {

                    if (!sousProduitIds.contains(widget.selectedSousProducts[k].apiId)) {
                          intToRemove.add(widget.selectedSousProducts[k].apiId);
                    }
                  }

                  print(intToRemove.toString());
                  print("before _removeSousProduct");
                  _removeSousProduct(intToRemove);
                  _getSelectedProductsName("20", widget.selectedSousProducts);

                } else {
                  widget.selectedSousProducts.clear();
                  selectedSousProductsName = response;
                }
              });
            }
          }else {
            widget.selectedSousProducts.clear();
            widget.selectedSousProducts = selectDatas;
          }
          _getSelectedProductsName(type, selectDatas);
      });
  }

  _removeProduct(List<int> tab){
    print("enter _removeProduct");
    print(widget.selectedProducts.toString());
    print(widget.selectedProducts.length);

    for (int i = 0; i < widget.selectedProducts.length; i++) {
      for (int k = 0; k < tab.length; k++) {
        if (widget.selectedProducts[i].apiId == tab[k] ) {
          setState(() {
            widget.selectedProducts.remove(widget.selectedProducts[i]);
          });
        }
      }
    }
    print("before selected data");
    print(widget.selectedProducts.toString());
    print(widget.selectedProducts.length);

  }

  _removeSousProduct(List<int> tab){
    print("enter _removeSousProduct");
    for (int i = 0; i < widget.selectedSousProducts.length; i++) {
      for (int k = 0; k < tab.length; k++) {
        if (widget.selectedSousProducts[i].apiId == tab[k] ) {
          setState(() {
            widget.selectedSousProducts.remove(widget.selectedSousProducts[i]);
          });
        }
      }
    }
  }

  _getSelectedProductsName(String type, List<FilterProduct> tab){

    String response="";
    for(int i=0; i < tab.length; i++){
      if(response.compareTo("")==1)response+=",";
      response+=tab[i].name;
    }
    if(type.compareTo("0")==0){
      setState(() {
        selectedCategoriesName = response;
      });
    }else
    if(type.compareTo("10")==0){
      setState(() {
         selectedProductsName = response;
      });
    }else{
      setState(() {
        selectedSousProductsName = response;
      });
    }

    print(type+"+++++++++++++"+response);
  }

  _getIdString( List<FilterProduct> tab){
    String response="";
    for(int i=0; i < tab.length; i++){
      if(response.compareTo("")==1)response+=",";
      response+=tab[i].apiId.toString();
    }
    return response;
  }

  _getIdSousProducts( List<LocalSousProduitEntity> tab){
    List<int> array = new  List<int>();
    for(int i=0; i < tab.length; i++){
      array.add(tab[i].apiId);
    }
    return array;
  }

  _getIdProducts( List<LocalProduitEntity> tab){
    List<int> array = new  List<int>();
    for(int i=0; i < tab.length; i++){
      array.add(tab[i].apiId);
    }
    return array;
  }


  _showAlertDialog(BuildContext context, CartItemModel CartItemModel,
      ConfigModel configModel, CartModel cartModel) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(
        allTranslations.text('cancel'),
        style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed: () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(
        allTranslations.text('confirm'),
        style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed: () {
        Navigator.of(context).pop(null);
      },
    );

    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(
        allTranslations.text('delete_cartitem_text'),
        style: new TextStyle(fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }
}

class ResponseFilterModel {
  List<FilterProduct> selectedCategories;
  List<FilterProduct> selectedProducts;
  List<FilterProduct> selectedSousProducts;
  double lowerValue;
  double upperValue;
  int disponibilite;
  bool filterPrix;

  ResponseFilterModel(this.selectedCategories,this.selectedProducts,this.selectedSousProducts, this.lowerValue, this.upperValue, this.disponibilite,this.filterPrix);
}


class RadioModel {
  int id;
  bool isSelected;
  final String buttonText;

  RadioModel(this.id,this.isSelected, this.buttonText);
}
