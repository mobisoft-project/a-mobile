
import 'dart:io';

import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/infoprice_model.dart';
import 'package:agrimobile/features/common/data/models/price_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_price_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_price_repository.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'dart:async';


import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/pdf.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:share/share.dart';

class PrixMarchePage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState();
  }
}



class PageState extends State<PrixMarchePage> {


  LocalPriceRepository _localPriceRepository = LocalPriceRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  List<FullPriceEntity> _allPriceEntity=[];
  bool _getAnnonce=true;
  bool _activeSearch=false;
  bool _printSearch=false;
  String _userToken="";
  String _nameProduit="";
  String _nameCity="";
  var cityKey = GlobalKey<FindDropdownState>();
  var produitKey = GlobalKey<FindDropdownState>();
  ConfigModel _myConfigModel ;

  var div= Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }




  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {

      _myConfigModel=configModel;
      return Scaffold(
          key: widget._scaffoldKey,
          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_mprice'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,

                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Visibility(child:  Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        _getAnnonce=true;
                        _activeSearch=false;
                      });
                      _nameCity="";
                      _nameProduit="";
                      produitKey.currentState.setSelectedItem(null);
                      cityKey.currentState.setSelectedItem(null);
                      _getPriceList();
                    },
                    child: Icon(Icons.close,color: Colors.white,),
                  )
              ),
                visible: _activeSearch,
              ),
              Visibility(child:  Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () async {

                      final isPermissionStatusGranted = await _requestPermissions();
                      if (isPermissionStatusGranted) {

                        String filename="Prix_Marche_";
                        filename+=_nameProduit.replaceAll(" ", "_");
                        filename+=_nameCity.replaceAll(" ", "_");


                        DateTime now = DateTime.now();
                        DateFormat formatter = DateFormat('dd_MM_yyyy_hh_mm');
                        String formatted = formatter.format(now);

                        filename="${filename}_${formatted}.pdf";

                        _getPermissionPath(context,filename,0);

                      }else{
                        _displaySnackBar(context,allTranslations.text('reject_permission'));
                      }

                    },
                    child: Icon(Icons.print,color: Colors.white,),
                  )
              ),
                visible: _printSearch,
              ),
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      UrlLauncher.launch('tel:+${configModel.callCenter}');
                    },
                    child: Icon(Icons.phone,color: Colors.white,),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return  Stack(      // Column
                  children: <Widget>[

                    Column(
                      children: [
                        SizedBox(height: 20),
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                children: [
                                  Text(allTranslations.text('select_produit'),style: TextStyle(
                                      color:Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500
                                  ),),
                                  SizedBox(height: 5),
                                  Container(
                                    margin: EdgeInsets.only(left:5,right:5),
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        top: 0,left: 16, right: 16, bottom: 0
                                    ),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(50)
                                        ),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black12,
                                              blurRadius: 5
                                          )
                                        ]
                                    ),
                                    child: FindDropdown<LocalPriceEntity>(
                                      key: produitKey,
                                      onFind: (String filter) => _getSpeculation(filter),
                                      searchBoxDecoration: InputDecoration(
                                        hintText: allTranslations.text('search'),
                                        border: OutlineInputBorder(),
                                      ),
                                      onChanged: (LocalPriceEntity value) {

                                        cityKey.currentState.setSelectedItem(null);
                                        _nameProduit="";
                                        _nameCity="";
                                        setState(() {
                                          _getAnnonce=true;
                                          _activeSearch=true;
                                        });

                                        if(value==null){
                                          _nameProduit="";
                                        }else{
                                          _nameProduit=value.nomProduit;
                                        }
                                        _getPriceList();


                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  Text(allTranslations.text('select_ville'),style: TextStyle(
                                      color:Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500
                                  ),),
                                  SizedBox(height: 5),
                                  Container(
                                    margin: EdgeInsets.only(left:5,right:5),
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                        top: 0,left: 16, right: 16, bottom: 0
                                    ),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(50)
                                        ),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                              color: Colors.black12,
                                              blurRadius: 5
                                          )
                                        ]
                                    ),
                                    child: FindDropdown<LocalPriceEntity>(
                                      key: cityKey,
                                      onFind: (String filter) => _getSpeculationcity(filter),
                                      searchBoxDecoration: InputDecoration(
                                        hintText: allTranslations.text('search'),
                                        border: OutlineInputBorder(),
                                      ),
                                      dropdownBuilder: (BuildContext context, LocalPriceEntity item) {
                                        return Container(
                                          width: MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.all(10),
                                          height: 40,
                                          decoration: BoxDecoration(
                                            border: Border.all(color: Theme.of(context).dividerColor),
                                            borderRadius: BorderRadius.circular(5),
                                            color: Colors.white,
                                          ),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: item==null?Text(""):Text(item.ville),
                                              ),
                                              Icon(
                                                Icons.arrow_drop_down,
                                                color: Colors.grey,
                                                size: 20,
                                              ),
                                              // SizedBox(width: 5,)
                                            ],
                                          ),
                                        );
                                      },
                                      dropdownItemBuilder:(BuildContext context, LocalPriceEntity item, bool isSelected) {
                                        return Container(
                                          decoration: !isSelected ? null : BoxDecoration(
                                            color: Colors.white,
                                          ),
                                          child: ListTile(
                                            selected: isSelected,
                                            title: item==null?Text(""):Text(item.ville),
                                          ),
                                        );
                                      },

                                      onChanged: (LocalPriceEntity data) {
                                        setState(() {
                                          _getAnnonce=true;
                                          _activeSearch=true;
                                        });
                                        _nameCity=data.ville;
                                        _getPriceList();
                                      },
                                    ),

                                  ),
                                ],
                              ),
                            )
                          ],
                        ),

                        Container(
                            padding:EdgeInsets.only(left:10.0,right:10.0,bottom: 20,top:10.0),
                            margin: EdgeInsets.only(bottom:70.0),
                            child:Row(
                              children: [

                                Container(
                                  width:80,
                                  height:20,
                                  child: Text(allTranslations.text('speculation'),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                          fontSize: 14
                                      )
                                  ),
                                ),
                                SizedBox(width:5),
                                Expanded(child: Text(allTranslations.text('ville'),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 14
                                    )
                                )),
                                SizedBox(width:5),
                                Expanded(child: Text(allTranslations.text('price'),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 14
                                    )
                                )),
                                SizedBox(width:5),
                                Expanded(child: Text(allTranslations.text('date_update'),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 14
                                    )
                                )),

                              ],
                            )
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(top:130.0),
                      child: _getSpeculationListview(),
                    ),

                  ],
                );
              }
          )
      );
    },
  );


  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userToken="${value.token}";
      });
      _getPriceList();

    });




  }

  Future<List<LocalPriceEntity>> _getSpeculation(String nameProduit ){
    return  _localPriceRepository.listFilterSpeculations(nameProduit);
  }

  Future<List<LocalPriceEntity>> _getSpeculationcity(String nameCity ){
    return  _localPriceRepository.listFilterSpeculationsCity(_nameProduit,nameCity);
  }



  _getPriceList(){

    setState(() {
      _printSearch = false;
    });
    //recuperation des prix disponible sur le marche
    _localPriceRepository.listSpeculations(_nameProduit).then((response) {
      if(response.length>0 ){
        print("Good");
        List<FullPriceEntity> tamponPrice=[];
        for(int i=0;i<response.length;i++){
          _localPriceRepository.listPrice(response[i].nomProduit,response[i].timestamp,_nameCity).then((fresponse) {
            if(fresponse.length>0 ){
              FullPriceEntity price =FullPriceEntity();
              price.nomProduit=response[i].nomProduit;
              price.allPrice=fresponse;
              tamponPrice.add(price);
            }
          });
        }

        Timer(Duration(seconds: 2),(){
          setState(() {
            _allPriceEntity = tamponPrice;
            _getAnnonce = false;
            _printSearch = true;
          });
          //_serverPrice();
        });

        _serverPrice();
      }else{

        _serverPrice();
      }
    });

  }

  _refreshPriceList(){

    //recuperation des prix disponible sur le marche
    _localPriceRepository.listSpeculations(_nameProduit).then((response) {
      if(response.length>0 ){

          List<FullPriceEntity> tamponPrice=[];
          for(int i=0;i<response.length;i++){
              _localPriceRepository.listPrice(response[i].nomProduit,response[i].timestamp,_nameCity).then((fresponse) {
                if(fresponse.length>0 ){
                    FullPriceEntity price =FullPriceEntity();
                    price.nomProduit=response[i].nomProduit;
                    price.allPrice=fresponse;
                    tamponPrice.add(price);
                }
              });
          }

          Timer(Duration(seconds: 3),(){
            setState(() {
              _getAnnonce = false;
              _allPriceEntity = tamponPrice;
            });
          });

      }
    });

  }

  _serverPrice(){

    Api api = ApiRepository();
    api.getPrice(_userToken).then((value) {
      if (value.isRight()) {
        value.all((a) {

          if(a.status.compareTo("ok")==0) {

            List<InfoPriceModel> allDatas=a.datas;

            if(allDatas!=null) {

              //enregistrement des speculations

              if (allDatas.length > 0) {
                int nbreTotal = allDatas.length;
                for (int i = 0; i < nbreTotal; i++) {

                  String dateRequest=allDatas[i].date;
                  int timestamp=allDatas[i].timestamp;
                  List<PriceModel> information=allDatas[i].information;

                  int nbreInfo = information.length;
                  if(nbreInfo>0){
                    for (int u = 0; u < nbreInfo; u++) {


                        LocalPriceEntity localPriceEntity = LocalPriceEntity.fromMap(information[u].toMap());


                        localPriceEntity.datePrice=dateRequest;
                        localPriceEntity.timestamp=timestamp;

                        //verifier si cette speculation existe deja
                        _localPriceRepository.existPrice(localPriceEntity.apiId).then((response) {
                            if (response.length > 0) {

                                localPriceEntity.idPrice = response[0].idPrice;
                                _localPriceRepository.update(localPriceEntity, response[0].idPrice);

                            }else {
                              localPriceEntity.idPrice = 0;
                              _localPriceRepository.save(localPriceEntity);
                            }

                        });

                    }

                  }

                }
                Timer(Duration(seconds: 5),(){
                  _refreshPriceList();
                });

              }else{
                setState(() {
                  _getAnnonce = false;
                });
              }

            }else{
              setState(() {
                _getAnnonce = false;
              });
            }

          }else{
            setState(() {
              _getAnnonce = false;
            });
          }

          return true;
        });
      } else {
        setState(() {
          _getAnnonce = false;
        });
        return false;
      }
    });
  }

  _getSpeculationListview(){

    if (_getAnnonce==true){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif',height:150),
            ),
            SizedBox(height: 30,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_loading'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else if (_allPriceEntity==null || _allPriceEntity.length==0 ){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_pempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else{
      return ListView.builder(
          itemCount:_allPriceEntity.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            FullPriceEntity infoPrice= _allPriceEntity[position];


            return Column(
                children: [

                  if(position==0)div,
                  Container(
                      padding:EdgeInsets.all(10.0),
                      child:Row(
                        children: [
                          Container(
                            width:80,
                            child: Text("${infoPrice.nomProduit}"),
                          ),
                          SizedBox(width:5),
                          Expanded(
                            child: _getPrixListview(infoPrice.allPrice),
                          )

                        ],
                      )
                  ),
                  div,

                ]
            );

          });

    }

  }

  _getPrixListview(List<LocalPriceEntity> detailPrice){

    return ListView.builder(
        itemCount:detailPrice.length,
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (BuildContext context, int position) {

          LocalPriceEntity infoPrice= detailPrice[position];

          return Column(
              children: [
                if(position!=0)div,
                Row(
                  children: [
                    Expanded(child: Text("${infoPrice.ville}")),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoPrice.prix}")),
                    SizedBox(width:5),
                    Expanded(child: Text("${FunctionUtils.convertFormatDate(infoPrice.datePrice)}")),
                  ],
                )

              ]
          );

        });

  }


  _reportView(BuildContext context,String filePath) async {

    String title=_nameCity;
    if(_nameCity.compareTo("")!=0){
      title=" ($_nameCity)";
    }
    final pw.Document pdf = pw.Document();

    pdf.addPage(pw.MultiPage(
        pageFormat:
        PdfPageFormat.letter.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        header: (pw.Context mycontext) {
          if (mycontext.pageNumber == 1) {
            return null;
          }


          return pw.Container(
              alignment: pw.Alignment.centerRight,
              margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
              padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
              child: pw.Text("${allTranslations.text('menu_mprice')} $_nameProduit $title",
                  style: new pw.TextStyle(
                    color: PdfColors.grey, fontSize: 17.0,)));
        },
        footer: (pw.Context context) {
          return pw.Container(
              alignment: pw.Alignment.centerRight,
              margin: const pw.EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
              child: pw.Text("${allTranslations.text('menu_mprice')} ---Page ${context.pageNumber} / ${context.pagesCount}",
                  style: new pw.TextStyle(
                    color: PdfColors.grey, fontSize: 17.0,)));
        },
        build: (pw.Context context) => <pw.Widget>[
          pw.Header(
              level: 0,
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  children: <pw.Widget>[
                    pw.Text("${allTranslations.text('menu_mprice')} $_nameProduit $title", textScaleFactor: 2),
                    //PdfLogo()
                  ])),


          pw.Padding(padding: const pw.EdgeInsets.all(10)),

          pw.Container(
              alignment: pw.Alignment.center,
              child: pw.Text("${allTranslations.text('detail_prix')}",
                  style: new pw.TextStyle(
                    color: PdfColors.black, fontSize: 20.0,))),

          pw.Column(
            children: _pdfWidget(context),
          ),


        ]));


    final File file = File(filePath);
    await file.writeAsBytes(await pdf.save());

    Timer(Duration(seconds: 1),(){

      Navigator.of(context).pop(null);

      _showAlertDialog(context,filePath);

    });

  }

  List<pw.Widget> _pdfWidget(pw.Context context ){


    List<pw.Widget> allContent=[] ;
    if(_allPriceEntity.length>0){

      for(var u=0;u<_allPriceEntity.length;u++){

        List<List<String>> allAfficheSimulation= [] ;
        if(u==0) {
          allContent.add(pw.Padding(padding: const pw.EdgeInsets.all(5)));
        }else{
          allContent.add(pw.Padding(padding: const pw.EdgeInsets.all(20)));
        }


        List<LocalPriceEntity> detailPrice=_allPriceEntity[u].allPrice;
        //creation de l'entete
        allAfficheSimulation.add(<String>["${allTranslations.text('speculation')}", "${allTranslations.text('ville')}", "${allTranslations.text('price')}","${allTranslations.text('date_update')}"]);

        if(detailPrice.length>0){

          String nomProduit=" ";
          int reset=(detailPrice.length/2).toInt();
          if(detailPrice.length==2)reset=0;

          for(var i=0;i<detailPrice.length;i++){
            nomProduit="";
            if(i.compareTo(reset)==0){
              nomProduit="${_allPriceEntity[u].nomProduit}";
            }

            List<String> afficheSimulation= <String>["$nomProduit","${detailPrice[i].ville}", "${detailPrice[i].prix}","${FunctionUtils.convertFormatDate(detailPrice[i].datePrice)}"];

            allAfficheSimulation.add(afficheSimulation);
          }
        }

        allContent.add( pw.Table.fromTextArray(context: context, data: allAfficheSimulation));
      }
    }

    return allContent ;

  }

  _showAlertDialog(BuildContext context,String filePath) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text( allTranslations.text('close'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget shareButton = FlatButton(
      child: Text( allTranslations.text('share'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);

        if (Platform.isAndroid) {

          final RenderBox box = context.findRenderObject();
          Share.shareFiles([filePath],
              subject: allTranslations.text('sharetitle'),
              text: allTranslations.text('sharecontent'),
              sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);

        }

      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('view'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => PdfViewerPage(path: filePath),
          ),
        );

      },
    );

    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(allTranslations.text('succes_download')
      ),
      actions: [
        cancelButton,
        shareButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<bool> _requestPermissions() async {
    var permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);
    }

    return permission == PermissionStatus.granted;
  }

  Future<void> _getPermissionPath(BuildContext context,String filename,int type) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('download_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });

    Directory _appDocDir= await getExternalStorageDirectory();

    String mypath=_appDocDir.path;
    if (Platform.isAndroid) {
      mypath=mypath.split("Android")[0];
    }


    final Directory _appDocDirFolder = Directory(
        '$mypath/${DataConstantesUtils.simulationFolder}/');

    if (await _appDocDirFolder.exists()) {
      String filePath = '${_appDocDirFolder.path}$filename';
      _reportView(context, filePath);

    } else {
      Future<Directory> _appDocDirNewFolder = _appDocDirFolder.create(
          recursive: true);
      _appDocDirNewFolder.then((response) {

        String filePath = '${response.path}$filename';
        _reportView(context, filePath);
      });
    }

  }


  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}

class PdfViewerPage extends StatelessWidget {
  final String path;
  const PdfViewerPage({Key key, this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
      path: path,
    );
  }
}


class FullPriceEntity  {
  String nomProduit;
  List<LocalPriceEntity> allPrice;
  FullPriceEntity();
}

