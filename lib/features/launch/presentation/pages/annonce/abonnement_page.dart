
import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/speculation_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_speculation_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_speculation_repository.dart';
import 'package:agrimobile/features/launch/presentation/widgets/liste_abonnement_widget.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'dart:async';

import 'package:agrimobile/features/common/data/models/config_model.dart';

class AbonnementPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState();
  }
}



class PageState extends State<AbonnementPage> {


  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();

  LocalSpeculationRepository _localSpeculationRepository = LocalSpeculationRepository();
  List<LocalSpeculationEntity> _localSpeculationEntity;


  bool _getSpeculation=true;
  String _userToken="";
  String _userId="";

  var div= Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }



  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
          key: widget._scaffoldKey,
          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_abonnement'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      showSearch(context: context, delegate: Search(_localSpeculationEntity,_userToken,widget._scaffoldKey,_userId,onrefresh));
                    },
                    child: Icon(
                      Icons.search,
                      size: 26.0,
                    ),
                  )
              ),
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      UrlLauncher.launch('tel:+${configModel.callCenter}');
                    },
                    child: Icon(Icons.phone,color: Colors.white,),
                  )
              ),
            ],
            backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return  _getAbonnementListview();
              }
          )
      );
    },
  );

  onrefresh(){
    _localSpeculationRepository.allSpeculation(int.parse(_userId)).then((response) {
      _localSpeculationEntity = response;
      setState(() {
      });
    });
  }

  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userToken="${value.token}";
        _userId="${value.id}";
      });
      _getAbonnementList(value.id);
    });


  }

  _getAbonnementList(int idUser){

    //recuperation des prix disponible sur le marche
    _localSpeculationRepository.allSpeculation(idUser).then((response) {
      if(response.length>0 ){

        setState(() {
          _getSpeculation=false;
          _localSpeculationEntity=response;
        });

      }else{
        setState(() {
          _getSpeculation=false;
        });
      }

    });

  }

  _getAbonnementListview(){

    if (_getSpeculation==true){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif',height:150) ,
            ),
            SizedBox(height: 30,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_loading'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else if (_localSpeculationEntity==null || _localSpeculationEntity.length==0 ){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_abempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else{
      return Container(
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          child: ListeAbonnementWidget(
            localSpeculationEntity:_localSpeculationEntity,
            scaffoldKey:  widget._scaffoldKey,
            userToken: _userToken,
            userId: _userId,
            onrefresh: (){
              _localSpeculationRepository.allSpeculation(int.parse(_userId)).then((response) {
                _localSpeculationEntity = response;
                setState(() {
                });
              });
            },
          )
      );

    }



  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}


class Search extends SearchDelegate {

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          query = "";
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container(
      child: Center(
        child: Text(""),
      ),
    );
  }

  LocalSpeculationRepository _localSpeculationRepository = LocalSpeculationRepository();

  List<LocalSpeculationEntity> listExample;
  List<LocalSpeculationEntity> suggestionList;
  GlobalKey<ScaffoldState> scaffoldKey;
  String userToken;
  String userId;
  final VoidCallback onrefresh;

  Search(this.listExample,this.userToken,this.scaffoldKey,this.userId,this.onrefresh): super(searchFieldLabel: allTranslations.text('search'));


  @override
  Widget buildSuggestions(BuildContext context) {
    suggestionList=[];
    String search=query.toLowerCase();
    search.isEmpty
        ? suggestionList = listExample //In the true case
        : suggestionList.addAll(listExample.where(
          (element) => element.nom.toLowerCase().contains(search)),
    );

    if (suggestionList==null || suggestionList.length==0){
      return  Center(
        child: Column(
          children: [
            SizedBox(height: 100,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_aempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else {
      return Container(
          decoration: BoxDecoration(
            color: FunctionUtils.colorFromHex("DDDDDD")
          ),
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          child: ListeAbonnementWidget(
            localSpeculationEntity:suggestionList,
            scaffoldKey: scaffoldKey,
            userToken: userToken,
            userId: userId,
            onrefresh: (){
              _localSpeculationRepository.allSpeculation(int.parse(userId)).then((response) {
                if(response.length>0 ){
                  listExample=response;
                    onrefresh();
                }

              });
            },
          )
      );

    }
  }




}

