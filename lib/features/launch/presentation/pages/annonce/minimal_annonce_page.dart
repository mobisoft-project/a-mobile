import 'package:flutter/material.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/detail_annonce_page.dart';

class MinimalAnnoncePage extends StatefulWidget {
  final String image;
  final LocalAnnonceEntity infoAnnonce;
  final LocalScoopEntity infoScoop;
  final String typePage;
  final int biztypeId;
  final VoidCallback onrefresh;

  const MinimalAnnoncePage({
    Key key,
    this.image,
    this.infoAnnonce,
    this.infoScoop,
    this.typePage,
    this.biztypeId,
    this.onrefresh,
  }) : super(key: key);

  @override
  MinimalAnnoncePageState createState() => MinimalAnnoncePageState();
}


class MinimalAnnoncePageState extends State<MinimalAnnoncePage> {
  @override
  Widget build(BuildContext context) => Consumer2<ConfigModel, CartModel>(
          builder: (context, configModel, cartModel, child) {
            LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
        String unite = widget.infoAnnonce.nomUnite;
        int annonce_price = widget.infoAnnonce.prix;
        double annonce_price_final = annonce_price * 1.0;

        List<String> recup_unite = unite.split(" (");
        List<String> recup_unite_second = recup_unite[1].split(") ");
        String final_unite = recup_unite_second[0].substring(0, recup_unite_second[0].length - 1);

        print("autorisation :"+ widget.infoAnnonce.nomCategorieFilleProduit+" - "+ widget.infoAnnonce.autorisation.toString());

        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(8.0),
                bottomLeft: Radius.circular(8.0),
                bottomRight: Radius.circular(8.0),
                topRight: Radius.circular(8.0)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.withOpacity(0.4),
                  offset: const Offset(1.1, 1.1),
                  blurRadius: 10.0),
            ],
          ),
          child: Material(
            color: Colors.transparent,
            child:Column(
                children: <Widget>[

              InkWell(
              focusColor: Colors.transparent,
                highlightColor: Colors.transparent,
                hoverColor: Colors.transparent,
                borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                splashColor: Color(0xFF2633C5).withOpacity(0.2),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            DetailAnnoncePage(widget.infoAnnonce.apiId,widget.infoScoop,widget.typePage,widget.biztypeId)),
                  ).then((value){
                      widget.onrefresh();
                  });
                },
                child: FutureBuilder<Widget>(
                    future: buildCustomGrid(context, widget.image),
                    builder:
                        (BuildContext context, AsyncSnapshot<Widget> snapshot) {
                      if (snapshot.hasData) {
                        return snapshot.data;
                      } else {
                        return Container(child: CircularProgressIndicator());
                      }
                    },
                  ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: 80,
                        height: 25,
                        decoration: new BoxDecoration(
                          //borderRadius: new BorderRadius.circular(5.0),
                          color: widget.infoAnnonce.biztypeId.compareTo(1) == 0 ? FunctionUtils.colorFromHex(configModel.offreColor).withOpacity(0.5): FunctionUtils.colorFromHex(configModel.demandeColor).withOpacity(0.5),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                              widget.infoAnnonce.biztypeId.compareTo(1) == 0 ? allTranslations.text('vente') :allTranslations.text('achat'),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontSize: 15,
                                color: Colors.white,
                              )),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 7, right: 7),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(height: 3),
                        Text(
                          widget.infoAnnonce.nomCategorieFilleProduit,
                          maxLines: 1,
                          style: kTitleTextstyle.copyWith(
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 4),
                        Row(
                          children: [
                             Expanded(
                              child: Text(
                        "${FunctionUtils.separateur(annonce_price_final)} ${configModel.devise}/${final_unite}",
                                maxLines: 1,
                                style: kTitleTextstyle.copyWith(
                                    fontSize: 14,
                                    color: FunctionUtils.colorFromHex(
                                        configModel.mainColor)),
                              ),
                            ),
                            /*GestureDetector(
                                onTap: () {},
                                child: Icon(
                                  Icons.more_horiz_rounded,
                                  size: 14.0,
                                  color: Colors.black38,
                                )),*/
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
          ),
        );
      });

  Future<Widget> buildCustomGrid(BuildContext context, String image) async {
    return CachedNetworkImage(
      imageUrl: '${DataConstantesUtils.IMAGE_ANNONCE_URL}$image',
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
          ),
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(8.0), topRight: Radius.circular(8.0)),
        ),
      ),
      height: MediaQuery.of(context).size.width / 2.2,
      width: MediaQuery.of(context).size.width,
      placeholder: (context, url) =>
          new Image.asset('assets/img/loading.gif', height: 20),
      errorWidget: (context, url, error) => new Icon(Icons.error),

    );
  }
}
