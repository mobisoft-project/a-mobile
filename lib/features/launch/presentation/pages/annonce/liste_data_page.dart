import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:agrimobile/features/common/data/models/filter_product_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_categorie_produit_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_produit_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_sous_produit_repository.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import 'package:flutter_range_slider/flutter_range_slider.dart' as frs;

class ListeDataPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String type;
  List<FilterProduct> selectedCategories;
  List<FilterProduct> selectedProducts;
  List<FilterProduct> selectedSousProducts;

  ListeDataPage(this.type, this.selectedCategories,this.selectedProducts, this.selectedSousProducts);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}

class PageState extends State<ListeDataPage> {
  List<FilterProduct> allProducts = new List<FilterProduct>();
  List<FilterProduct> selectedDatas = new List<FilterProduct>();


  LocalCategorieProduitRepository _localCategorieProduitRepository=LocalCategorieProduitRepository();
  LocalProduitRepository _localProduitRepository=LocalProduitRepository();
  LocalSousProduitRepository _localSousProduitRepository=LocalSousProduitRepository();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getDatasList();
  }

  @override
  Widget build(BuildContext context) => Consumer2<ConfigModel, CartModel>(
          builder: (context, configModel, cartModel, child) {

        return Scaffold(
            key: widget._scaffoldKey,
            appBar: new AppBar(
              centerTitle: true,
              title: Text(
                // widget.type == "10"?allTranslations.text('produit_filter'):allTranslations.text('sousproduit_filter'),
                widget.type == "0"?allTranslations.text('categorie_filter'):widget.type == "10"?allTranslations.text('produit_filter'):allTranslations.text('sousproduit_filter'),
                //allTranslations.text('filter'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
              backgroundColor:
                  FunctionUtils.colorFromHex(configModel.mainColor),
              elevation: 0.0,
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(selectedDatas),
              ),
            ),
            backgroundColor: FunctionUtils.colorFromHex("#F2F2F2"),
            body: Builder(builder: (BuildContext myContext) {
              return Column(children: <Widget>[
                Expanded(
                  child: allProducts.length>0 ? ListView(
                    children: allProducts.map((FilterProduct product) {
                      return ListTile(
                          title: new Row(
                            children: <Widget>[
                              new Expanded(child: new Text(product.name)),
                              new Checkbox(
                                  value: product.isCheck,
                                  onChanged: (bool value) {
                                    setState(() {
                                      product.isCheck = value;
                                      if(value==true){
                                          selectedDatas.add(product);
                                        }else{
                                          selectedDatas.remove(product);
                                        }
                                    });
                                  })
                            ],
                          ));;
                    }).toList(),
                  ):_getEmptyView(),
                ),
                if(allProducts.length>0)Divider(),
                if(allProducts.length>0) Padding(
                  padding: const EdgeInsets.only(left: 15,right: 15),
                  child:   Row(
                    children: <Widget>[
                      Expanded(
                        child:  Container(
                          height: 40.0,
                          width: 150.0,
                          decoration: new BoxDecoration(
                            border: new Border.all(
                                width: 1.5,
                                color: Colors.black54),
                            borderRadius:
                            const BorderRadius.all(const Radius.circular(60.0)),
                          ),
                          child:  GestureDetector(
                            onTap: () {
                              setState(() {
                                print('annuler');
                                Navigator.of(context).pop(null);
                              });
                            },
                            child: Center(
                              child: Text(
                                allTranslations.text('cancel'),
                                style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14.0,
                                    color:Colors.black54),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 20.0,),
                      Expanded(
                        child: Container(
                          // height: 50,
                            child: Container(
                              height: 40.0,
                              width: 150.0,
                              child: Material(
                                borderRadius: BorderRadius.circular(60.0),
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                                //elevation: 7.0,
                                child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      Navigator.of(context).pop(selectedDatas);
                                      //_recupSelectedData();
                                    });
                                  },
                                  child: Center(
                                    child: Text(
                                      allTranslations.text('validate'),
                                      style: TextStyle(
                                          color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                            )
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 5.0,)
              ]);
            }));
      });



  _getEmptyView(){
    return Center(
      child: Column(
        children: [
          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          SizedBox(height: 0,),
          Container(
            width:300,
            child: Text(
              allTranslations.text('searching_aempty'),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black54,
                fontWeight: FontWeight.bold,
                fontSize: 22.0,
              ),
            ),
          )
        ],
      ),
    );
  }

  _getDatasList() {
    //10. liste produits
    //2. liste sous produits
    if (widget.type == "0") {

      _localCategorieProduitRepository.filterProduit("").then((categories) {

        for(int i=0; i < categories.length; i++){

          setState(() {
            // bool state =   widget.selectedProducts !=null && widget.selectedProducts.length > 0?widget.selectedProducts.contains(produitEntity[i].apiId)?true:false:false;
            bool state =  false;

            if( widget.selectedCategories !=null && widget.selectedCategories.length > 0){
              List<int> Ids = _getIds(widget.selectedCategories);
              if(Ids.contains(categories[i].apiId)){
                state =  true;
              }else{
                state =  false;
              }
            }
            FilterProduct product  = FilterProduct(categories[i].name, state,categories[i].apiId);
            allProducts.add(product);
            if(state==true)selectedDatas.add(product);
          });
        }
      });

    }else if (widget.type == "10") {
      String filter = _getIdString(widget.selectedCategories);
      _localProduitRepository.filterCategorieProduitList(filter).then((produitEntity) {

        for(int i=0; i < produitEntity.length; i++){

          setState(() {
            // bool state =   widget.selectedProducts !=null && widget.selectedProducts.length > 0?widget.selectedProducts.contains(produitEntity[i].apiId)?true:false:false;
            bool state =  false;

            if( widget.selectedProducts !=null && widget.selectedProducts.length > 0){
              List<int> Ids = _getIds(widget.selectedProducts);
              if(Ids.contains(produitEntity[i].apiId)){
                state =  true;
              }else{
                state =  false;
              }
            }
            FilterProduct product  = FilterProduct(produitEntity[i].nomCategorieFille, state,produitEntity[i].apiId);
            allProducts.add(product);
            if(state==true)selectedDatas.add(product);
          });
        }
      });
    } else {
      String filter = _getIdString(widget.selectedProducts);
      print(filter);
      _localSousProduitRepository.filterSousProduitList(filter).then((produitEntity) {
        print(produitEntity.length);
        if(produitEntity.length > 0){
        for(int i=0; i < produitEntity.length; i++){
          setState(() {
            // bool state =   widget.selectedProducts !=null && widget.selectedProducts.length > 0?widget.selectedProducts.contains(produitEntity[i].apiId)?true:false:false;
            bool state =  false;

            if( widget.selectedSousProducts !=null && widget.selectedSousProducts.length > 0){
              List<int> Ids = _getIds(widget.selectedSousProducts);
              if(Ids.contains(produitEntity[i].apiId)){
                state =  true;
              }else{
                state =  false;
              }
            }
            FilterProduct product  = FilterProduct(produitEntity[i].nomCategorieFille, state,produitEntity[i].apiId);
            allProducts.add(product);
            if(state==true)selectedDatas.add(product);
          });
          }
        }else{
          selectedDatas.clear();
        }
      });
    }
  }

  _getIds(List<FilterProduct> tab){
    List<int> array = new  List<int>();
    for(int i=0; i < tab.length; i++){
      array.add(tab[i].apiId);
    }
    return array;
  }


  _getIdString(List<FilterProduct> tab){
    String response="";
    for(int i=0; i < tab.length; i++){
      if(response.compareTo("")==1)response+=",";
      response+=tab[i].apiId.toString();
    }
    return response;
  }
}


