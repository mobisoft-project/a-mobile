import 'dart:async';

import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/my_statistics_dto.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_account_statistics_repository.dart.dart';
import 'package:fl_chart/fl_chart.dart';

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_account_statistics_entity.dart';

class SatistiqueMarchandPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  _SatistiqueMarchandPageState createState() => _SatistiqueMarchandPageState();
}

class _SatistiqueMarchandPageState extends State<SatistiqueMarchandPage> with SingleTickerProviderStateMixin{
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalMyStatisticsRepository _localStatisticsRepository = LocalMyStatisticsRepository();
  List<LocalMyStatisticsEntity> localStatisticsData;
  
  String _userToken="";
  String offreSpeculation = "";
  String _userId="";
  Api api = ApiRepository();
  
  @override
  void initState() {
    super.initState();
    _getUserInformations();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
        builder: (context, configModel, child){
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:  Align(
            child: Container(
              child: Text(allTranslations.text('menu_statistique_marchand'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  _getStatisticsFromServer(configModel);
                },
                child: Icon(
                  Icons.refresh,
                  size: 26.0,
                ),
              )
            ),
          ],
          backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
        body: Builder(builder: (BuildContext myContext){
          if(localStatisticsData == null || localStatisticsData.length == 0){
            return Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  Material(
                    shape: CircleBorder(),
                    clipBehavior: Clip.hardEdge,
                    color: Colors.transparent,
                    child: Image.asset('assets/img/loading.gif', height: 100),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              ),
            );
          }else{
            return CustomScrollView(
              physics: ClampingScrollPhysics(),
              slivers: [
                _buildHeader(),
                SliverPadding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  sliver: SliverToBoxAdapter(
                    child: _statGrid(),
                  ),
                ),
                SliverPadding(
                  padding: const EdgeInsets.only(top: 20.0),
                  sliver: SliverToBoxAdapter(
                    child: _statbarChart(),
                  ),
                ),
              ],
            );
          }
          
        })
      );
    }
  );

  _getUserInformations(){
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userToken = "${value.token}";
        _userId="${value.id}";
      });
      _getUserStatistics();
    });
  }

  _getUserStatistics() async {
    _localStatisticsRepository
    .userStatistics(int.parse(_userId))
    .then((List<LocalMyStatisticsEntity> value){
      setState(() {
        localStatisticsData = value;
      });

      if (localStatisticsData.isEmpty) {
        _getRemoteStatistics(0);
        return;
      }
    });
  }

  void _getRemoteStatistics(int caller){
    MyStatisticsDto _myStatDto = MyStatisticsDto.create(
      accessToken: DataConstantesUtils.API_TOKEN,
      userToken: _userToken
    );
    api.getMyStatistics(_myStatDto).then((value) {
      if (value.isRight()) {
        value.all((userRemoteStat) {
          if (userRemoteStat != null && userRemoteStat.status.compareTo("000") == 0){
            FunctionUtils.saveMyStatistics(userRemoteStat.totalOffre, userRemoteStat.activeOffre, userRemoteStat.chiffreAffaire, userRemoteStat.offreSpeculation, int.parse(_userId));
            Timer(Duration(seconds: 2),(){
              _localStatisticsRepository.userStatistics(int.parse(_userId)).then((res){
                if(res.length > 0){
                  setState(() {
                    localStatisticsData = res;
                  });
                }
              });
              if(caller == 1){
                Navigator.of(context).pop(null);
              }
            });
            return true;
          }else{
            if(caller == 1){
                Navigator.of(context).pop(null);
              }
            return false;
          }
        });
      }else{
        if(caller == 1){
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });
  }

  _getStatisticsFromServer(ConfigModel configModel) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.all(12),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(allTranslations.text('refresh_processing')),
              SizedBox(height: 20),
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(
                  FunctionUtils.colorFromHex(configModel.mainColor)
                ),
              ),
            ],
          )
        );
      }
    );
    _getRemoteStatistics(1);
  }

  _statbarChart(){
    List _offreSpeculation;
    List nameSpeculation = [];
    List numberSpeculation = [];
    offreSpeculation = localStatisticsData[0].offreSpeculation;
    _offreSpeculation = jsonDecode(offreSpeculation);
    for(var i = 0; i < _offreSpeculation.length; i++){
      if(_offreSpeculation[i]['nbre'].toInt() > 0){
        nameSpeculation.add(_offreSpeculation[i]['name'].toString());
        numberSpeculation.add(_offreSpeculation[i]['nbre'].toDouble());
      }
    }
    if(numberSpeculation == null || numberSpeculation.length == 0){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 100),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0),
            Container(
              width: 300,
              child: Text(
                allTranslations.text('searching_statistics_empty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 15.0
                ),
              ),
            )
          ],
        ),
      );
    }else{
    return Container(
      height: MediaQuery.of(context).size.width * 1.10,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(20.0),
            alignment: Alignment.centerLeft,
            child: Text(
              'Par spéculation',
              style: const TextStyle(
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.85,
            child: BarChart(
              BarChartData(
                alignment: BarChartAlignment.spaceAround,
                maxY: 16.0,
                barTouchData: BarTouchData(enabled: false),
                titlesData: FlTitlesData(
                  show: true,
                  bottomTitles: SideTitles(
                    margin: 10.0,
                    showTitles: true,
                    rotateAngle: 35.0,
                    getTitles: (value){
                      for(var i = 0; i < nameSpeculation.length; i++){
                        if(value.toInt() == i){
                          return nameSpeculation[i]; 
                        }
                      }
                    },
                  ),
                  leftTitles: SideTitles(
                      margin: 10.0,
                      showTitles: true,
                      getTitles: (value) {
                        if (value == 0) {
                          return '0';
                        } else if (value % 3 == 0) {
                          return '${value ~/ 3 * 5}';
                        }
                        return '';
                      }),
                ),
                gridData: FlGridData(
                  show: true,
                  checkToShowHorizontalLine: (value) => value % 3 == 0,
                  getDrawingHorizontalLine: (value) => FlLine(
                    color: Colors.black12,
                    strokeWidth: 1.0,
                    dashArray: [5],
                  ),
                ),
                borderData: FlBorderData(
                  show: false,
                ),
                barGroups: numberSpeculation
                    .asMap()
                    .map((key, value) => MapEntry(
                        key,
                        BarChartGroupData(
                          x: key,
                          barRods: [
                            BarChartRodData(
                              y: value,
                              colors: [
                                Colors.purple,
                              ],
                            ),
                          ],
                        )))
                    .values
                    .toList(),
              ),
            ),
          ),
        ],
      ),
    );
    }
  }
  _formatPrice(int number){
    double nmberToDouble = number * 1.0;
    return FunctionUtils.separateur(nmberToDouble) + " F CFA";
  }
  _statGrid(){
    return Container(
      height: MediaQuery.of(context).size.height * 0.25,
      child: Column(
        children: [
          Flexible(
            child: Row(
              children: [
                _buildStatCard(
                  'Chiffre d\'affaire', 
                  _formatPrice(localStatisticsData[0]?.chiffreAffaire),
                  Colors.blue
                ),
              ],
            ),
          ),
          Flexible(
            child: Row(
              children: [
                _buildStatCard(
                  'Total offre',
                   localStatisticsData[0]?.totalOffre.toString() ?? "", 
                  Colors.blueGrey
                ),
                _buildStatCard(
                  'Offre active', 
                   localStatisticsData[0]?.activeOffre.toString() ?? "", 
                  Colors.green
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Expanded _buildStatCard(String title, String count, MaterialColor color){
    return Expanded(
      child: Container(
        margin: const EdgeInsets.all(8.0),
        padding: const EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: const TextStyle(
                color: Colors.white, fontSize: 13.0, fontWeight: FontWeight.w600,
              ),
            ),
            Text(
              count,
              style: const TextStyle(
                color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  SliverPadding _buildHeader() {
    return SliverPadding(
      padding: const EdgeInsets.all(25.0),
      sliver: SliverToBoxAdapter(
        child: Text(
          "Vos offres",
          style: const TextStyle(
            color: Colors.black,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}