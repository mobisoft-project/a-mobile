
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_meteo_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_meteo_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/meteo/detail_meteo_page.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:agrimobile/features/launch/presentation/pages/meteo/ask_meteo_page.dart';

class MeteoPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState();
  }
}



class PageState extends State<MeteoPage> {


  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();

  LocalMeteoRepository _localMeteoRepository = LocalMeteoRepository();
  List<LocalMeteoEntity> _localMeteoEntity;


  bool _getMeto=true;
  String _userToken="";
  String _userId="";

  var div= Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }



  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
          key: widget._scaffoldKey,
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AskMeteoPage()),
              ).then((value){
                _getMetoList(int.parse(_userId));
              });

            },
            child: Icon(
              Icons.add,
              color: Colors.white
            ),
            backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
          ),
          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_meteo'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      UrlLauncher.launch('tel:+${configModel.callCenter}');
                    },
                    child: Icon(Icons.phone,color: Colors.white,),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return ListView(

                    children: <Widget>[
                      if(_getMeto==false && _localMeteoEntity!=null &&  _localMeteoEntity.length>0)Container(
                          height:MediaQuery. of(context). size. height/3,
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            color: Colors.black,

                          ),
                          child:Column(
                            children: [
                              Text(
                                "${_localMeteoEntity[0].date}",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white ,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    width: 60,
                                    child: Column(
                                      children: [
                                        Text(
                                          "${_localMeteoEntity[0].tempMax}°C",
                                          style: TextStyle(
                                            color: Colors.white ,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16.0,
                                          ),
                                        ),
                                        SizedBox(height: 15,),
                                        Text(
                                          "${_localMeteoEntity[0].tempMin}°C",
                                          style: TextStyle(
                                            color: Colors.white70,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(child: Column(
                                    children: [
                                      Image.asset('assets/meteo/${_localMeteoEntity[0].description.replaceAll(" ", "-").replaceAll("é", "e").replaceAll("è", "e")}.png',height:100,width: 100,),
                                      Text(
                                        "${_localMeteoEntity[0].description}",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14.0,
                                        ),
                                      )
                                    ],
                                  )),

                                  Container(
                                    // padding: EdgeInsets.only(top:20,bottom: 20),
                                    width: 60,
                                    height: MediaQuery. of(context). size. height/3-60,
                                    child: Column(
                                      children: [

                                        Image.asset('assets/meteo/sunrise.png',height:50,width: 50,),
                                        Text(
                                          "${_localMeteoEntity[0].tempMorn}°C",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14.0,
                                          ),
                                        ),
                                        Spacer(),
                                        Image.asset('assets/meteo/sunset.png',height:50,width: 50,),
                                        Text(
                                          "${_localMeteoEntity[0].tempEve}°C",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14.0,
                                          ),
                                        ),

                                      ],
                                    ),
                                  )
                                ],

                              )
                            ],
                          )
                      ),

                      _getAbonnementListview(),
                    ]
                );

              }
          )
      );
    },
  );

  
  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userToken="${value.token}";
        _userId="${value.id}";
      });
      _getMetoList(int.parse(_userId));
    });


  }

  _getMetoList(int idUser){

    //recuperation des prix disponible sur le marche
    _localMeteoRepository.allMeteo(idUser).then((response) {
      if(response.length>0 ){

        setState(() {
          _getMeto=false;
          _localMeteoEntity=response;
        });

      }else{
        setState(() {
          _getMeto=false;
        });
      }

    });

  }

  _getAbonnementListview(){

    if (_getMeto==true){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/loading.gif',height:150),
            ),
            SizedBox(height: 30,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_loading'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else if (_localMeteoEntity==null || _localMeteoEntity.length==0 ){
      return Center(
        child: Column(
          children: [
            SizedBox(height: 50,),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg',height:250),
            ),
            SizedBox(height: 0,),
            Container(
              width:300,
              child: Text(
                allTranslations.text('searching_mempty'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black54,
                  fontWeight: FontWeight.bold,
                  fontSize: 22.0,
                ),
              ),
            )
          ],
        ),
      );
    }else{
      return ListView.builder(
          padding: EdgeInsets.all( 20.0),
          itemCount:_localMeteoEntity.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            LocalMeteoEntity infoMeteo= _localMeteoEntity[position];

            String name=infoMeteo.description.replaceAll(" ", "-").replaceAll("é", "e").replaceAll("è", "e") ;


            return Column(
                children: [

                  MaterialButton(
                    elevation: 0.0,
                    child: Row(
                      children: [
                       // SizedBox(width: 10,),
                        Image.asset('assets/meteo/$name.png',height:50,width: 50,),
                        SizedBox(width: 10,),
                        Expanded(child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              "${infoMeteo.date}",
                              style: TextStyle(
                                color: Colors.black ,
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              ),
                            ),
                            Text(
                              "${infoMeteo.description}",
                              style: TextStyle(
                                color: Colors.black54,
                                fontWeight: FontWeight.w500,
                                fontSize: 14.0,
                              ),
                            ),
                          ],
                        )),
                      ],
                    ),
                    //textColor: Colors.white,
                    onPressed: (){

                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DetailMeteoPage(infoMeteo)),
                      );
                    },
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  )
                  ,
                  div,
                ]
            );

          });
    }

  }


}

