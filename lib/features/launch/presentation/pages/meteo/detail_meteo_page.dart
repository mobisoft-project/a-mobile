import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/dto/connection_dto.dart';
import 'package:agrimobile/features/common/data/dto/meteo_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_meteo_entity.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/account_page.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:styled_text/styled_text.dart';



class DetailMeteoPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  LocalMeteoEntity infoMeteo;
  DetailMeteoPage(this.infoMeteo);

  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState(this.infoMeteo);
  }
}



class PageState extends State<DetailMeteoPage> {

  LocalMeteoEntity _infoMeteo;
  PageState(this._infoMeteo);


  @override
  void initState() {
    super.initState();
  }




  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:  Align(
            // alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(right: 50.0),
              alignment: Alignment.center,
              child: Text(allTranslations.text('detail_meteo'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,

                ),
              ),
            ),
          ),
          backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        body: Stack(
            children: <Widget>[
              Container(
                  height:MediaQuery. of(context). size. height/3,
                  padding: EdgeInsets.all(20),

                  child:Column(
                    children: [
                      Text(
                        "${_infoMeteo.date}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black ,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      Row(
                        children: [
                          Container(
                            width: 60,
                            child: Column(
                              children: [
                                Text(
                                  "${_infoMeteo.tempMax}°C",
                                  style: TextStyle(
                                    color: Colors.black ,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.0,
                                  ),
                                ),
                                SizedBox(height: 15,),
                                Text(
                                  "${_infoMeteo.tempMin}°C",
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(child: Column(
                            children: [
                              Image.asset('assets/meteo/${_infoMeteo.description.replaceAll(" ", "-").replaceAll("é", "e").replaceAll("è", "e")}.png',height:100,width: 100,),
                              Text(
                                "${_infoMeteo.description}",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14.0,
                                ),
                              )
                            ],
                          )),

                          Container(
                            // padding: EdgeInsets.only(top:20,bottom: 20),
                            width: 60,
                            height: MediaQuery. of(context). size. height/3-60,
                            child: Column(
                              children: [

                                Image.asset('assets/meteo/sunrise.png',height:50,width: 50,),
                                Text(
                                  "${_infoMeteo.tempMorn}°C",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14.0,
                                  ),
                                ),
                                Spacer(),
                                Image.asset('assets/meteo/sunset.png',height:50,width: 50,),
                                Text(
                                  "${_infoMeteo.tempEve}°C",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14.0,
                                  ),
                                ),

                              ],
                            ),
                          )
                        ],

                      )
                    ],
                  )
              ),
              Container(
                  margin: EdgeInsets.only(top:MediaQuery. of(context). size. height/3),
                  width: MediaQuery. of(context).size.width,
                  height:MediaQuery. of(context). size. height/2+60,
                  padding: EdgeInsets.only(top:100),
                  decoration: BoxDecoration(
                    color: Colors.black87,
                  ),
                  child:Column(
                    children: [
                      Text(
                        "${allTranslations.text('humidite')} ${_infoMeteo.humidity} %",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white ,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      SizedBox(height: 20,),
                      Text(
                        "${allTranslations.text('temp_day')}  ${_infoMeteo.tempDay} °C",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white ,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      SizedBox(height: 20,),
                      Text(
                        "${allTranslations.text('temp_night')}  ${_infoMeteo.tempNight} °C",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white ,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                    ],
                  )
              )
            ]
        ),
      );
    },
  );


}

