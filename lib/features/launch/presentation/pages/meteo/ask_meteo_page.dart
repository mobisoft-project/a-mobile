import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/dto/connection_dto.dart';
import 'package:agrimobile/features/common/data/dto/meteo_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/account_page.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:styled_text/styled_text.dart';



class AskMeteoPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  State<StatefulWidget> createState() {


    // TODO: implement createState

    return PageState();
  }
}



class PageState extends State<AskMeteoPage> {

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  TextEditingController _dateDebutController=TextEditingController();
  TextEditingController _dateFinController=TextEditingController();
  MeteoDto _meteoDto = MeteoDto();
  String _accessToken="";
  String _userId="";
  String _latitude="";
  String _longitude="";

  final format= DateFormat("dd/MM/yyyy");
  int years=2020;


  @override
  void initState() {
    super.initState();

    var now =new DateTime.now();
    var formatter =new DateFormat("yyyy");
    years=int.parse(formatter.format(now))+1;

    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:  Align(
            // alignment: Alignment.topCenter,
            child: Container(
              margin: EdgeInsets.only(right: 50.0),
              alignment: Alignment.center,
              child: Text(allTranslations.text('ask_meteo'),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,

                ),
              ),
            ),
          ),
          backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        body: Stack(

            children: <Widget>[
              ListView(
                  padding: EdgeInsets.all( 20.0),
                  children: <Widget>[
                    Text(allTranslations.text('ask_meteodescription'),
                      style: TextStyle(
                        color: Colors.black54,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                    SizedBox(height: 30,),
                    Container(
                      width: MediaQuery.of(context).size.width/1.2,
                      height: 45,
                      margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          top: 4,left: 16, right: 16, bottom: 4
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5
                            )
                          ]
                      ),
                      child:  DateTimeField(
                        format: format,
                        controller: _dateDebutController,
                        onShowPicker: (context, currentValue) {
                          return showDatePicker(
                              context: context,
                              firstDate: DateTime.now(),
                              initialDate: DateTime.now(),
                              lastDate: DateTime.now().add(Duration(days: 10))
                          );
                        },
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          icon: Icon(Icons.date_range,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                          ),
                          hintText:  allTranslations.text('datedebut'),
                        ),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width/1.2,
                      height: 45,
                      margin: EdgeInsets.only(top: 12),
                      padding: EdgeInsets.only(
                          top: 4,left: 16, right: 16, bottom: 4
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(50)
                          ),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                blurRadius: 5
                            )
                          ]
                      ),
                      child:  DateTimeField(
                        format: format,
                        controller: _dateFinController,
                        onShowPicker: (context, currentValue) {
                          return showDatePicker(
                              context: context,
                              firstDate: DateTime.now(),
                              initialDate: DateTime.now(),
                              lastDate: DateTime.now().add(Duration(days: 10))
                          );
                        },
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          icon: Icon(Icons.date_range,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                          ),
                          hintText:  allTranslations.text('datefin'),
                        ),
                      ),
                    ),

                    SizedBox(height: 50),
                    MaterialButton(
                      color:  FunctionUtils.colorFromHex(configModel.mainColor),
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color:  FunctionUtils.colorFromHex(configModel.mainColor))
                      ),
                      textColor: Colors.white,
                      onPressed: () {

                        if(_latitude.compareTo("")!=0 && _longitude.compareTo("")!=0){

                          if(_dateDebutController.text.compareTo("")!=0 && _dateFinController.text.compareTo("")!=0 ) {

                            DateTime dateDebut = DateTime.parse(FunctionUtils.convertFormat(_dateDebutController.text));
                            DateTime dateFin = DateTime.parse(FunctionUtils.convertFormat(_dateFinController.text));

                            int differenceInDays = dateFin.difference(dateDebut).inDays;

                            if(differenceInDays>=0) {


                              _meteoDto.nbre="${(differenceInDays+1)}";
                              _meteoDto.accessToken="$_accessToken";
                              _meteoDto.idUser="$_userId";
                              _meteoDto.latitude="$_latitude";
                              _meteoDto.longitude="$_longitude";
                              _meteoDto.dateDebut="${dateDebut.toString().split(" ")[0]}";
                              _askMeteo(configModel);

                            }else{
                              _displaySnackBar(context, allTranslations.text('datemeteo_error'));
                            }

                          }else{
                            _displaySnackBar(context, allTranslations.text('emptyField'));
                          }

                        }else{
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => AccountPage("1")),
                          );
                          _displaySnackBar(context, allTranslations.text('emptyLocation'));
                        }

                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(
                            allTranslations.text('submit'),
                            style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white, fontSize: 16.0),
                          ),
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(0, 15, 0, 15),

                    ),
                  ])
            ]
        ),
      );
    },
  );

  
  _getUserInformation(){
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _accessToken=value.token;
        _userId="${value.id}";
        _latitude="${value.latitude}";
        _longitude="${value.longitude}";
      });

    });


  }
  
  

  _askMeteo(ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('meteo_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.askMeteo(_meteoDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if(a.status.compareTo("ok")==0){


            int total=a.meteos.length;
            if( total>0 ){

                String text="";
                //enregistrement des meteos
                FunctionUtils.saveMeteo(a.meteos,int.parse((_userId)));
                for(var i=0;i<total;i++){
                  //if(text.compareTo("")!="")text+="<br/><br/>";

                  text+= "<bold>${a.meteos[i].date}</bold><br/>Le secteur de votre champ serait : <bold>${a.meteos[i].description}</bold>. La température moyenne de la journée serait : ${ a.meteos[i].tempDay} "
                      "°C. La Température dans la matinée serait : ${a.meteos[i].tempMorn} °C. La Température dans l après-midi serait: ${a.meteos[i].tempEve}  °C. La Température dans la nuit serait : ${a.meteos[i].tempNight}"
                      " °C. L humidité de la journée serait : ${a.meteos[i].humidity}%.<br/><br/>";

                }

                Navigator.of(context).pop(null);
                _showAlertDialog(context,text);

            }else{
              Navigator.of(context).pop(null);
              _displaySnackBar(context, allTranslations.text('error_process'));
            }


            return true;

          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });

  }

  _showAlertDialog(BuildContext context,String content) {

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('ok'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        Navigator.pop(context,"update");
      },
    );


    AlertDialog alert = AlertDialog(
      content:Container(
        height:400,
        width: MediaQuery.of(context).size.width,
        child:ListView(
            padding: EdgeInsets.all( 20.0),
            children: <Widget>[
              StyledText(
                  text: content,
                  styles: {
                    'bold': TextStyle(fontWeight: FontWeight.bold),
                  },
                  style: new TextStyle( fontSize: 15.0)
              )
            ]
        )
      ),
      /*content:StyledText(
        text: content,
        styles: {
          'bold': TextStyle(fontWeight: FontWeight.bold),
        },
        style: new TextStyle( fontSize: 15.0)
      ),*/
      actions: [
        cancelButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}

