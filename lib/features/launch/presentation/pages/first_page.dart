import 'dart:async';

import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/core/utils/connection_status.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_country_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_region_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/registration_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:agrimobile/allTranslations.dart';


class FirstPage extends StatefulWidget {
  @override
  FirstPageState createState() => FirstPageState();
}

class FirstPageState extends State<FirstPage> {


  LocalRegionRepository _localRegionRepository = LocalRegionRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();

  @override
  void initState() {
    super.initState();

    _getServerCountry();
    _getServerRegion();

  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
        //backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
        body: Container(
          /*decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/img/splash.jpg"),
              fit: BoxFit.cover,
            ),
          ),*/
          child:   Container(
            //height: MediaQuery.of(context).size.height/2,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(top: 62),
            child: Column(
              children: <Widget>[
                Spacer(),
                Center(
                  child: Image(
                    image: AssetImage("assets/img/longlogo.png"),
                    height: 150,
                  ),
                ),

                Spacer(),
                InkWell(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginPage("0")),
                    );
                  },
                  child: Container(
                    height: 45,
                    width: MediaQuery.of(context).size.width/1.2,
                    decoration: BoxDecoration(
                        color:FunctionUtils.colorFromHex(configModel.mainColor),
                        borderRadius: BorderRadius.all(
                            Radius.circular(50)
                        )
                    ),
                    child: Center(
                      child: Text(allTranslations.text('connexion').toUpperCase(),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                ),
                //Spacer(),
                SizedBox(
                  height: 20,
                ),
                InkWell(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RegistrationPage()),
                    );
                  },
                  child: Container(
                    height: 45,
                    width: MediaQuery.of(context).size.width/1.2,
                    decoration: BoxDecoration(
                        color:FunctionUtils.colorFromHex(configModel.mainColor),
                        borderRadius: BorderRadius.all(
                            Radius.circular(50)
                        )
                    ),
                    child: Center(
                      child: Text('Inscription'.toUpperCase(),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
          ),
        ),
      );
    },
  );

  _getServerRegion () async {

    _localRegionRepository.allRegion().then((response) {

      _appSharedPreferences.getLastUpdateRegion().then((value){
        bool continueProcess=true;
        int currentLdate=DateTime.now().millisecondsSinceEpoch;
         if(value.compareTo("0")!=0){
            if( (currentLdate-int.parse(value))<DataConstantesUtils.REQUEST_SECONDE){
              continueProcess=false;
            }
         }


          if(response.length==0 || (response.length>0 && continueProcess==true)){
            Api api = ApiRepository();
            api.getRegion().then((value) {
              if (value.isRight()) {
                value.all((a) {
                  FunctionUtils.saveRegion(a);
                  _appSharedPreferences.setLastUpdateRegion("$currentLdate");
                  return true;
                });
              } else {
                return false;
              }
            });
          }
      });

    });

  }

  _getServerCountry () async {

    _localRegionRepository.allRegion().then((response) {

      _appSharedPreferences.getLastUpdateCountry().then((value){
        bool continueProcess=true;
        int currentLdate=DateTime.now().millisecondsSinceEpoch;
         if(value.compareTo("0")!=0){
            if( (currentLdate-int.parse(value))<DataConstantesUtils.REQUEST_SECONDE){
              continueProcess=false;
            }
         }


          if(response.length==0 || (response.length>0 && continueProcess==true)){
              Api api = ApiRepository();
              api.getCountry().then((value) {
                if (value.isRight()) {
                  value.all((a) {
                    FunctionUtils.saveCountry(a).then((value){
                      _appSharedPreferences.setLastUpdateCountry("$currentLdate");
                    });
                    return true;
                  });
                } else {
                  return false;
                }
              });
          }
      });

    });

  }

}
