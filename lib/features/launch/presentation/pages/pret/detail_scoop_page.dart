
import 'package:agrimobile/core/utils/preference.dart';
import 'dart:async';
import 'dart:convert';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/detailscoop_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';

import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';

import 'package:agrimobile/features/common/domain/repositories/local_scoop_repository.dart';
import 'package:agrimobile/features/common/data/dto/simulation_dto.dart';

class DetailScoopPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final LocalScoopEntity infoScoop;
  DetailScoopPage(this.infoScoop);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState(this.infoScoop);
  }
}



class PageState extends State<DetailScoopPage> {

  var statutKey = GlobalKey<FindDropdownState>();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalScoopEntity _infoScoop;
  PageState(this._infoScoop);

  List<DetailScoopModel> _allComposantScoop;
  bool _haveScoop=false;
  String _statutScoop="";
  String _userToken="";
  double _height=100;

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 80.0)),

          appBar: new AppBar(
            title:  Text("${_infoScoop.nameAssociation}: ${_infoScoop.cantonAssociation}",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,

              ),
            ),
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),



          body:Builder(
              builder: (BuildContext myContext)
              {

                return  Stack(
                 // crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    if(_infoScoop.roleMember.compareTo("4")==0 || _infoScoop.roleMember.compareTo("3")==0)Container(
                      padding: EdgeInsets.only(left:20,right: 20,top: 10,bottom: 10),
                      child: Column(
                        children: [
                          Text("${allTranslations.text('type_statutScoop')}",style: TextStyle(
                              color:Colors.red,
                              fontSize: 16,
                              fontWeight: FontWeight.bold
                          ),),
                          SizedBox(height: 5),
                          FindDropdown(
                            key: statutKey,
                            items: ["${allTranslations.text('type_association1')}","${allTranslations.text('type_association2')}","${allTranslations.text('type_association3')}"],
                            searchBoxDecoration: InputDecoration(
                              hintText: allTranslations.text('search'),
                              border: OutlineInputBorder(),
                            ),
                            emptyBuilder: (BuildContext context){
                              return  Container(
                                decoration: new BoxDecoration(
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                      color: Colors.red,
                                    ),
                                    color: Colors.red),
                                height: 50,
                                margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                                child: Center(
                                  child: Text("${allTranslations.text('liste_empty')}",
                                    style: new TextStyle(
                                        color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                                  ),
                                ),
                              );
                            },
                            onChanged: (String item){

                              _statutScoop="";
                              if(item!=null){
                                if(item.compareTo("${allTranslations.text('type_association1')}")==0){
                                  _statutScoop="1";
                                }else if(item.compareTo("${allTranslations.text('type_association2')}")==0){
                                  _statutScoop="2";
                                }else if(item.compareTo("${allTranslations.text('type_association3')}")==0){
                                  _statutScoop="3";
                                }
                              }
                            },

                          ),
                        ],
                      ),
                    ),
                    if(_infoScoop.roleMember.compareTo("4")!=0 && _infoScoop.roleMember.compareTo("3")!=0)Container(
                      height: 60,
                      padding: EdgeInsets.only(left:20,right: 20,top: 10,bottom: 10),
                      child:Center(
                          child: Text("${allTranslations.text('type_association${_infoScoop.typeAssociation}')}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color:Colors.red,
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                            ),
                          )
                      )
                    ),
                    SizedBox(height: 20,),
                    Container(
                      margin: EdgeInsets.only(left:10,right: 10,top: _height,bottom: 60),
                      child:   _getDetailListview(configModel),
                    ),
                    Container(
                      // height: 65.0,
                      alignment: Alignment.bottomCenter,
                      //
                      child:
                      Container(

                        decoration: BoxDecoration(
                          border: Border.all(
                              color:Colors.white70,
                              style: BorderStyle.solid,
                              width: 1.0
                          ),
                          color: Colors.white70,
                        ),

                        child: Row(
                          children: [
                            Expanded(
                              child: Text(""),
                            ),
                            if(_infoScoop.roleMember.compareTo("4")==0 || _infoScoop.roleMember.compareTo("3")==0)Expanded(
                              child: Container(
                                child:
                                MaterialButton(

                                  onPressed: () {
                                     if(_infoScoop.typeAssociation.compareTo(_statutScoop)!=0){
                                        _changeStatut(configModel);
                                     }else{
                                       _displaySnackBar(context, allTranslations.text('no_changemade'));
                                     }
                                  },

                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 45.0,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                                            style: BorderStyle.solid,
                                            width: 1.0
                                        ),
                                        color: FunctionUtils.colorFromHex(configModel.mainColor) ,
                                        borderRadius: BorderRadius.circular(5.0)
                                    ),
                                    child: Text(allTranslations.text('save_update'),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white
                                      )
                                      ,
                                    ),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),

                              ),

                            )
                          ],
                        ),
                      ),



                    ),
                  ],
                );

              }
          )
      );
    },
  );

  _getUserInformation(){

    if(_infoScoop.roleMember.compareTo("4")!=0 && _infoScoop.roleMember.compareTo("3")!=0){
      setState(() {
        _height=60;
      });
    }else{
      setState(() {
        _height=100;
      });
    }
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userToken=value.token;
      });
    });

    String detailScoop=_infoScoop.memberAssociation;
    List<dynamic> responseMap = jsonDecode(detailScoop.toString());

    setState(() {
      _haveScoop=true;
      _allComposantScoop =responseMap.map((i) => DetailScoopModel.fromMap(i)).toList();
    });

    Timer(Duration(seconds: 1),(){
      _statutScoop=_infoScoop.typeAssociation;
      if(statutKey!=null && statutKey.currentState!=null) {
        statutKey.currentState.setSelectedItem(allTranslations.text('type_association$_statutScoop'));
      }

    });




  }

  _getDetailListview(ConfigModel configModel){

    if(_haveScoop==false) {
      return Column(
        children: [
          SizedBox(height: 100,),
          Image.asset('assets/img/loading.gif',width: 150,height: 150,),
          SizedBox(height: 30,),
          Center(
            child: Text("${allTranslations.text('getting_processing')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );
    }else if(_allComposantScoop==null || _allComposantScoop.length==0){

      return Column(
        children: [
          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('detailscoop_empty')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );

    }else{
      return  ListView.builder(
          itemCount: _allComposantScoop.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            DetailScoopModel infoScoop=  _allComposantScoop[position];

            return Container(
              child: Column(
                children: [

                  MaterialButton(

                    elevation: 0.0,
                    onPressed: ()  {

                    },
                    child: Container(

                      padding: EdgeInsets.fromLTRB(10, 3, 10, 3),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color:FunctionUtils.colorFromHex("DEDEDE"),
                              style: BorderStyle.solid,
                              width: 1.0
                          ),
                          borderRadius: BorderRadius.circular(5.0)
                      ),
                      child: Row(
                        children: [
                          CircleAvatar(
                              radius: 15,
                              backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
                              child:CircleAvatar(
                                radius: 14.0,
                                backgroundColor:Colors.white,
                                child: Icon(Icons.person,
                                  color: FunctionUtils.colorFromHex(configModel.mainColor),
                                  size: 20,
                                ),
                              )
                          ),
                          SizedBox(width: 5,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                Row(
                                    children:[
                                      Expanded(child:   Text("${infoScoop.fullName}",
                                        style: new TextStyle(
                                            color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                      ),),
                                      SizedBox(width: 15,),
                                      Container(
                                        width: 85,
                                        child:  Text("${infoScoop.role}",
                                          style: new TextStyle(color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 12.0,fontWeight: FontWeight.bold),
                                        ),
                                      )
                                    ]
                                ),
                                SizedBox(height: 2,),
                                Row(
                                    children:[
                                      Expanded(child: Text("${infoScoop.phoneNumber}",
                                        style: new TextStyle(
                                            color: Colors.black45, fontSize: 12.0,fontWeight: FontWeight.w500),
                                      ),),
                                      SizedBox(width: 15,),
                                      Container(
                                        width: 85,
                                        child:  Text("${infoScoop.superficie} Hectare(s)",
                                          style: new TextStyle(
                                              color: Colors.black45, fontSize: 12.0,fontWeight: FontWeight.w500),
                                        ),
                                      )
                                    ]
                                )
                              ],
                            ),
                          ),
                          Icon(Icons.navigate_next, color: FunctionUtils.colorFromHex(configModel.mainColor),size: 20,),
                        ],
                      ),
                    ),
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  ),
                ],
              ),
            );

          });
    }

  }


  _changeStatut (ConfigModel configModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('update_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
    });
    SimulationDto simulationDto=SimulationDto();

    simulationDto.userToken=_userToken;
    simulationDto.keyScoop="${_infoScoop.idAssociation}";
    simulationDto.statutScoop=_statutScoop;


    Api api = ApiRepository();
    api.sendStatut(simulationDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          if(a.status.compareTo("000")==0){


            LocalScoopRepository localScoopRepository = LocalScoopRepository();
            localScoopRepository.existScoop(_infoScoop.idAssociation).then((response) {

              if (response.length > 0) {
                response[0].typeAssociation = _statutScoop;
                localScoopRepository.update(response[0], response[0].idScoop);
                Navigator.of(context).pop(null);
                Navigator.of(context).pop("updated");
              }else{
                Navigator.of(context).pop(null);
                _displaySnackBar(context, allTranslations.text('error_process'));
              }
            });


            return true;

          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
          }

          return true;
        });
      } else {
        _displaySnackBar(context, allTranslations.text('error_process'));
        Navigator.of(context).pop(null);
        return false;
      }
    });



  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
