
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/dto/eop_dto.dart';
import 'package:agrimobile/features/common/data/dto/simulation_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_eop_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_pret_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_eop_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_pret_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_upret_member_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_upret_page.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:find_dropdown/find_dropdown.dart';

class UserPretPage extends StatefulWidget {
  LocalPretEntity defaultPretEntity;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  UserPretPage(this.defaultPretEntity);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState(this.defaultPretEntity);
  }
}



class PageState extends State<UserPretPage> {

  LocalPretEntity defaultPretEntity;
  PageState(this.defaultPretEntity);

  LocalEopRepository _localEopRepository = LocalEopRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();

  TextEditingController _superficieController=TextEditingController();

  LocalEopEntity selectedEop ;
  final _formKey = GlobalKey<FormState>();

  int _userId=0;
  String _userToken="";
  String _keyEop="";

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );
  List<Step> steps = List();
  int _currentStep = 0;

  ConfigModel _myConfigModel ;

  final eopKey = GlobalKey<FindDropdownState>();
  List<String> _allfile =[];
  List<File>  _image;

  TextEditingController _denominationController=TextEditingController();
  TextEditingController _descriptionController=TextEditingController();

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(

    builder: (context, configModel, child) {

      _myConfigModel=configModel;

      steps = [
        Step(
            title:  Text(allTranslations.text('title_eop'),style: TextStyle(
              fontSize: 12,
            )),
            subtitle: Text(allTranslations.text('sub_eop'),style: TextStyle(
              fontSize: 10,
            )),
            isActive: true,
            state: _currentStep==0?StepState.editing:StepState.complete,
            content:
            Form(
                key: _formKey,
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("${allTranslations.text('selectEop')} **",style: TextStyle(
                        color:Colors.black,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                    ),),
                    SizedBox(height: 5),
                    FindDropdown<LocalEopEntity>(
                      key: eopKey,
                      onFind: (String filter) => getEop(filter),
                      searchBoxDecoration: InputDecoration(
                        hintText: allTranslations.text('search'),
                        border: OutlineInputBorder(),
                      ),
                      dropdownBuilder: (BuildContext context, LocalEopEntity item) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10),
                          height: 40,
                          decoration: BoxDecoration(
                            border: Border.all(color: Theme.of(context).dividerColor),
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white,
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: item==null?Text(""):Text("${item.nameSite} (${item.nameSpeculation})"),
                              ),
                              Icon(
                                Icons.arrow_drop_down,
                                color: Colors.grey,
                                size: 20,
                              ),
                              // SizedBox(width: 5,)
                            ],
                          ),
                        );
                      },
                      dropdownItemBuilder:(BuildContext context, LocalEopEntity item, bool isSelected) {
                        return Container(
                          decoration: !isSelected ? null : BoxDecoration(
                            color: Colors.white,
                          ),
                          child: ListTile(
                            selected: isSelected,
                            title: item==null?Text(""):Text("${item.nameSite} (${item.nameSpeculation})"),
                          ),
                        );
                      },
                      emptyBuilder: (BuildContext context){
                        return  Container(
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                color: Colors.red,
                              ),
                              color: Colors.red),
                          height: 50,
                          margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                          child: Center(
                            child: Text("${allTranslations.text('liste_empty')}",
                              style: new TextStyle(
                                  color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                            ),
                          ),
                        );
                      },

                      onChanged: (LocalEopEntity data) {

                        _allfile=[];
                        _keyEop="";
                        selectedEop=data;
                        if(data!=null ){
                          _keyEop="${data.keyEop}";

                          if(data.fichierEop!=null && data.fichierEop.compareTo("")!=0) {
                            setState(() {
                              _allfile = data.fichierEop.split(";");
                            });
                          }else{
                            setState(() {
                              _allfile=[];
                            });
                          }
                        }
                        _image=[];
                        _image= new List(_allfile.length);


                      },
                    ),

                    SizedBox(height: 10.0),


                  ],
                )
            )


        ),
        Step(
          isActive: _currentStep<1?false:true,
          state: _currentStep<1?StepState.disabled:_currentStep==1?StepState.editing:StepState.complete,
          title:  Text(allTranslations.text('info'),style: TextStyle(
            fontSize: 12,
          )),
          subtitle: Text(allTranslations.text('sub_superficie'),style: TextStyle(
            fontSize: 10,
          )),
          content:
          Form(
              key: _formKey,
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("${allTranslations.text('superficie')} **",style: TextStyle(
                      color:Colors.black,
                      fontSize: 13,
                      fontWeight: FontWeight.w500
                  ),),
                  SizedBox(height: 5),
                  Container(
                    color: Colors.white,
                    child: TextFormField(
                        controller: _superficieController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          isDense: true,
                          prefixIcon: Icon(Icons.map,size: 18.0,color: Colors.black45),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        )
                    ),
                  ),

                ],
              )
          ),
        ),
        Step(
            title:  Text(allTranslations.text('title_file'),style: TextStyle(
              fontSize: 12,
            )),
            subtitle: Text(allTranslations.text('subtitle_file'),style: TextStyle(
              fontSize: 10,
            ),
            ),
            isActive: _currentStep<2?false:true,
            state: _currentStep<2?StepState.disabled:_currentStep==2?StepState.editing:StepState.complete,
            content:
            Form(
                key: _formKey,
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: _getProjetFile(),
                )
            )
        ),
      ];

      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 80.0)),

          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_udemandepret'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,

                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _savePretStart ();
                    },
                    child: Icon(
                      Icons.backup,
                      size: 26.0,
                    ),
                  )
              ),Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _getServerEop (configModel,1);
                    },
                    child: Icon(
                      Icons.refresh,
                      size: 26.0,
                    ),
                  )
              ),
             
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return
                  Column(children: <Widget>[
                    Expanded(
                      child: Stepper(
                        steps: steps,
                        type: StepperType.horizontal,
                        currentStep: _currentStep,
                        onStepContinue: next,
                        onStepTapped: (step) => goTo(step),
                        onStepCancel: cancel,
                        controlsBuilder:
                            (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                          return Row(
                            children: <Widget>[
                            ],
                          );
                        },
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomCenter,
                      child:
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                              color:Colors.white70,
                              style: BorderStyle.solid,
                              width: 1.0
                          ),
                          color: Colors.white70,
                        ),

                        child: Row(
                          children: [
                            Expanded(
                              child: Visibility(
                                child:
                                Container(
                                  child:
                                  MaterialButton(

                                    onPressed: () {
                                       cancel();
                                    },

                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 45.0,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color:FunctionUtils.colorFromHex(configModel.mainColor),
                                              style: BorderStyle.solid,
                                              width: 1.0
                                          ),
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(5.0)
                                      ),
                                      child: Text(
                                        allTranslations.text('previor'),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: FunctionUtils.colorFromHex(configModel.mainColor)
                                        )
                                        ,
                                      ),
                                    ),
                                    padding: EdgeInsets.all(10),
                                  ),

                                ),
                                visible: _currentStep==0?false:true,
                              ),
                            ),
                            Expanded(
                              child:
                              Container(
                                child:
                                MaterialButton(

                                  onPressed: () {
                                    next();
                                  },

                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 45.0,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                                            style: BorderStyle.solid,
                                            width: 1.0
                                        ),
                                        color: FunctionUtils.colorFromHex(configModel.mainColor) ,
                                        borderRadius: BorderRadius.circular(5.0)
                                    ),
                                    child: Text(_currentStep==2?allTranslations.text('submitbtn'):allTranslations.text('nextbtn'),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white
                                      )
                                      ,
                                    ),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),

                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ]);
              }
          )
      );
    },
  );

  _getProjetFile(){

    Widget defaultContent= Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        Text("${allTranslations.text('denomination_projet')} **",style: TextStyle(
            color:Colors.black,
            fontSize: 13,
            fontWeight: FontWeight.w500
        ),),
        SizedBox(height: 5),
        Container(
          child: TextFormField(
              controller: _denominationController,
              decoration: InputDecoration(
                isDense: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              )
          ),
        ),
        SizedBox(height: 20),
        Text("${allTranslations.text('description_projet')} **",style: TextStyle(
            color:Colors.black,
            fontSize: 13,
            fontWeight: FontWeight.w500
        ),),
        SizedBox(height: 5),
        Container(
          child: TextFormField(
              controller: _descriptionController,
              textInputAction: TextInputAction.newline,
              keyboardType: TextInputType.multiline,
              minLines: 4,
              maxLines: null,
              decoration: InputDecoration(
                isDense: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              )
          ),
        ),

      ],
    );

    List<Widget> allWidget =[];
    allWidget.add(defaultContent);

    if(_allfile!=null &&  _allfile.length>0) {

      allWidget.add( SizedBox(height: 20));
      allWidget.add( Text("${allTranslations.text('add_document')}",style: TextStyle(
          color:Colors.red,
          fontSize: 15,
          fontWeight: FontWeight.bold
      ),),);
      for(int u=0;u<_allfile.length;u++){

        String filename="";
        if(_image[u]!=null){
          List<String> pathInfo=_image[u].path.split("/");
          filename=pathInfo[pathInfo.length-1];
        }

        Widget fileContent= Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20),
            Text("${_allfile[u]} ",style: TextStyle(
                color:Colors.black,
                fontSize: 13,
                fontWeight: FontWeight.w500
            ),),
            SizedBox(height: 5),
            RawMaterialButton(
              onPressed: () {
                //_showChoiceDialog(context,configModel);
                _openGallery(u);
              },
              child: new Icon( Icons.insert_drive_file, color: FunctionUtils.colorFromHex(_myConfigModel.mainColor), size: 25.0, ),
              shape: new CircleBorder(),
              elevation: 2.0,
              fillColor: Colors.white,
              padding: const EdgeInsets.all(10.0),
            ),
            if(filename.compareTo("")!=0)Text("${filename} ",style: TextStyle(
                color:Colors.red,
                fontSize: 12,
                fontWeight: FontWeight.w500
            ),),

          ],
        );

        allWidget.add(fileContent);

      }


    }
    return allWidget ;

  }

  Future _openGallery(int position) async {

    FilePickerCross myFile = await FilePickerCross.importFromStorage(
      type: FileTypeCross.any,       // Available: `any`, `audio`, `image`, `video`, `custom`. Note: not available using FDE
      //fileExtension: 'txt, md, pdf'     // Only if FileTypeCross.custom . May be any file extension like `dot`, `ppt,pptx,odp`
    );

    if(myFile != null) {
      setState(() {
        _image[position] = File(myFile.path);

      });
    }

  }

  Future<List<LocalEopEntity>> getEop(String filter){
    //
    return  _localEopRepository.filterEop(_userId,filter);
  }

  next() {


    if(_currentStep + 1 != steps.length){
      goTo(_currentStep + 1);
    }else{
      goTo(3);
    }

  }

  cancel() {
    if (_currentStep > 0) {
      goTo(_currentStep - 1);
    }else{
      Navigator.pop(context, true);
    }
  }

  goTo(int step) {

    int oldStep=_currentStep;

    if(step==0) {

      setState(() => _currentStep = step);

    }else if(step==1){
      //verification si toute les informations sur la voiture sont ok
      if( (_keyEop.compareTo("")!=0 )){
        setState(() => _currentStep = step);
      }else{
        _displaySnackBar(context, allTranslations.text('empty_field'));
      }

    }else if(step==2){
      //verifier si la superficie est definie
      if(_superficieController.text.compareTo("")==0 || _superficieController.text.compareTo("0")==0){
        _displaySnackBar(context, allTranslations.text('empty_field'));
      }else{
        setState(() => _currentStep = step);
      }

    }else if(step==3){

      if(_denominationController.text.compareTo("")!=0 && _descriptionController.text.compareTo("")!=0 ){
        bool verifcationFile=true;

        if(_image!=null && _image.length>0){
          for(int u=0;u<_image.length;u++){

            if(_image[u]==null || _image[u].path.compareTo("")==0){
              verifcationFile=false;
            }
          }
        }
        if(verifcationFile==true){
          _sendPret();
        }else{
          _displaySnackBar(context, allTranslations.text('empty_fichier'));
        }

      }else{
        _displaySnackBar(context, allTranslations.text('empty_projet'));
      }

    }

    if(oldStep==1 && step==0){

      if(_keyEop.compareTo("")!=0) {
        _localEopRepository.oneEop(_userId,"$_keyEop").then((response) {

          if (response != null) {

            if(eopKey!=null && eopKey.currentState!=null) {
              eopKey.currentState.setSelectedItem(response);
            }

          }
        });
      }

    }

  }

  _sendPret () async {


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('pret_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });


    SimulationDto simulationDto=SimulationDto();

    simulationDto.userToken=_userToken;
    simulationDto.keyEop=_keyEop;
    simulationDto.superficie=_superficieController.text;
    simulationDto.denominationProjet=_denominationController.text;
    simulationDto.descriptionProjet=_descriptionController.text;
    simulationDto.fileProjet=_image;


    Api api = ApiRepository();
    api.sendUpret(simulationDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          if(a.status.compareTo("000")==0){


            //Enregistrement dans la base local

            //String contentInfo =  json.encode(a.simulation);
            String contentInfo = jsonEncode(a.simulation.map((e) => e.toJson()).toList());

            DateTime now = DateTime.now();
            DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm');
            String formatted = formatter.format(now);

            LocalPretRepository localPretRepository = LocalPretRepository();
            LocalPretEntity localPretEntity = LocalPretEntity();


            localPretEntity.idScoop="0";
            localPretEntity.idEop=_keyEop;
            localPretEntity.superficie="${_superficieController.text}";
            localPretEntity.dateDemande=formatted;
            localPretEntity.idUser=_userId;
            localPretEntity.etatDemande=0;
            localPretEntity.typePret=1;
            localPretEntity.fichierProjet=a.allFichier;
            localPretEntity.idProjet=a.keyProjet;
            localPretEntity.titleDemande=a.message;
            localPretEntity.descriptionDemande=a.description;
            localPretEntity.contenuDemande=contentInfo;

            if(defaultPretEntity.idPret!=null){
              localPretRepository.update(localPretEntity,defaultPretEntity.idPret).then((value) {
                Navigator.of(context).pop(null);
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      DetailUmpretPage(a.simulation[0], a.message, a.keyProjet,
                          a.allFichier)),
                );
              });
            }else {
              localPretEntity.idPret = 0;
              localPretRepository.save(localPretEntity).then((value) {
                Navigator.of(context).pop(null);
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      DetailUmpretPage(a.simulation[0], a.message, a.keyProjet,
                          a.allFichier)),
                );
              });
            }

            return true;

          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
          }

          return true;
        });
      } else {
        _displaySnackBar(context, allTranslations.text('error_process'));
        Navigator.of(context).pop(null);
        return false;
      }
    });


  }

  _getUserInformation(){

    if(defaultPretEntity.idPret!=null){
      _superficieController.text=defaultPretEntity.superficie;
      _denominationController.text=defaultPretEntity.titleDemande;
      _descriptionController.text=defaultPretEntity.descriptionDemande;
      setState(() {
        _keyEop=defaultPretEntity.idEop;
      });



    }

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userId=value.id;
        _userToken=value.token;
      });

      if(_keyEop.compareTo("")!=0) {
        _localEopRepository.oneEop(_userId,"$_keyEop").then((response) {
          selectedEop=response;
          if (response != null) {
            if(eopKey!=null && eopKey.currentState!=null) {
              eopKey.currentState.setSelectedItem(response);
            }


            _allfile=[];
            if(response.fichierEop!=null && response.fichierEop.compareTo("")!=0) {
              setState(() {
                _allfile = response.fichierEop.split(";");
              });
            }else{
              setState(() {
                _allfile=[];
              });
            }
            _image=[];
            _image= new List(_allfile.length);
            List<dynamic> default_fichier=jsonDecode(defaultPretEntity.fichierProjet) ;
            if(default_fichier.length>0){
              for(int u=0;u<default_fichier.length;u++){

                if(default_fichier[u].compareTo("")!=0){
                     _image[u] = File(default_fichier[u]);
                }
              }
            }
            //mise en place des fichiers par defaut selections

          }
        });
      }

      _geteEopList(_userId);
    });

  }

  _geteEopList(int userId){

    //recuperation des produits de la personne
    _localEopRepository.listEop(userId).then((response) {

      if(response.length==0 ){
        _getServerEop (null,0);
      }

    });

  }

  _savePretStart ()  {

        if( (_keyEop.compareTo("")!=0 )){
            if(_superficieController.text.compareTo("")==0 || _superficieController.text.compareTo("0")==0){
              _displaySnackBar(context, allTranslations.text('empty_save1'));
            }else {
              _savePretEnd();
               /*
              if(_denominationController.text.compareTo("")!=0 && _descriptionController.text.compareTo("")!=0 ){
                bool verifcationFile=true;

                if(_image!=null && _image.length>0){
                  for(int u=0;u<_image.length;u++){

                    if(_image[u]==null || _image[u].path.compareTo("")==0){
                      verifcationFile=false;
                    }
                  }
                }
                if(verifcationFile==true){
                  _savePretEnd();
                }else{
                  _displaySnackBar(context, allTranslations.text('empty_fichier'));
                }

              }else{
                _displaySnackBar(context, allTranslations.text('empty_projet'));
              }
              */
            }
        }else{
          _displaySnackBar(context, allTranslations.text('empty_save1'));
        }
  }

  _savePretEnd ()  {


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('pret_saving')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });



        List<String> allSelected =[];
        if(_image!=null && _image.length>0){
          for(int u=0;u<_image.length;u++){

            if(_image[u]!=null &&  _image[u].path.compareTo("")!=0){
              allSelected.add(_image[u].path);
            }else{
              allSelected.add("");
            }
          }
        }

        String allFichier = jsonEncode(allSelected);

        print(allFichier);

        LocalPretRepository localPretRepository = LocalPretRepository();
        LocalPretEntity localPretEntity = LocalPretEntity();
        if(defaultPretEntity.idPret!=null){
          localPretEntity=defaultPretEntity;
        }

        Map<String, dynamic> info={
          'infoEop': jsonEncode(selectedEop.toDatabase()),
        };

        localPretEntity.idEop=_keyEop;
        localPretEntity.superficie="${_superficieController.text}";
        localPretEntity.fichierProjet=allFichier;
        localPretEntity.titleDemande=_denominationController.text;
        localPretEntity.descriptionDemande=_descriptionController.text;
        localPretEntity.contenuDemande=jsonEncode(info);


        if(defaultPretEntity.idPret!=null){
          print("Update");
          localPretRepository.update(localPretEntity,defaultPretEntity.idPret).then((value) {
            Navigator.of(context).pop(null);
            Navigator.of(context).pop("saving");
          });

        }else {

          print("Create");
          DateTime now = DateTime.now();
          DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm');
          String formatted = formatter.format(now);

          localPretEntity.idUser=_userId;
          localPretEntity.idScoop="0";
          localPretEntity.idPret = 0;
          localPretEntity.etatDemande=-1;
          localPretEntity.typePret=1;
          localPretEntity.idProjet="";
          localPretEntity.dateDemande=formatted;

          localPretRepository.save(localPretEntity).then((value) {
            Navigator.of(context).pop(null);
            Navigator.of(context).pop("saving");
          });

        }
    }

  _getServerEop (ConfigModel configModel,int typeRequest) async {


    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    EopDto eopDto=EopDto();
    eopDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getEop(eopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          FunctionUtils.saveEop(a.mesEop,_userId);

          if(typeRequest.compareTo(1)==0) {
            Navigator.of(context).pop(null);
          }

          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });


  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}
