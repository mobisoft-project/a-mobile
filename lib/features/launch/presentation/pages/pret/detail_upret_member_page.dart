
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:agrimobile/core/utils/core_constantes.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/composanteop_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/detaileop_model.dart';
import 'package:agrimobile/features/common/data/models/eop_model.dart';
import 'package:agrimobile/features/common/data/models/simulation_model.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/add_document_page.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/pdf.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:share/share.dart';

import 'package:dio/dio.dart';
import 'package:progress_dialog/progress_dialog.dart';


class DetailUmpretPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final  SimulationModel  infoSimulation;
  final  String  nameScoop;
  final  String  keyProjet;
  final  String  fichierProjet;
  DetailUmpretPage(this.infoSimulation,this.nameScoop,this.keyProjet,this.fichierProjet);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState(this.infoSimulation,nameScoop,this.keyProjet,this.fichierProjet);
  }
}



class PageState extends State<DetailUmpretPage> {

  final _formKey = GlobalKey<FormState>();
  SimulationModel  _infoSimulation;
  String  _nameProjet;
  String  _keyProjet;
  String  _fichierProjet;
  PageState(this._infoSimulation,this._nameProjet,this._keyProjet,this._fichierProjet);

  EopModel _infoProjet;
  ConfigModel _myConfigModel;
  List<DetailEopModel> _allComposantEop;
  List<String> _allFichier=[];
  bool _haveEop=false;
  bool _haveComposant=false;
  int _postion=0;
  String _fileName="";

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  final simulationKey = GlobalKey<FindDropdownState>();

  @override
  void initState() {
    super.initState();
    _infoProjet=_infoSimulation.information;
    _fileName=_nameProjet;
    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      _myConfigModel=configModel;
      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 0.0)),

          appBar: new AppBar(
            title:  Text("$_nameProjet",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,

              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () async {

                      final isPermissionStatusGranted = await _requestPermissions();
                      if (isPermissionStatusGranted) {

                        String filename=_fileName.replaceAll(" ", "_");
                        filename=filename.replaceAll("'", "");
                        filename=filename.replaceAll("(", "");
                        filename=filename.replaceAll(")", "");


                        DateTime now = DateTime.now();
                        DateFormat formatter = DateFormat('dd_MM_yyyy_hh_mm');
                        String formatted = formatter.format(now);

                        filename="${filename}_${formatted}.pdf";

                        _getPermissionPath(context,filename,0);

                      }else{
                         _displaySnackBar(context,allTranslations.text('reject_permission'));
                      }
                    },
                    child: Icon(
                      Icons.print,
                      size: 26.0,
                    ),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          floatingActionButton: Visibility(
            child:  FloatingActionButton(
              onPressed: () {
                // ajout de nouveau fichier

               //
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      AddDocumentPage(_keyProjet,_nameProjet)),
                ).then((value){
                   if(value!=null){
                       setState(() {
                         _fichierProjet=value;
                         if(_fichierProjet.compareTo("")!=0){
                           _allFichier=_fichierProjet.split(";");
                         }
                       });
                       _displaySnackBar(context, allTranslations.text('success_document'));
                   }
                });
              },
              child: Icon(Icons.insert_drive_file,color: Colors.white),
              backgroundColor: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
            ),
            visible: _keyProjet.compareTo("")==0?false:true,
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return Stack(
                  children: <Widget>[
                    ListView(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      children: <Widget>[

                        Form(
                            key: _formKey,
                            child:
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[

                                SizedBox(height: 10.0),
                                _getMemberDetail(configModel),

                                if(_allFichier!=null && _allFichier.length>0)Container(
                                  padding: EdgeInsets.only(left:10,right:10),
                                  child: Column(
                                    children: [
                                      //SizedBox(height: 10.0),
                                      Text("${allTranslations.text('list_document')}",style: TextStyle(
                                          color:Colors.red,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold
                                      ),),
                                      SizedBox(height: 10.0),
                                    ],
                                  ),
                                ),

                                _getDocumentListview(myContext),
                                if(_allFichier!=null && _allFichier.length>0)SizedBox(height: 70.0),
                              ],
                            )
                        )
                      ],
                    )

                  ],
                );

              }
          )
      );
    },
  );

  _reportView(BuildContext context,String filePath) async {


    final pw.Document pdf = pw.Document();

    pdf.addPage(pw.MultiPage(
        pageFormat:
        PdfPageFormat.letter.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        header: (pw.Context mycontext) {
          if (mycontext.pageNumber == 1) {
            return null;
          }
          return pw.Container(
              alignment: pw.Alignment.centerRight,
              margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
              padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
              child: pw.Text(_nameProjet,
                  style: new pw.TextStyle(
                      color: PdfColors.grey, fontSize: 17.0,)));
        },
        footer: (pw.Context context) {
          return pw.Container(
              alignment: pw.Alignment.centerRight,
              margin: const pw.EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
              child: pw.Text("$_nameProjet ---Page ${context.pageNumber} / ${context.pagesCount}",
                  style: new pw.TextStyle(
                    color: PdfColors.grey, fontSize: 17.0,)));
        },
        build: (pw.Context context) => <pw.Widget>[
          pw.Header(
              level: 0,
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  children: <pw.Widget>[
                    pw.Text("$_fileName", textScaleFactor: 2),
                    //PdfLogo()
              ])),


          pw.Padding(padding: const pw.EdgeInsets.all(10)),
          pw.Container(
              alignment: pw.Alignment.center,
              child: pw.Text("${allTranslations.text('resume_pret')}",
                  style: new pw.TextStyle(
                    color: PdfColors.black, fontSize: 20.0,))),

          pw.Padding(padding: const pw.EdgeInsets.all(10)),
          pw.Container(
              padding: const pw.EdgeInsets.all(5),
              alignment: pw.Alignment.topLeft,
              //decoration:  pw.BoxDecoration(border:pw.BoxBorder(bottom: true, width: 0.5, color: PdfColors.grey)),
              decoration: pw.BoxDecoration(
                  borderRadius: pw.BorderRadius.circular(4.0),
                  border: pw.Border.all(
                    color: PdfColors.grey,
                  ),
                  color: PdfColors.grey50),
              child: pw.Column(
                children: [
                  pw.Row(
                    children: [

                      pw.Expanded(
                        child:   pw.Text("${_infoProjet.nameSpeculation} sur ${_infoProjet.superficie} hectare(s)",
                          style: new  pw.TextStyle(
                              color: PdfColors.green, fontSize: 17.0,fontWeight:  pw.FontWeight.bold),
                        ),
                      ),

                      

                    ],
                  ),

                  pw.SizedBox(height:10),
                  pw.Row(
                    children: [
                      pw.Container(
                          width: 130,
                          child: pw.Text("${allTranslations.text('name_site')}",
                            style: new pw.TextStyle(
                                color: PdfColors.black, fontSize: 14.0,fontWeight: pw.FontWeight.bold),
                          )
                      ),
                      pw.Expanded(child: pw.Text(": ${_infoProjet.nameSite}")),
                    ],
                  ),

                  pw.Row(
                    children: [
                      pw.Container(
                          width: 130,
                          child: pw.Text("${allTranslations.text('montant_total')}",
                            style: new pw.TextStyle(
                                color: PdfColors.black, fontSize: 14.0,fontWeight: pw.FontWeight.bold),
                          )
                      ),
                      pw.Expanded(child:  pw.Text(": ${_infoProjet.montantTotal} ${_myConfigModel.devise}")),
                    ],
                  ),
                  pw.Row(
                    children: [
                      pw.Container(
                          width: 130,
                          child: pw.Text("${allTranslations.text('interet_eop')}",
                            style: new pw.TextStyle(
                                color: PdfColors.black, fontSize: 14.0,fontWeight: pw.FontWeight.bold),
                          )
                      ),
                      pw.Expanded(child: pw.Text(": ${_infoProjet.interetEop} %",style: new pw.TextStyle(
                          color: PdfColors.red, fontSize: 14.0,fontWeight: pw.FontWeight.bold))),
                    ],
                  ),
                  pw.Row(
                    children: [
                      pw.Container(
                          width: 130,
                          child: pw.Text("${allTranslations.text('duree_eop')}",
                            style: new pw.TextStyle(
                                color: PdfColors.black, fontSize: 14.0,fontWeight: pw.FontWeight.bold),
                          )
                      ),
                      pw.Expanded(child: pw.Text(": ${_infoProjet.dureeEop} mois",style: new pw.TextStyle(
                          color: PdfColors.red, fontSize: 14.0,fontWeight: pw.FontWeight.bold))),
                    ],
                  ),
                  pw.Row(
                    children: [
                      pw.Container(
                          width: 130,
                          child: pw.Text("${allTranslations.text('interet_prix')}",
                            style: new pw.TextStyle(
                                color: PdfColors.black, fontSize: 14.0,fontWeight: pw.FontWeight.bold),
                          )
                      ),
                      pw.Expanded(child: pw.Text(": ${_infoProjet.montantInteret} ${_myConfigModel.devise}",style: new pw.TextStyle(
                          color: PdfColors.red, fontSize: 14.0,fontWeight: pw.FontWeight.bold))),
                    ],
                  ),
                  pw.Row(
                    children: [
                      pw.Container(
                          width: 130,
                          child: pw.Text("${allTranslations.text('projet_interet')}",
                            style: new pw.TextStyle(
                                color: PdfColors.black, fontSize: 14.0,fontWeight: pw.FontWeight.bold),
                          )
                      ),
                      pw.Expanded(child: pw.Text(": ${_infoProjet.projetInteret} ${_myConfigModel.devise}",style: new pw.TextStyle(
                          color: PdfColors.green, fontSize: 14.0,fontWeight: pw.FontWeight.bold))),
                    ],
                  ),
                  pw.SizedBox(height:10),
                  pw.Text("${_infoProjet.descriptionEop}",
                    textAlign: pw.TextAlign.left,
                    style: new pw.TextStyle(
                        color: PdfColors.black, fontSize: 14.0),
                  ),
                  pw.SizedBox(height:5),

                ]
              )
          ),


          pw.Padding(padding: const pw.EdgeInsets.all(10)),

          pw.Container(
              alignment: pw.Alignment.center,
              child: pw.Text("${allTranslations.text('detail_simultation')}",
                  style: new pw.TextStyle(
                    color: PdfColors.black, fontSize: 20.0,))),

          pw.Column(
            children: _pdfWidget(context),
          ),


        ]));


    final File file = File(filePath);
    await file.writeAsBytes(await pdf.save());

    Timer(Duration(seconds: 1),(){

        Navigator.of(context).pop(null);

        _showAlertDialog(context,filePath);

    });

  }

  List<pw.Widget> _pdfWidget(pw.Context context ){


    List<pw.Widget> allContent=[] ;
    if(_allComposantEop.length>0){
      for(var u=0;u<_allComposantEop.length;u++){


        List<ComposantEopModel> detailSimulation=_allComposantEop[u].detailComposant;

        List<List<String>> allAfficheSimulation= [] ;


        allContent.add( pw.Padding(padding: const pw.EdgeInsets.all(20)));
        pw.Widget titleTable= pw.Row(
          children: [
            pw.Text("${_allComposantEop[u].nomComposants}: ",
              style: new pw.TextStyle(
                color: PdfColors.black, fontSize: 20.0,),
            ),
            pw.Text("${_allComposantEop[u].montantParametre} ${_myConfigModel.devise}",
              style: new pw.TextStyle(
                color: PdfColors.green, fontSize: 20.0,),
            ),
          ],
        );
        allContent.add(titleTable);
        allContent.add( pw.Padding(padding: const pw.EdgeInsets.all(5)));



        //creation de l'entete
        allAfficheSimulation.add(<String>["${allTranslations.text('description')}", "${allTranslations.text('etiquete')}", "${allTranslations.text('qte')}/hectare","Sup (hectare)"
          ,"${allTranslations.text('prixunitaire')} (${_myConfigModel.devise})","${allTranslations.text('prixtotal')} (${_myConfigModel.devise})"]);

        if(detailSimulation.length>0){
          for(var i=0;i<detailSimulation.length;i++){
            ComposantEopModel infoDetail=detailSimulation[i];

            List<String> afficheSimulation= <String>["${infoDetail.description}","${infoDetail.etiquette}", "${infoDetail.qte} ${infoDetail.unites}", "${infoDetail.superficie}",
              "${infoDetail.coutUnitaire}","${infoDetail.coutTotal}"];

            allAfficheSimulation.add(afficheSimulation);
          }
        }

        allContent.add( pw.Table.fromTextArray(context: context, data: allAfficheSimulation));
      }
    }
    
    return allContent ;
    
  }

  _showAlertDialog(BuildContext context,String filePath) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text( allTranslations.text('close'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget shareButton = FlatButton(
      child: Text( allTranslations.text('share'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);

        if (Platform.isAndroid) {

          final RenderBox box = context.findRenderObject();
          Share.shareFiles([filePath],
              subject: allTranslations.text('sharetitle'),
              text: allTranslations.text('sharecontent'),
              sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);

        }

      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('view'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => PdfViewerPage(path: filePath),
          ),
        );

      },
    );

    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(allTranslations.text('succes_download')
      ),
      actions: [
        cancelButton,
        shareButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<bool> _requestPermissions() async {
    var permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);

    if (permission != PermissionStatus.granted) {
      await PermissionHandler().requestPermissions([PermissionGroup.storage]);
      permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.storage);
    }

    return permission == PermissionStatus.granted;
  }

  Future<void> _getPermissionPath(BuildContext context,String filename,int type) async {


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('download_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });




    Directory _appDocDir= await getExternalStorageDirectory();

    String mypath=_appDocDir.path;
    if (Platform.isAndroid) {
      mypath=mypath.split("Android")[0];
    }


    final Directory _appDocDirFolder = Directory(
        '$mypath/${DataConstantesUtils.simulationFolder}/');

    if (await _appDocDirFolder.exists()) {
      String filePath = '${_appDocDirFolder.path}$filename';

      if(type.compareTo(0)==0) {
        _reportView(context, filePath);
      }else if(type.compareTo(1)==0) {
        _verifyFile(context,filename, filePath);
      }
    } else {
      Future<Directory> _appDocDirNewFolder = _appDocDirFolder.create(
          recursive: true);
      _appDocDirNewFolder.then((response) {
        String filePath = '${response.path}$filename';
        if(type.compareTo(0)==0) {
          _reportView(context, filePath);
        }else if(type.compareTo(1)==0) {
          _verifyFile(context,filename, filePath);
        }
      });
    }
  }

  _verifyFile(BuildContext context,String fileName, String filePath) async {
    Navigator.of(context).pop(null);
    final File file = File(filePath);

    print(filePath);
    if (await file.exists()) {
      OpenFile.open(filePath);
    }else{
      String serverPath="${SERVER_URL}/admin/images/document/$fileName";
       _startdownload(serverPath,filePath);
    }
  }


  Future<void> _startdownload(uri, savePath) async {

    String  progress="0";
    ProgressDialog pr = new ProgressDialog(context);
    pr = new ProgressDialog(context,type: ProgressDialogType.Download, isDismissible: false, showLogs: false);
    pr.style(
        message: allTranslations.text('download_loading'),
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600)
    );
    pr.show();



    Dio dio = Dio();

    dio.download(
      uri,
      savePath,
      onReceiveProgress: (rcv, total) {
        progress = ((rcv / total) * 100).toStringAsFixed(0);

        if(int.parse(progress)>0) {
          pr.update(
            progress: int.parse(progress) * 1.0,
            message: allTranslations.text('download_loading'),
            progressWidget: Container(
                padding: EdgeInsets.all(8.0),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
            ),
            maxProgress: 100.0,
            progressTextStyle: TextStyle(
                color: Colors.black,
                fontSize: 13.0,
                fontWeight: FontWeight.w400),
            messageTextStyle: TextStyle(
                color: Colors.black,
                fontSize: 19.0,
                fontWeight: FontWeight.w600),
          );
        }
      },
      deleteOnError: true,
    ).then((_) {

      pr.hide().then((isHidden) {

        OpenFile.open(savePath);
      });

    });
  }

  

  _getMemberDetail(ConfigModel configModel){

    if(_haveEop==true ) {
      return Column(
        children: [
          SizedBox(height: 100,),
          Image.asset('assets/img/loading.gif',width: 150,height: 150,),
          SizedBox(height: 30,),
          Center(
            child: Text("${allTranslations.text('getting_mprocessing')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );
    }else  if (_infoProjet!=null){
      return
        Column(
            children: <Widget>[



              SizedBox(height: 20,),
              Row(
                children: [
                  SizedBox(width: 5,),
                  Icon(Icons.arrow_forward, color: Colors.black54,size: 20,),
                  SizedBox(width: 3,),
                  Text("${allTranslations.text('resume_pret')}",
                    style: new TextStyle(
                        color: Colors.black, fontSize: 20.0,fontWeight: FontWeight.bold),
                  ),

                ],
              ),
              Container(
                margin: EdgeInsets.only(left:40,right: 40),
                child: Column(
                  children: [
                    SizedBox(height: 20,),
                    MaterialButton(
                      color: Colors.white,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color:  Colors.black12)
                      ),
                      textColor: Colors.white,

                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [

                          Row(
                            children: [

                              Expanded(
                                child:  Text("${_infoProjet.nameSpeculation} sur ${_infoProjet.superficie} hectare(s)",
                                  style: new TextStyle(
                                      color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 17.0,fontWeight: FontWeight.bold),
                                ),
                              ),

                              

                            ],
                          ),

                          //
                          SizedBox(height:10),
                          Row(
                            children: [
                              Container(
                                  width: 130,
                                  child: Text("${allTranslations.text('name_site')}",
                                    style: new TextStyle(
                                        color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                  )
                              ),
                              Expanded(child: Text(": ${_infoProjet.nameSite}")),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                  width: 130,
                                  child: Text("${allTranslations.text('montant_total')}",
                                    style: new TextStyle(
                                        color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                  )
                              ),
                              Expanded(child: Text(": ${_infoProjet.montantTotal} ${configModel.devise}")),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                  width: 130,
                                  child: Text("${allTranslations.text('interet_eop')}",
                                    style: new TextStyle(
                                        color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                  )
                              ),
                              Expanded(child: Text(": ${_infoProjet.interetEop} %",style: new TextStyle(
                                  color: Colors.red, fontSize: 14.0,fontWeight: FontWeight.bold))),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                  width: 130,
                                  child: Text("${allTranslations.text('duree_eop')}",
                                    style: new TextStyle(
                                        color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                  )
                              ),
                              Expanded(child: Text(": ${_infoProjet.dureeEop} mois",style: new TextStyle(
                                  color: Colors.red, fontSize: 14.0,fontWeight: FontWeight.bold))),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                  width: 130,
                                  child: Text("${allTranslations.text('interet_prix')}",
                                    style: new TextStyle(
                                        color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                  )
                              ),
                              Expanded(child: Text(": ${_infoProjet.montantInteret} ${configModel.devise}",style: new TextStyle(
                                  color: Colors.red, fontSize: 14.0,fontWeight: FontWeight.bold))),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                  width: 130,
                                  child: Text("${allTranslations.text('projet_interet')}",
                                    style: new TextStyle(
                                        color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                  )
                              ),
                              Expanded(child: Text(": ${_infoProjet.projetInteret} ${configModel.devise}",style: new TextStyle(
                                  color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 14.0,fontWeight: FontWeight.bold))),
                            ],
                          ),
                          SizedBox(height:10),
                          Text("${_infoProjet.descriptionEop}",
                            style: new TextStyle(
                                color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.w700),
                          ),
                          SizedBox(height:5),
                        ],
                      ),
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),

                    )
                  ],
                ),
              ),
              SizedBox(height: 40,),
              Row(
                children: [
                  SizedBox(width: 5,),
                  Icon(Icons.arrow_forward, color: Colors.black54,size: 20,),
                  SizedBox(width: 3,),
                  Text("${allTranslations.text('detail_simultation')}",
                    style: new TextStyle(
                        color: Colors.black, fontSize: 20.0,fontWeight: FontWeight.bold),
                  ),

                ],
              ),
              Container(
                margin: EdgeInsets.only(left:10,right: 10),
                child: _getDetailListview(configModel),
              ),

            ]
        );
    }else{
      return Container();
    }

  }

  _getUserInformation(){


    if(_fichierProjet.compareTo("")!=0){
      _allFichier=_fichierProjet.split(";");
    }
    _allComposantEop=[];

    if(_infoProjet!=null) {
      String detail_eop = _infoProjet.composantEop;
      List<dynamic> responseMap = jsonDecode(detail_eop.toString());

      setState(() {
        _haveComposant = true;
        _allComposantEop =
            responseMap.map((i) => DetailEopModel.fromMap(i)).toList();
      });

    }


  }

  _getComposantListview(List<ComposantEopModel> detailComposant,ConfigModel configModel){

    return ListView.builder(
        itemCount:detailComposant.length,
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (BuildContext context, int position) {

          ComposantEopModel infoDetail= detailComposant[position];

          return Column(
              children: [
                hr,
                if(position==0) Row(
                  children: [
                    Expanded(child: Text("${allTranslations.text('description')}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                    SizedBox(width:5),
                    Expanded(child: Text("${allTranslations.text('etiquete')}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                    SizedBox(width:5),
                    Expanded(child: Text("${allTranslations.text('qte')}/hectare",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                    SizedBox(width:5),
                    Expanded(child: Text("Sup (hectare)",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                    SizedBox(width:5),
                    Expanded(child: Text("${allTranslations.text('prixunitaire')} (${configModel.devise})",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                    SizedBox(width:5),
                    Expanded(child: Text("${allTranslations.text('prixtotal')} (${configModel.devise})",

                        style: TextStyle(

                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                  ],
                ),
                if(position==0) hr,
                Row(
                  children: [
                    Expanded(child: Text("${infoDetail.description}")),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoDetail.etiquette}")),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoDetail.qte} ${infoDetail.unites}")),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoDetail.superficie}",textAlign: TextAlign.right)),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoDetail.coutUnitaire}",textAlign: TextAlign.right)),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoDetail.coutTotal}",textAlign: TextAlign.right)),
                  ],
                )

              ]
          );

        });

  }

  _getDocumentListview(BuildContext selectContext) {

    if(_allFichier!=null && _allFichier.length>0){

      var listview= ListView.builder(
          itemCount: _allFichier.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            String fileUrl=_allFichier[position];
            String name_file=_allFichier[position];
            name_file=name_file.split("__")[0];
            name_file=name_file.replaceAll('_', ' ');
            name_file=name_file.split(".")[0];

            return Container(
              padding: EdgeInsets.only(left:10,right:10),
              child: Column(
                children: [

                  MaterialButton(

                    elevation: 0.0,
                    onPressed: ()  async {
                      final isPermissionStatusGranted = await _requestPermissions();
                      if (isPermissionStatusGranted) {
                          _getPermissionPath(context,fileUrl,1);
                      }else{
                        _displaySnackBar(context,allTranslations.text('reject_permission'));
                      }
                    },
                    child: Container(
                      padding: EdgeInsets.fromLTRB(10, 3, 10, 3),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color:FunctionUtils.colorFromHex("DEDEDE"),
                              style: BorderStyle.solid,
                              width: 1.0
                          ),
                          borderRadius: BorderRadius.circular(5.0)
                      ),
                      child: Row(
                        children: [
                          CircleAvatar(
                              radius: 15,
                              backgroundColor: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
                              child:CircleAvatar(
                                radius: 14.0,
                                backgroundColor:Colors.white,
                                child: Icon(Icons.attach_file,
                                  color: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
                                  size: 20,
                                ) ,
                              )
                          ),
                          SizedBox(width: 5,),
                          Expanded(
                            child:  Text("${name_file}",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                            ),
                          ),
                          Icon(Icons.cloud_download, color: FunctionUtils.colorFromHex(_myConfigModel.mainColor),size: 20,),
                        ],
                      ),
                    ),
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  ),
                ],
              ),
            );

          });

      return listview;

    }else{

      return Container();

    }
  }

  _getDetailListview(ConfigModel configModel){

    if(_haveComposant==false) {
      return Column(
        children: [
          SizedBox(height: 100,),
          Image.asset('assets/img/loading.gif',width: 150,height: 150,),
          SizedBox(height: 30,),
          Center(
            child: Text("${allTranslations.text('getting_processing')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );
    }else if(_allComposantEop==null || _allComposantEop.length==0){

      return Column(
        children: [
          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('detaileop_empty')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );

    }else{
      return  ListView.builder(
          itemCount: _allComposantEop.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            DetailEopModel infoEop=  _allComposantEop[position];

            return Container(
              margin: EdgeInsets.only(left:10,right: 10,bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  if(position==0)SizedBox(height: 20,),
                  Row(
                    children: [
                      Text("${infoEop.nomComposants}",
                        style: new TextStyle(
                            color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                      ),
                      Text(": ${infoEop.montantParametre} ${configModel.devise}",style: new TextStyle(
                          color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 14.0,fontWeight: FontWeight.bold))
                    ],
                  ),
                  SizedBox(height:5),
                  _getComposantListview(infoEop.detailComposant,configModel),

                  SizedBox(height:40),


                ],
              ),
            );

          });
    }

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}


class PdfViewerPage extends StatelessWidget {
  final String path;
  const PdfViewerPage({Key key, this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
      path: path,
    );
  }
}
