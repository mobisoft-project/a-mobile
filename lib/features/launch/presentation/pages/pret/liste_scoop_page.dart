
import 'dart:async';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/dto/scoop_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_scoop_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_scoop_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/liste_annonce_page.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';

class ListeScoopPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}


class PageState extends State<ListeScoopPage> {

  LocalScoopRepository _localScoopRepository = LocalScoopRepository();
  List<LocalScoopEntity> _allLocalScoopEntity;
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  bool _haveEop=false;
  int _userId=0;
  String _userToken="";

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 80.0)),

          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_listescoop'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,

                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _getServerScoop (configModel,1);
                    },
                    child: Icon(
                      Icons.refresh,
                      size: 26.0,
                    ),
                  )
              ),
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      UrlLauncher.launch('tel:+${configModel.callCenter}');
                    },
                    child: Icon(Icons.phone,color: Colors.white,),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
         // backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
          body:Builder(
              builder: (BuildContext myContext)
              {
                  return  _getScoopListview(configModel);
              }
          )
      );
    },
  );

  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
          setState(() {
            _userId=value.id;
            _userToken=value.token;
          });
          _getScoopList(_userId);
    });

  }

  _getScoopList(int userId){

    //recuperation des produits de la personne
    _localScoopRepository.listScoop(userId).then((response) {

      if(response.length>0 ){

        setState(() {
          _haveEop = true;
          _allLocalScoopEntity = response;
        });

      }else{
        _getServerScoop (null,0);
      }

    });

  }

  _getScoopListview(ConfigModel configModel){

    if(_haveEop==false) {
      return Column(
        children: [
          SizedBox(height: 100,),
          Image.asset('assets/img/loading.gif',width: 150,height: 150,),
          SizedBox(height: 30,),
          Row(
             children: [
               SizedBox(width: 30,),
               Expanded(
                 child: Center(
                   child: Text("${allTranslations.text('sgetting_processing')}",
                     textAlign: TextAlign.center,
                     style: new TextStyle(
                         color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                   ),
                 ),
               ),
               SizedBox(width: 30,),
             ],
          )

        ],
      );
    }else if(_allLocalScoopEntity==null || _allLocalScoopEntity.length==0){


      return Column(
        children: [
          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('scoop_empty')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );

    }else{
      return  ListView.builder(
          itemCount: _allLocalScoopEntity.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            LocalScoopEntity infoScoop=  _allLocalScoopEntity[position];
                  print(infoScoop.typeAssociation);
            return Container(
              margin: EdgeInsets.only(left:10,right: 10,bottom: 10),

              child: Column(
                children: [
                  if(position==0)SizedBox(height: 10,),

                  MaterialButton(
                    color: Colors.white,
                    elevation: 0.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: FunctionUtils.colorFromHex("E6EBEF"))
                    ),
                    textColor: Colors.white,
                    onPressed: () {
                      _showBottom(infoScoop);
                    },
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child:  Row(
                            children: [

                              Expanded(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("${infoScoop.nameAssociation}",
                                      style: new TextStyle(
                                          color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 15.0,fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(width: 5,),
                                    Text(": ${infoScoop.totalSuperficie} Hectares",
                                      style: new TextStyle(
                                          color: Colors.black54, fontSize: 13.0,fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                ),
                              ),
                              Icon(Icons.supervised_user_circle,
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                                size: 30,
                              ),
                              Text(" ${infoScoop.totalMembre}",
                                style: new TextStyle(
                                    color: Colors.black54, fontSize: 25.0,fontWeight: FontWeight.w500),
                              )
                            ],
                          ),
                        ),

                        Container(
                          padding: EdgeInsets.fromLTRB(5, 5, 0, 0),
                          child:  Row(
                            children: [

                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text("${infoScoop.cantonAssociation}: ",
                                          style: new TextStyle(
                                              color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(height: 5,),
                                        Text("(${allTranslations.text('type_association${infoScoop.typeAssociation}')})",
                                          style: new TextStyle(
                                              color: Colors.red, fontSize: 13.0,fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 5,),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Icon(Icons.arrow_forward, color: Colors.black54,size: 20,),
                                            SizedBox(width: 3,),
                                            Expanded(
                                              child:Text("${infoScoop.adresseAssociation}",
                                                style: new TextStyle(
                                                    color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                              )
                                            ),

                                          ],
                                        ),
                                        Row(
                                          children: [
                                            SizedBox(width: 25,),
                                            Expanded(
                                              child:Text("${infoScoop.descriptionAssociation}",
                                                style: new TextStyle(
                                                    color: Colors.black54, fontSize: 13.0,fontWeight: FontWeight.w500),
                                              )
                                            ),

                                          ],
                                        ),
                                        SizedBox(height: 10,),

                                      ],
                                    )

                                  ],
                                ),
                              ),
                              Container(
                                  width: 70,
                                  height: 50,
                                  child: ClipPath(
                                    clipper: MyClipper(),
                                    child: Container(
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                                begin: Alignment.topCenter,
                                                end: Alignment.bottomCenter,
                                                colors: [Colors.blueAccent,Colors.blueAccent]
                                            )
                                        ),
                                        child: Image.asset("assets/img/mes_contacts.png", fit: BoxFit.cover)
                                    ),
                                  )
                              ),

                            ],
                          ),
                        ),



                      ],
                    ),
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                  ),

                ],
              ),
            );

          });
    }

  }

  _getServerScoop (ConfigModel configModel,int typeRequest) async {


    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    ScoopDto scoopDto=ScoopDto();
    scoopDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getScoop(scoopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

         FunctionUtils.saveScoop(a.mesScoop,_userId);

          Timer(Duration(seconds: 2),(){

            _localScoopRepository.listScoop(_userId).then((response) {

              if(response.length>0 ){
                setState(() {
                  _haveEop=true;
                  _allLocalScoopEntity = response;
                });

              }else{
                setState(() {
                  _haveEop=true;
                });
              }

            });

            if(typeRequest.compareTo(1)==0) {
              Navigator.of(context).pop(null);
            }
          });



          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        setState(() {
          _haveEop=true;
        });
        return false;
      }
    });


  }

  void _showBottom(LocalScoopEntity infoScoop){
    showModalBottomSheet<void>(
        context: context,
        /*bottom sheet is like a drawer that pops off where you can put any
      controls you want, it is used typically for user notifications*/
        //builder lets your code generate the code
        builder: (BuildContext context){
          return new Container(

            color: FunctionUtils.colorFromHex("D4D4D4"),
            //color: Utils.colorFromHex("C8C8C8"),
            height: 155,
            width: double.infinity,
            padding: EdgeInsets.all(20.0),

            child: Column(
              children: <Widget>[

                Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.only(bottom: 20.0),
                  child:
                  Text(allTranslations.text('page_of'),

                    textAlign: TextAlign.start,
                    style: TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.black54,
                        fontSize: 15.0,
                        fontWeight: FontWeight.normal
                    ),
                  ),
                ),

                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => DetailScoopPage(infoScoop)),
                    ).then((value){
                      if(value!=null && value.compareTo("updated")==0){
                        _getScoopList(_userId);
                        _displaySnackBar(context, allTranslations.text('success_update'));
                      }
                    });
                  },
                  child:   Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(bottom: 20.0),
                      child:
                      RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(Icons.people, size: 18,color: Colors.black54,),
                              ),
                            ),

                            //  SizedBox(width: 20.0),
                            TextSpan(
                              text:allTranslations.text('scoopUser'),
                              style: TextStyle(
                                  decoration: TextDecoration.none,
                                  color: Colors.black87,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal
                              ),
                            ),
                          ],
                        ),
                      )

                  ),
                ),

                GestureDetector(
                  onTap: () {
                    int venteBiztypeId = 1;
                    Navigator.pop(context);
                    // party yvon
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyAnnoncePage(DataConstantesUtils.ANNONCE_SCOOP_PAGE,0,infoScoop.idAssociation,venteBiztypeId)),
                    );
                  },
                  child:   Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(bottom: 20.0),
                      child:
                      RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Icon(Icons.photo, size: 18,color: Colors.black54,),
                              )
                            ),
                            //  SizedBox(width: 20.0),
                            TextSpan(
                              text:allTranslations.text('scoopAnnonce'),
                              style: TextStyle(
                                  decoration: TextDecoration.none,
                                  color: Colors.black87,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal
                              ),
                            ),
                          ],
                        ),
                      )

                  ),
                ),
              ],
            ),
          );
        }
    );
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }
}



class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {


    double factor = 30.0;
    var path = Path();
    path.lineTo(0, size.height - factor);

    path.quadraticBezierTo(0, size.height, factor, size.height);
    path.lineTo(size.width-factor, size.height);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.lineTo(factor, 0);
    path.quadraticBezierTo(0, 0, 0, factor);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class ClipPainter extends CustomClipper<Path>{
  @override

  Path getClip(Size size) {
    var height = size.height;
    var width = size.width;
    var path = new Path();

    path.lineTo(0, size.height );
    path.lineTo(size.width , height);
    path.lineTo(size.width , 0);

    /// [Top Left corner]
    var secondControlPoint =  Offset(0  ,0);
    var secondEndPoint = Offset(width * .2  , height *.5);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy, secondEndPoint.dx, secondEndPoint.dy);



    /// [Left Middle]
    var fifthControlPoint =  Offset(width * .3  ,height * .5);
    var fiftEndPoint = Offset(  width * .23, height *.6);
    path.quadraticBezierTo(fifthControlPoint.dx, fifthControlPoint.dy, fiftEndPoint.dx, fiftEndPoint.dy);


    /// [Bottom Left corner]
    var thirdControlPoint =  Offset(0  ,height);
    var thirdEndPoint = Offset(width , height  );
    path.quadraticBezierTo(thirdControlPoint.dx, thirdControlPoint.dy, thirdEndPoint.dx, thirdEndPoint.dy);



    path.lineTo(0, size.height  );
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }


}