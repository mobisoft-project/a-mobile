
import 'dart:async';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/dto/eop_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_eop_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_eop_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_eop_page.dart';
import 'package:agrimobile/features/launch/presentation/widgets/liste_annonce_widget.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';

class ListeEopPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}


class PageState extends State<ListeEopPage> {

  LocalEopRepository _localEopRepository = LocalEopRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  List<LocalEopEntity> _allLocalEopEntity;
  bool _haveEop=false;
  int _userId=0;
  String _userToken="";

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 80.0)),

          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_listeeop'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,

                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _getServerEop (configModel,1);
                    },
                    child: Icon(
                      Icons.refresh,
                      size: 26.0,
                    ),
                  )
              ),
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      UrlLauncher.launch('tel:+${configModel.callCenter}');
                    },
                    child: Icon(Icons.phone,color: Colors.white,),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
         // backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return  _getEopListview(configModel);
              }
          )
      );
    },
  );

  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
          setState(() {
            _userId=value.id;
            _userToken=value.token;
          });
          _geteEopList(_userId);
    });

  }

  _geteEopList(int userId){


    //recuperation des produits de la personne
    _localEopRepository.listEop(userId).then((response) {

      if(response.length>0 ){

        setState(() {
          _haveEop = true;
          _allLocalEopEntity = response;
        });

      }else{
        _getServerEop (null,0);
      }

    });




  }


  _getEopListview(ConfigModel configModel){

    if(_haveEop==false) {
      return Column(
        children: [
          SizedBox(height: 100,),
          Image.asset('assets/img/loading.gif',width: 150,height: 150,),
          SizedBox(height: 30,),
          Row(
            children: [
              SizedBox(width: 30,),
              Expanded(
                child: Center(
                  child: Text("${allTranslations.text('getting_processing')}",
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(width: 30,),
            ],
          )

        ],
      );
    }else if(_allLocalEopEntity==null || _allLocalEopEntity.length==0){
      return Column(
        children: [
          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('eop_empty')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );
    }else{
      return  ListView.builder(
          itemCount: _allLocalEopEntity.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            LocalEopEntity infoEop=  _allLocalEopEntity[position];

            return Container(
              margin: EdgeInsets.only(left:10,right: 10,bottom: 10),

              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  if(position==0)SizedBox(height: 10,),
                  MaterialButton(
                    elevation: 0.0,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DetailEopPage(infoEop)),
                      );
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text("${infoEop.nameSpeculation}",
                          style: new TextStyle(
                              color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 17.0,fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height:10),
                        Row(
                          children: [
                            Container(
                                width: 130,
                                child: Text("${allTranslations.text('name_site')}",
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                )
                            ),
                            Expanded(child: Text(": ${infoEop.nameSite}")),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                                width: 130,
                                child: Text("${allTranslations.text('montant_total')}",
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                )
                            ),
                            Expanded(child: Text(": ${infoEop.montantTotal} ${configModel.devise}")),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                                width: 130,
                                child: Text("${allTranslations.text('interet_eop')}",
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                )
                            ),
                            Expanded(child: Text(": ${infoEop.interetEop} %",style: new TextStyle(
                                color: Colors.red, fontSize: 14.0,fontWeight: FontWeight.bold))),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                                width: 130,
                                child: Text("${allTranslations.text('duree_eop')}",
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                )
                            ),
                            Expanded(child: Text(": ${infoEop.dureeEop} ans",style: new TextStyle(
                                color: Colors.red, fontSize: 14.0,fontWeight: FontWeight.bold))),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                                width: 130,
                                child: Text("${allTranslations.text('interet_prix')}",
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                )
                            ),
                            Expanded(child: Text(": ${infoEop.montantInteret} ${configModel.devise}",style: new TextStyle(
                                color: Colors.red, fontSize: 14.0,fontWeight: FontWeight.bold))),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                                width: 130,
                                child: Text("${allTranslations.text('projet_interet')}",
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                )
                            ),
                            Expanded(child: Text(": ${infoEop.projetInteret} ${configModel.devise}",style: new TextStyle(
                                color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 14.0,fontWeight: FontWeight.bold))),
                          ],
                        ),
                        SizedBox(height:10),
                        Text("${infoEop.descriptionEop}",
                          style: new TextStyle(
                              color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.w700),
                        ),
                        SizedBox(height:5),
                      ],
                    ),
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                  ),
                  hr
                ],
              ),
            );

          });
    }

  }


  _getServerEop (ConfigModel configModel,int typeRequest) async {


    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    EopDto eopDto=EopDto();
    eopDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getEop(eopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

         FunctionUtils.saveEop(a.mesEop,_userId);

          Timer(Duration(seconds: 2),(){

            _localEopRepository.listEop(_userId).then((response) {

              if(response.length>0 ){
                setState(() {
                  _haveEop=true;
                  _allLocalEopEntity = response;
                });

              }else{
                setState(() {
                  _haveEop=true;
                });
              }

            });

            if(typeRequest.compareTo(1)==0) {
              Navigator.of(context).pop(null);
            }
          });



          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        setState(() {
          _haveEop=true;
        });
        return false;
      }
    });


  }

}
