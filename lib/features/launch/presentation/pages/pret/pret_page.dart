import 'dart:convert';
import 'dart:async';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/simulation_dto.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_scoop_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_upret_member_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_upret_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/liste_eop_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/liste_scoop_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/user_simulation_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/scoop_simulation_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/scoop_pret_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/user_pret_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_spret_member_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/domain/repositories/local_pret_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_pret_entity.dart';
import 'package:agrimobile/core/utils/preference.dart';

import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:agrimobile/features/common/data/models/simulation_model.dart';
import 'package:agrimobile/features/common/data/models/eop_model.dart';

class PretPage extends StatefulWidget {

  PretPage();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}



class PageState extends State<PretPage> with SingleTickerProviderStateMixin  {

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TabController _tabController;
  List<Tab> _myTabs;
  bool _visibleFinish = false;
  bool _visibleEncours = false;
  bool _visibleSccop = false;
  int _userId=0;
  String _userToken="";

  List<String> areaListData = <String>[];

  LocalScoopRepository _localScoopRepository = LocalScoopRepository();
  LocalPretRepository _localPretRepository = LocalPretRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  List<LocalPretEntity> _allLocalPretEntity;

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  Icon _myicon=Icon(Icons.add);

  @override
  void initState() {
    super.initState();

    _tabController = TabController(vsync: this, length: 2);
    _visibleFinish = false;
    _visibleEncours = false;
    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      _myTabs = <Tab>[
        new Tab(text: "${allTranslations.text('pret_encours')}"),
        new Tab(text: "${allTranslations.text('pret_ecoule')}"),
      ];

      return Scaffold(
          key: _scaffoldKey,
          appBar: new AppBar(
           // centerTitle: true,
            title: Text( allTranslations.text('pret_agricole'),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _getServerPret (configModel,1);
                    },
                    child: Icon(
                      Icons.refresh,
                      size: 26.0,
                    ),
                  )
              ),

              PopupMenuButton(
                itemBuilder: (BuildContext bc) => [
                  PopupMenuItem(child: Text(allTranslations.text('menu_udemandepret')), value: "ask_upret"),
                  if(_visibleSccop)PopupMenuItem(child: Text(allTranslations.text('menu_sdemandepret')), value: "ask_spret"),
                  PopupMenuItem(child: Text(allTranslations.text('menu_simulation_pret1')), value: "simulation_upret"),
                  if(_visibleSccop)PopupMenuItem(child: Text(allTranslations.text('menu_simulation_pret2')), value: "simulation_spret"),
                  PopupMenuItem(child: Text(allTranslations.text('menu_listeeop')), value: "all_eop"),
                  PopupMenuItem(child: Text(allTranslations.text('menu_listescoop')), value: "all_scoop"),
                ],
                onSelected: (selected) {

                  if(selected.compareTo("ask_upret")==0) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => UserPretPage(new LocalPretEntity())),
                    ).then((value){
                      if(value!=null && value.compareTo("saving")==0){
                        _displaySnackBar(context, allTranslations.text('saving_success'));
                      }
                      _getPretList();
                    });
                  }else if(selected.compareTo("ask_spret")==0) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ScoopPretPage(new LocalPretEntity())),
                    ).then((value){
                      if(value!=null && value.compareTo("saving")==0){
                        _displaySnackBar(context, allTranslations.text('saving_success'));
                      }
                      _getPretList();
                    });
                  }else if(selected.compareTo("simulation_upret")==0) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => UserSimulationPage()),
                    );
                  }else if(selected.compareTo("simulation_spret")==0) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ScoopSimulationPage()),
                    );
                  }else if(selected.compareTo("all_eop")==0) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ListeEopPage()),
                    );
                  }else if(selected.compareTo("all_scoop")==0) {

                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ListeScoopPage()),
                    );
                  }

                },
              ),
            ],
            backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),

          floatingActionButton:  SpeedDial(
            // both default to 16
            marginRight: 18,
            marginBottom: 20,
          //  animatedIcon: AnimatedIcons.menu_close,
            animatedIconTheme: IconThemeData(size: 22.0),
            child: _myicon,
            closeManually: false,
            curve: Curves.bounceIn,
            overlayColor: Colors.black,
            overlayOpacity: 0.5,
            onOpen: (){
              setState(() {
                _myicon=Icon(Icons.close);
              });
            },
            onClose:() {
              setState(() {
                _myicon=Icon(Icons.add);
              });
            },
           /* tooltip: 'Speed Dial',
            heroTag: 'speed-dial-hero-tag',*/

            backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
            foregroundColor: Colors.white,
            elevation: 8.0,
            shape: CircleBorder(),
            children: [
              SpeedDialChild(
                  child: Icon(Icons.person),
                  backgroundColor: Colors.red,
                  label: allTranslations.text('menu_udemandepret'),
                  labelStyle: TextStyle(fontSize: 18.0),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>UserPretPage(new LocalPretEntity())),
                    ).then((value){
                      if(value!=null && value.compareTo("saving")==0){
                        _displaySnackBar(context, allTranslations.text('saving_success'));
                      }
                      _getPretList();
                    });
                  }
              ),
              if(_visibleSccop)SpeedDialChild(
                child: Icon(Icons.accessibility),
                backgroundColor: Colors.blue,
                label: allTranslations.text('menu_sdemandepret'),
                labelStyle: TextStyle(fontSize: 18.0),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ScoopPretPage(new LocalPretEntity())),
                  ).then((value){
                    if(value!=null && value.compareTo("saving")==0){
                      _displaySnackBar(context, allTranslations.text('saving_success'));
                    }
                    _getPretList();
                  });
                },
              ),

            ],
          ),

          backgroundColor: Colors.white,
          body:Builder(
              builder: (BuildContext myContext)
              {
                return  Column(      // Column
                  children: <Widget>[

                    Container(
                      color:FunctionUtils.colorFromHex("dfdfdf"),        // Tab Bar color change
                      child: TabBar(           // TabBar
                        controller: _tabController,
                        unselectedLabelColor: FunctionUtils.colorFromHex("737373"),
                        labelColor: FunctionUtils.colorFromHex("020000"),
                        indicatorWeight: 3,
                        indicatorColor: FunctionUtils.colorFromHex(configModel.mainColor),
                        tabs: _myTabs,
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: TabBarView(         // Tab Bar View
                        physics: BouncingScrollPhysics(),
                        controller: _tabController,
                        children: <Widget>[
                          _pretEncours(configModel),
                          _pretFinalise(configModel),

                        ],
                      ),
                    ),
                  ],
                );
              }
          )

      );
    },
  );

  _pretEncours(ConfigModel configModel){

    if(_visibleEncours==false) {
      return Column(
        children: [
          SizedBox(height: 100,),
          Image.asset('assets/img/loading.gif',width: 150,height: 150,),
          SizedBox(height: 30,),
          Row(
            children: [
              SizedBox(width: 30,),
              Expanded(
                child: Center(
                  child: Text("${allTranslations.text('pgetting_processing')}",
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(width: 30,),
            ],
          )

        ],
      );
    } if(_allLocalPretEntity==null || _allLocalPretEntity.length==0){

      return Column(
        children: [
          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('liste_empty')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );

    }else{

      return  ListView.builder(
          itemCount: _allLocalPretEntity.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            List<SimulationModel> simulationList=null;

            EopModel infoEop;
            int nbre_users=0;
            if(_allLocalPretEntity[position].etatDemande.compareTo(-1)==0) {

                 Map<String, dynamic> contenuDemande=jsonDecode(_allLocalPretEntity[position].contenuDemande);
                 Map<String, dynamic> getEop=jsonDecode(contenuDemande['infoEop']);

                 String infoInteret="${getEop['montant_interet']}";
                 String infoMontant="${getEop['montant_total']}";
                 double interet=double.parse(infoInteret.replaceAll(" ", "")) * double.parse(_allLocalPretEntity[position].superficie.replaceAll(" ", ""));
                 double montant=double.parse(infoMontant.replaceAll(" ", "")) * double.parse(_allLocalPretEntity[position].superficie.replaceAll(" ", ""));
                 double montantInteret=interet+montant;

                infoEop=EopModel.create(nameSpeculation:"${getEop['name_speculation']}",superficie:_allLocalPretEntity[position].superficie,nameSite:"${getEop['name_site']}"
                    ,montantTotal:"${FunctionUtils.separateur(montant)}",interetEop:int.parse("${getEop['interet_eop']}"),dureeEop:int.parse("${getEop['duree_eop']}"),montantInteret:"${FunctionUtils.separateur(interet)}",projetInteret:"${FunctionUtils.separateur(montantInteret)}",descriptionEop:_allLocalPretEntity[position].descriptionDemande);
            }else{

              var listSimulation = jsonDecode(
                  _allLocalPretEntity[position].contenuDemande) as List;

              if (listSimulation != null) simulationList =
                  listSimulation.map((i) => SimulationModel.fromMap(i))
                      .toList();
              infoEop=simulationList[0].information;
              nbre_users=simulationList.length -1;

            }



            Widget Contenair=Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [

                Row(
                  children: [

                    Expanded(
                      child:  Text("${infoEop.nameSpeculation} sur ${infoEop.superficie} hectare(s)",
                        style: new TextStyle(
                            color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 17.0,fontWeight: FontWeight.bold),
                      ),
                    ),

                    if(_allLocalPretEntity[position].typePret==2 && _allLocalPretEntity[position].etatDemande!=-1)Row(
                      children: [
                        Icon(Icons.supervised_user_circle,
                          color: FunctionUtils.colorFromHex(configModel.mainColor),
                          size: 20,
                        ),
                        Text(" $nbre_users",
                          style: new TextStyle(
                              color: Colors.black54, fontSize: 22.0,fontWeight: FontWeight.w500),
                        )
                      ],
                    ),

                  ],
                ),

                //
                SizedBox(height:10),
                Row(
                  children: [
                    Container(
                        width: 130,
                        child: Text("${allTranslations.text('name_site')}",
                          style: new TextStyle(
                              color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                        )
                    ),
                    Expanded(child: Text(": ${infoEop.nameSite}")),
                  ],
                ),
                Row(
                  children: [
                    Container(
                        width: 130,
                        child: Text("${allTranslations.text('montant_total')}",
                          style: new TextStyle(
                              color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                        )
                    ),
                    Expanded(child: Text(": ${infoEop.montantTotal} ${configModel.devise}")),
                  ],
                ),
                Row(
                  children: [
                    Container(
                        width: 130,
                        child: Text("${allTranslations.text('interet_eop')}",
                          style: new TextStyle(
                              color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                        )
                    ),
                    Expanded(child: Text(": ${infoEop.interetEop} %",style: new TextStyle(
                        color: Colors.red, fontSize: 14.0,fontWeight: FontWeight.bold))),
                  ],
                ),
                Row(
                  children: [
                    Container(
                        width: 130,
                        child: Text("${allTranslations.text('duree_eop')}",
                          style: new TextStyle(
                              color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                        )
                    ),
                    Expanded(child: Text(": ${infoEop.dureeEop} ans",style: new TextStyle(
                        color: Colors.red, fontSize: 14.0,fontWeight: FontWeight.bold))),
                  ],
                ),
                Row(
                  children: [
                    Container(
                        width: 130,
                        child: Text("${allTranslations.text('interet_prix')}",
                          style: new TextStyle(
                              color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                        )
                    ),
                    Expanded(child: Text(": ${infoEop.montantInteret} ${configModel.devise}",style: new TextStyle(
                        color: Colors.red, fontSize: 14.0,fontWeight: FontWeight.bold))),
                  ],
                ),
                Row(
                  children: [
                    Container(
                        width: 130,
                        child: Text("${allTranslations.text('projet_interet')}",
                          style: new TextStyle(
                              color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                        )
                    ),
                    Expanded(child: Text(": ${infoEop.projetInteret} ${configModel.devise}",style: new TextStyle(
                        color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 14.0,fontWeight: FontWeight.bold))),
                  ],
                ),
                SizedBox(height:10),
                Text("${infoEop.descriptionEop}",
                  style: new TextStyle(
                      color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.w700),
                ),
                SizedBox(height:5),
              ],
            );

            return Column(
              children: [
                if(position==0)SizedBox(height: 10,),

                MaterialButton(
                  color: Colors.white,
                  elevation: 0.0,

                  textColor: Colors.white,
                  onPressed: () {

                    if(_allLocalPretEntity[position].typePret==2) {
                          if(_allLocalPretEntity[position].etatDemande.compareTo(-1)==0) {
                              Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => ScoopPretPage(_allLocalPretEntity[position])),
                              ).then((value){
                                  if(value!=null && value.compareTo("saving")==0){
                                  _displaySnackBar(context, allTranslations.text('saving_success'));
                                  }
                                  _getPretList();
                              });
                          }else {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>
                                  DetailSmpretPage(simulationList,
                                      _allLocalPretEntity[position].titleDemande,
                                      _allLocalPretEntity[position].idProjet,
                                      _allLocalPretEntity[position].fichierProjet)),
                            );
                          }
                    }else{

                      if(_allLocalPretEntity[position].etatDemande.compareTo(-1)==0) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => UserPretPage(_allLocalPretEntity[position])),
                          ).then((value){
                            if(value!=null && value.compareTo("saving")==0){
                              _displaySnackBar(context, allTranslations.text('saving_success'));
                            }
                            _getPretList();
                          });
                      }else{
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) =>
                                DetailUmpretPage(simulationList[0], _allLocalPretEntity[position].titleDemande,_allLocalPretEntity[position].idProjet,_allLocalPretEntity[position].fichierProjet)),
                          );
                      }
                    }
                  },
                  child: Column(
                    children: [

                      SizedBox(height: 20,),
                      Row(
                        children: [
                          SizedBox(width: 5,),
                          Icon(Icons.arrow_forward, color: Colors.black54,size: 20,),
                          SizedBox(width: 3,),
                          Text("${ FunctionUtils.convertPdate(_allLocalPretEntity[position].dateDemande)}",
                            style: new TextStyle(
                                color: Colors.black, fontSize: 20.0,fontWeight: FontWeight.bold),
                          ),

                        ],
                      ),
                      Container(
                        margin: EdgeInsets.only(left:40,right: 40),
                        child: Column(
                          children: [
                            SizedBox(height: 20,),
                            MaterialButton(
                              color: Colors.white,
                              elevation: 0.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  side: BorderSide(color:  Colors.black12)
                              ),
                              textColor: Colors.white,

                              child: _allLocalPretEntity[position].etatDemande.compareTo(-1)==0?Banner(
                                message: allTranslations.text('attenteSend'),
                                location: BannerLocation.topEnd,
                                color: FunctionUtils.colorFromHex(configModel.demandeColor),
                                child:Contenair  ,
                              ):Contenair,
                              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),

                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 20,),

                    ],
                  ),
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),

                ),

                SizedBox(height: 10,),

              ],
            );

          });
    }

  }

  _pretFinalise(ConfigModel configModel){

    if(_visibleFinish==false || areaListData.length<=0){
      return Column(

        children: [

          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('empty_pret2')}",
              textAlign: TextAlign.center,
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );
    }else {
      return ListView(
          padding: EdgeInsets.only(left:20.0,right:20.0,bottom:20.0),
          children: <Widget>[
            Container(
            )
          ]);
    }


  }

  _getUserInformation(){
      //recuperation de l'id de la personne
      _appSharedPreferences.getUserInformation().then((value) {
        setState(() {
          _userId=value.id;
          _userToken=value.token;
        });
        _getPretList();
        _localScoopRepository.filterScoopRight(_userId,"").then((value){
           if(value.length>0){
              setState(() {
                 _visibleSccop=true;
              });
           }else{
             setState(() {
               _visibleSccop=false;
             });
           }
        });
      });
  }

  _getPretList(){
    //recuperation des produits de la personne
    _localPretRepository.listPret(_userId).then((response) {

      if(response.length>0 ){

        setState(() {
          _visibleEncours = true;
          _allLocalPretEntity = response;
        });

      }else{
        _getServerPret (null,0);
      }

    });

  }

  _getServerPret (ConfigModel configModel,int typeRequest) async {

    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }


    SimulationDto simulationDto=SimulationDto();
    simulationDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getProjet(simulationDto).then((value) {
      if (value.isRight()) {
        value.all((a) {


          FunctionUtils.saveProjet(a.allProject,_userId);

          Timer(Duration(seconds: 2),(){

            _localPretRepository.listPret(_userId).then((response) {

              if(response.length>0 ){
                setState(() {
                  _visibleEncours = true;
                  _allLocalPretEntity = response;
                });

              }else{
                setState(() {
                  _visibleEncours=true;
                });
              }

            });

            if(typeRequest.compareTo(1)==0) {
              Navigator.of(context).pop(null);
            }
          });

          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        setState(() {
          _visibleEncours=true;
        });
        return false;
      }
    });

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}

