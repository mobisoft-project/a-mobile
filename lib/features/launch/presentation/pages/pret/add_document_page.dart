
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:agrimobile/core/utils/core_constantes.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/image_dto.dart';
import 'package:agrimobile/features/common/data/dto/simulation_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/composanteop_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/detaileop_model.dart';
import 'package:agrimobile/features/common/data/models/eop_model.dart';
import 'package:agrimobile/features/common/data/models/simulation_model.dart';
import 'package:agrimobile/features/common/data/models/tampon_image_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_pret_repository.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:intl/intl.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import 'package:pdf/widgets.dart' as pw;
import 'package:pdf/pdf.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:share/share.dart';

import 'package:dio/dio.dart';
import 'package:progress_dialog/progress_dialog.dart';


class AddDocumentPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final  String  keyProjet;
  final  String  nomProjet;
  AddDocumentPage(this.keyProjet,this.nomProjet);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState(this.keyProjet,this.nomProjet);
  }
}



class PageState extends State<AddDocumentPage> {

  final _formKey = GlobalKey<FormState>();
  String  _keyProjet;
  String  _nomProjet;
  PageState(this._keyProjet,this._nomProjet);

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  ConfigModel _myConfigModel;
  int _nbreFile=0;
  List<TamponImageModel> _allImage =[];
  List<TextEditingController> _descriptionController =[];
  String _userToken="";

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      _myConfigModel=configModel;
      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 0.0)),

          appBar: new AppBar(
            title:  Text("$_nomProjet",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 14.0,

              ),
            ),           
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          floatingActionButton: Visibility(
            child:  FloatingActionButton(
              onPressed: () {
                _nbreFile++;
                _descriptionController.add(TextEditingController());
                TamponImageModel newImage=new TamponImageModel();
                newImage.id=_nbreFile;
                newImage.titre="";
                newImage.path="";
                 setState(() {
                   _allImage.add(newImage);
                 });

               },
              child: Icon(Icons.add,color: Colors.white),
              backgroundColor: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
            ),
            visible: _keyProjet.compareTo("")==0?false:true,
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return Stack(
                  children: <Widget>[
                    ListView(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      children: <Widget>[

                        Form(
                            key: _formKey,
                            child:
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                _getDocListview(),
                                if(_allImage!=null && _allImage.length>0)Container(
                                  padding: EdgeInsets.only(left: 30.0,right:30.0,top:50.0,bottom: 50),
                                  child: MaterialButton(
                                    color:  FunctionUtils.colorFromHex(configModel.mainColor),
                                    elevation: 0.0,
                                    shape: RoundedRectangleBorder(
                                        side: BorderSide(color:  FunctionUtils.colorFromHex(configModel.mainColor))
                                    ),
                                    textColor: Colors.white,
                                    onPressed: () {
                                          bool readysend=true;
                                          if(_allImage.length>0){
                                            for(int u=0; u<_allImage.length;u++){
                                               if(_allImage[u].titre.compareTo("")==0 || _allImage[u].path.compareTo("")==0){
                                                 readysend=false;
                                               }
                                            }
                                          }else{
                                            readysend=false;
                                          }

                                          if(readysend==true){
                                               _sendPret();
                                          }else{
                                              _displaySnackBar(context,allTranslations.text('emptyField'));
                                          }
                                    },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        Text(
                                          allTranslations.text('send_doc'),
                                          style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white, fontSize: 16.0),
                                        ),
                                      ],
                                    ),
                                    padding: EdgeInsets.fromLTRB(0, 15, 0, 15),

                                  ),
                                )



                              ],
                            )
                        )
                      ],
                    )
                  ],
                );
              }
          )
      );
    },
  );



  _sendPret () async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('document_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });


    ImageDto imageDto=ImageDto();

    imageDto.userToken=_userToken;
    imageDto.keyProjet=_keyProjet;
    imageDto.fileProjet=_allImage;


    Api api = ApiRepository();
    api.sendDocument(imageDto).then((value) {
      if (value.isRight()) {
        value.all((a) {


          if(a.status.compareTo("000")==0){

            LocalPretRepository localPretRepository = LocalPretRepository();
            localPretRepository.existPret(_keyProjet).then((value){

                print(value);
                if(value.length>0){
                  value[0].fichierProjet=a.message;
                  localPretRepository.update(value[0], value[0].idPret).then((value){
                      Navigator.of(context).pop(null);
                      Navigator.of(context).pop(a.message);
                  });
                }
            });

          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
          }

          return true;
        });
      } else {
        _displaySnackBar(context, allTranslations.text('error_process'));
        Navigator.of(context).pop(null);
        return false;
      }
    });

  }

  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userToken=value.token;
      });
    });

  }


  _getDocListview(){


    if(_allImage==null || _allImage.length==0) {

      return Column(

        children: [

          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('click_docadd')}",
              textAlign: TextAlign.center,
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );

    }else{

      return  ListView.builder(
          itemCount:_allImage.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {


            return Container(
                margin:EdgeInsets.all(10),
                child:Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 20),
                    Row(
                      children: [
                        Expanded(child: Text("${allTranslations.text('denomination_document')}",style: TextStyle(
                            color:Colors.black,
                            fontSize: 13,
                            fontWeight: FontWeight.w500
                        ),),),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              _allImage.removeWhere((item) => item.id == _allImage[position].id);
                            });
                          },
                          child: Icon(Icons.cancel, color: Colors.red,size: 20,),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                    Container(
                      child: TextFormField(
                          controller: _descriptionController[_allImage[position].id-1],
                          decoration: InputDecoration(
                            isDense: true,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                          ),
                          onChanged:(value){
                            setState(() {
                              _allImage[position].titre=value;
                            });
                          }
                      ),
                    ),

                    SizedBox(height: 20),
                    Text("${allTranslations.text('add_tdocument')}",style: TextStyle(
                        color:Colors.black,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                    ),),
                    SizedBox(height: 5),
                    RawMaterialButton(
                      onPressed: () {
                         _openGallery(position);
                      },
                      child: new Icon( Icons.insert_drive_file, color: FunctionUtils.colorFromHex(_myConfigModel.mainColor), size: 25.0, ),
                      shape: new CircleBorder(),
                      elevation: 2.0,
                      fillColor: Colors.white,
                      padding: const EdgeInsets.all(10.0),
                    ),
                    if(_allImage[position].path.compareTo("")!=0)Text("${_allImage[position].path} ",style: TextStyle(
                        color:Colors.red,
                        fontSize: 12,
                        fontWeight: FontWeight.w500
                    ),),
                  ],
                )
            );

          });
    }
  }

  Future _openGallery(int position) async {

    FilePickerCross myFile = await FilePickerCross.importFromStorage(
      type: FileTypeCross.any,
    );

    if(myFile != null) {
      setState(() {
        _allImage[position].path = myFile.path;

      });
    }

  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}

