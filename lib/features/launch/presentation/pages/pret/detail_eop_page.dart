
import 'dart:async';
import 'dart:convert';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/dto/eop_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/composanteop_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/detaileop_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_eop_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_eop_repository.dart';
import 'package:agrimobile/features/launch/presentation/widgets/liste_annonce_widget.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';

class DetailEopPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final LocalEopEntity infoEop;
  DetailEopPage(this.infoEop);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState(this.infoEop);
  }
}



class PageState extends State<DetailEopPage> {

  LocalEopEntity _infoEop;
  PageState(this._infoEop);

  List<DetailEopModel> _allComposantEop;
  bool _haveEop=false;

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 80.0)),

          appBar: new AppBar(
            title:  Text("${_infoEop.nameSite}: ${_infoEop.nameSpeculation}",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,

              ),
            ),
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return  _getDetailListview(configModel);
              }
          )
      );
    },
  );

  _getUserInformation(){

    String detail_eop=_infoEop.composantEop;
    List<dynamic> responseMap = jsonDecode(detail_eop.toString());

    setState(() {
      _haveEop=true;
      _allComposantEop =responseMap.map((i) => DetailEopModel.fromMap(i)).toList();
    });


  }

  _getComposantListview(List<ComposantEopModel> detailComposant,ConfigModel configModel){

    return ListView.builder(
        itemCount:detailComposant.length,
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (BuildContext context, int position) {

          ComposantEopModel infoDetail= detailComposant[position];


          return Column(
              children: [
                hr,
                if(position==0) Row(
                  children: [
                    Expanded(child: Text("${allTranslations.text('description')}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                    SizedBox(width:5),
                    Expanded(child: Text("${allTranslations.text('etiquete')}",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                    SizedBox(width:5),
                    Expanded(child: Text("${allTranslations.text('qte')}/hectare",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                    SizedBox(width:5),
                    Expanded(child: Text("${allTranslations.text('prixunitaire')} (${configModel.devise})",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                    SizedBox(width:5),
                    Expanded(child: Text("${allTranslations.text('prixtotal')} (${configModel.devise})",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14.0,

                        ))),
                  ],
                ),
                if(position==0) hr,
                Row(
                  children: [
                    Expanded(child: Text("${infoDetail.description}")),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoDetail.etiquette}")),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoDetail.qte} ${infoDetail.unites}")),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoDetail.coutUnitaire}",textAlign: TextAlign.right)),
                    SizedBox(width:5),
                    Expanded(child: Text("${infoDetail.coutTotal}",textAlign: TextAlign.right)),
                  ],
                )

              ]
          );

        });

  }


  _getDetailListview(ConfigModel configModel){

    if(_haveEop==false) {
      return Column(
        children: [
          SizedBox(height: 100,),
          Image.asset('assets/img/loading.gif',width: 150,height: 150,),
          SizedBox(height: 30,),
          Center(
            child: Text("${allTranslations.text('getting_processing')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );
    }else if(_allComposantEop==null || _allComposantEop.length==0){

      return Column(
        children: [
          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('detaileop_empty')}",
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );

    }else{
      return  ListView.builder(
          itemCount: _allComposantEop.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            DetailEopModel infoEop=  _allComposantEop[position];

            return Container(
              margin: EdgeInsets.only(left:10,right: 10,bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  if(position==0)SizedBox(height: 20,),
                  Row(
                    children: [
                      Text("${infoEop.nomComposants}",
                        style: new TextStyle(
                            color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                      ),
                      Text(": ${infoEop.montantParametre} ${configModel.devise}",style: new TextStyle(
                          color: FunctionUtils.colorFromHex(configModel.mainColor), fontSize: 14.0,fontWeight: FontWeight.bold))
                    ],
                  ),
                  SizedBox(height:5),
                  _getComposantListview(infoEop.detailComposant,configModel),

                  SizedBox(height:40),


                ],
              ),
            );

          });
    }

  }


}
