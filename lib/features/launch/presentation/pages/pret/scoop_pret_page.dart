import 'dart:async';
import 'dart:convert';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/dto/scoop_dto.dart';
import 'package:agrimobile/features/common/data/dto/eop_dto.dart';
import 'package:agrimobile/features/common/data/dto/simulation_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/detailscoop_model.dart';
import 'package:agrimobile/features/common/data/models/simulation_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_eop_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_pret_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_eop_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_pret_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_scoop_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_spret_member_page.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:image_picker/image_picker.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'dart:io';

class ScoopPretPage extends StatefulWidget {

  LocalPretEntity defaultPretEntity;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  ScoopPretPage(this.defaultPretEntity);


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState(this.defaultPretEntity);
  }
}



class PageState extends State<ScoopPretPage> {

  LocalPretEntity defaultPretEntity;
  PageState(this.defaultPretEntity);

  LocalEopRepository _localEopRepository = LocalEopRepository();
  LocalScoopRepository _localScoopRepository = LocalScoopRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  List<TamponMemberModel> _listeMembers=[];
  LocalEopEntity selectedEop ;

  TextEditingController _superficieController=TextEditingController();
  TextEditingController _denominationController=TextEditingController();
  TextEditingController _descriptionController=TextEditingController();

  final _formKey = GlobalKey<FormState>();

  int _userId=0;
  String _userToken="";
  String _keyEop="";
  String _keyScoop="";
  int _tamponid=0;
  String _fullname="";

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  List<Step> steps = List();
  int _currentStep = 0;

  ConfigModel _myConfigModel ;
  List<DetailScoopModel> _allComposantScoop;
  List<String> _allfile =[];

  List<File>  _image;

  final eopKey = GlobalKey<FindDropdownState>();
  final scoopKey = GlobalKey<FindDropdownState>();


  bool _visible=false;
  bool _visibleError=false;

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel> (

    builder: (context, configModel, child) {

      _myConfigModel=configModel;

      steps = [
        Step(

          isActive: true,
          state: _currentStep==0?StepState.editing:StepState.complete,
          title:  Text(allTranslations.text('scoop'),style: TextStyle(
            fontSize: 12,
          )),
          subtitle: Text(allTranslations.text('sub_superficie'),style: TextStyle(
            fontSize: 10,
          )),
          content:
          Form(
              key: _formKey,
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("${allTranslations.text('selectScoop')} **",style: TextStyle(
                      color:Colors.black,
                      fontSize: 13,
                      fontWeight: FontWeight.w500
                  ),),
                  SizedBox(height: 5),
                  FindDropdown<LocalScoopEntity>(
                    key: scoopKey,
                    onFind: (String filter) => getScoop(filter),
                    searchBoxDecoration: InputDecoration(
                      hintText: allTranslations.text('search'),
                      border: OutlineInputBorder(),
                    ),
                    dropdownBuilder: (BuildContext context, LocalScoopEntity item) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(10),
                        height: 40,
                        decoration: BoxDecoration(
                          border: Border.all(color: Theme.of(context).dividerColor),
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white,
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: item==null?Text(""):Text("${item.nameAssociation} (${item.cantonAssociation})"),
                            ),
                            Icon(
                              Icons.arrow_drop_down,
                              color: Colors.grey,
                              size: 20,
                            ),
                            // SizedBox(width: 5,)
                          ],
                        ),
                      );
                    },
                    dropdownItemBuilder:(BuildContext context, LocalScoopEntity item, bool isSelected) {
                      return Container(
                        decoration: !isSelected ? null : BoxDecoration(
                          color: Colors.white,
                        ),
                        child: ListTile(
                          selected: isSelected,
                          title: item==null?Text(""):Text("${item.nameAssociation} (${item.cantonAssociation})"),
                        ),
                      );
                    },
                    emptyBuilder: (BuildContext context){
                      return  Container(
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(4.0),
                            border: Border.all(
                              color: Colors.red,
                            ),
                            color: Colors.red),
                        height: 50,
                        margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                        child: Center(
                          child: Text("${allTranslations.text('liste_empty')}",
                            style: new TextStyle(
                                color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                          ),
                        ),
                      );
                    },

                    onChanged: (LocalScoopEntity data) {

                      _keyScoop="";
                      _allComposantScoop=[];
                      setState(() {
                        _visible=false;
                        _listeMembers=[];

                      });
                      if(data!=null ){
                        _keyScoop="${data.idAssociation}";
                        setState(() {
                          _visible=true;
                        });
                        List<dynamic> responseMap = jsonDecode(data.memberAssociation.toString());
                        _allComposantScoop=responseMap.map((i) => DetailScoopModel.fromMap(i)).toList();
                      }

                    },
                  ),
                  SizedBox(height: 10.0),
                  Visibility(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _getMemberListview(),

                      ],
                    ),
                    visible: _visible,
                  )

                ],
              )
          ),
        ),
        Step(
            title:  Text(allTranslations.text('title_eop'),style: TextStyle(
              fontSize: 12,
            )),
            subtitle: Text(allTranslations.text('sub_eop'),style: TextStyle(
              fontSize: 10,
            )),
            isActive: _currentStep<1?false:true,
            state: _currentStep<1?StepState.disabled:_currentStep==1?StepState.editing:StepState.complete,
            content:
            Form(
                key: _formKey,
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("${allTranslations.text('selectEop')} **",style: TextStyle(
                        color:Colors.black,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                    ),),
                    SizedBox(height: 5),
                    FindDropdown<LocalEopEntity>(
                      key: eopKey,
                      onFind: (String filter) => getEop(filter),
                      searchBoxDecoration: InputDecoration(
                        hintText: allTranslations.text('search'),
                        border: OutlineInputBorder(),
                      ),
                      dropdownBuilder: (BuildContext context, LocalEopEntity item) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10),
                          height: 40,
                          decoration: BoxDecoration(
                            border: Border.all(color: Theme.of(context).dividerColor),
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white,
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: item==null?Text(""):Text("${item.nameSite} (${item.nameSpeculation})"),
                              ),
                              Icon(
                                Icons.arrow_drop_down,
                                color: Colors.grey,
                                size: 20,
                              ),
                              // SizedBox(width: 5,)
                            ],
                          ),
                        );
                      },
                      dropdownItemBuilder:(BuildContext context, LocalEopEntity item, bool isSelected) {
                        return Container(
                          decoration: !isSelected ? null : BoxDecoration(
                            color: Colors.white,
                          ),
                          child: ListTile(
                            selected: isSelected,
                            title: item==null?Text(""):Text("${item.nameSite} (${item.nameSpeculation})"),
                          ),
                        );
                      },
                      emptyBuilder: (BuildContext context){
                        return  Container(
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                color: Colors.red,
                              ),
                              color: Colors.red),
                          height: 50,
                          margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                          child: Center(
                            child: Text("${allTranslations.text('liste_empty')}",
                              style: new TextStyle(
                                  color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                            ),
                          ),
                        );
                      },

                      onChanged: (LocalEopEntity data) {
                        selectedEop=data;
                        _allfile=[];
                        _keyEop="";
                        if(data!=null ){
                          _keyEop="${data.keyEop}";

                          if(data.fichierEop!=null && data.fichierEop.compareTo("")!=0) {
                            setState(() {
                              _allfile = data.fichierEop.split(";");
                            });
                          }else{
                            setState(() {
                              _allfile=[];
                            });
                          }
                        }
                        _image=[];
                        _image= new List(_allfile.length);


                      },
                    ),

                    SizedBox(height: 10.0),


                  ],
                )
            )
        ),
        Step(
            title:  Text(allTranslations.text('title_file'),style: TextStyle(
              fontSize: 12,
            )),
            subtitle: Text(allTranslations.text('subtitle_file'),style: TextStyle(
              fontSize: 10,
            ),
            ),
            isActive: _currentStep<2?false:true,
            state: _currentStep<2?StepState.disabled:_currentStep==2?StepState.editing:StepState.complete,
            content:
            Form(
                key: _formKey,
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: _getProjetFile(),
                )
            )
        ),
      ];

      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 120.0)),
          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_pret2'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,

                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _savePretStart ();
                    },
                    child: Icon(
                      Icons.backup,
                      size: 26.0,
                    ),
                  )
              ),
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _getServerEop (configModel,1);
                    },
                    child: Icon(
                      Icons.refresh,
                      size: 26.0,
                    ),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          floatingActionButton: Visibility(
            child:  FloatingActionButton(
              onPressed: () {
                _addMembre();
              },
              child: Icon(Icons.people,color: Colors.white),
              backgroundColor: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
            ),
            visible: _currentStep==0 && _visible?true:false,
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return
                  Column(children: <Widget>[
                    Expanded(
                      child: Stepper(
                        steps: steps,
                        type: StepperType.horizontal,
                        currentStep: _currentStep,
                        onStepContinue: next,
                        onStepTapped: (step) => goTo(step),
                        onStepCancel: cancel,
                        controlsBuilder:
                            (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                          return Row(
                            children: <Widget>[
                            ],
                          );
                        },
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomCenter,
                      child:
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                              color:Colors.white70,
                              style: BorderStyle.solid,
                              width: 1.0
                          ),
                          color: Colors.white70,
                        ),

                        child: Row(
                          children: [
                            Expanded(
                              child: Visibility(
                                child:
                                Container(
                                  child:
                                  MaterialButton(

                                    onPressed: () {
                                       cancel();
                                    },

                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 45.0,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color:FunctionUtils.colorFromHex(configModel.mainColor),
                                              style: BorderStyle.solid,
                                              width: 1.0
                                          ),
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(5.0)
                                      ),
                                      child: Text(
                                        allTranslations.text('previor'),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: FunctionUtils.colorFromHex(configModel.mainColor)
                                        )
                                        ,
                                      ),
                                    ),
                                    padding: EdgeInsets.all(10),
                                  ),

                                ),
                                visible: _currentStep==0?false:true,
                              ),
                            ),
                            Expanded(
                              child:
                              Container(
                                child:
                                MaterialButton(

                                  onPressed: () {
                                    next();
                                  },

                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 45.0,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                                            style: BorderStyle.solid,
                                            width: 1.0
                                        ),
                                        color: FunctionUtils.colorFromHex(configModel.mainColor) ,
                                        borderRadius: BorderRadius.circular(5.0)
                                    ),
                                    child: Text(_currentStep==2?allTranslations.text('submitbtn'):allTranslations.text('nextbtn'),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white
                                      )
                                      ,
                                    ),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),

                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ]);
              }
          )
      );
    },
  );

  _getProjetFile(){

    Widget defaultContent= Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        Text("${allTranslations.text('denomination_projet')} **",style: TextStyle(
            color:Colors.black,
            fontSize: 13,
            fontWeight: FontWeight.w500
        ),),
        SizedBox(height: 5),
        Container(
          child: TextFormField(
              controller: _denominationController,
              decoration: InputDecoration(
                isDense: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              )
          ),
        ),
        SizedBox(height: 20),
        Text("${allTranslations.text('description_projet')} **",style: TextStyle(
            color:Colors.black,
            fontSize: 13,
            fontWeight: FontWeight.w500
        ),),
        SizedBox(height: 5),
        Container(
          child: TextFormField(
              controller: _descriptionController,
              textInputAction: TextInputAction.newline,
              keyboardType: TextInputType.multiline,
              minLines: 4,
              maxLines: null,
              decoration: InputDecoration(
                isDense: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              )
          ),
        ),

      ],
    );

    List<Widget> allWidget =[];
    allWidget.add(defaultContent);

    if(_allfile!=null &&  _allfile.length>0) {

      allWidget.add( SizedBox(height: 20));
      allWidget.add( Text("${allTranslations.text('add_document')}",style: TextStyle(
          color:Colors.red,
          fontSize: 15,
          fontWeight: FontWeight.bold
      ),),);
      for(int u=0;u<_allfile.length;u++){

        String filename="";
        if(_image[u]!=null){
          List<String> pathInfo=_image[u].path.split("/");
          filename=pathInfo[pathInfo.length-1];
        }

        Widget fileContent= Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20),
            Text("${_allfile[u]} ",style: TextStyle(
                color:Colors.black,
                fontSize: 13,
                fontWeight: FontWeight.w500
            ),),
            SizedBox(height: 5),
            RawMaterialButton(
              onPressed: () {
                //_showChoiceDialog(context,configModel);
                _openGallery(u);
              },
              child: new Icon( Icons.insert_drive_file, color: FunctionUtils.colorFromHex(_myConfigModel.mainColor), size: 25.0, ),
              shape: new CircleBorder(),
              elevation: 2.0,
              fillColor: Colors.white,
              padding: const EdgeInsets.all(10.0),
            ),
            if(filename.compareTo("")!=0)Text("${filename} ",style: TextStyle(
                color:Colors.red,
                fontSize: 12,
                fontWeight: FontWeight.w500
            ),),

          ],
        );

        allWidget.add(fileContent);

      }


    }
    return allWidget ;

  }

  Future _openGallery(int position) async {

    FilePickerCross myFile = await FilePickerCross.importFromStorage(
        type: FileTypeCross.any,       // Available: `any`, `audio`, `image`, `video`, `custom`. Note: not available using FDE
        //fileExtension: 'txt, md, pdf'     // Only if FileTypeCross.custom . May be any file extension like `dot`, `ppt,pptx,odp`
    );

    if(myFile != null) {
      setState(() {
        _image[position] = File(myFile.path);

      });
    }

  }

  void _addMembre(){

    _fullname="";
      _superficieController.text="";
      setState(() {
        _visibleError=false;
        _tamponid=0;
      });

      Widget cancelButton = FlatButton(
        child: Text(allTranslations.text('cancel'),style: new TextStyle(
            fontWeight: FontWeight.bold, fontSize: 14.0,color: Colors.grey),
        ),
        onPressed:  () {
          Navigator.of(context).pop(null);
        },
      );
      Widget continueButton = FlatButton(
        child: Text(allTranslations.text('add'),style: new TextStyle(
            fontWeight: FontWeight.bold, fontSize: 14.0,color:FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
        ),
        onPressed:  () {

          if(_tamponid.compareTo(0)!=0  && _superficieController.text.compareTo("")!=0  && _superficieController.text.compareTo("0")!=0) {

            TamponMemberModel newFriend=TamponMemberModel();
            newFriend.id= _tamponid;
            newFriend.nom= _fullname;
            newFriend.superficie= _superficieController.text;

            if ((_listeMembers.firstWhere((it) => it.id == newFriend.id,
                orElse: () => null)) != null) {

            }else{
             setState(() {
               _listeMembers.add(newFriend);
             });
              Navigator.of(context).pop(null);
            }

          }

        },
      );

      showDialog(
        context: context,
        builder: (BuildContext context) {

          return AlertDialog(
            content: StatefulBuilder(  // You need this, notice the parameters below:
              builder: (BuildContext context, StateSetter setState) {
                return SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Visibility(
                        child: Container(
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                color: Colors.red,
                              ),
                              color: Colors.red),
                          height: 40,
                          margin: EdgeInsets.only(bottom:10),
                          child: Center(
                            child: Text("${allTranslations.text('already_added')}",
                              style: new TextStyle(
                                  color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        visible: _visibleError,
                      ),
                      Text("${allTranslations.text('lastname_friend')} **",style: TextStyle(
                          color:Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.w500
                      ),),
                      SizedBox(height: 5),
                      FindDropdown<DetailScoopModel>(
                        onFind: (String filter) => _getMember(filter),
                        searchBoxDecoration: InputDecoration(
                          hintText: allTranslations.text('search'),
                          border: OutlineInputBorder(),
                        ),
                        dropdownBuilder: (BuildContext context, DetailScoopModel item) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(10),
                            height: 40,
                            decoration: BoxDecoration(
                              border: Border.all(color: Theme.of(context).dividerColor),
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: item==null?Text(""):Text("${item.fullName} ${item.phoneNumber}"),
                                ),
                                Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.grey,
                                  size: 20,
                                ),
                                // SizedBox(width: 5,)
                              ],
                            ),
                          );
                        },
                        dropdownItemBuilder:(BuildContext context, DetailScoopModel item, bool isSelected) {
                          return Container(
                            decoration: !isSelected ? null : BoxDecoration(
                              color: Colors.white,
                            ),
                            child: ListTile(
                              selected: isSelected,
                              title: item==null?Text(""):Text("${item.fullName} ${item.phoneNumber}"),
                            ),
                          );
                        },

                        onChanged: (DetailScoopModel data) {
                          if(data!=null) {

                            if ((_listeMembers.firstWhere((it) =>
                            it.id == data.idAgriculteur,
                                orElse: () => null)) != null) {
                              setState(() {
                                _visibleError = true;
                              });
                            } else {
                              setState(() {
                                _visibleError = false;
                              });

                              _tamponid = data.idAgriculteur;
                              _fullname = data.fullName;
                              _superficieController.text = "${data.superficie}";

                            }
                          }
                        },
                      ),
                      SizedBox(height: 10.0),


                      Text("${allTranslations.text('superficie_friend')} **",style: TextStyle(
                          color:Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.w500
                      ),),
                      SizedBox(height: 5),
                      Container(
                        child: TextFormField(
                            controller: _superficieController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              isDense: true,
                              prefixIcon: Icon(Icons.map,size: 18.0,color: Colors.black45),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                            )
                        ),
                      ),


                    ],
                  ),
                );
              },
            ),
            actions: [
              cancelButton,
              continueButton,
            ],
          );
        },
      );

    }

  Future<List<DetailScoopModel>> _getMember(String filter) async{

      List<DetailScoopModel> suggestionList=[];



      if(_allComposantScoop!=null && _allComposantScoop.length>0) {
          String search = filter.toLowerCase();
          search.isEmpty
              ? suggestionList = _allComposantScoop //In the true case
              : suggestionList.addAll(_allComposantScoop.where(
                (element) =>
            element.fullName.toLowerCase().contains(search) ||
                element.phoneNumber.toLowerCase().contains(search),
          ));
      }

      return suggestionList ;

  }

  _getMemberListview(){

    if(_listeMembers==null || _listeMembers.length==0) {

      return Column(

        children: [

          SizedBox(height: 50,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('click_toadd')}",
              textAlign: TextAlign.center,
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );

    }else{

      return  ListView.builder(
          itemCount:_listeMembers.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            TamponMemberModel compagnon=_listeMembers[position];

            return Container(

              child: Column(
                children: [
                  if(position==0) SizedBox(height: 50,),
                  if(position==0)Row(
                    children: [

                      Icon(Icons.arrow_forward, color: Colors.black54,size: 20,),
                      SizedBox(width: 3,),
                      Text("${allTranslations.text('membre_cooperative')}",
                        style: new TextStyle(
                            color: Colors.black54, fontSize: 20.0,fontWeight: FontWeight.bold),
                      ),

                    ],
                  ),
                  if(position==0) SizedBox(height: 10,),
                  MaterialButton(

                    elevation: 0.0,
                    onPressed: ()  {

                    },
                    child: Container(
                      padding: EdgeInsets.fromLTRB(10, 3, 10, 3),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color:FunctionUtils.colorFromHex("DEDEDE"),
                            style: BorderStyle.solid,
                            width: 1.0
                        ),
                        borderRadius: BorderRadius.circular(5.0),
                        color: Colors.white,
                      ),
                      child: Row(
                        children: [
                          CircleAvatar(
                              radius: 15,
                              backgroundColor: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
                              child:CircleAvatar(
                                radius: 14.0,
                                backgroundColor:Colors.white,
                                child: Icon(Icons.person,
                                  color: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
                                  size: 20,
                                ),
                              )
                          ),
                          SizedBox(width: 5,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [


                                Text("${compagnon.nom} ",
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 2,),
                                Text("${compagnon.superficie} hectare(s)",
                                  textAlign: TextAlign.end,
                                  style: new TextStyle(
                                      color: FunctionUtils.colorFromHex(_myConfigModel.mainColor), fontSize: 12.0,fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              _showConfirmDeleteMember(context,compagnon);
                            },
                            child: Icon(Icons.delete, color: Colors.red,size: 15,),
                          ),

                        ],
                      ),
                    ),
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  ),
                  SizedBox(height: 10,),
                ],
              ),
            );

          });
    }
  }

  _showConfirmDeleteMember(BuildContext context,TamponMemberModel compagnon) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0,color: Colors.grey),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0,color:FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        setState(() {
          _listeMembers.remove(compagnon);
        });

      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(allTranslations.text('deleteFriend_text'),style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<List<LocalEopEntity>> getEop(String filter){
    //
    return  _localEopRepository.filterEop(_userId,filter);
  }

  Future<List<LocalScoopEntity>> getScoop(String filter){

    return  _localScoopRepository.filterScoopRight(_userId,filter);
  }

  next() {
    if(_currentStep + 1 != steps.length){
      goTo(_currentStep + 1);
    }else{
      goTo(3);
    }

  }

  cancel() {
    if (_currentStep > 0) {
      goTo(_currentStep - 1);
    }else{
      Navigator.pop(context, true);
    }
  }

  goTo(int step) {

    int oldStep=_currentStep;

    if(step==0) {

      setState(() => _currentStep = step);

    }else if(step==1){

        if(_keyScoop.compareTo("")==0 || _listeMembers==null || _listeMembers.length==0){
          _displaySnackBar(context, allTranslations.text('empty_field'));
        }else{
          setState(() => _currentStep = step);
        }

    }else if(step==2){

        if( (_keyEop.compareTo("")!=0 )){
          setState(() => _currentStep = step);
        }else{
          _displaySnackBar(context, allTranslations.text('empty_field'));
        }

    }else if(step==3){

       if(_denominationController.text.compareTo("")!=0 && _descriptionController.text.compareTo("")!=0 ){
            bool verifcationFile=true;

            if(_image!=null && _image.length>0){
              for(int u=0;u<_image.length;u++){

                if(_image[u]==null || _image[u].path.compareTo("")==0){
                  verifcationFile=false;
                }

              }
            }
            if(verifcationFile==true){
                _sendPret();
            }else{
              _displaySnackBar(context, allTranslations.text('empty_fichier'));
            }

       }else{
         _displaySnackBar(context, allTranslations.text('empty_projet'));
       }


    }


    if((oldStep==0 || oldStep==2) && step==1){

      if(_keyEop.compareTo("")!=0) {
        _localEopRepository.oneEop(_userId,"$_keyEop").then((response) {

          if (response != null) {

            if(eopKey!=null && eopKey.currentState!=null) {
              eopKey.currentState.setSelectedItem(response);
            }

          }
        });
      }

    }

    if(oldStep==1 && step==0){

      if(_keyScoop.compareTo("")!=0) {
        _localScoopRepository.oneScoop(_userId,"$_keyScoop").then((response) {

          if (response != null) {

            if(scoopKey!=null && scoopKey.currentState!=null) {
              scoopKey.currentState.setSelectedItem(response);
            }

          }
        });
      }

    }


  }

  _sendPret () async {

    double superficie=0;

    String allUser="";
    for(int i=0;i<_listeMembers.length;i++){
      if(allUser.compareTo("")!=0)allUser+=",";
      allUser+='{"id":"${_listeMembers[i].id}","nom":"${_listeMembers[i].nom}","superficie":"${_listeMembers[i].superficie}"}';

      superficie+=double.parse(_listeMembers[i].superficie.replaceAll(",", ".")) ;
    }
    allUser='[$allUser]';

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('pret_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });


    SimulationDto simulationDto=SimulationDto();

    simulationDto.userToken=_userToken;
    simulationDto.keyEop=_keyEop;
    simulationDto.keyScoop=_keyScoop;
    simulationDto.jsonScoop=allUser;
    simulationDto.denominationProjet=_denominationController.text;
    simulationDto.descriptionProjet=_descriptionController.text;
    simulationDto.fileProjet=_image;


    Api api = ApiRepository();
    api.sendSpret(simulationDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          if(a.status.compareTo("000")==0){


            //Enregistrement dans la base local

            //String contentInfo =  json.encode(a.simulation);
            String contentInfo = jsonEncode(a.simulation.map((e) => e.toJson()).toList());

            DateTime now = DateTime.now();
            DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm');
            String formatted = formatter.format(now);

            LocalPretRepository localPretRepository = LocalPretRepository();
            LocalPretEntity localPretEntity = LocalPretEntity();

            localPretEntity.idScoop=_keyScoop;
            localPretEntity.idEop=_keyEop;
            localPretEntity.superficie="$superficie";
            localPretEntity.dateDemande=formatted;
            localPretEntity.idUser=_userId;
            localPretEntity.etatDemande=0;
            localPretEntity.typePret=2;
            localPretEntity.fichierProjet=a.allFichier;
            localPretEntity.idProjet=a.keyProjet;
            localPretEntity.titleDemande=a.message;
            localPretEntity.descriptionDemande=a.description;
            localPretEntity.contenuDemande=contentInfo;

            if(defaultPretEntity.idPret!=null){
              localPretRepository.update(localPretEntity,defaultPretEntity.idPret).then((value) {
                  Navigator.of(context).pop(null);
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => DetailSmpretPage(a.simulation,a.message,a.keyProjet,a.allFichier)),
                  );
              });
            }else {
              localPretEntity.idPret = 0;
              localPretRepository.save(localPretEntity).then((value) {
                  Navigator.of(context).pop(null);
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (context) => DetailSmpretPage(a.simulation,a.message,a.keyProjet,a.allFichier)),
                  );
              });
            }


            return true;

          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
          }

          return true;
        });
      } else {
        _displaySnackBar(context, allTranslations.text('error_process'));
        Navigator.of(context).pop(null);
        return false;
      }
    });


  }

  _getUserInformation(){


    if(defaultPretEntity.idPret!=null){

      _denominationController.text=defaultPretEntity.titleDemande;
      _descriptionController.text=defaultPretEntity.descriptionDemande;
      setState(() {
          _keyEop=defaultPretEntity.idEop;
          _keyScoop=defaultPretEntity.idScoop;
      });



    }


    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userId=value.id;
        _userToken=value.token;
      });


      if(_keyScoop.compareTo("")!=0) {
        _localScoopRepository.oneScoop(_userId,"$_keyScoop").then((response) {

          if (response != null) {
            if(scoopKey!=null && scoopKey.currentState!=null) {
              scoopKey.currentState.setSelectedItem(response);
              setState(() {
                _visible=true;
              });
            }

            Map<String, dynamic> contenuDemande=jsonDecode(defaultPretEntity.contenuDemande);
            List<dynamic> allUser=jsonDecode(contenuDemande['user']);

            if(allUser.length>0){
              for(var u=0;u<allUser.length;u++){
                setState(() {
                 _listeMembers.add(new TamponMemberModel.create(id:int.parse(allUser[u]['id']),nom:allUser[u]['nom'],superficie:allUser[u]['superficie']));
                });
              }
            }

            List<dynamic> responseMap = jsonDecode(response.memberAssociation.toString());
            _allComposantScoop=responseMap.map((i) => DetailScoopModel.fromMap(i)).toList();

          }
        });
      }

      if(_keyEop.compareTo("")!=0) {
        _localEopRepository.oneEop(_userId,"$_keyEop").then((response) {
          selectedEop=response;
          if (response != null) {
            if(eopKey!=null && eopKey.currentState!=null) {
              eopKey.currentState.setSelectedItem(response);
            }

            _allfile=[];
            if(response.fichierEop!=null && response.fichierEop.compareTo("")!=0) {
              setState(() {
                _allfile = response.fichierEop.split(";");
              });
            }else{
              setState(() {
                _allfile=[];
              });
            }
            _image=[];
            _image= new List(_allfile.length);
            List<dynamic> default_fichier=jsonDecode(defaultPretEntity.fichierProjet) ;
            if(default_fichier.length>0){
              for(int u=0;u<default_fichier.length;u++){

                if(default_fichier[u].compareTo("")!=0){
                  _image[u] = File(default_fichier[u]);
                }
              }
            }
            //mise en place des fichiers par defaut selections

          }
        });
      }


      _geteEopList(_userId);
    });

  }

  _geteEopList(int userId){

    //recuperation des produits de la personne
    _localEopRepository.listEop(userId).then((response) {

      if(response.length==0 ){
        _getServerEop (null,0);
      }

    });

    //recuperation des produits de la personne
    _localScoopRepository.listScoop(userId).then((response) {

      if(response.length==0 ){
         _getServerScoop (null,0);
      }

    });;

  }

  _getServerEop (ConfigModel configModel,int typeRequest) async {


    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    EopDto eopDto=EopDto();
    eopDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getEop(eopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          FunctionUtils.saveEop(a.mesEop,_userId);

          if(typeRequest.compareTo(1)==0) {
            Navigator.of(context).pop(null);
            _getServerScoop (configModel,1);
          }

          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });


  }

  _getServerScoop (ConfigModel configModel,int typeRequest) async {


    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    ScoopDto scoopDto=ScoopDto();
    scoopDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getScoop(scoopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          FunctionUtils.saveScoop(a.mesScoop,_userId);
          if(typeRequest.compareTo(1)==0) {
            Navigator.of(context).pop(null);
          }
          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });


  }

  _savePretStart ()  {

    if(_keyScoop.compareTo("")!=0 && _listeMembers!=null && _listeMembers.length>0){
      if( (_keyEop.compareTo("")==0 )){
        _displaySnackBar(context, allTranslations.text('empty_save2'));
      }else {
        _savePretEnd();
        /*
              if(_denominationController.text.compareTo("")!=0 && _descriptionController.text.compareTo("")!=0 ){
                bool verifcationFile=true;

                if(_image!=null && _image.length>0){
                  for(int u=0;u<_image.length;u++){

                    if(_image[u]==null || _image[u].path.compareTo("")==0){
                      verifcationFile=false;
                    }
                  }
                }
                if(verifcationFile==true){
                  _savePretEnd();
                }else{
                  _displaySnackBar(context, allTranslations.text('empty_fichier'));
                }

              }else{
                _displaySnackBar(context, allTranslations.text('empty_projet'));
              }
              */
      }
    }else{
      _displaySnackBar(context, allTranslations.text('empty_save2'));
    }
  }

  _savePretEnd ()  {


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('pret_saving')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });

    List<String> allSelected =[];
    if(_image!=null && _image.length>0){
      for(int u=0;u<_image.length;u++){

        if(_image[u]!=null &&  _image[u].path.compareTo("")!=0){
          allSelected.add(_image[u].path);
        }else{
          allSelected.add("");
        }
      }
    }

    String allFichier = jsonEncode(allSelected);


    double superficie=0;

    String allUser="";
    for(int i=0;i<_listeMembers.length;i++){
      if(allUser.compareTo("")!=0)allUser+=",";
      allUser+='{"id":"${_listeMembers[i].id}","nom":"${_listeMembers[i].nom}","superficie":"${_listeMembers[i].superficie}"}';

      superficie+=double.parse(_listeMembers[i].superficie.replaceAll(",", ".")) ;
    }
    allUser='[$allUser]';



    Map<String, dynamic> info={
      'user': allUser,
      'infoEop': jsonEncode(selectedEop.toDatabase()),
    };

    LocalPretRepository localPretRepository = LocalPretRepository();
    LocalPretEntity localPretEntity = LocalPretEntity();
    if(defaultPretEntity.idPret!=null){
      localPretEntity=defaultPretEntity;
    }

    localPretEntity.idScoop=_keyScoop;
    localPretEntity.idEop=_keyEop;
    localPretEntity.superficie="$superficie";
    localPretEntity.fichierProjet=allFichier;
    localPretEntity.titleDemande=_denominationController.text;
    localPretEntity.descriptionDemande=_descriptionController.text;
    localPretEntity.contenuDemande=jsonEncode(info);


    if(defaultPretEntity.idPret!=null){

      localPretRepository.update(localPretEntity,defaultPretEntity.idPret).then((value) {
        Navigator.of(context).pop(null);
        Navigator.of(context).pop("saving");
      });

    }else {

      DateTime now = DateTime.now();
      DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm');
      String formatted = formatter.format(now);

      localPretEntity.idUser=_userId;

      localPretEntity.idPret = 0;
      localPretEntity.etatDemande=-1;
      localPretEntity.typePret=2;
      localPretEntity.idProjet="";
      localPretEntity.dateDemande=formatted;

      localPretRepository.save(localPretEntity).then((value) {
        Navigator.of(context).pop(null);
        Navigator.of(context).pop("saving");
      });

    }
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}

class TamponMemberModel {

  int id;
  String nom;
  String superficie;


  TamponMemberModel();

  TamponMemberModel.create({this.id,this.nom, this.superficie});


}