import 'dart:convert';
import 'dart:async';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/launch/presentation/widgets/home_pret_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/pret_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/autre_pret_page.dart';

class HomePretPage extends StatefulWidget {

  HomePretPage();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}



class PageState extends State<HomePretPage> with SingleTickerProviderStateMixin  {

  final _scaffoldKey = GlobalKey<ScaffoldState>();


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {

      var size = MediaQuery.of(context).size;
      final List<PretButton> titles = [
        PretButton.create(name:allTranslations.text('pret_agricole'),image:"assets/img/pret.png",widget:PretPage()),
        PretButton.create(name:allTranslations.text('autres_prets'),image:"assets/img/pret.png",widget:AutrePretPage())
      ];


      return Scaffold(
          key: _scaffoldKey,
          appBar: new AppBar(
           // centerTitle: true,
            title: Text( allTranslations.text('menu_pret'),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
            // backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),

          body:Stack(
            children: <Widget>[
              Container(
                child: SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 15.0),
                        Expanded(
                          child: GridView.count(
                            crossAxisCount: 2,
                            childAspectRatio: .85,
                            crossAxisSpacing: 20,
                            mainAxisSpacing: 20,
                            children: List.generate(titles.length, (index) {
                              PretButton pretButton = titles[index];
                              return GestureDetector(
                                onTap: () {
                                  print(pretButton.widget);
                                  if(pretButton.name == allTranslations.text('pret_agricole')) {
                                    Navigator.push(
                                      context, MaterialPageRoute(builder: (context) => pretButton.widget),
                                    );
                                  }

                                },
                                child: HomePretWidget(
                                  title: pretButton.name,
                                  pngSrc: pretButton.image,
                                ),
                              );
                            }),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),

      );
    },
  );

}

class PretButton{
  String name;
  String image;
  Widget widget;

  PretButton.create({this.name,this.image, this.widget});

}

