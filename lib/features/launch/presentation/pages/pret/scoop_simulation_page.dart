import 'dart:async';
import 'dart:convert';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/dto/scoop_dto.dart';
import 'package:agrimobile/features/common/data/dto/eop_dto.dart';
import 'package:agrimobile/features/common/data/dto/simulation_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/detailscoop_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_eop_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_eop_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_scoop_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_spret_member_page.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:find_dropdown/find_dropdown.dart';

class ScoopSimulationPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}



class PageState extends State<ScoopSimulationPage> {


  LocalEopRepository _localEopRepository = LocalEopRepository();
  LocalScoopRepository _localScoopRepository = LocalScoopRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  List<TamponMemberModel> _listeMembers=[];

  TextEditingController _superficieController=TextEditingController();

  final _formKey = GlobalKey<FormState>();

  int _userId=0;
  String _userToken="";
  String _keyEop="";
  String _keyScoop="";
  int _tamponid=0;
  String _fullname="";

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );
  List<Step> steps = List();
  int _currentStep = 0;

  ConfigModel _myConfigModel ;
  List<DetailScoopModel> _allComposantScoop;

  final eopKey = GlobalKey<FindDropdownState>();
  final scoopKey = GlobalKey<FindDropdownState>();


  bool _visible=false;
  bool _visibleError=false;

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel> (

    builder: (context, configModel, child) {

      _myConfigModel=configModel;

      steps = [
        Step(

          isActive: true,
          state: _currentStep==0?StepState.editing:StepState.complete,
          title:  Text(allTranslations.text('scoop')),
          subtitle: Text(allTranslations.text('sub_superficie')),
          content:
          Form(
              key: _formKey,
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("${allTranslations.text('selectScoop')} **",style: TextStyle(
                      color:Colors.black,
                      fontSize: 13,
                      fontWeight: FontWeight.w500
                  ),),
                  SizedBox(height: 5),
                  FindDropdown<LocalScoopEntity>(
                    key: scoopKey,
                    onFind: (String filter) => getScoop(filter),
                    searchBoxDecoration: InputDecoration(
                      hintText: allTranslations.text('search'),
                      border: OutlineInputBorder(),
                    ),
                    dropdownBuilder: (BuildContext context, LocalScoopEntity item) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.all(10),
                        height: 40,
                        decoration: BoxDecoration(
                          border: Border.all(color: Theme.of(context).dividerColor),
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white,
                        ),
                        child: Row(
                          children: [
                            Expanded(
                              child: item==null?Text(""):Text("${item.nameAssociation} (${item.cantonAssociation})"),
                            ),
                            Icon(
                              Icons.arrow_drop_down,
                              color: Colors.grey,
                              size: 20,
                            ),
                            // SizedBox(width: 5,)
                          ],
                        ),
                      );
                    },
                    dropdownItemBuilder:(BuildContext context, LocalScoopEntity item, bool isSelected) {
                      return Container(
                        decoration: !isSelected ? null : BoxDecoration(
                          color: Colors.white,
                        ),
                        child: ListTile(
                          selected: isSelected,
                          title: item==null?Text(""):Text("${item.nameAssociation} (${item.cantonAssociation})"),
                        ),
                      );
                    },
                    emptyBuilder: (BuildContext context){
                      return  Container(
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(4.0),
                            border: Border.all(
                              color: Colors.red,
                            ),
                            color: Colors.red),
                        height: 50,
                        margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                        child: Center(
                          child: Text("${allTranslations.text('liste_empty')}",
                            style: new TextStyle(
                                color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                          ),
                        ),
                      );
                    },

                    onChanged: (LocalScoopEntity data) {

                      _keyScoop="";
                      _allComposantScoop=[];
                      setState(() {
                        _visible=false;
                        _listeMembers=[];

                      });
                      if(data!=null ){
                        _keyScoop="${data.idAssociation}";
                        setState(() {
                          _visible=true;
                        });
                        List<dynamic> responseMap = jsonDecode(data.memberAssociation.toString());
                        _allComposantScoop=responseMap.map((i) => DetailScoopModel.fromMap(i)).toList();
                      }

                    },
                  ),
                  SizedBox(height: 10.0),
                  Visibility(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        _getMemberListview(),

                      ],
                    ),
                    visible: _visible,
                  )

                ],
              )
          ),
        ),
        Step(
            title:  Text(allTranslations.text('title_eop')),
            subtitle: Text(allTranslations.text('sub_eop')),
            isActive: _currentStep<1?false:true,
            state: _currentStep<1?StepState.disabled:_currentStep==1?StepState.editing:StepState.complete,
            content:
            Form(
                key: _formKey,
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("${allTranslations.text('selectEop')} **",style: TextStyle(
                        color:Colors.black,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                    ),),
                    SizedBox(height: 5),
                    FindDropdown<LocalEopEntity>(
                      key: eopKey,
                      onFind: (String filter) => getEop(filter),
                      searchBoxDecoration: InputDecoration(
                        hintText: allTranslations.text('search'),
                        border: OutlineInputBorder(),
                      ),
                      dropdownBuilder: (BuildContext context, LocalEopEntity item) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10),
                          height: 40,
                          decoration: BoxDecoration(
                            border: Border.all(color: Theme.of(context).dividerColor),
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white,
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: item==null?Text(""):Text("${item.nameSite} (${item.nameSpeculation})"),
                              ),
                              Icon(
                                Icons.arrow_drop_down,
                                color: Colors.grey,
                                size: 20,
                              ),
                              // SizedBox(width: 5,)
                            ],
                          ),
                        );
                      },
                      dropdownItemBuilder:(BuildContext context, LocalEopEntity item, bool isSelected) {
                        return Container(
                          decoration: !isSelected ? null : BoxDecoration(
                            color: Colors.white,
                          ),
                          child: ListTile(
                            selected: isSelected,
                            title: item==null?Text(""):Text("${item.nameSite} (${item.nameSpeculation})"),
                          ),
                        );
                      },
                      emptyBuilder: (BuildContext context){
                        return  Container(
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                color: Colors.red,
                              ),
                              color: Colors.red),
                          height: 50,
                          margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                          child: Center(
                            child: Text("${allTranslations.text('liste_empty')}",
                              style: new TextStyle(
                                  color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                            ),
                          ),
                        );
                      },

                      onChanged: (LocalEopEntity data) {

                        _keyEop="";
                        if(data!=null ){
                          _keyEop="${data.keyEop}";
                        }


                      },
                    ),

                    SizedBox(height: 10.0),


                  ],
                )
            )
        ),

      ];

      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 120.0)),
          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_simulation_pret2'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,

                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _getServerEop (configModel,1);
                    },
                    child: Icon(
                      Icons.refresh,
                      size: 26.0,
                    ),
                  )
              ),
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          floatingActionButton: Visibility(
            child:  FloatingActionButton(
              onPressed: () {
                _addMembre();
              },
              child: Icon(Icons.people,color: Colors.white),
              backgroundColor: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
            ),
            visible: _currentStep==0 && _visible?true:false,
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return
                  Column(children: <Widget>[
                    Expanded(
                      child: Stepper(
                        steps: steps,
                        type: StepperType.horizontal,
                        currentStep: _currentStep,
                        onStepContinue: next,
                        onStepTapped: (step) => goTo(step),
                        onStepCancel: cancel,
                        controlsBuilder:
                            (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                          return Row(
                            children: <Widget>[
                            ],
                          );
                        },
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomCenter,
                      child:
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                              color:Colors.white70,
                              style: BorderStyle.solid,
                              width: 1.0
                          ),
                          color: Colors.white70,
                        ),

                        child: Row(
                          children: [
                            Expanded(
                              child: Visibility(
                                child:
                                Container(
                                  child:
                                  MaterialButton(

                                    onPressed: () {
                                       cancel();
                                    },

                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 45.0,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color:FunctionUtils.colorFromHex(configModel.mainColor),
                                              style: BorderStyle.solid,
                                              width: 1.0
                                          ),
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(5.0)
                                      ),
                                      child: Text(
                                        allTranslations.text('previor'),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: FunctionUtils.colorFromHex(configModel.mainColor)
                                        )
                                        ,
                                      ),
                                    ),
                                    padding: EdgeInsets.all(10),
                                  ),

                                ),
                                visible: _currentStep==0?false:true,
                              ),
                            ),
                            Expanded(
                              child:
                              Container(
                                child:
                                MaterialButton(

                                  onPressed: () {
                                    next();
                                  },

                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 45.0,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                                            style: BorderStyle.solid,
                                            width: 1.0
                                        ),
                                        color: FunctionUtils.colorFromHex(configModel.mainColor) ,
                                        borderRadius: BorderRadius.circular(5.0)
                                    ),
                                    child: Text(_currentStep==1?allTranslations.text('submitbtn'):allTranslations.text('nextbtn'),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white
                                      )
                                      ,
                                    ),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),

                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ]);
              }
          )
      );
    },
  );

  void _addMembre(){

    _fullname="";
      _superficieController.text="";
      setState(() {
        _visibleError=false;
        _tamponid=0;
      });

      Widget cancelButton = FlatButton(
        child: Text(allTranslations.text('cancel'),style: new TextStyle(
            fontWeight: FontWeight.bold, fontSize: 14.0,color: Colors.grey),
        ),
        onPressed:  () {
          Navigator.of(context).pop(null);
        },
      );
      Widget continueButton = FlatButton(
        child: Text(allTranslations.text('add'),style: new TextStyle(
            fontWeight: FontWeight.bold, fontSize: 14.0,color:FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
        ),
        onPressed:  () {

          if(_tamponid.compareTo(0)!=0  && _superficieController.text.compareTo("")!=0  && _superficieController.text.compareTo("0")!=0) {

            TamponMemberModel newFriend=TamponMemberModel();
            newFriend.id= _tamponid;
            newFriend.nom= _fullname;
            newFriend.superficie= _superficieController.text;

            if ((_listeMembers.firstWhere((it) => it.id == newFriend.id,
                orElse: () => null)) != null) {

            }else{
             setState(() {
               _listeMembers.add(newFriend);
             });
              Navigator.of(context).pop(null);
            }

          }

        },
      );

      showDialog(
        context: context,
        builder: (BuildContext context) {

          return AlertDialog(
            content: StatefulBuilder(  // You need this, notice the parameters below:
              builder: (BuildContext context, StateSetter setState) {
                return SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Visibility(
                        child: Container(
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                color: Colors.red,
                              ),
                              color: Colors.red),
                          height: 40,
                          margin: EdgeInsets.only(bottom:10),
                          child: Center(
                            child: Text("${allTranslations.text('already_added')}",
                              style: new TextStyle(
                                  color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        visible: _visibleError,
                      ),
                      Text("${allTranslations.text('lastname_friend')} **",style: TextStyle(
                          color:Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.w500
                      ),),
                      SizedBox(height: 5),
                      FindDropdown<DetailScoopModel>(
                        onFind: (String filter) => _getMember(filter),
                        searchBoxDecoration: InputDecoration(
                          hintText: allTranslations.text('search'),
                          border: OutlineInputBorder(),
                        ),
                        dropdownBuilder: (BuildContext context, DetailScoopModel item) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(10),
                            height: 40,
                            decoration: BoxDecoration(
                              border: Border.all(color: Theme.of(context).dividerColor),
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white,
                            ),
                            child: Row(
                              children: [
                                Expanded(
                                  child: item==null?Text(""):Text("${item.fullName} ${item.phoneNumber}"),
                                ),
                                Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.grey,
                                  size: 20,
                                ),
                                // SizedBox(width: 5,)
                              ],
                            ),
                          );
                        },
                        dropdownItemBuilder:(BuildContext context, DetailScoopModel item, bool isSelected) {
                          return Container(
                            decoration: !isSelected ? null : BoxDecoration(
                              color: Colors.white,
                            ),
                            child: ListTile(
                              selected: isSelected,
                              title: item==null?Text(""):Text("${item.fullName} ${item.phoneNumber}"),
                            ),
                          );
                        },

                        onChanged: (DetailScoopModel data) {
                          if(data!=null) {

                            if ((_listeMembers.firstWhere((it) =>
                            it.id == data.idAgriculteur,
                                orElse: () => null)) != null) {
                              setState(() {
                                _visibleError = true;
                              });
                            } else {
                              setState(() {
                                _visibleError = false;
                              });

                              _tamponid = data.idAgriculteur;
                              _fullname = data.fullName;
                              _superficieController.text = "${data.superficie}";

                            }
                          }
                        },
                      ),
                      SizedBox(height: 10.0),


                      Text("${allTranslations.text('superficie_friend')} **",style: TextStyle(
                          color:Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.w500
                      ),),
                      SizedBox(height: 5),
                      Container(
                        child: TextFormField(
                            controller: _superficieController,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              isDense: true,
                              prefixIcon: Icon(Icons.map,size: 18.0,color: Colors.black45),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                            )
                        ),
                      ),


                    ],
                  ),
                );
              },
            ),
            actions: [
              cancelButton,
              continueButton,
            ],
          );
        },
      );

    }

  Future<List<DetailScoopModel>> _getMember(String filter) async{

      List<DetailScoopModel> suggestionList=[];

      if(_allComposantScoop!=null && _allComposantScoop.length>0) {
          String search = filter.toLowerCase();
          search.isEmpty
              ? suggestionList = _allComposantScoop //In the true case
              : suggestionList.addAll(_allComposantScoop.where(
                (element) =>
            element.fullName.toLowerCase().contains(search) ||
                element.phoneNumber.toLowerCase().contains(search),
          ));
      }

      return suggestionList ;

  }

   _getMemberListview(){

    if(_listeMembers==null || _listeMembers.length==0) {

      return Column(

        children: [

          SizedBox(height: 100,),
          Material(
            shape: CircleBorder(),
            clipBehavior: Clip.hardEdge,
            color: Colors.transparent,
            child: Image.asset('assets/img/splash.jpg',height:250),
          ),
          Center(
            child: Text("${allTranslations.text('click_toadd')}",
              textAlign: TextAlign.center,
              style: new TextStyle(
                  color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
            ),
          ),

        ],
      );
    }else{

      return  ListView.builder(
          itemCount:_listeMembers.length,
          shrinkWrap: true, // 1st add
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            TamponMemberModel compagnon=_listeMembers[position];

            return Container(

              child: Column(
                children: [
                  if(position==0) SizedBox(height: 50,),
                  if(position==0)Row(
                    children: [

                      Icon(Icons.arrow_forward, color: Colors.black54,size: 20,),
                      SizedBox(width: 3,),
                      Text("${allTranslations.text('membre_cooperative')}",
                        style: new TextStyle(
                            color: Colors.black54, fontSize: 20.0,fontWeight: FontWeight.bold),
                      ),

                    ],
                  ),
                  if(position==0) SizedBox(height: 10,),
                  MaterialButton(

                    elevation: 0.0,
                    onPressed: ()  {

                    },
                    child: Container(
                      padding: EdgeInsets.fromLTRB(10, 3, 10, 3),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        border: Border.all(
                            color:FunctionUtils.colorFromHex("DEDEDE"),
                            style: BorderStyle.solid,
                            width: 1.0
                        ),
                        borderRadius: BorderRadius.circular(5.0),
                        color: Colors.white,
                      ),
                      child: Row(
                        children: [
                          CircleAvatar(
                              radius: 15,
                              backgroundColor: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
                              child:CircleAvatar(
                                radius: 14.0,
                                backgroundColor:Colors.white,
                                child: Icon(Icons.person,
                                  color: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
                                  size: 20,
                                ),
                              )
                          ),
                          SizedBox(width: 5,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [


                                Text("${compagnon.nom} ",
                                  style: new TextStyle(
                                      color: Colors.black, fontSize: 14.0,fontWeight: FontWeight.bold),
                                ),
                                SizedBox(height: 2,),
                                Text("${compagnon.superficie} hectare(s)",
                                  textAlign: TextAlign.end,
                                  style: new TextStyle(
                                      color: FunctionUtils.colorFromHex(_myConfigModel.mainColor), fontSize: 12.0,fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              _showConfirmDeleteMember(context,compagnon);
                            },
                            child: Icon(Icons.delete, color: Colors.red,size: 15,),
                          ),

                        ],
                      ),
                    ),
                    padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  ),
                  SizedBox(height: 10,),
                ],
              ),
            );


          });
    }
  }

  _showConfirmDeleteMember(BuildContext context,TamponMemberModel compagnon) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0,color: Colors.grey),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0,color:FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        setState(() {
          _listeMembers.remove(compagnon);
        });

      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(allTranslations.text('deleteFriend_text'),style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<List<LocalEopEntity>> getEop(String filter){
    //
    return  _localEopRepository.filterEop(_userId,filter);
  }

  Future<List<LocalScoopEntity>> getScoop(String filter){

    return  _localScoopRepository.filterScoopRight(_userId,filter);
  }

  next() {


    if(_currentStep + 1 != steps.length){
      goTo(_currentStep + 1);
    }else{
      goTo(2);
    }

  }

  cancel() {
    if (_currentStep > 0) {
      goTo(_currentStep - 1);
    }else{
      Navigator.pop(context, true);
    }
  }

  goTo(int step) {

    int oldStep=_currentStep;

    if(step==0) {

      setState(() => _currentStep = step);

    }else if(step==1){

        if(_keyScoop.compareTo("")==0 || _listeMembers==null || _listeMembers.length==0){
          _displaySnackBar(context, allTranslations.text('empty_field'));
        }else{
          setState(() => _currentStep = step);
        }

    }else if(step==2){

        if( (_keyEop.compareTo("")!=0 )){
          _sendSimulation();
        }else{
          _displaySnackBar(context, allTranslations.text('empty_field'));
        }

    }

    if(oldStep==0 && step==1){

      if(_keyEop.compareTo("")!=0) {
        _localEopRepository.oneEop(_userId,"$_keyEop").then((response) {

          if (response != null) {

            if(eopKey!=null && eopKey.currentState!=null) {
              eopKey.currentState.setSelectedItem(response);
            }

          }
        });
      }

    }

    if(oldStep==1 && step==0){

      if(_keyScoop.compareTo("")!=0) {
        _localScoopRepository.oneScoop(_userId,"$_keyScoop").then((response) {

          if (response != null) {

            if(scoopKey!=null && scoopKey.currentState!=null) {
              scoopKey.currentState.setSelectedItem(response);
            }

          }
        });
      }

    }


  }

  _sendSimulation () async {

    String allUser="";
    for(int i=0;i<_listeMembers.length;i++){
      if(allUser.compareTo("")!=0)allUser+=",";
      allUser+='{"id":"${_listeMembers[i].id}","nom":"${_listeMembers[i].nom}","superficie":"${_listeMembers[i].superficie}"}';
    }
    allUser='[$allUser]';



    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('simulation_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });


    SimulationDto simulationDto=SimulationDto();

    simulationDto.userToken=_userToken;
    simulationDto.keyEop=_keyEop;
    simulationDto.keyScoop=_keyScoop;
    simulationDto.jsonScoop=allUser;


    Api api = ApiRepository();
    api.sendSsimulation(simulationDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          Navigator.of(context).pop(null);
          if(a.status.compareTo("000")==0){

            if(a.status.compareTo("000")==0){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => DetailSmpretPage(a.simulation,a.message,"","")),
              );
            }else{
              _displaySnackBar(context, a.message);
            }

            return true;
          }else{
             _displaySnackBar(context, a.message);
          }

          return true;
        });
      } else {
        _displaySnackBar(context, allTranslations.text('error_process'));
         Navigator.of(context).pop(null);
        return false;
      }
    });


  }

  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userId=value.id;
        _userToken=value.token;
      });
      _geteEopList(_userId);
    });

  }

  _geteEopList(int userId){

    //recuperation des produits de la personne
    _localEopRepository.listEop(userId).then((response) {

      if(response.length==0 ){
        _getServerEop (null,0);
      }

    });

    //recuperation des produits de la personne
    _localScoopRepository.listScoop(userId).then((response) {

      if(response.length==0 ){
         _getServerScoop (null,0);
      }

    });;

  }

  _getServerEop (ConfigModel configModel,int typeRequest) async {


    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    EopDto eopDto=EopDto();
    eopDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getEop(eopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          FunctionUtils.saveEop(a.mesEop,_userId);

          if(typeRequest.compareTo(1)==0) {
            Navigator.of(context).pop(null);
            _getServerScoop (configModel,1);
          }

          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });


  }

  _getServerScoop (ConfigModel configModel,int typeRequest) async {


    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    ScoopDto scoopDto=ScoopDto();
    scoopDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getScoop(scoopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          FunctionUtils.saveScoop(a.mesScoop,_userId);
          if(typeRequest.compareTo(1)==0) {
            Navigator.of(context).pop(null);
          }
          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });


  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}

class TamponMemberModel {

  int id;
  String nom;
  String superficie;


  TamponMemberModel();

  TamponMemberModel.create({this.id,this.nom, this.superficie});


}