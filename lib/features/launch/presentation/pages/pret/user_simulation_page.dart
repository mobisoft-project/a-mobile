
import 'dart:async';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/dto/eop_dto.dart';
import 'package:agrimobile/features/common/data/dto/simulation_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_eop_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_eop_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_upret_page.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:find_dropdown/find_dropdown.dart';

class UserSimulationPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}



class PageState extends State<UserSimulationPage> {


  LocalEopRepository _localEopRepository = LocalEopRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();

  TextEditingController _superficieController=TextEditingController();

  final _formKey = GlobalKey<FormState>();

  int _userId=0;
  String _userToken="";
  String _keyEop="";

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );
  List<Step> steps = List();
  int _currentStep = 0;

  ConfigModel _myConfigModel ;

  final eopKey = GlobalKey<FindDropdownState>();

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(

    builder: (context, configModel, child) {

      _myConfigModel=configModel;

      steps = [
        Step(
            title:  Text(allTranslations.text('title_eop')),
            subtitle: Text(allTranslations.text('sub_eop')),
            isActive: true,
            state: _currentStep==0?StepState.editing:StepState.complete,
            content:
            Form(
                key: _formKey,
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("${allTranslations.text('selectEop')} **",style: TextStyle(
                        color:Colors.black,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                    ),),
                    SizedBox(height: 5),
                    FindDropdown<LocalEopEntity>(
                      key: eopKey,
                      onFind: (String filter) => getEop(filter),
                      searchBoxDecoration: InputDecoration(
                        hintText: allTranslations.text('search'),
                        border: OutlineInputBorder(),
                      ),
                      dropdownBuilder: (BuildContext context, LocalEopEntity item) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(10),
                          height: 40,
                          decoration: BoxDecoration(
                            border: Border.all(color: Theme.of(context).dividerColor),
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white,
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: item==null?Text(""):Text("${item.nameSite} (${item.nameSpeculation})"),
                              ),
                              Icon(
                                Icons.arrow_drop_down,
                                color: Colors.grey,
                                size: 20,
                              ),
                              // SizedBox(width: 5,)
                            ],
                          ),
                        );
                      },
                      dropdownItemBuilder:(BuildContext context, LocalEopEntity item, bool isSelected) {
                        return Container(
                          decoration: !isSelected ? null : BoxDecoration(
                            color: Colors.white,
                          ),
                          child: ListTile(
                            selected: isSelected,
                            title: item==null?Text(""):Text("${item.nameSite} (${item.nameSpeculation})"),
                          ),
                        );
                      },
                      emptyBuilder: (BuildContext context){
                        return  Container(
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                color: Colors.red,
                              ),
                              color: Colors.red),
                          height: 50,
                          margin: EdgeInsets.only(top:50,bottom:((MediaQuery. of(context). size. height)/2)-30),
                          child: Center(
                            child: Text("${allTranslations.text('liste_empty')}",
                              style: new TextStyle(
                                  color: Colors.white70, fontSize: 14.0,fontWeight: FontWeight.bold),
                            ),
                          ),
                        );
                      },

                      onChanged: (LocalEopEntity data) {

                        _keyEop="";
                        if(data!=null ){
                          _keyEop="${data.keyEop}";
                        }


                      },
                    ),

                    SizedBox(height: 10.0),


                  ],
                )
            )


        ),
        Step(
          isActive: _currentStep<1?false:true,
          state: _currentStep<1?StepState.disabled:_currentStep==1?StepState.editing:StepState.complete,
          title:  Text(allTranslations.text('info')),
          subtitle: Text(allTranslations.text('sub_superficie')),
          content:
          Form(
              key: _formKey,
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("${allTranslations.text('superficie')} **",style: TextStyle(
                      color:Colors.black,
                      fontSize: 13,
                      fontWeight: FontWeight.w500
                  ),),
                  SizedBox(height: 5),
                  Container(
                    color: Colors.white,
                    child: TextFormField(
                        controller: _superficieController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          isDense: true,
                          prefixIcon: Icon(Icons.map,size: 18.0,color: Colors.black45),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        )
                    ),
                  ),

                ],
              )
          ),
        ),
      ];

      return Scaffold(
          key: widget._scaffoldKey,
          bottomSheet: Padding(padding: EdgeInsets.only(bottom: 80.0)),

          appBar: new AppBar(
            title:  Align(
              child: Container(
                child: Text(allTranslations.text('menu_simulation_pret1'),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,

                  ),
                ),
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _getServerEop (configModel,1);
                    },
                    child: Icon(
                      Icons.refresh,
                      size: 26.0,
                    ),
                  )
              ),
             
            ],
            backgroundColor:  FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return
                  Column(children: <Widget>[
                    Expanded(
                      child: Stepper(
                        steps: steps,
                        type: StepperType.horizontal,
                        currentStep: _currentStep,
                        onStepContinue: next,
                        onStepTapped: (step) => goTo(step),
                        onStepCancel: cancel,
                        controlsBuilder:
                            (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                          return Row(
                            children: <Widget>[
                            ],
                          );
                        },
                      ),
                    ),
                    Container(
                      alignment: Alignment.bottomCenter,
                      child:
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                              color:Colors.white70,
                              style: BorderStyle.solid,
                              width: 1.0
                          ),
                          color: Colors.white70,
                        ),

                        child: Row(
                          children: [
                            Expanded(
                              child: Visibility(
                                child:
                                Container(
                                  child:
                                  MaterialButton(

                                    onPressed: () {
                                       cancel();
                                    },

                                    child: Container(
                                      alignment: Alignment.center,
                                      height: 45.0,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color:FunctionUtils.colorFromHex(configModel.mainColor),
                                              style: BorderStyle.solid,
                                              width: 1.0
                                          ),
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(5.0)
                                      ),
                                      child: Text(
                                        allTranslations.text('previor'),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: FunctionUtils.colorFromHex(configModel.mainColor)
                                        )
                                        ,
                                      ),
                                    ),
                                    padding: EdgeInsets.all(10),
                                  ),

                                ),
                                visible: _currentStep==0?false:true,
                              ),
                            ),
                            Expanded(
                              child:
                              Container(
                                child:
                                MaterialButton(

                                  onPressed: () {
                                    next();
                                  },

                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 45.0,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                                            style: BorderStyle.solid,
                                            width: 1.0
                                        ),
                                        color: FunctionUtils.colorFromHex(configModel.mainColor) ,
                                        borderRadius: BorderRadius.circular(5.0)
                                    ),
                                    child: Text(_currentStep==1?allTranslations.text('submitbtn'):allTranslations.text('nextbtn'),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white
                                      )
                                      ,
                                    ),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),

                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ]);
              }
          )
      );
    },
  );

  Future<List<LocalEopEntity>> getEop(String filter){
    //
    return  _localEopRepository.filterEop(_userId,filter);
  }

  next() {


    if(_currentStep + 1 != steps.length){
      goTo(_currentStep + 1);
    }else{
      goTo(2);
    }

  }

  cancel() {
    if (_currentStep > 0) {
      goTo(_currentStep - 1);
    }else{
      Navigator.pop(context, true);
    }
  }

  goTo(int step) {

    int oldStep=_currentStep;

    if(step==0) {

      setState(() => _currentStep = step);

    }else if(step==1){
      //verification si toute les informations sur la voiture sont ok
      if( (_keyEop.compareTo("")!=0 )){
        setState(() => _currentStep = step);
      }else{
        _displaySnackBar(context, allTranslations.text('empty_field'));
      }

    }else if(step==2){
      //verifier si la superficie est definie
      if(_superficieController.text.compareTo("")==0 || _superficieController.text.compareTo("0")==0){
          _displaySnackBar(context, allTranslations.text('empty_field'));
      }else{
        _sendSimulation();
      }

    }

    if(oldStep==1 && step==0){

      if(_keyEop.compareTo("")!=0) {
        _localEopRepository.oneEop(_userId,"$_keyEop").then((response) {

          if (response != null) {

            if(eopKey!=null && eopKey.currentState!=null) {
              eopKey.currentState.setSelectedItem(response);
            }

          }
        });
      }

    }

  }

  _sendSimulation () async {


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('simulation_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });


    SimulationDto simulationDto=SimulationDto();
    simulationDto.userToken=_userToken;
    simulationDto.keyEop=_keyEop;
    simulationDto.superficie=_superficieController.text;


    Api api = ApiRepository();
    api.sendUsimulation(simulationDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          Navigator.of(context).pop(null);
          if(a.status.compareTo("000")==0){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DetailUpretPage(a.simulation[0])),
            );
          }else{

            _displaySnackBar(context, a.message);
          }

          return true;
        });
      } else {
        _displaySnackBar(context, allTranslations.text('error_process'));
         Navigator.of(context).pop(null);
        return false;
      }
    });


  }

  _getUserInformation(){

    //recuperation de l'id de la personne
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userId=value.id;
        _userToken=value.token;
      });
      _geteEopList(_userId);
    });

  }

  _geteEopList(int userId){

    //recuperation des produits de la personne
    _localEopRepository.listEop(userId).then((response) {

      if(response.length==0 ){
        _getServerEop (null,0);
      }

    });

  }

  _getServerEop (ConfigModel configModel,int typeRequest) async {


    if(typeRequest.compareTo(1)==0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    EopDto eopDto=EopDto();
    eopDto.userToken=_userToken;


    Api api = ApiRepository();
    api.getEop(eopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          FunctionUtils.saveEop(a.mesEop,_userId);

          if(typeRequest.compareTo(1)==0) {
            Navigator.of(context).pop(null);
          }

          return true;
        });
      } else {
        if(typeRequest.compareTo(1)==0) {
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });


  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

}
