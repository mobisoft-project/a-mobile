import 'dart:async';
import 'dart:convert';
 import 'dart:io';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/dto/eop_dto.dart';
import 'package:agrimobile/features/common/data/dto/autre_pret_dto.dart';
import 'package:agrimobile/features/common/data/dto/liste_type_pret_banque_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/liste_type_pret_banque_response_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_eop_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_pret_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_eop_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_type_pret_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_upret_member_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/pret/detail_upret_page.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:agrimobile/features/common/domain/entities/local_type_pret_entity.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:dio/dio.dart';

class AutrePretSuscribePage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  AutrePretSuscribePage();
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}

class PageState extends State<AutrePretSuscribePage> {
  PageState();

  LocalEopRepository _localEopRepository = LocalEopRepository();
  LocalTypePretRepository _localTypePretRepository = LocalTypePretRepository();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  List<TypePretBanqueModel> _allTypePretBanqueModel;
  TextEditingController _montantController = TextEditingController();
  TypePretBanqueModel _selectedTypePretBanque;

  LocalEopEntity selectedEop;
  LocalTypePretEntity selectedTypePret;
  final _formKey = GlobalKey<FormState>();

  int _userId = 0;
  String _userToken = "";
  String _keyEop = "";
  String _keyTypePret = "";
  int _idTypePret = 0;

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );
  List<Step> steps = List();
  int _currentStep = 0;
  bool _visible = false;

  ConfigModel _myConfigModel;

  final eopKey = GlobalKey<FindDropdownState>();
  final typePretKey = GlobalKey<FindDropdownState>();
  List<String> _allfile = [];
  List<File> _image;

  TextEditingController _denominationController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
        builder: (context, configModel, child) {
          _myConfigModel = configModel;

          steps = [
            Step(
                title: Text(allTranslations.text('title_types_prets'),
                    style: TextStyle(
                      fontSize: 12,
                    )),
                subtitle: Text(allTranslations.text('Les types de prets'),
                    style: TextStyle(
                      fontSize: 10,
                    )),
                isActive: true,
                state:
                    _currentStep == 0 ? StepState.editing : StepState.complete,
                content: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${allTranslations.text('selectTypePret')} **",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 13,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox(height: 5),
                        FindDropdown<LocalTypePretEntity>(
                          key: typePretKey,
                          onFind: (String filter) => getTypePrets(filter),
                          searchBoxDecoration: InputDecoration(
                            hintText: allTranslations.text('search'),
                            border: OutlineInputBorder(),
                          ),
                          dropdownBuilder:
                              (BuildContext context, LocalTypePretEntity item) {
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.all(10),
                              height: 40,
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Theme.of(context).dividerColor),
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white,
                              ),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: item == null
                                        ? Text("")
                                        : Text("${item.libelle}"),
                                  ),
                                  Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.grey,
                                    size: 20,
                                  ),
                                  // SizedBox(width: 5,)
                                ],
                              ),
                            );
                          },
                          dropdownItemBuilder: (BuildContext context,
                              LocalTypePretEntity item, bool isSelected) {
                            return Container(
                              decoration: !isSelected
                                  ? null
                                  : BoxDecoration(
                                      color: Colors.white,
                                    ),
                              child: ListTile(
                                selected: isSelected,
                                title: item == null
                                    ? Text("")
                                    : Text("${item.libelle}"),
                              ),
                            );
                          },
                          emptyBuilder: (BuildContext context) {
                            return Container(
                              decoration: new BoxDecoration(
                                  borderRadius: BorderRadius.circular(4.0),
                                  border: Border.all(
                                    color: Colors.red,
                                  ),
                                  color: Colors.red),
                              height: 50,
                              margin: EdgeInsets.only(
                                  top: 50,
                                  bottom:
                                      ((MediaQuery.of(context).size.height) /
                                              2) -
                                          30),
                              child: Center(
                                child: Text(
                                  "${allTranslations.text('liste_empty')}",
                                  style: new TextStyle(
                                      color: Colors.white70,
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            );
                          },
                          onChanged: (LocalTypePretEntity data) {
                            _allfile = [];
                            _keyTypePret = "";
                            _idTypePret = 0;
                            selectedTypePret = data;

                            print(data.toDatabase());
                            if (data != null) {
                              setState(() {
                                _keyTypePret = "${data.typePretKey}";
                                _idTypePret = data.apiId;
                                _visible = false;
                                print(_keyTypePret);
                              });
                            }
                            _image = [];
                            _image = new List(_allfile.length);
                          },
                        ),
                        SizedBox(height: 40.0),
                        Text(
                          "${allTranslations.text('montant')} **",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 13,
                              fontWeight: FontWeight.w500),
                        ),
                        SizedBox(height: 5),
                        Container(
                          child: TextFormField(
                              keyboardType: TextInputType.number,
                              controller: _montantController,
                              decoration: InputDecoration(
                                isDense: true,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                              ),
                            onChanged: (String data){
                                setState(() {
                                     _visible = false;
                                });
                            },
                          ),
                        ),
                      ],
                    ))),
            Step(
              isActive: _currentStep < 1 ? false : true,
              state: _currentStep < 1
                  ? StepState.disabled
                  : _currentStep == 1
                      ? StepState.editing
                      : StepState.complete,
              title: Text(allTranslations.text('Offres'),
                  style: TextStyle(
                    fontSize: 12,
                  )),
              subtitle: Text(allTranslations.text('les_prets_banquaires'),
                  style: TextStyle(
                    fontSize: 10,
                  )),
              content: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _getBanquesListview(configModel),
                    ],
                  )),
            ),
            Step(
                title: Text(allTranslations.text('title_file'),
                    style: TextStyle(
                      fontSize: 12,
                    )),
                subtitle: Text(
                  allTranslations.text('subtitle_file'),
                  style: TextStyle(
                    fontSize: 10,
                  ),
                ),
                isActive: _currentStep < 2 ? false : true,
                state: _currentStep < 2
                    ? StepState.disabled
                    : _currentStep == 2
                        ? StepState.editing
                        : StepState.complete,
                content: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: _getBanqueFile(),
                    ))),
          ];

          return Scaffold(
              key: widget._scaffoldKey,
              bottomSheet: Padding(padding: EdgeInsets.only(bottom: 80.0)),
              appBar: new AppBar(
                title: Align(
                  child: Container(
                    child: Text(
                      allTranslations.text('demande autres prets'),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                ),

                backgroundColor:
                    FunctionUtils.colorFromHex(configModel.mainColor),
                elevation: 0.0,
              ),
              body: Builder(builder: (BuildContext myContext) {
                return Column(children: <Widget>[
                  Expanded(
                    child: Stepper(
                      steps: steps,
                      type: StepperType.horizontal,
                      currentStep: _currentStep,
                      onStepContinue: next,
                      onStepTapped: (step) => goTo(step),
                      onStepCancel: cancel,
                      controlsBuilder: (BuildContext context,
                          {VoidCallback onStepContinue,
                          VoidCallback onStepCancel}) {
                        return Row(
                          children: <Widget>[],
                        );
                      },
                    ),
                  ),
                  Container(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: Colors.white70,
                            style: BorderStyle.solid,
                            width: 1.0),
                        color: Colors.white70,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Visibility(
                              child: Container(
                                child: MaterialButton(
                                  onPressed: () {
                                    cancel();
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 45.0,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: FunctionUtils.colorFromHex(
                                                configModel.mainColor),
                                            style: BorderStyle.solid,
                                            width: 1.0),
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(5.0)),
                                    child: Text(
                                      allTranslations.text('previor'),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: FunctionUtils.colorFromHex(
                                              configModel.mainColor)),
                                    ),
                                  ),
                                  padding: EdgeInsets.all(10),
                                ),
                              ),
                              visible: _currentStep == 0 ? false : true,
                            ),
                          ),
                          Expanded(
                            child: Container(
                              child: MaterialButton(
                                onPressed: () {
                                  next();
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  height: 45.0,
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: FunctionUtils.colorFromHex(
                                              configModel.mainColor),
                                          style: BorderStyle.solid,
                                          width: 1.0),
                                      color: FunctionUtils.colorFromHex(
                                          configModel.mainColor),
                                      borderRadius: BorderRadius.circular(5.0)),
                                  child: Text(
                                    _currentStep == 2
                                        ? allTranslations.text('submitbtn')
                                        : allTranslations.text('nextbtn'),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                ),
                                padding: EdgeInsets.all(10),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ]);
              }));
        },
      );

  _getBanquesListview(ConfigModel configModel) {
    if (_visible == false) {
      return Column(
        children: [
          SizedBox(
            height: 100,
          ),
          Image.asset(
            'assets/img/loading.gif',
            width: 150,
            height: 150,
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              SizedBox(
                width: 30,
              ),
              Expanded(
                child: Center(
                  child: Text(
                    "${allTranslations.text('pgetting_processing')}",
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                        color: Colors.black,
                        fontSize: 14.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(
                width: 30,
              ),
            ],
          )
        ],
      );
    }else{
      if (_allTypePretBanqueModel != null && _allTypePretBanqueModel.length > 0) {
        return ListView.builder(
            //padding: EdgeInsets.all( 20.0),
            itemCount: _allTypePretBanqueModel.length,
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            itemBuilder: (BuildContext context, int position) {
              TypePretBanqueModel infoTypePretBanque =
                  _allTypePretBanqueModel[position];

              Widget Contenair = Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "${allTranslations.text('date_operation')} ${infoTypePretBanque.nom}",
                    style: TextStyle(
                      color: infoTypePretBanque.isSelected
                          ? Colors.white
                          : Colors.black,
                      //fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                    ),
                  ),
                  SizedBox(height: 2.5),
                  Text(
                    "${allTranslations.text('duree')} ${infoTypePretBanque.duree}",
                    style: TextStyle(
                      color: infoTypePretBanque.isSelected
                          ? Colors.white
                          : Colors.black,
                      //fontWeight: FontWeight.w500,
                      fontSize: 14.0,
                    ),
                  ),
                  SizedBox(height: 2.5),
                  Text(
                    "${allTranslations.text('taux_remboursement')} ${infoTypePretBanque.taux}",
                    style: TextStyle(
                      color: infoTypePretBanque.isSelected
                          ? Colors.white
                          : Colors.black,
                      fontSize: 14.0,
                    ),
                  ),
                  SizedBox(height: 2.5),
                  Text(
                    "${allTranslations.text('montant_demande')} ${infoTypePretBanque.montantDemande} ${configModel.devise}",
                    style: TextStyle(
                      color: infoTypePretBanque.isSelected
                          ? Colors.white
                          : Colors.black,
                      //fontWeight: FontWeight.w500,
                      fontSize: 15.0,
                    ),
                  ),
                  SizedBox(height: 2.5),
                  Text(
                    "${allTranslations.text('montant_interet')} ${infoTypePretBanque.montantInteret} ${configModel.devise} ",
                    style: TextStyle(
                      color: infoTypePretBanque.isSelected
                          ? Colors.white
                          : Colors.black,
                      // fontWeight: FontWeight.w500,
                      fontSize: 14.0,
                    ),
                  ),
                  SizedBox(height: 2.5),
                  Text(
                    "${allTranslations.text('montant_a_payer')} ${infoTypePretBanque.montantAPayer} ${configModel.devise} ",
                    style: TextStyle(
                      color: infoTypePretBanque.isSelected
                          ? Colors.white
                          : Colors.black,
                      // fontWeight: FontWeight.w500,
                      fontSize: 14.0,
                    ),
                  ),
                ],
              );

              return Container(
                  decoration: BoxDecoration(
                      color: infoTypePretBanque.isSelected
                          ? FunctionUtils.colorFromHex(configModel.mainColor)
                          : Colors.white,
                      boxShadow: [
                        BoxShadow(color: Colors.black12, blurRadius: 5)
                      ]),
                  margin: EdgeInsets.only(bottom: 10),
                  padding: EdgeInsets.only(left: 20, top: 10, bottom: 10),
                  height: 120,
                  child: GestureDetector(
                      onTap: () {
                        setState(() {
                          _allfile = [];
                          //reset all banques
                          _allTypePretBanqueModel.forEach((element) => element.isSelected = false);

                          //select banque
                          infoTypePretBanque.isSelected = true;
                          _selectedTypePretBanque = infoTypePretBanque;

                          //check if have documents
                          if (infoTypePretBanque.documents != null &&
                              infoTypePretBanque.documents.compareTo("") != 0) {
                            _allfile = infoTypePretBanque.documents.split(",");
                          } else {
                            _allfile = [];
                          }
                          _image = [];
                          _image = new List(_allfile.length);
                        });
                      },
                      child: infoTypePretBanque.isSelected
                          ? Banner(
                              message: allTranslations.text('select_banque'),
                              location: BannerLocation.topEnd,
                              color: FunctionUtils.colorFromHex(
                                  configModel.bioColor),
                              child: Contenair,
                            )
                          : Contenair));
            });
      } else {
        return Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Material(
              shape: CircleBorder(),
              clipBehavior: Clip.hardEdge,
              color: Colors.transparent,
              child: Image.asset('assets/img/splash.jpg', height: 250),
            ),
            Center(
              child: Text(
                "${allTranslations.text('liste_empty')}",
                style: new TextStyle(
                    color: Colors.black,
                    fontSize: 14.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        );
      }
    }
  }

  _getBanqueFile() {
    Widget defaultContent= Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        Text("${allTranslations.text('denomination_projet')} **",style: TextStyle(
            color:Colors.black,
            fontSize: 13,
            fontWeight: FontWeight.w500
        ),),
        SizedBox(height: 5),
        Container(
          child: TextFormField(
              controller: _denominationController,
              decoration: InputDecoration(
                isDense: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              )
          ),
        ),
        SizedBox(height: 20),
        Text("${allTranslations.text('description_projet')} **",style: TextStyle(
            color:Colors.black,
            fontSize: 13,
            fontWeight: FontWeight.w500
        ),),
        SizedBox(height: 5),
        Container(
          child: TextFormField(
              controller: _descriptionController,
              textInputAction: TextInputAction.newline,
              keyboardType: TextInputType.multiline,
              minLines: 4,
              maxLines: null,
              decoration: InputDecoration(
                isDense: true,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              )
          ),
        ),

      ],
    );

    List<Widget> allWidget =[];
    allWidget.add(defaultContent);

    if (_allfile != null && _allfile.length > 0) {
      allWidget.add(SizedBox(height: 20));
      allWidget.add(
        Text(
          "${allTranslations.text('add_document')}",
          style: TextStyle(
              color: Colors.red, fontSize: 15, fontWeight: FontWeight.bold),
        ),
      );
      for (int u = 0; u < _allfile.length; u++) {
        String filename = "";
        if (_image[u] != null) {
          List<String> pathInfo = _image[u].path.split("/");
          filename = pathInfo[pathInfo.length - 1];
        }

        Widget fileContent = Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 20),
            Text(
              "${_allfile[u]} ",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 13,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(height: 5),
            RawMaterialButton(
              onPressed: () {
                //_showChoiceDialog(context,configModel);
                _openGallery(u);
              },
              child: new Icon(
                Icons.insert_drive_file,
                color: FunctionUtils.colorFromHex(_myConfigModel.mainColor),
                size: 25.0,
              ),
              shape: new CircleBorder(),
              elevation: 2.0,
              fillColor: Colors.white,
              padding: const EdgeInsets.all(10.0),
            ),
            if (filename.compareTo("") != 0)
              Text(
                "${filename} ",
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 12,
                    fontWeight: FontWeight.w500),
              ),
          ],
        );

        allWidget.add(fileContent);
      }
    }
    return allWidget;
  }

  Future _openGallery(int position) async {
    FilePickerCross myFile = await FilePickerCross.importFromStorage(
      type: FileTypeCross
          .any, // Available: `any`, `audio`, `image`, `video`, `custom`. Note: not available using FDE
      //fileExtension: 'txt, md, pdf'     // Only if FileTypeCross.custom . May be any file extension like `dot`, `ppt,pptx,odp`
    );

    if (myFile != null) {
      setState(() {
        _image[position] = File(myFile.path);
      });
    }
  }

  Future<List<LocalTypePretEntity>> getTypePrets(String filter) {
    return _localTypePretRepository.filterTypePret(filter);
  }

  getBanques(int _idTypePret) async {
    _allTypePretBanqueModel = [];
    ListeTypePretBanqueDto listeTransporterDto = ListeTypePretBanqueDto();
    listeTransporterDto.accessToken = DataConstantesUtils.API_TOKEN;
    listeTransporterDto.typePretApiId = _idTypePret;
    listeTransporterDto.montant = int.parse(_montantController.text);

    FormData formData = new FormData.fromMap(listeTransporterDto.toMap());

    print(listeTransporterDto.toMap());
    print(formData.fields);

    Dio dio = new Dio();

    return await dio
        .post(DataConstantesUtils.LISTE_TYPE_PRET_BANQUE_SERVER_URL,
            data: formData)
        .then((response) {
      var data = response.toString();
      print(data);

      Map responseMap = jsonDecode(data);
      ListeTypePretBanqueResponseModel infoModel =
          ListeTypePretBanqueResponseModel.fromMap(responseMap);

      if (infoModel.status.compareTo("000") == 0) {
        setState(() {
          _allTypePretBanqueModel = infoModel.information;
          _selectedTypePretBanque=null;
          Timer(
              Duration(seconds: 1),() {
                setState(() {
                  _visible = true;
                });
            });
        });
      } else {
        _visible = true;
      }
    }).catchError((error) {
      print(error.toString());
    });
  }

  next() {
    if (_currentStep + 1 != steps.length) {
      goTo(_currentStep + 1);
    } else {
      goTo(3);
    }
  }

  cancel() {
    if (_currentStep > 0) {
      goTo(_currentStep - 1);
    } else {
      Navigator.pop(context, true);
    }
  }

  goTo(int step) {
    print(step);

    int oldStep = _currentStep;

    if (step == 0) {
      setState(() => _currentStep = step);
    } else if (step == 1) {
      //verification si le type de pret est chosi
      if ((_keyTypePret.compareTo("") != 0)) {
        if (_montantController.text.compareTo("") != 0 &&
            _montantController.text.compareTo("") != 0) {
          setState(() {
            _currentStep = step;
          });
          if(_visible==false)getBanques(_idTypePret);
        } else {
          _displaySnackBar(context, allTranslations.text('empty_montant'));
        }
      } else {
        _displaySnackBar(context, allTranslations.text('empty_keypret'));
      }
    } else if (step == 2) {

      //verifier si la banque est choisie
      if (_selectedTypePretBanque == null) {
        _displaySnackBar(context, allTranslations.text('empty_field'));
      } else {
        setState(() => _currentStep = step);
      }
    } else if (step == 3) {
      if(_denominationController.text.compareTo("")!=0 && _descriptionController.text.compareTo("")!=0 ){
        bool verifcationFile = true;

        if (_image != null && _image.length > 0) {
          for (int u = 0; u < _image.length; u++) {
            if (_image[u] == null || _image[u].path.compareTo("") == 0) {
              verifcationFile = false;
            }
          }
        }
        if (verifcationFile == true) {
          _sendPret();
        } else {
          _displaySnackBar(context, allTranslations.text('empty_fichier'));
        }
      }else{
        _displaySnackBar(context, allTranslations.text('empty_projet'));
      }
    }

    if (oldStep == 1 && step == 0) {
      if (_keyTypePret.compareTo("") != 0) {
        _localTypePretRepository.oneTypePret("$_keyTypePret").then((response) {
          if (response != null) {
            if (typePretKey != null && typePretKey.currentState != null) {
              typePretKey.currentState.setSelectedItem(response);
            }
          }
        });
      }
    } else if (oldStep == 2 && step == 1) {
      _checkIfBanqueSelected();
    }
  }

  _checkIfBanqueSelected(){
    print('small conditions');
    if (_selectedTypePretBanque != null) {
      if (_allTypePretBanqueModel != null && _allTypePretBanqueModel.length > 0) {
        _allTypePretBanqueModel .forEach((element) => element.isSelected = false);

        _allTypePretBanqueModel.forEach((element) => {
          // print(_selectedTypePretBanque.toMap())
          if (_selectedTypePretBanque.apiId == element.apiId)
            setState(() {
              element.isSelected = true;
            })
        });
      }
    }
  }

  _sendPret() async {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('pret_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(
                      FunctionUtils.colorFromHex(_myConfigModel.mainColor)),
                )
              ],
            ),
          );
        });

    AutrePretDto autrePretDto = AutrePretDto();

    autrePretDto.userToken = _userToken;
    autrePretDto.idPretBanque = "${_selectedTypePretBanque.apiId}";
    autrePretDto.titre = _denominationController.text;
    autrePretDto.description = _descriptionController.text;
    autrePretDto.montant = _montantController.text;
    autrePretDto.fileBanque = _image;

    Api api = ApiRepository();
    api.sendAutrePret(autrePretDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if (a.status.compareTo("000") == 0) {

            //print
          } else {
            Navigator.of(context).pop(null);
            _displaySnackBar(context, a.message);
          }

          return true;
        });
      } else {
        _displaySnackBar(context, allTranslations.text('error_process'));
        Navigator.of(context).pop(null);
        return false;
      }
    });
  }

  _getUserInformation() {}

  _geteEopList(int userId) {
    //recuperation des produits de la personne
    _localEopRepository.listEop(userId).then((response) {
      if (response.length == 0) {
        _getServerEop(null, 0);
      }
    });
  }

  _savePretStart() {
    if ((_keyEop.compareTo("") != 0)) {
      if (_montantController.text.compareTo("") == 0 ||
          _montantController.text.compareTo("0") == 0) {
        _displaySnackBar(context, allTranslations.text('empty_save1'));
      } else {
        _savePretEnd();
        /*
              if(_denominationController.text.compareTo("")!=0 && _descriptionController.text.compareTo("")!=0 ){
                bool verifcationFile=true;

                if(_image!=null && _image.length>0){
                  for(int u=0;u<_image.length;u++){

                    if(_image[u]==null || _image[u].path.compareTo("")==0){
                      verifcationFile=false;
                    }
                  }
                }
                if(verifcationFile==true){
                  _savePretEnd();
                }else{
                  _displaySnackBar(context, allTranslations.text('empty_fichier'));
                }

              }else{
                _displaySnackBar(context, allTranslations.text('empty_projet'));
              }
              */
      }
    } else {
      _displaySnackBar(context, allTranslations.text('empty_save1'));
    }
  }

  _savePretEnd() {}

  _getServerEop(ConfigModel configModel, int typeRequest) async {
    if (typeRequest.compareTo(1) == 0) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('refresh_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(
                        FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });
    }

    EopDto eopDto = EopDto();
    eopDto.userToken = _userToken;

    Api api = ApiRepository();
    api.getEop(eopDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          FunctionUtils.saveEop(a.mesEop, _userId);

          if (typeRequest.compareTo(1) == 0) {
            Navigator.of(context).pop(null);
          }

          return true;
        });
      } else {
        if (typeRequest.compareTo(1) == 0) {
          Navigator.of(context).pop(null);
        }
        return false;
      }
    });
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
