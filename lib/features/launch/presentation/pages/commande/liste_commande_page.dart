import 'dart:convert';
import 'dart:async';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/commande_response_message_model.dart';
import 'package:agrimobile/features/common/data/models/commande_detail_model.dart';
import 'package:agrimobile/features/common/data/models/commande_model.dart';
import 'package:agrimobile/features/common/data/models/commande_user_model.dart';
import 'package:agrimobile/features/common/data/dto/liste_commande_dto.dart';
import 'package:agrimobile/features/common/domain/entities/local_commande_entity.dart';
import 'package:agrimobile/features/launch/presentation/widgets/commande_widget.dart';
import 'package:agrimobile/features/common/domain/repositories/local_commande_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:html/dom.dart' as dom;
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';

class ListeCommandePage extends StatefulWidget {
  String type;

  ListeCommandePage(this.type);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}



class PageState extends State<ListeCommandePage> with SingleTickerProviderStateMixin  {
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalCommandeRepository _localCommandeRepository = LocalCommandeRepository();
  List<LocalCommandeEntity> _allLocalCommandeEntitySend=[];
  List<LocalCommandeEntity> _allLocalCommandeEntityReceive=[];

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TabController _tabController;
  List<Tab> _myTabs;

  List<String> areaListData = <String>[];

  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );



  @override
  void initState() {
    super.initState();

    _tabController = TabController(vsync: this, length: 2);

    _getCommandesFromDatabase("1");
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      _myTabs = <Tab>[
        new Tab(
            text: "${allTranslations.text('menu_mes_commandes')}"
        ),
       new Tab(text: "${allTranslations.text('menu_commandes_recues')}"),
      ];

      return Scaffold(
          key: _scaffoldKey,
          appBar: new AppBar(

            centerTitle: true,
            title: Text( allTranslations.text('menu_commande'),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _getCommandesInfo ("1");
                    },
                    child: Icon(
                      Icons.refresh,
                      size: 26.0,
                    ),
                  )
              ),
            ],
            backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          backgroundColor: FunctionUtils.colorFromHex("DDDDDD").withOpacity(0.9),
          body:Builder(
              builder: (BuildContext myContext)
              {
                return  Column(      // Column
                  children: <Widget>[

                    Container(
                      color:FunctionUtils.colorFromHex("dfdfdf"),        // Tab Bar color change
                      child: TabBar(           // TabBar
                        controller: _tabController,
                        unselectedLabelColor: FunctionUtils.colorFromHex("737373"),
                        labelColor: FunctionUtils.colorFromHex("020000"),
                        indicatorWeight: 3,
                        indicatorColor: FunctionUtils.colorFromHex(configModel.mainColor),
                        tabs: _myTabs,
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: TabBarView(         // Tab Bar View
                        physics: BouncingScrollPhysics(),
                        controller: _tabController,
                        children: <Widget>[
                           _mesCommandesScreen(configModel, _allLocalCommandeEntitySend),
                          _commandesRecueScreen(configModel, _allLocalCommandeEntityReceive),
                        ],
                      ),
                    ),
                  ],
                );
              }
          )

      );
    },
  );



 _mesCommandesScreen(ConfigModel configModel,  List<LocalCommandeEntity> allLocalCommandeEntitySend){
    return Stack(
        children: <Widget>[
          allLocalCommandeEntitySend.length == 0 ? Center(
              child: Column(
                children: [
                  SizedBox(height: 100,),
                  Material(
                    shape: CircleBorder(),
                    clipBehavior: Clip.hardEdge,
                    color: Colors.transparent,
                    child: Image.asset('assets/img/splash.jpg',height:250),
                  ),
                  SizedBox(height: 0,),
                  Container(
                    width:300,
                    child: Text(
                      allTranslations.text('no_commande_send'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black54,
                        fontWeight: FontWeight.bold,
                        fontSize: 22.0,
                      ),
                    ),
                  )
                ],
              ),
            ):
          ListView.builder(
            padding: EdgeInsets.symmetric(horizontal: 5.0,vertical: 5.0),
              itemCount: allLocalCommandeEntitySend.length,
              shrinkWrap: true,
              // 1st add
              physics: ClampingScrollPhysics(),
              itemBuilder: (BuildContext context, int position) {
                LocalCommandeEntity commande = allLocalCommandeEntitySend[position];
                  return CommandeWidget(
                      infoCommande:commande,
                      onrefresh: (){
                      setState(() {
                        _getCommandesFromDatabase("0");
                        //widget.onrefresh();
                      });
                  });
          })
        ]
    );
  }
  _commandesRecueScreen(ConfigModel configModel,  List<LocalCommandeEntity> allLocalCommandeEntityReceive){
    return Stack(
        children: <Widget>[
          allLocalCommandeEntityReceive.length == 0 ? Center(
            child: Column(
              children: [
                SizedBox(height: 100,),
                Material(
                  shape: CircleBorder(),
                  clipBehavior: Clip.hardEdge,
                  color: Colors.transparent,
                  child: Image.asset('assets/img/splash.jpg',height:250),
                ),
                SizedBox(height: 0,),
                Container(
                  width:300,
                  child: Text(
                    allTranslations.text('no_commande_receive'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black54,
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0,
                    ),
                  ),
                )
              ],
            ),
          ):
          ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: 5.0,vertical: 5.0),
              itemCount: allLocalCommandeEntityReceive.length,
              shrinkWrap: true,
              // 1st add
              physics: ClampingScrollPhysics(),
              itemBuilder: (BuildContext context, int position) {
                LocalCommandeEntity commande = allLocalCommandeEntityReceive[position];
                return CommandeWidget(
                    infoCommande:commande,
                    onrefresh: (){
                      setState(() {
                        _getCommandesFromDatabase("0");
                        //widget.onrefresh();
                      });
                    }
                );
              })
        ]
    );
  }

  _getCommandesInfo (String type) async {

    _appSharedPreferences.getUserInformation().then((user) {

   if(type.compareTo("1")==0) {
     showDialog(
         context: context,
         barrierDismissible: false,
         builder: (context) {
           return AlertDialog(
             backgroundColor: Colors.white,
             contentPadding: EdgeInsets.all(12),
             content: Column(
               mainAxisSize: MainAxisSize.min,
               children: <Widget>[
                 Text(allTranslations.text('refresh_processing')),
                 SizedBox(
                   height: 20,
                 ),
                 CircularProgressIndicator(
                   valueColor: AlwaysStoppedAnimation(Colors.green),
                 )
               ],
             ),
           );
         });
   }
    Api api = ApiRepository();
    ListeCommandeDto listeCommandeDto = ListeCommandeDto();

    listeCommandeDto.accessToken=DataConstantesUtils.API_TOKEN;
    listeCommandeDto.userToken=user.token;

    api.getCommande(listeCommandeDto).then((value) {
        if (value.isRight()) {
          value.all((a) {
            List<CommandeResponseMessageModel> commandeSendList = a.send_commande;
            List<CommandeResponseMessageModel> commandeReceiveList = a.receive_commande;


            List<CommandeModel> commandeSendSaveList=[];
            List<CommandeModel> commandeReceiveSaveList=[];

            int nbreTotalSend = commandeSendList.length;
            int nbreTotalReceive = commandeReceiveList.length;

            for (int i = 0; i < nbreTotalSend; i++) {

              CommandeModel uneCommandeSend =  CommandeModel();
              CommandeUserModel uneCommandeUserS = commandeSendList[i].user_info;
              List<CommandeDetailModel> commandeSendDetailList = commandeSendList[i].detail_commande;

              uneCommandeSend.idCommande = commandeSendList[i].id_commande;
              uneCommandeSend.keyCommande = commandeSendList[i].key_commande;
              uneCommandeSend.numeroCommande = commandeSendList[i].numero_commande;
              uneCommandeSend.dateCommande = commandeSendList[i].date_commande;
              uneCommandeSend.amountCommande = commandeSendList[i].amount_commande;
              uneCommandeSend.etatCommande = commandeSendList[i].etat_commande;
              uneCommandeSend.urlPaiementCommande =commandeSendList[i].url_paiement;
              uneCommandeSend.NomPrenomUserCommande = uneCommandeUserS.nom + " "+  uneCommandeUserS.prenom;
              uneCommandeSend.emailUserCommande =uneCommandeUserS.email;
              uneCommandeSend.telephoneUserCommande = uneCommandeUserS.telephone;

              commandeSendSaveList.add(uneCommandeSend);
              FunctionUtils.saveDetailCommande(commandeSendDetailList,commandeSendList[i].key_commande);
            }
            print(commandeSendSaveList.toString());

            for (int j = 0; j < nbreTotalReceive; j++) {
              CommandeModel uneCommandeReceive =  CommandeModel();
              CommandeUserModel uneCommandeUserR = commandeReceiveList[j].user_info;
              List<CommandeDetailModel> commandeReceiveDetailList = commandeReceiveList[j].detail_commande;

              uneCommandeReceive.idCommande = commandeReceiveList[j].id_commande;
              uneCommandeReceive.keyCommande = commandeReceiveList[j].key_commande;
              uneCommandeReceive.numeroCommande = commandeReceiveList[j].numero_commande;
              uneCommandeReceive.dateCommande = commandeReceiveList[j].date_commande;
              uneCommandeReceive.amountCommande = commandeReceiveList[j].amount_commande;
              uneCommandeReceive.etatCommande = commandeReceiveList[j].etat_commande;
              uneCommandeReceive.urlPaiementCommande =commandeReceiveList[j].url_paiement;
              uneCommandeReceive.NomPrenomUserCommande = uneCommandeUserR.nom + " "+  uneCommandeUserR.prenom;
              uneCommandeReceive.emailUserCommande =uneCommandeUserR.email;
              uneCommandeReceive.telephoneUserCommande = uneCommandeUserR.telephone;
              commandeReceiveSaveList.add(uneCommandeReceive);
              FunctionUtils.saveDetailCommande(commandeReceiveDetailList,commandeReceiveList[j].key_commande);
            }

            FunctionUtils.saveCommande(commandeSendSaveList,1);
            FunctionUtils.saveCommande(commandeReceiveSaveList,2);



            Timer(Duration(seconds: 2),(){
              _getCommandesFromDatabase("0");
              if(type.compareTo("1")==0) {
                Navigator.of(context).pop(null);
              }
            });


            return true;
          });
        } else {
          if(type.compareTo("1")==0) {
            Navigator.of(context).pop(null);
          }
          return false;
        }
      });
    });

  }

  _getCommandesFromDatabase (String typeRefresh) async {

        //envoyées
        _localCommandeRepository.listCommande(1).then((response) {
            setState(() {
              _allLocalCommandeEntitySend = response;
            });
        });

        //recues
        _localCommandeRepository.listCommande(2).then((res) {
            setState(() {
              _allLocalCommandeEntityReceive = res;
            });
        });

        if(typeRefresh.compareTo("1")==0) {
          _getCommandesInfo("0");
        }
    }
}

