import 'dart:convert';
import 'dart:async';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/launch/presentation/widgets/cart_counter_widget.dart';
import 'package:agrimobile/features/launch/presentation/widgets/cart_counter_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:html/dom.dart' as dom;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/launch/presentation/pages/commande/cart_page.dart';
import 'package:flutter/material.dart';
//import 'package:shopping_app/global.dart';



class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(icon: Icon(Icons.close),onPressed: (){
              Navigator.push(context, MaterialPageRoute(
                  fullscreenDialog: true,
                  builder: (_) => CartPage()));
            }),
            Row(
              children: <Widget>[
                IconButton(icon: Icon(Icons.search), onPressed: (){

                }),
                IconButton(icon:Icon(Icons.filter), onPressed: (){

                }),
                IconButton(icon: Container(
                  child: Center(
                    child: Text("3",style: TextStyle(
                        color: Colors.black,
                        fontSize: 13,
                        fontWeight: FontWeight.w600
                    ),),
                  ),
                  decoration: BoxDecoration(
                      color: Colors.black,
                      shape: BoxShape.circle
                  ),
                ), onPressed: (){
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => CartPage()));
                }),
              ],
            )
          ],
        ),
      ),
      body: getBody(),
    );
  }
  Widget getBody(){
    return ListView(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 40,left: 30,right: 30,bottom: 30),child:  Text("My Bag",style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w600
        ),),),
        Column(
          children: List.generate(3, (index){
            return  Padding(
              padding: const EdgeInsets.only(left: 30,right: 30,bottom: 30),
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        // color: Colors.grey,
                        boxShadow: [BoxShadow(
                            spreadRadius: 0.5,
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 1
                        )],
                        borderRadius: BorderRadius.circular(20)
                    ),
                    child: Container(
                      // padding: const EdgeInsets.only(top: 0,left: 25,right: 25,bottom: 25),
                      child: Column(

                        children: <Widget>[

                          Center(
                            child: Container(
                              width: 120,
                              height: 70,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image:
                                      NetworkImage(
                                        "http://devussdmtn.agristore.ci/b2b/themes/site/agro_img/annonces/default_image/Aubergine/2.jpg",
                                      ),
                                      fit: BoxFit.cover
                                  )
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: 20,),
                  Expanded(child: Column(

                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("aubergine",style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600
                      ),),
                      SizedBox(height: 15,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("1500",style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.w500
                          ),),
                          Text("x1",style: TextStyle(
                              fontSize: 14,
                              color: Colors.black.withOpacity(0.5),
                              fontWeight: FontWeight.w500
                          ),)
                        ],)
                    ],
                  ))
                ],
              ),
            );
          }),
        ),
        SizedBox(height: 50,),
        Padding(padding: EdgeInsets.only(left: 30,right: 30),child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text("Total",style: TextStyle(
                fontSize: 22,
                color: Colors.black.withOpacity(0.5),
                fontWeight: FontWeight.w600
            ),),
            Text("\$ 508.00",style: TextStyle(
                fontSize: 22,

                fontWeight: FontWeight.w600
            ),),
          ],
        ),),
        SizedBox(height: 30,),
        Padding(

          padding: const EdgeInsets.only(left: 20,right: 20),
          child: FlatButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
              color: Colors.black,
              onPressed: (){

              }, child: Container(
            height: 50,
            child: Center(
              child: Text("CHECKOUT",style: TextStyle(
                  color: Colors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.w600
              ),),
            ),
          )
          ),
        )
      ],
    );
  }
}