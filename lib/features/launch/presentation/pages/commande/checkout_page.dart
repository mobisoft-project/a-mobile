import 'dart:convert';
import 'dart:async';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/commande_send_dto.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:agrimobile/features/common/data/models/cart_item_model.dart';
import 'package:agrimobile/features/common/data/models/commande_detail_model.dart';
import 'package:agrimobile/features/common/data/models/commande_send_model.dart';
import 'package:agrimobile/features/common/data/models/commande_model.dart';
import 'package:agrimobile/features/common/data/models/commande_response_message_model.dart';
import 'package:agrimobile/features/common/data/models/commande_user_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/launch/presentation/widgets/cart_counter_widget.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:html/dom.dart' as dom;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import 'package:url_launcher/url_launcher.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/commande/checkout_final_page.dart';
//import 'package:shopping_app/global.dart';
import 'package:agrimobile/features/common/data/models/user_information_model.dart';


class CheckoutPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List <CartItemModel> allItems;
  double total = 0;






  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}

class PageState extends State<CheckoutPage> {

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  String _userKey="";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer3<ConfigModel,CartModel,UserInformationModel>(
    builder: (context, configModel, cartModel,userConnected,child) {
      widget.allItems = cartModel.allItems;
      widget.total = cartModel.total;



      return Scaffold(
          key: widget._scaffoldKey,
          appBar: new AppBar(

            centerTitle: true,
            title: Text(allTranslations.text('menu_panier'),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
            backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          backgroundColor: Colors.white,
          body: Builder(
              builder: (BuildContext myContext) {
                //cartModel.clearCart();
                return Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 15),
                      Expanded(
                        child: cartModel.getCount() > 0 ?ListView.separated(
                          separatorBuilder: (context, index) =>Padding(
                              padding: const EdgeInsets.only(left: 15,right: 30,bottom: 5),
                            child:   Divider(
                              color: Colors.black.withOpacity(0.3),
                            ),
                          ),
                          itemCount: cartModel.getCount(),
                          itemBuilder: (ctx, i) {
                            CartItemModel _item = widget.allItems[i];
                            LocalAnnonceEntity infoAnnonce = _item.annonce;
                            print('titre: '+ _item.nomCategorieFilleProduit);
                            print(_item.toMap());
                            int _maxQte = infoAnnonce.stock;
                            int _qte= _item.qte;
                            String _titre = infoAnnonce.title!=null && infoAnnonce.title.compareTo("")!=0 ? infoAnnonce.title : _item.nomCategorieFilleProduit;

                            String _imageName="";
                            String _serverImageName="";

                            _serverImageName="";
                            _imageName=infoAnnonce.picture;
                            if(_imageName.compareTo("")!=0){
                              _serverImageName = DataConstantesUtils.IMAGE_ANNONCE_URL + "" + _imageName;
                            }
                            int annonce_price = infoAnnonce.prix;
                            double annonce_price_final = annonce_price * 1.0;


                            return Padding(
                              padding: const EdgeInsets.only(left: 15,right: 30,bottom: 30),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(
                                      // color: Colors.grey,
                                        boxShadow: [BoxShadow(
                                            spreadRadius: 0.5,
                                            color: Colors.black.withOpacity(0.1),
                                            blurRadius: 1
                                        )],
                                        borderRadius: BorderRadius.circular(20)
                                    ),
                                    child: Container(
                                      // padding: const EdgeInsets.only(top: 0,left: 25,right: 25,bottom: 25),
                                      child: Column(
                                        children: <Widget>[
                                          Center(
                                            child: Container(
                                              width: 90,
                                              height: 90,
                                              decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(10),
                                                  image: DecorationImage(
                                                      image: NetworkImage(_serverImageName),
                                                      fit: BoxFit.cover
                                                  )
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(width: 20,),
                                  Expanded(
                                      child:  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(_titre,
                                          style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold
                                      )),
                                     SizedBox(height: 5.0,),
                                      Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                            "${FunctionUtils.separateur(annonce_price_final)} ${configModel.devise}",
                                              style: TextStyle(
                                                fontSize: 14,fontWeight: FontWeight.w500),
                                            ),
                                            Text("${FunctionUtils.separateur(cartModel.itemCartTotal(_item)*1.0)} ${configModel.devise}",
                                              style: TextStyle(
                                                fontSize: 14,fontWeight: FontWeight.w500),
                                            ),
                                          ]
                                      ),

                                      SizedBox(height: 10,),

                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          CartCounterWidget(
                                             item: _item,
                                             qte: _qte,
                                              maxQte: _maxQte,
                                              indivisibilite: infoAnnonce.indivisibilite,
                                              onrefresh: (){
                                                  setState(() {
                                                  });
                                              }
                                           ),
                                          GestureDetector(
                                            child: Container(
                                              width: 30.0,
                                              padding: const EdgeInsets.all(5.0),
                                              child: Icon(
                                                Icons.delete,
                                                size: 18.0,
                                                color: Colors.red.withOpacity(0.8)
                                              ),
                                            ),
                                            onTap: () {
                                              _showAlertDialog(context, _item,configModel,cartModel);
                                            },
                                          ),
                                        // MyCounter()
                                        ],
                                      )
                                    ],
                                  ))
                                ],
                              ),
                            );
                          },
                        ):Center(
                          child: Column(
                            children: [
                              // SizedBox(height: 100,),
                              Material(
                                shape: CircleBorder(),
                                clipBehavior: Clip.hardEdge,
                                color: Colors.transparent,
                                child: Image.asset('assets/img/splash.jpg',height:250),
                              ),
                              SizedBox(height: 0,),
                              Container(
                                width:300,
                                child: Text(
                                  allTranslations.text('cart_empty'),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Colors.black54,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 22.0,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Divider(),
                      if(cartModel.getCount() > 0)Padding(
                          padding: const EdgeInsets.only(left: 15,right: 15),
                        child:   Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("TOTAL", style: Theme
                                      .of(context)
                                      .textTheme
                                      .subtitle),
                                  Text( "${FunctionUtils.separateur(widget.total)} ${configModel.devise}",
                                      style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold,fontSize: 20.0)),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: 50,
                                child: RaisedButton(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child:Text( allTranslations.text('checkout'),
                                          style: TextStyle(fontSize: 12, color: Colors.white),
                                      ),
                                      ),
                                      Icon(Icons.navigate_next, color: Colors.white,size: 20,)
                                    ],
                                    ) ,
                                  onPressed: () {

                                    FunctionUtils.pressedMenu(context,"${userConnected.id}",userConnected.validationUser,"M080",CheckoutFinalPage(),widget._scaffoldKey,2);

                                  },
                                  color: Colors.black,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                );
              }
          )
      );
    }
  );



  _getUserInformation(){
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userKey="${value.id}";
      });
    });

  }

  _showAlertDialog(BuildContext context,CartItemModel CartItemModel,ConfigModel configModel,CartModel cartModel) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
          _confirmeDelete(CartItemModel,configModel,cartModel);
      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(
        allTranslations.text('delete_cartitem_text'),
        style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _checkout(ConfigModel configModel,CartModel cartModel) async {

    _appSharedPreferences.getUserInformation().then((user) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('order_loading')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });
    Api api = ApiRepository();
    CommandeSendDto _commandeSendDto = CommandeSendDto();
    List<CartItemModel> items = cartModel.getItems();

    List<CommandeSendModel> commande_list=[];

    for (var item in items) {
      CommandeSendModel commande =  CommandeSendModel();
      
      commande.id = item.annonce.apiId.toString();
      commande.qte = item.qte.toString();
      commande.prix = item.annonce.prix.toString();
      commande_list.add(commande);
    }
    debugPrint(commande_list.map((i) => i.toMap()).toString());

     _commandeSendDto.accessToken=DataConstantesUtils.API_TOKEN;
     _commandeSendDto.userToken=user.token;
     _commandeSendDto.infoCommande=commande_list;

    api.sendCommande(_commandeSendDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          if(a.status.compareTo("000")==0){
            CommandeResponseMessageModel message = a.message;
            CommandeUserModel commandeUser = message.user_info;
            List<CommandeDetailModel> commandeDetailList = message.detail_commande;
             print(message.user_info.toString());
            // print(commandeUser.toMap());

            List<CommandeModel> commandeSaveList=[];

            CommandeModel uneCommande =  CommandeModel();

            uneCommande.idCommande = message.id_commande;
            uneCommande.keyCommande = message.key_commande;
            uneCommande.numeroCommande = message.numero_commande;
            uneCommande.dateCommande = message.date_commande;
            uneCommande.amountCommande = message.amount_commande;
            uneCommande.etatCommande = message.etat_commande;
            uneCommande.urlPaiementCommande =message.url_paiement;
            uneCommande.NomPrenomUserCommande = commandeUser.nom + " "+  commandeUser.prenom;
            uneCommande.emailUserCommande = commandeUser.email;
            uneCommande.telephoneUserCommande = commandeUser.telephone;

            commandeSaveList.add(uneCommande);

            //save commande
            FunctionUtils.saveCommande(commandeSaveList,1).then((value){
              print("save commande good " + value.toString());

              //save details commande
              //FunctionUtils.saveDetailsCommande(commande_save_list);
                  FunctionUtils.saveDetailCommande(commandeDetailList,message.key_commande).then((value){
                    print("save commande detail good +++" + value.toString());

                        //vider le panier
                        _displaySnackBar(context, allTranslations.text('error_process'));
                         cartModel.clearCart();
                        _appSharedPreferences.setCart(cartModel.toMap());
                        _launchURL(message.url_paiement);
                          Navigator.of(context).pop(null);
                          Navigator.of(context).pop(null);
                  });
            });
            //Timer(Duration(seconds: 2),(){});
            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
    });
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }


  _confirmeDelete(CartItemModel cartItemModel,ConfigModel configModel,CartModel cartModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('cdelete_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Navigator.of(context).pop(null);
    cartModel.removeFromCart(cartItemModel);
    _appSharedPreferences.setCart(cartModel.toMap());

    setState(() {
      widget.allItems = cartModel.allItems;
    });
    return false;
  }


}

