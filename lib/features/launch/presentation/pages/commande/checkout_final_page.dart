import 'dart:convert';
import 'dart:async';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/commande_send_dto.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:agrimobile/features/common/data/models/cart_item_model.dart';
import 'package:agrimobile/features/common/data/models/commande_detail_model.dart';
import 'package:agrimobile/features/common/data/models/commande_send_model.dart';
import 'package:agrimobile/features/common/data/models/commande_model.dart';
import 'package:agrimobile/features/common/data/models/commande_response_message_model.dart';
import 'package:agrimobile/features/common/data/models/commande_user_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/launch/presentation/widgets/cart_counter_widget.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:html/dom.dart' as dom;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'dart:core';
import 'package:url_launcher/url_launcher.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';
import 'package:find_dropdown/find_dropdown.dart';
import 'package:agrimobile/features/common/domain/entities/local_moyen_transport_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_mode_livraison_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_mode_livraison_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_moyen_transport_repository.dart';

import 'package:agrimobile/features/common/data/models/liste_transporter_response_model.dart';
import 'package:agrimobile/features/common/data/dto/liste_transporter_dto.dart';

import 'package:agrimobile/features/common/domain/entities/local_commande_detail_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/launch/presentation/widgets/commande_detail_widget.dart';

class CheckoutFinalPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List <CartItemModel> allItems;
  double total = 0;


  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}

class PageState extends State<CheckoutFinalPage> {

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  String _userKey="";

  var _modeLivraisonKey = GlobalKey<FindDropdownState>();
  var _moyenTransportKey = GlobalKey<FindDropdownState>();
  var _transporterKey = GlobalKey<FindDropdownState>();

  LocalModeLivraisonRepository _localModeLivraisonRepository=LocalModeLivraisonRepository();
  LocalMoyenTransportRepository _localMoyenTransportRepository=LocalMoyenTransportRepository();


  int _modeLivraison=0;
  int _moyenTransport=0;
  int _transporterId=0;

  List<TransporterModel> _allListTransporters=[];
  bool _visibleMoyenTransport=false;
  bool _visibleTransporter=false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUserInformation();
  }

  @override
  Widget build(BuildContext context) => Consumer2<ConfigModel,CartModel>(
    builder: (context, configModel, cartModel,child) {
      widget.allItems = cartModel.allItems;
      widget.total = cartModel.total;
      int nbre = cartModel.getCount();

      return Scaffold(
          key: widget._scaffoldKey,
          appBar: new AppBar(

            centerTitle: true,
            title: Text(allTranslations.text('validate_commande'),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
            backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
          backgroundColor: Colors.white,
          body: Builder(
              builder: (BuildContext myContext) {
                return Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                     ListView(
                            shrinkWrap: true,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15.0),
                                child:Column(
                                  children: [
                                    SizedBox(height: 5.0,),
                                    Text(
                                      "Résumé",
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                          color: FunctionUtils.colorFromHex(configModel.mainColor)),
                                    ),
                                    SizedBox(height: 10.0,),
                                    ListView.separated(
                                        separatorBuilder: (context, index) =>Padding(
                                          padding: const EdgeInsets.only(left: 15,right: 30,bottom: 5),
                                          child:   Divider(
                                            color: Colors.black.withOpacity(0.3),
                                          ),
                                        ),
                                        padding: EdgeInsets.symmetric(horizontal: 5.0,vertical: 5.0),
                                        itemCount: widget.allItems.length,
                                        shrinkWrap: true,
                                        // 1st add
                                        physics: ClampingScrollPhysics(),
                                        itemBuilder: (BuildContext context, int position) {
                                          CartItemModel cartItem = widget.allItems[position];
                                          LocalAnnonceEntity localAnnonce = cartItem.annonce;
                                          // localAnnonce.nomCategorieFilleProduit
                                          // if(localAnnonce.title!=null && localAnnonce.title.compareTo("")!=0)

                                          String _titre = localAnnonce.title!=null && localAnnonce.title.compareTo("")!=0 ?cartItem.nomCategorieFilleProduit + " ("+ localAnnonce.title+")" : cartItem.nomCategorieFilleProduit;

                                          LocalCommandeDetailEntity commandeDetail = LocalCommandeDetailEntity.create(
                                              idCommandeDetail: position,
                                              keyCommande: "",
                                              nomProduitCommandeDetail:_titre,
                                              prixTotalCommandeDetail:cartModel.itemCartTotal(cartItem).toString(),
                                              prixUnitaireCommandeDetail: FunctionUtils.separateur(localAnnonce.prix*1.0),
                                              qteProduitCommandeDetail: cartItem.qte.toString()
                                          );
                                          return CommandeDetailWidget(infoCommandeDetail:commandeDetail);
                                        }
                                    )
                                  ],
                                )
                              ),
                              Divider(
                                thickness: 1.0,
                                color: FunctionUtils.colorFromHex(configModel.mainColor),
                              ),
                              ListTile(
                                // leading: Icon(Icons.navigate_next, color: Colors.grey,size: 10,),
                                title: Row(
                                    children: [
                                      Icon(Icons.navigate_next, color: Colors.grey,size: 30,),
                                      SizedBox(width: 15),
                                      Expanded(
                                          child:Text(
                                            "${allTranslations.text('commande_product_count')}"+nbre.toString(),
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black87),
                                          )),

                                    ]
                                ),
                              ),
                              Divider(),
                              ListTile(
                                  // leading: Icon(Icons.navigate_next, color: Colors.grey,size: 10,),
                                  title: Row(
                                    children: [
                                      Icon(Icons.navigate_next, color: Colors.grey,size: 30,),
                                      SizedBox(width: 15),
                                    Expanded(
                                    child:Text(
                                      "${allTranslations.text('commande_amount')} ${FunctionUtils.separateur(widget.total)} ${configModel.devise}",
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.red),
                                    )),

                                    ]
                                  ),
                              ),
                              ]
                        ),
                      Divider(),
                      Padding(
                        padding: const EdgeInsets.only(left: 15,right: 15),
                        child:
                        MaterialButton(
                          color:  FunctionUtils.colorFromHex(configModel.mainColor),
                          elevation: 0.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.0),
                              side: BorderSide(color:  FunctionUtils.colorFromHex(configModel.mainColor))
                          ),
                          textColor: Colors.white,
                          onPressed: () {
                              if(_modeLivraison.compareTo(0)!=0 ) {

                              if(_visibleMoyenTransport==true ) {
                                 if( _moyenTransport.compareTo(0)!=0 ) {
                                     if( _transporterId.compareTo(0)!=0 ) {
                                       print("ok1");
                                          _checkout(configModel, cartModel);
                                     } else {
                                       _displaySnackBar(context, allTranslations.text('emptyTransporter'));
                                     }

                                 } else {
                                   _displaySnackBar(context, allTranslations.text('emptyMoyenTransport'));
                                 }

                                } else {
                                     _checkout(configModel, cartModel);
                                }

                              } else {
                                _displaySnackBar(context, allTranslations.text('emptyModeLivraison'));
                              }


                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Text(allTranslations.text('checkout_final'),
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white, fontSize: 16.0),
                              ),
                            ],
                          ),
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 15),

                        ),
                      ),

                      SizedBox(height: 5.0)
                    ],
                  ),
                );
              }
          )
      );
    }
  );

  _getUserInformation(){
    _appSharedPreferences.getUserInformation().then((value) {
      setState(() {
        _userKey="${value.id}";
      });
    });

  }

  _showAlertDialog(BuildContext context,CartItemModel CartItemModel,ConfigModel configModel,CartModel cartModel) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
          _confirmeDelete(CartItemModel,configModel,cartModel);
      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(
        allTranslations.text('delete_cartitem_text'),
        style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<List<LocalModeLivraisonEntity>> getModesLivraisons(String filter){
    Future<List<LocalModeLivraisonEntity>> localModeLivraison =   _localModeLivraisonRepository.filterModeLivraison(filter);
    // return  _localModeLivraisonRepository.filterModeLivraison(filter);
    return localModeLivraison;
  }

  Future<List<LocalMoyenTransportEntity>> getMoyenTransports(String filter){
    return  _localMoyenTransportRepository.filterMoyenTransport(filter);
  }


  Future<List<TransporterModel>> getTransporterList2(String filter)async{

    ListeTransporterDto listeTransporterDto = ListeTransporterDto();
    listeTransporterDto.accessToken=DataConstantesUtils.API_TOKEN;
    listeTransporterDto.moyenTransportApiId=_moyenTransport;


    FormData formData = new FormData.fromMap(listeTransporterDto.toMap());

    print(listeTransporterDto.toMap());
    print(formData.fields);

    Dio dio = new Dio();

    print(DataConstantesUtils.LISTE_TRANSPORTER_SERVER_URL);
   return await dio.post(DataConstantesUtils.LISTE_TRANSPORTER_SERVER_URL,data: formData).then((response) {

    var data = response.toString();
    print(data);


    Map responseMap = jsonDecode(data);
    ListeTransporterResponseModel infoModel=ListeTransporterResponseModel.fromMap(responseMap);

    if(infoModel.status.compareTo("000")==0){
      return  infoModel.information;
    }
   }).catchError((error){
     print(error.toString());
   });
  }


  Future<List<TransporterModel>> getTransporterList(String filter){
      Api api = ApiRepository();
      ListeTransporterDto listeTransporterDto = ListeTransporterDto();

      listeTransporterDto.accessToken=DataConstantesUtils.API_TOKEN;
      listeTransporterDto.moyenTransportApiId=_moyenTransport;

      api.getTransportersListe(listeTransporterDto).then((value) {
        if (value.isRight()) {
          value.all((a) {
            List<TransporterModel> list_transporters = a.information;
            setState(() {
              _allListTransporters = list_transporters;
            });
          });
        }
      });
  }

  _checkout(ConfigModel configModel,CartModel cartModel) async {


    _appSharedPreferences.getUserInformation().then((user) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('order_loading')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });
    Api api = ApiRepository();
    CommandeSendDto _commandeSendDto = CommandeSendDto();
    List<CartItemModel> items = cartModel.getItems();

    List<CommandeSendModel> commande_list=[];

    for (var item in items) {
      CommandeSendModel commande =  CommandeSendModel();
      
      commande.id = item.annonce.apiId.toString();
      commande.qte = item.qte.toString();
      commande.prix = item.annonce.prix.toString();
      commande_list.add(commande);
    }
    debugPrint(commande_list.map((i) => i.toMap()).toString());

     _commandeSendDto.accessToken=DataConstantesUtils.API_TOKEN;
     _commandeSendDto.userToken=user.token;
     _commandeSendDto.modeLivraison=_modeLivraison;
     _commandeSendDto.moyenTransport=_moyenTransport;
     _commandeSendDto.transporterId=_transporterId;

     _commandeSendDto.infoCommande=commande_list;

    api.sendCommande(_commandeSendDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          if(a.status.compareTo("000")==0){
            CommandeResponseMessageModel message = a.message;
            CommandeUserModel commandeUser = message.user_info;
            List<CommandeDetailModel> commandeDetailList = message.detail_commande;
             print(message.user_info.toString());
            // print(commandeUser.toMap());

            List<CommandeModel> commandeSaveList=[];

            CommandeModel uneCommande =  CommandeModel();

            uneCommande.idCommande = message.id_commande;
            uneCommande.keyCommande = message.key_commande;
            uneCommande.numeroCommande = message.numero_commande;
            uneCommande.dateCommande = message.date_commande;
            uneCommande.amountCommande = message.amount_commande;
            uneCommande.modeLivraisonCommande = message.mode_livraison;
            uneCommande.motCleModeLivraisonCommande = message.mot_cle_mode_livraison;
            uneCommande.moyenTransportCommande = message.moyen_transport;
            uneCommande.transporterCommande = message.transporter;
            uneCommande.etatCommande = message.etat_commande;
            uneCommande.urlPaiementCommande =message.url_paiement;
            uneCommande.NomPrenomUserCommande = commandeUser.nom + " "+  commandeUser.prenom;
            uneCommande.emailUserCommande = commandeUser.email;
            uneCommande.telephoneUserCommande = commandeUser.telephone;

            commandeSaveList.add(uneCommande);

            //save commande
            FunctionUtils.saveCommande(commandeSaveList,1).then((value){
              print("save commande good " + value.toString());

              //save details commande
              //FunctionUtils.saveDetailsCommande(commande_save_list);
                  FunctionUtils.saveDetailCommande(commandeDetailList,message.key_commande).then((value){
                    print("save commande detail good +++" + value.toString());

                        //vider le panier
                         cartModel.clearCart();
                        _displaySnackBar(context, allTranslations.text('commande_success'));
                        _appSharedPreferences.setCart(cartModel.toMap());
                       //_launchURL(message.url_paiement);
                          Navigator.of(context).pop(null);
                          Navigator.of(context).pop(null);
                          Navigator.of(context).pop(null);
                  });
            });
            //Timer(Duration(seconds: 2),(){});
            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
    });
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }


  _confirmeDelete(CartItemModel cartItemModel,ConfigModel configModel,CartModel cartModel) async {

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('cdelete_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Navigator.of(context).pop(null);
    cartModel.removeFromCart(cartItemModel);
    _appSharedPreferences.setCart(cartModel.toMap());
    Navigator.of(context).pop(null);

    setState(() {
      widget.allItems = cartModel.allItems;
    });
    return false;
  }


}

