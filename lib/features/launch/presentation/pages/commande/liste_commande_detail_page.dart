import 'dart:convert';
import 'dart:async';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_commande_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_commande_detail_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_commande_repository.dart';
import 'package:agrimobile/features/launch/presentation/widgets/commande_detail_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:html/dom.dart' as dom;

import 'package:agrimobile/features/common/data/dto/commande_delete_dto.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'dart:core';
import 'package:url_launcher/url_launcher.dart';

class ListeCommandeDetailPage extends StatefulWidget {
  LocalCommandeEntity infoCommande;

  ListeCommandeDetailPage({this.infoCommande});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PageState();
  }
}



class PageState extends State<ListeCommandeDetailPage> with SingleTickerProviderStateMixin  {

  List<LocalCommandeDetailEntity> _allLocalCommandeDetailEntity=[];

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalCommandeRepository _localCommandeRepository = LocalCommandeRepository();
  final _scaffoldKey = GlobalKey<ScaffoldState>();


  final hr = Divider(
    thickness: 1.0,
    color: Colors.black12,
  );



  @override
  void initState() {
    super.initState();

    _getCommandesDetailFromDatabase(widget.infoCommande.keyCommande);
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {

      int annonce_commande =  widget.infoCommande.amountCommande;
      double annonce_commande_final = annonce_commande * 1.0;

      return Scaffold(
          key: _scaffoldKey,
          appBar: new AppBar(

            centerTitle: true,
            title: Text("Commande N° : "+widget.infoCommande.numeroCommande,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
            actions: <Widget>[
             if(widget.infoCommande.etatCommande == 0) Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _launchURL(widget.infoCommande.urlPaiementCommande);
                    },
                    child: Icon(
                      Icons.payment,
                      size: 26.0,
                    ),
                  )
              ),
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      _showAlertDialog(context, configModel,widget.infoCommande);
                    },
                    child: Icon(
                      Icons.delete,
                      size: 26.0,
                    ),
                  )
              )
            ],
            backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
            elevation: 0.0,
          ),
         backgroundColor: FunctionUtils.colorFromHex("DDDDDD").withOpacity(0.9),
          body:Builder(
              builder: (BuildContext myContext)
              {
                // String _dateCreate= FunctionUtils.convertDate(widget.infoCommande.dateCommande);
                return SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      children: [
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          //color: Colors.pink,
                          // elevation: 1,
                          child:Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                        "${FunctionUtils.separateur(annonce_commande_final)} ${configModel.devise}",
                                            style: TextStyle(color: Colors.red,fontSize: 22,fontWeight: FontWeight.bold))
                                      ],
                                    ),
                                    Expanded(
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          Container(
                                            height: 25,
                                            padding: EdgeInsets.all(5.0),
                                            decoration: new BoxDecoration(
                                              borderRadius: new BorderRadius.circular(20.0),
                                              color: widget.infoCommande.etatCommande == 0 ? FunctionUtils.colorFromHex("AE682D"):FunctionUtils.colorFromHex(configModel.mainColor),
                                            ),
                                            child: Text(widget.infoCommande.etatCommande == 0 ?allTranslations.text('pending_order'):allTranslations.text('delivred_order'),
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  //fontWeight: FontWeight.w700,
                                                  fontSize: 12,
                                                  color: Colors.white,
                                                )),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 5),
                                Row(
                                  children: [
                                    Icon(Icons.calendar_today_rounded, color: FunctionUtils.colorFromHex(configModel.mainColor),size: 10.0,),
                                    SizedBox(width: 5,),
                                    Expanded(
                                      child:Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(widget.infoCommande.dateCommande, style: TextStyle(color: Colors.black)),
                                        ],
                                      ) ,
                                    ),
                                  ],
                                ),
                                SizedBox(height: 5),
                                Container(
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Text(allTranslations.text('info_client'), style: TextStyle(color: FunctionUtils.colorFromHex(configModel.mainColor),fontSize: 18,fontWeight: FontWeight.w700,)),
                                      SizedBox(height: 5),
                                      Text.rich(
                                        TextSpan(
                                          text: "${allTranslations.text('name')} : ",
                                          style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,),
                                          children:
                                          <TextSpan>[
                                            TextSpan(
                                              text: widget.infoCommande.NomPrenomUserCommande,
                                              style: TextStyle(
                                                decoration: TextDecoration.none,
                                                color: Colors.black54,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 5),
                                      Text.rich(
                                        TextSpan(
                                          text: "${allTranslations.text('email')} : ",
                                          style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,),
                                          children:
                                          <TextSpan>[
                                            TextSpan(
                                              text: widget.infoCommande.emailUserCommande,
                                              style: TextStyle(
                                                decoration: TextDecoration.none,
                                                color: Colors.black54,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 5),
                                      Text.rich(
                                        TextSpan(
                                          text: "${allTranslations.text('phone')} : ",
                                          style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,),
                                          children:
                                          <TextSpan>[
                                            TextSpan(
                                              text: widget.infoCommande.telephoneUserCommande,
                                              style: TextStyle(
                                                decoration: TextDecoration.none,
                                                color: Colors.black54,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                          child:Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Stack(

                                  children: [
                                    _allLocalCommandeDetailEntity.length == 0 ? Container(
                                        alignment: FractionalOffset.center,
                                        child:  Center(
                                          child: Text(allTranslations.text('no_commande_send')),
                                        )
                                    ):ListView.separated(
                                        separatorBuilder: (context, index) =>Padding(
                                          padding: const EdgeInsets.only(left: 15,right: 30,bottom: 5),
                                          child:   Divider(
                                            color: Colors.black.withOpacity(0.3),
                                          ),
                                        ),
                                        padding: EdgeInsets.symmetric(horizontal: 5.0,vertical: 5.0),
                                        itemCount: _allLocalCommandeDetailEntity.length,
                                        shrinkWrap: true,
                                        // 1st add
                                        physics: ClampingScrollPhysics(),
                                        itemBuilder: (BuildContext context, int position) {
                                          LocalCommandeDetailEntity commandeDetail = _allLocalCommandeDetailEntity[position];
                                          return CommandeDetailWidget(infoCommandeDetail:commandeDetail);
                                        })
                                  ],
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                );
              }
          )
      );
    },
  );


  _getCommandesDetailFromDatabase (String keyCommande) async {
    //details
    _localCommandeRepository.detailsCommande(keyCommande).then((response) {
      if(response.length>0 ){
        setState(() {
          _allLocalCommandeDetailEntity = response;
        });
      }
    });
  }

  _showAlertDialog(BuildContext context,ConfigModel configModel, LocalCommandeEntity commande) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        _confirmeDelete(configModel, commande);
      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(
        allTranslations.text('delete_commande_text'),
        style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _confirmeDelete(ConfigModel configModel, LocalCommandeEntity commande) async {
    _appSharedPreferences.getUserInformation().then((user) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('cdelete_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });

      //remove

      print(commande.keyCommande);
      Api api = ApiRepository();
      CommandeDeleteDto deleteCommandeDto = CommandeDeleteDto();

      deleteCommandeDto.accessToken=DataConstantesUtils.API_TOKEN;
      deleteCommandeDto.userToken=user.token;
      deleteCommandeDto.keyCommande=commande.keyCommande;

      api.deleteCommande(deleteCommandeDto).then((value) {
        if (value.isRight()) {
          value.all((a) {
            if(a.status.compareTo("000")==0) {

              // delete in database
              FunctionUtils.deleteCommande(commande).then((value){
                Navigator.of(context).pop(null);
              });

              Navigator.of(context).pop(null);

              return true;
            }else{
              Navigator.of(context).pop(null);
              _displaySnackBar(context, allTranslations.text('error_process'));
              return false;
            }
          });
        } else {
          Navigator.of(context).pop(null);
          return false;
        }
      });
      return false;
    });
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    // widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

