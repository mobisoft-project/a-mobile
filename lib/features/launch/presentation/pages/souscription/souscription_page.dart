import 'dart:async';
import 'dart:convert';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/souscription_dto.dart';
import 'package:agrimobile/features/common/data/dto/souscription_send_dto.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_souscription_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_user_sourciption_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_souscription_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_user_souscription_repository.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:url_launcher/url_launcher.dart';


class SouscriptionPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final VoidCallback onRefresh;

  SouscriptionPage({this.onRefresh});
  @override
  _SouscriptionPageState createState() => _SouscriptionPageState();
}

class _SouscriptionPageState extends State<SouscriptionPage> with SingleTickerProviderStateMixin {
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();

  LocalSouscriptionRepository _localTypeSouscriptionRepository = LocalSouscriptionRepository();
  LocalUserSouscriptionRepository _localUserTypeSouscriptionRepository = LocalUserSouscriptionRepository();
  
  List<LocalSouscriptionEntity> _localSouscriptionEntity;
  List<LocalUserSouscriptionEntity> _localUserSouscriptionEntity;

  Api api = ApiRepository();

  List<String> denoKey = [];
  bool _haveService = true;
  TabController _myController;
  int _mySelectedIndex = 0;
  int sttSouscription = 0;
  List<Widget> my_list = [
    Tab(text: "Mes services"),
    Tab(text: "Services"),
  ];

  @override
  void initState() {
    super.initState();
    _getSouscriptionFromDatabase("1", 0);

    _myController = TabController(length: my_list.length, vsync: this);

    _myController.addListener(() {
      setState(() {
        _mySelectedIndex = _myController.index;
      });
    });
  }

  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child){
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:  Align(
            child: Container(
              child: Text(allTranslations.text('menu_souscription'),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
            ),
          ),
          actions: [
            Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  _getSouscriptionFromDatabase("1", 1);
                },
                child: Icon(
                  Icons.refresh,
                  size: 26.0,
                ),
              )
            ),
          ],
          backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
        body: Builder(builder: (BuildContext myContext){
          /* if(_haveService == false){
            return Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 100,
                  ),
                  Material(
                    shape: CircleBorder(),
                    clipBehavior: Clip.hardEdge,
                    color: Colors.transparent,
                    child: Image.asset('assets/img/loading.gif', height: 100),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                ],
              ),
            );
          }else{ */
            return Column(
              children: [
                Container(
                  child: TabBar(
                    tabs: my_list,
                    controller: _myController,
                    labelColor: FunctionUtils.colorFromHex("020000"),
                    unselectedLabelColor: FunctionUtils.colorFromHex("737373"),
                    indicatorColor: FunctionUtils.colorFromHex(configModel.mainColor)
                  ),
                  color: FunctionUtils.colorFromHex("dfdfdf"),
                ),
                Expanded(
                  child: TabBarView(
                  controller: _myController,
                  children: [
                      _getUserSubscribeServices(_localUserSouscriptionEntity),
                      _getTypeSouscritionListView(configModel, _localSouscriptionEntity)
                    ],
                  ),
                )
              ],
            );
          }
          
        /* } */
        )
      );
    },
  );

  _getSouscriptionFromDatabase(String typeRefresh, int caller) async {
    _appSharedPreferences.getUserInformation().then((utilisateur){
      print('--4------|||||---------');
      print('***SetStateVariables***');
      print('--4------|||||---------');
      //Mes services
      _localUserTypeSouscriptionRepository
       .userListSouscription(utilisateur.id)
       .then((userServices) {
          setState(() {
            _haveService = false;
            _localUserSouscriptionEntity = userServices;
          });
       });

      //Liste des services
      _localTypeSouscriptionRepository
      .listTypeSouscription(utilisateur.id)
      .then((typeServices) {
          setState(() {
            _haveService = false;
            _localSouscriptionEntity = typeServices;
          });
      });
    });

    if(typeRefresh.compareTo("1")==0) {
      print('--1---|||||-------');
      print('***InitialState***');
      print('--1---|||||-------');
      _getRemoteUserTypeSouscription();
      _getRemoteTypeSouscription(caller);
    }
  }

  _getRemoteUserTypeSouscription() async {
      print('--2------|||||---------');
      print('***RemoteUserService***');
      print('--2------|||||---------');
    _appSharedPreferences.getUserInformation().then((utilisateur){
      SouscriptionDto _userSouscriptionDto = SouscriptionDto.create(
        accessToken: DataConstantesUtils.API_TOKEN,
        userToken: utilisateur.token
      );

      api.getUserTypeSouscription(_userSouscriptionDto).then((data){
        if(data.isRight()){
          data.all((userTypeSouscription) {
            if(userTypeSouscription != null && userTypeSouscription.status.compareTo("000") == 0){
              if(userTypeSouscription.userSouscription.isEmpty){
                return false;
              }
              FunctionUtils.saveUserSouscription(userTypeSouscription.userSouscription, utilisateur.id);
              Timer(Duration(seconds: 2),(){
                _getSouscriptionFromDatabase("0", 0);
              });
              return true;
            }else{
              return false;
            }
          });
        }else{
          return false;
        }
      });
    });
  }

  _getRemoteTypeSouscription(int cller) async {
      print('--3------|||||---------');
      print('***RemoteTypeService***');
      print('--3------|||||---------');
    _appSharedPreferences.getUserInformation().then((utilisateur){
      if(cller == 1) {
        showDialog(
         context: context,
         barrierDismissible: false,
         builder: (context) {
           return AlertDialog(
             backgroundColor: Colors.white,
             contentPadding: EdgeInsets.all(12),
             content: Column(
               mainAxisSize: MainAxisSize.min,
               children: <Widget>[
                 Text(allTranslations.text('refresh_processing')),
                 SizedBox(
                   height: 20,
                 ),
                 CircularProgressIndicator(
                   valueColor: AlwaysStoppedAnimation(Colors.green),
                 )
               ],
             ),
           );
          }
        );
      }

      SouscriptionDto _souscriptionDto = SouscriptionDto.create(
        accessToken: DataConstantesUtils.API_TOKEN,
        userToken: utilisateur.token
      );

      api.getTypeSouscription(_souscriptionDto).then((value){
        if(value.isRight()){
          value.all((typeSouscriptionResponse) {
            if(typeSouscriptionResponse != null && typeSouscriptionResponse.status.compareTo("000") == 0){
              if(typeSouscriptionResponse.allTypeSouscription.isEmpty){
                if(cller == 1){
                  Navigator.of(context).pop(null);
                }
                return false;
              }
              FunctionUtils.saveTypeSouscription(typeSouscriptionResponse.allTypeSouscription, utilisateur.id);
              Timer(Duration(seconds: 2),(){
                _getSouscriptionFromDatabase("0", 0);
                if(cller == 1){
                  Navigator.of(context).pop(null);
                }
              });  
              return true;
            }else{
              if(cller == 1){
                Navigator.of(context).pop(null);
              }
              return false;
            }
          });
        }else{
          if(cller == 1){
            Navigator.of(context).pop(null);
          }
          return false;
        }
      });
    });
  }

  _getTypeSouscritionListView(ConfigModel configModel, List<LocalSouscriptionEntity> _localSouscriptionEntity){
    return Stack(
      children: [
        _localSouscriptionEntity == null || _localSouscriptionEntity.length == 0 ? Center(
          child: Column(
            children: [
              SizedBox(height: 100),
              Material(
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                color: Colors.transparent,
                child: Image.asset('assets/img/splash.jpg',height:250),
              ),
              SizedBox(height: 0),
              Container(
                width: 300,
                child: Text(
                  allTranslations.text('searching_service_empty'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0
                  ),
                ),
              )
            ],
          ),
        ): Container(
          padding: EdgeInsets.all(10.0),
          child: ListView.builder(
            itemCount: _localSouscriptionEntity.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: (){
                  showDialog(
                    context: context,
                    builder: (BuildContext context){
                      return _seeMoreBox(_localSouscriptionEntity[index], sttSouscription);
                    }
                  );
                },
                child: Card(
                  child:  ListTile(
                    key: Key("${_localSouscriptionEntity[index].keyTypeSouscription}"),
                    leading: Text(
                      _localSouscriptionEntity[index].denoTypeSouscription,
                      style: TextStyle(
                        color: FunctionUtils.colorFromHex(configModel.mainColor),
                        fontWeight: FontWeight.w800,
                      ),
                      textAlign: TextAlign.left
                    ),
                    title: InkWell(
                      child: Text(
                        "Détails", 
                        textAlign: TextAlign.right,
                        style: TextStyle(color: Color(0xFF146C54))
                      ),
                    )
                  ),
                ),
              );
            },
          )
        )
      ],
    );
  }

  _getDenoType(List<LocalUserSouscriptionEntity> userSouscriptionEntity, int index){
    String tUsrSouscrip = "";
    tUsrSouscrip = userSouscriptionEntity[index].typeSouscription;
    denoKey.add(jsonDecode(tUsrSouscrip)['key_type_souscription'].toString());
    return jsonDecode(tUsrSouscrip)['deno_type_souscription'].toString();
  }

  _getUserSubscribeServices(List<LocalUserSouscriptionEntity> _localUserSouscriptionEntity){
    return Stack(
      children: [
        _localUserSouscriptionEntity == null || _localUserSouscriptionEntity.length == 0 ? Center(
          child: Column(
            children: [
              SizedBox(height: 100),
              Material(
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                color: Colors.transparent,
                child: Image.asset('assets/img/splash.jpg',height:250),
              ),
              SizedBox(height: 0),
              Container(
                width: 300,
                child: Text(
                  allTranslations.text('searching_emptyservice'),
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black54,
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0
                  ),
                ),
              )
            ],
          ),
        ): Container(
        padding: EdgeInsets.all(10.0),
        child: ListView.builder(
        itemCount: _localUserSouscriptionEntity.length,
          itemBuilder: (context, index) {
            return Container(
              child: Container(
                margin: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                constraints: BoxConstraints.expand(),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _localUserSouscriptionEntity[index].numeroSouscription,
                      style: TextStyle(
                        color: Color(0xFF146C54),
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    Container(height: 5.0),
                    Text(
                      //index.toString()
                      _getDenoType(_localUserSouscriptionEntity, index),
                      
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 8.0),
                      height: 2.0,
                      width: 30.0,
                      color: new Color(0xFF146C54)
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: Text("Date de début"),
                        ),
                        Expanded(
                            child: Text("Date d'échéance"),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                          Expanded(
                            child: Text(
                                FunctionUtils.convertPdateWithoutHour(_localUserSouscriptionEntity[index].datePaiement),
                              style: TextStyle(
                                color: Colors.green
                              )
                            ),
                          ),
                          Expanded(
                            child: Text(
                                FunctionUtils.convertPdateWithoutHour(_localUserSouscriptionEntity[index].dateFinsouscription),
                              style: TextStyle(
                                color: Colors.red
                              )
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                height: 120.0,
                margin: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 14.0),
                decoration: BoxDecoration(
                  color: Color(0xFFffffff),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(8.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 10.0,
                      offset: Offset(0.0, 10.0)
                    )
                  ]
                ),
              );
            },
          ),
        )
      ],
    );
  }

  _doSubscribe(String keyType, String service){
    _appSharedPreferences.getUserInformation().then((utilisateur){
      SouscriptionSendDto _souscriptionSendDto =  SouscriptionSendDto();
      _souscriptionSendDto.accessToken = DataConstantesUtils.API_TOKEN;
      _souscriptionSendDto.userToken   = utilisateur.token;
      _souscriptionSendDto.keyTypeSouscription = keyType;

      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "Souscription au service "+ service + " en cours...",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Color(0xFF146C54)),
                )
              ],
            ),
          );
        }
      );

      Api api = ApiRepository();
      api.sendSouscription(_souscriptionSendDto).then((data){
        if(data != null && data.isRight()){
          data.all((delta){
            if(delta.status.compareTo("000") == 0){
                _displaySnackBar(context, allTranslations.text('souscription_service_success'));
              _launchURL(delta.souscriptionInfo.urlPaiement);
              Timer(Duration(seconds: 2), (){
                Navigator.of(context).pop(null);
                return true;
              });
            }else if(delta.status.compareTo("001") == 0){
              _displaySnackBar(context, allTranslations.text('souscription_service_already'));
              Navigator.of(context).pop(null);
              return true;
            }else{
              _displaySnackBar(context, allTranslations.text('error_process'));
              Navigator.of(context).pop(null);
              return false;
            }
          });
        }else{
          Navigator.of(context).pop(null);
          return false;
        }
      });
    });
  }

  _displaySnackBar(BuildContext context, message){
    if(message == null){
      message = "Opération en cours...";
    }
    final snackBar = SnackBar(
      content: Text(message),
    );
    widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      Navigator.of(context).pop(null);
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _seeMoreBox(LocalSouscriptionEntity data, int statutScription){
      return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20)
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Stack(
      children: [
        Container(
          padding: EdgeInsets.only(left: 20, top: 65, right: 20, bottom: 20),
          margin: EdgeInsets.only(top: 40),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                offset: Offset(0,10), 
                blurRadius: 10
              ),
            ]
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                data.denoTypeSouscription,
                style: TextStyle(
                  fontSize: 18, 
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 15),
              Table(
                border: TableBorder.all(color: Colors.transparent),
                children: [
                  TableRow(
                    children: [
                      Text(
                        'Prix', 
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        'Durée',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ]),
                  TableRow(
                    children: [
                      Text(
                        data.prixSouscription.toString() + " FCFA",
                        style: TextStyle(
                          color: Colors.grey
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        data.dureeSouscription.toString() + " Jours",
                        style: TextStyle(
                          color: Colors.grey
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ])
                ],
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Align(
                    alignment: Alignment.bottomRight,
                    child:FlatButton(
                      onPressed: () {
                        _doSubscribe(data.keyTypeSouscription, data.denoTypeSouscription);
                        //Navigator.of(context).pop(null);
                      },
                      child: Text(
                        "Je souscris", 
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF146C54)
                        )
                      ))
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        "Annuler",
                        style: TextStyle(
                          fontSize: 13,
                          color: Colors.lightBlue
                        )
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ],
      )
    );
  }
}