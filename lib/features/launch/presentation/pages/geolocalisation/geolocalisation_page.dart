import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';


class GeolocalisationPage extends StatefulWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  _GeolocalisationPageState createState() => _GeolocalisationPageState();
}

class _GeolocalisationPageState extends State<GeolocalisationPage> {
  Completer<GoogleMapController> _controller = Completer();
  static const LatLng _center = const LatLng(45.521563, -122.677433);
  final Set<Marker> _markers = {};
  LatLng _lastMapPosition = _center;
  MapType _currentMapType = MapType.normal;

  void _onMapTypeButtonPressed(){
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }

  void _onAddMarkerButtonPressed(){
    setState((){
      _markers.add(Marker(
        markerId: MarkerId(_lastMapPosition.toString()),
        position: _lastMapPosition,
        infoWindow: InfoWindow(
          title: 'Ceci est un texte de test',
          snippet: 'Je fais un autre test ici',
        ),
        icon: BitmapDescriptor.defaultMarker,
      ));
    });
  }

  void _onCameraMove(CameraPosition position){
    _lastMapPosition = position.target;
  }

  void _onMapCreated(GoogleMapController controller){
    _controller.complete(controller);
  }

  @override
  Widget build(BuildContext context)  => Consumer<ConfigModel>(
    builder: (context, configModel, child){
      return Scaffold(
        key: widget._scaffoldKey,
        appBar: new AppBar(
          title:  Align(
            child: Container(
              child: Text("Géolocalisation des acteurs",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
            ),
          ),
          actions: [],
          backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
          elevation: 0.0,
        ),
        //backgroundColor: FunctionUtils.colorFromHex("DDDDDD"),
        body: Builder(builder: (BuildContext myContext){
            return Stack(
              children: [
                GoogleMap(
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: CameraPosition(
                    target: _center,
                    zoom: 11.0,
                  ),
                  mapType: _currentMapType,
                  markers: _markers,
                  onCameraMove: _onCameraMove,
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Column(
                      children: [
                        FloatingActionButton(
                          onPressed: _onMapTypeButtonPressed,
                          materialTapTargetSize: MaterialTapTargetSize.padded,
                          backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
                          child: const Icon(
                            Icons.map, 
                            size: 36.0
                          ),
                        ),
                        SizedBox(height: 16.0),
                        FloatingActionButton(
                          onPressed: _onAddMarkerButtonPressed,
                          materialTapTargetSize: MaterialTapTargetSize.padded,
                          backgroundColor: FunctionUtils.colorFromHex(configModel.mainColor),
                          child: const Icon(
                            Icons.add_location, 
                            size: 36.0
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            );
          }
        )
      );
    }
  );
}