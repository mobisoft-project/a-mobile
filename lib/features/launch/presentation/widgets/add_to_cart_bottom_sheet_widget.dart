import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/annonce_dto.dart';
import 'package:agrimobile/features/common/data/dto/contact_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

// e_commerce/lib/widget/add_to_cart_bottom_sheet.dart
class AddToCartBottomSheet extends StatefulWidget {

  AddToCartBottomSheet({
    this.infoAnnonce,
    this.scaffoldKey
  });
  // const AddToCartBottomSheet({Key key}) : super(key: key);

  LocalAnnonceEntity infoAnnonce;
  GlobalKey<ScaffoldState> scaffoldKey;

  @override
  _AddToCartBottomSheetState createState() => _AddToCartBottomSheetState();


}

class _AddToCartBottomSheetState extends State<AddToCartBottomSheet> {
  int _quantity = 1;
  TextEditingController _qteController=TextEditingController();
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();

  //_quantity = 0;
  //AppState state;
  // ...


  @override
  void initState() {
    super.initState();

    setState(() {
      _qteController.text = widget.infoAnnonce.stock.toString();
      _quantity = widget.infoAnnonce.stock;
    });
  }


  @override
  Widget build(BuildContext context) =>
      Consumer2<ConfigModel, CartModel>(
        builder: (context, configModel, cartModel, child) {

          String titre = widget.infoAnnonce.title!=null && widget.infoAnnonce.title.compareTo("")!=0 ? widget.infoAnnonce.title : widget.infoAnnonce.nomCategorieFilleProduit;
          String unite = widget.infoAnnonce.nomUnite;
          int maxQte = widget.infoAnnonce.stock;

          return ConstrainedBox(
              constraints: BoxConstraints(
                minWidth: MediaQuery
                    .of(context)
                    .size
                    .width,
                //minHeight: MediaQuery.of(context).size.height / 2,
                maxHeight: MediaQuery
                    .of(context)
                    .size
                    .height / 2,
              ),
              child:
              Container(
                padding: const EdgeInsets.all(50.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child:
                      Text("Ajouter ${titre} au panier : ${unite}", style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold),),
                    ),
                    SizedBox(height: 20.0,),
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            IconButton(icon: Icon(Icons.remove, size: 40.0),
                              onPressed: () => {
                              setState(() {
                                _quantity = int.parse(_qteController.text);
                                  if(_quantity > 1)_quantity -= 1;
                                  _qteController.text = _quantity.toString();
                                })
                              },
                            ),
                            SizedBox(width: 40.0,),
                            // decrease qty button
                            Expanded(
                              child:
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.black12,
                                 // borderRadius: new BorderRadius.circular(10.0),
                                ),
                                child:   TextField(
                                  controller: _qteController,
                                  keyboardType: TextInputType.number,
                                  cursorColor: Colors.black54,
                                  autofocus: true,
                                  onChanged: (text) {
                                    if(text.compareTo("")==0){
                                      setState(() {
                                        _qteController.text = "1";
                                      });
                                    }else{
                                      var value = int.parse(text);
                                      if(value < 1){
                                        setState(() {
                                          _qteController.text = "1";
                                        });
                                      }else{
                                        if(value > maxQte)value = maxQte;
                                        setState(() {
                                          _qteController.text = value.toString() ;
                                        });
                                      }
                                    }
                                  },
                                  decoration: InputDecoration(
                                    hintText:  allTranslations.text('qte_minimum'),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)
                                    ),

                                  ),
                                  style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold, color: Colors.black54),

                                ),
                              ),

                            ),

                            // Text("$_quantity", style: TextStyle(
                            //     fontSize: 20.0, fontWeight: FontWeight.bold)),
                            // current quanity
                            SizedBox(width: 40.0,),
                            IconButton(
                              icon: Icon(Icons.add, size: 40.0),
                              onPressed: () => {
                                setState(() {
                                  _quantity = int.parse(_qteController.text);
                                    if(_quantity < maxQte)_quantity += 1;
                                    _qteController.text = _quantity.toString();
                                  })
                              },
                            )
                            // increase qty button
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    RaisedButton(
                        color: FunctionUtils.colorFromHex(
                            configModel.mainColor),
                        textColor: Colors.white,
                        child: Text(
                          "Ajouter".toUpperCase(),
                        ),
                        onPressed: () => {
                          _addToCart(widget.infoAnnonce, cartModel)
                        }
                      //state.updateCartTotal(_quantity)
                    )
                  ],
                ),
              )

          );
        },
      );

  _addToCart(LocalAnnonceEntity annonce,CartModel _cartModel) async {

    String _message = "Produit bien ajouté";
    _quantity = int.parse(_qteController.text);

    print(_quantity);
    _cartModel.addToCart(annonce, _quantity);
    _appSharedPreferences.setCart(_cartModel.toMap());


    _displaySnackBar(context,_message );
    Navigator.of(context).pop(null);
    return false;
  }



  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
       widget.scaffoldKey.currentState.showSnackBar(snackBar);

  }
}
