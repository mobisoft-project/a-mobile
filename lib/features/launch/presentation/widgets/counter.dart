import 'package:flutter/material.dart';
import 'package:agrimobile/core/utils/colors.dart';

class Counter extends StatelessWidget {
  final int number;
  final Color color;
  final String title;



  const Counter({
    Key key,
    this.number,
    this.color,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    double fullsize=25;

    if(number>999){
      fullsize=25;
    }else if(number>9999){
      fullsize=20;
    }else if(number>99999){
      fullsize=15;
    }else if(number>999999){
      fullsize=10;
    }

    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(6),
          height: 25,
          width: 25,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: color.withOpacity(.26),
          ),
          child: Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.transparent,
              border: Border.all(
                color: color,
                width: 2,
              ),
            ),
          ),
        ),
        SizedBox(height: 10),
        Text(
          "$number",
          style: TextStyle(
            fontSize: fullsize,
            color: color,
          ),
        ),
        Text(title, style: kSubTextStyle),
      ],
    );
  }
}


