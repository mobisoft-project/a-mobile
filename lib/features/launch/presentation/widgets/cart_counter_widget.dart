import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/models/cart_item_model.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/allTranslations.dart';

class CartCounterWidget extends StatefulWidget {

  CartCounterWidget({
    this.item,
    this.qte,
    this.maxQte,
    this.indivisibilite,
    this.onrefresh,
  });

  final VoidCallback onrefresh;
  int qte;
  int maxQte;
  int indivisibilite;
  CartItemModel item;

  @override
  CartCounterWidgetState createState() => CartCounterWidgetState();
}

class CartCounterWidgetState extends State<CartCounterWidget> {
  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Consumer<CartModel>(
      builder: (context, cartModel,child) {
        int _currentAmount = widget.qte;

        return Row(
          children: <Widget>[
            GestureDetector(
              child: Container(
                width: 30.0,
                padding: const EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.black.withOpacity(0.5)
                ),
                child: Icon(
                  Icons.remove,
                  size: 15.0,
                  color: Colors.white,
                ),
              ),
              onTap: () {
              if(widget.indivisibilite.compareTo(1)==0){

                  if(widget.qte == 1){
                    _confirmDecremente(widget.item,cartModel);
                  }else{
                    _decremente(widget.item,cartModel);
                    setState(() {
                      widget.qte -= 1;
                    });
                  }
              }
              },
            ),
            SizedBox(width: 15),
            /*Expanded(
              child:
              Container(
                decoration: BoxDecoration(
                  color: Colors.black12,
                  // borderRadius: new BorderRadius.circular(10.0),
                ),
                child: TextField(
                  controller: _qteController,
                  keyboardType: TextInputType.number,
                  cursorColor: Colors.black54,
                  autofocus: true,
                  onChanged: (text) {
                    if(text.compareTo("")==0){
                      setState(() {
                        _qteController.text = "1";
                      });
                    }else{
                      var value = int.parse(text);
                      if(value < 1){
                        setState(() {
                          _qteController.text = "1";
                        });
                      }else{
                        if(value > widget.maxQte)value = widget.maxQte;
                        setState(() {
                          _qteController.text = value.toString() ;
                        });
                      }
                    }
                  },
                  /*decoration: InputDecoration(
                    //hintText:  allTranslations.text('qte_minimum'),
                    border: new OutlineInputBorder(
                        borderSide: new BorderSide(color: Colors.teal)
                    ),
                  ),*/
                  style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.bold, color: Colors.black54),
                ),
              ),
            ),*/

            GestureDetector(
              child: Text(
                "$_currentAmount",
                style: Theme
                    .of(context)
                    .textTheme
                    .title,
              ),
              onTap: () {
                _showQteAlert(context,_currentAmount,widget.maxQte);
              },
            ),
            SizedBox(width: 15),
            GestureDetector(
              child: Container(
                padding: const EdgeInsets.all(5.0),
                width: 30.0,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.black.withOpacity(0.5)
                ),
                child: Icon(
                  Icons.add,
                  size: 15.0,
                  color: Colors.white,
                ),
              ),
              onTap: () {
                if(widget.indivisibilite.compareTo(1)==0){
                if(widget.qte < widget.maxQte){
                  _incremente(widget.item,cartModel);
                  setState(() {
                    widget.qte += 1;
                  });
                }
              }
              },
            ),
          ],
        );
      }
  );


  _confirmDecremente(CartItemModel cartItemModel,CartModel cartModel) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        _decremente(cartItemModel,cartModel);
        setState(() {
          widget.qte -= 1;
        });
        Navigator.of(context).pop(null);
      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(
        allTranslations.text('decremente_delete_text'),
        style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }



  _showQteAlert(BuildContext context,int qte, int maxQte) {
    TextEditingController _qteController=TextEditingController();
    setState(() {
      _qteController.text = qte.toString();
    });
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {

      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content:Container(
        height: 105,
        child: Column(
          children: [
            Text(allTranslations.text('edit_qte'), style: new TextStyle( fontSize: 15.0),),
            SizedBox(
              height: 25,
            ),
            Container(
              decoration: BoxDecoration(
                color: Colors.black12,
                // borderRadius: new BorderRadius.circular(10.0),
              ),
              child:   TextField(
                enabled: widget.indivisibilite.compareTo(1)==0?true:false,
                controller: _qteController,
                keyboardType: TextInputType.number,
                cursorColor: Colors.black54,
                autofocus: true,
                onChanged: (text) {
                  if(text.compareTo("")==0){
                    setState(() {
                      _qteController.text = "1";
                    });
                  }else{
                    var value = int.parse(text);
                    if(value < 1){
                      setState(() {
                        _qteController.text = "1";
                      });
                    }else{
                      if(value > maxQte)value = maxQte;
                      setState(() {
                        _qteController.text = value.toString() ;
                      });
                    }
                  }
                },
                decoration: InputDecoration(
                  hintText:  allTranslations.text('qte_minimum'),
                  border: new OutlineInputBorder(
                      borderSide: new BorderSide(color: Colors.teal)
                  ),

                ),
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold, color: Colors.black54),

              ),
            ),
          ],
        ) ,
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


  _incremente(CartItemModel cartItemModel,CartModel cartModel) async {
    cartModel.addToCart(cartItemModel.annonce, 1);
    _appSharedPreferences.setCart(cartModel.toMap());
    widget.onrefresh();
  }

  _decremente(CartItemModel CartItemModel,CartModel cartModel) async {
    cartModel.decreaseQte(CartItemModel,1);
    _appSharedPreferences.setCart(cartModel.toMap());
    widget.onrefresh();
  }

}