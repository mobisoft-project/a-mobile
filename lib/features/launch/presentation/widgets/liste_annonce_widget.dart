
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/launch/presentation/widgets/annonce_widget.dart';
import 'package:flutter/material.dart';

class ListeAnnonceWidget extends StatefulWidget {

  ListeAnnonceWidget({
    this.allAnnonce,
    this.scoop,
    this.typePage,
    this.biztype,
    this.annonceContact,
    this.userKey,
    this.idSearch,
    this.scaffoldKey,
    this.onrefresh,
  });

  final VoidCallback onrefresh;
  List<LocalAnnonceEntity> allAnnonce;
  List<String>annonceContact;
  GlobalKey<ScaffoldState> scaffoldKey;
  String userKey;
  String typePage;
  String idSearch;
  LocalScoopEntity scoop;
  int biztype;


  @override
  ListeAnnonceWidgetState createState() => ListeAnnonceWidgetState();
}

class ListeAnnonceWidgetState extends State<ListeAnnonceWidget> {

  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {

    return  ListView.builder(
        itemCount: widget.allAnnonce.length,
        shrinkWrap: true,
        // 1st add
        physics: ClampingScrollPhysics(),
        itemBuilder: (BuildContext context, int position) {

          return AnnonceWidget(
            infoAnnonce:widget.allAnnonce[position],
            scaffoldKey:  widget.scaffoldKey,
            userKey: widget.userKey,
            typeAssociation: widget.scoop.typeAssociation,
            typePage: widget.typePage,
            userScoopRole: widget.scoop.roleMember,
            biztypeId: widget.biztype,
            onrefresh: (){
              _localAnnonceRepository.listAnnonce(widget.typePage,widget.userKey,widget.scoop.idAssociation,widget.biztype).then((response) {
                widget.allAnnonce = response;
                setState(() {
                });
                widget.onrefresh();
              });
            },
          );

        });

  }



}
