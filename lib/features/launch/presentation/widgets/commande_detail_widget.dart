import 'dart:io';

import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_commande_detail_entity.dart';
import 'package:agrimobile/features/common/data/dto/commande_delete_dto.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'dart:core';
import 'package:url_launcher/url_launcher.dart';
import 'package:agrimobile/features/launch/presentation/pages/commande/liste_commande_detail_page.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

  class CommandeDetailWidget extends StatefulWidget {

    CommandeDetailWidget({
    this.infoCommandeDetail,
      this.onrefresh,
  });
    final VoidCallback onrefresh;
    LocalCommandeDetailEntity infoCommandeDetail;

  @override
  CommandeDetailWidgetState createState() => CommandeDetailWidgetState();
}

class CommandeDetailWidgetState extends State<CommandeDetailWidget> {

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  //LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      String _qte = widget.infoCommandeDetail.qteProduitCommandeDetail+' x '+ widget.infoCommandeDetail.prixUnitaireCommandeDetail;


      List<String> recup_unite = widget.infoCommandeDetail.prixTotalCommandeDetail.split(" ${configModel.devise}");

      int annonce_commande =  int.parse(recup_unite[0]);
      double annonce_commande_final = annonce_commande * 1.0;
      print(annonce_commande_final);

      return  Container(
        width: 200,
        child:
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
              Text(widget.infoCommandeDetail.nomProduitCommandeDetail,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                ),
              ),
            SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(_qte,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                       // fontWeight: FontWeight.w700,
                        fontSize: 12,
                        color: Colors.black,
                      )),
                  Container(
                    //width: 50,
                    height: 25,
                    padding: EdgeInsets.all(5.0),
                    child: Text(
                        "${FunctionUtils.separateur(annonce_commande_final)} ${configModel.devise}",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 12,
                          color: Colors.black,
                        )),
                  )
                ],
              ),
          ],
        ),
      );
    },
  );


  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }



}
