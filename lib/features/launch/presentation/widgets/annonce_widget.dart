import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/annonce_dto.dart';
import 'package:agrimobile/features/common/data/dto/contact_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/add_annonce_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:agrimobile/features/common/data/models/cart_model.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:agrimobile/features/launch/presentation/widgets/add_to_cart_bottom_sheet_widget.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';
import 'dart:async';
import 'package:agrimobile/features/common/data/models/annonce_model.dart';
import 'package:agrimobile/features/common/data/models/user_information_model.dart';


class AnnonceWidget extends StatefulWidget {

  AnnonceWidget({
    this.infoAnnonce,
    this.userKey,
    this.scaffoldKey,
    this.typeAssociation,
    this.userScoopRole,
    this.typePage,
    this.biztypeId,
    this.onrefresh,
  });

  final VoidCallback onrefresh;
  LocalAnnonceEntity infoAnnonce;
  GlobalKey<ScaffoldState> scaffoldKey;
  String userKey;
  String typeAssociation;
  String typePage;
  int biztypeId;
  String userScoopRole;

  @override
  AnnonceWidgetState createState() => AnnonceWidgetState();
}

class AnnonceWidgetState extends State<AnnonceWidget> {

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  int _quantity = 1;
  bool _enabled = true;
  bool  _defaultCondition=false;
  TextEditingController _qteController=TextEditingController();
  FocusNode fn_qte;
  final controller = ScrollController();

  @override
  void initState() {
    super.initState();
    fn_qte = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    fn_qte.dispose();
  }



  @override
  Widget build(BuildContext context) => Consumer3<ConfigModel,CartModel,UserInformationModel>(
    builder: (context, configModel,cartModel,userConnected, child) {
      String _imageName="";
      String _serverImageName="";
      String countryDraf = "";
      String type = "";
      String phrasetype = "";

      LocalAnnonceEntity localAnnonce=widget.infoAnnonce;
      List<String> admin_roles = ["3", "4"];
      _serverImageName="";

      _imageName=localAnnonce.picture;
      if(_imageName.compareTo("")!=0){
        _serverImageName =DataConstantesUtils.IMAGE_ANNONCE_URL + "" + _imageName;
      }
      countryDraf=localAnnonce.paysAlpha;
      type="";
      if(localAnnonce.biztypeId.compareTo(1)==0) {
        //type = "offre";
        type = "vente";

        phrasetype = "${allTranslations.text('offre_immediat')}";
        if(localAnnonce.disponibilite.compareTo(1)!=0) {
          phrasetype = "${allTranslations.text('offre_ulterieur')} ${localAnnonce.moisDisponibilite}";
        }
      }else {
       // type="demande";
        type="achat";

        phrasetype = "${allTranslations.text('demande_immediat')}";
        if(localAnnonce.disponibilite.compareTo(1)!=0) {
          phrasetype = "${allTranslations.text('demande_ulterieur')} ${localAnnonce.moisDisponibilite}";
        }

      }
      int annonce_price = localAnnonce.prix;
      double annonce_price_final = annonce_price * 1.0;



      Widget image=Container(
          constraints: new BoxConstraints.expand(
            height: 300.0,
          ),

          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: NetworkImage(_serverImageName),
              fit: BoxFit.cover,
            ),
          ),
          child: localAnnonce.autorisation.compareTo(1)==0?new Stack(
            children: <Widget>[
              if(widget.userKey.compareTo("${localAnnonce.userId}")!=0) new Positioned(
                bottom: 0.0,
                child: Container(
                  // height: 70,
                  width: MediaQuery.of(context).size.width-10,
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    color:(FunctionUtils.colorFromHex(configModel.mainColor)).withOpacity(0.7),
                  ),
                  child: Text(localAnnonce.autorisation.compareTo(1)==0?"${allTranslations.text('use_contact')} ${localAnnonce.contact}":
                  "${allTranslations.text('use_number1')} ${localAnnonce.identifiant} ${allTranslations.text('use_number2')} " ,
                    style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white,fontSize: 13.5),
                  ),
                ),
              ),
            ],
          ):Container()
      );

      return SingleChildScrollView(
          controller: controller,
          child:Container(
        margin: EdgeInsets.only(bottom:20),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5.0)
        ),
        child: Column(
          children: [
            Container(
              padding:  EdgeInsets.only(left:10,right:10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: FunctionUtils.colorFromHex(configModel.mainColor),
                ),
                borderRadius: BorderRadius.only(topLeft: Radius.circular(5),topRight: Radius.circular(5)),
                color: FunctionUtils.colorFromHex(configModel.mainColor),
              ),
              height: 40,
              child: Row(
                children: [
                  Image.asset("assets/gif/$countryDraf.gif",width: 20,height: 20,),
                  Expanded(
                    child: Text(" - ${localAnnonce.nomCategorieFilleProduit}",style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 13,
                      color: Colors.white,
                    )),
                  ),
                  /*GestureDetector(
                    onTap: () {

                    },
                    child:Icon(Icons.verified_user_outlined,size: 20.0,color: Colors.white)
                ),*/
                  SizedBox(width: 5,),
                  Container(
                    //width: 50,
                    height: 20,
                    padding: EdgeInsets.only(top:3,left:3,right:3),
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(5.0),
                      color: localAnnonce.biztypeId.compareTo(1)==0?FunctionUtils.colorFromHex(configModel.offreColor):FunctionUtils.colorFromHex(configModel.demandeColor),
                    ),
                    child: Text(allTranslations.text('$type'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 12,
                          color: Colors.white,
                        )),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left:10,right:10),
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              height: 40,
              child: Align(
                alignment: Alignment.center, // Align however you like (i.e .centerRight, centerLeft)
                child: Text(phrasetype,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 12,
                      color: FunctionUtils.colorFromHex(configModel.mainColor),
                    )),
              ),
            ),

            localAnnonce.bio.compareTo(1)==0?Container(
              child: ClipRect(
                child: GestureDetector(
                    onTap: () {
                      _showAlertDialog(context,localAnnonce,"5",configModel);
                    },
                    child:Banner(
                      message: allTranslations.text('bio'),
                      location: BannerLocation.topEnd,
                      color: FunctionUtils.colorFromHex(configModel.bioColor),
                      child: image,
                    )
                ),
              ),
            ):image,


            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: Colors.white,
                ),
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5),bottomRight: Radius.circular(5)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  if(widget.typePage.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE)==0 && admin_roles.contains(widget.userScoopRole))Text.rich(
                    TextSpan(
                      text: "${allTranslations.text('type_association${localAnnonce.statutPrive}')}",
                      style: TextStyle(fontSize: 14, color: localAnnonce.statutPrive==1?FunctionUtils.colorFromHex(configModel.mainColor):Colors.red,fontWeight: FontWeight.bold,),
                    ),
                  ),
                  if(localAnnonce.title!=null && localAnnonce.title.compareTo("")!=0)Text.rich(
                    TextSpan(
                      text: "${allTranslations.text('variete')} : ",
                      style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,),
                      children:
                      <TextSpan>[
                        TextSpan(
                          text: localAnnonce.title,
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.black54,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      text: "${allTranslations.text('lieu')} : ",
                      style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,),
                      children:
                      <TextSpan>[
                        TextSpan(
                          text: localAnnonce.lieu,
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.black54,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      text: "${allTranslations.text('qte')} : ",
                      style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,),
                      children:
                      <TextSpan>[
                        TextSpan(
                          text: "${localAnnonce.stock} ${localAnnonce.nomUnite}",
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      text: "${allTranslations.text('prix_unitaire')} : ",
                      style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,),
                      children:
                      <TextSpan>[
                        TextSpan(
                          text:  "${FunctionUtils.separateur(annonce_price_final)} ${configModel.devise}",
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: FunctionUtils.colorFromHex(configModel.mainColor),
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text.rich(
                    TextSpan(
                      text: "${allTranslations.text('identifiant')} : ",
                      style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,),
                      children:
                      <TextSpan>[
                        TextSpan(
                          text: localAnnonce.uniqueCode,
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.black54,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ),
                  if(widget.userKey.compareTo("${localAnnonce.userId}")!=0 && localAnnonce.noteVendeur >0)
                    Row(
                      children: [
                        Text("${allTranslations.text('identifiant')} : ",style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,)),
                        GestureDetector(
                            onTap: () {
                              _showAlertDialog(context,localAnnonce,"3",configModel);
                            },
                            child:RatingBar.builder(
                                initialRating: localAnnonce.noteVendeur*1.0,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemSize:20,
                                itemCount: 5,
                                itemPadding: EdgeInsets.symmetric(horizontal: 1),
                                itemBuilder: (context, _) =>
                                    Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                      size: 18,
                                    )
                            )
                          /**/
                        )
                      ],
                    ),
                  Row(
                    children: [
                      Expanded(
                        child:  Text.rich(
                          TextSpan(
                            text: "${allTranslations.text('expire')} : ",
                            style: TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold,),
                            children:
                            <TextSpan>[
                              TextSpan(
                                text: FunctionUtils.expireDate(localAnnonce.time,localAnnonce.categorieDuree) ,
                                style: TextStyle(
                                  decoration: TextDecoration.none,
                                  color: Colors.black54,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      if(widget.userKey.compareTo("${localAnnonce.userId}")!=0 && localAnnonce.biztypeId.compareTo(1)==0 && widget.typePage.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE)!=0 )Row(
                        children: [
                          SizedBox(width: 15),
                          /* GestureDetector(
                              onTap: () {

                                //_showQuickAddToCart(context, localAnnonce, widget.scaffoldKey);
                                _showAddToCartDialog(configModel,cartModel,annonce_price_final);
                              },
                              child:Icon(Icons.shopping_cart_outlined ,size: 20.0)
                          ), */
                        ],
                      ),
                      GestureDetector(
                          onTap: () {
                            FunctionUtils.shareAnnonce(localAnnonce,configModel);
                          },
                          child:Row(
                            children: [
                              SizedBox(width: 15,),
                              Icon(Icons.share ,size: 20.0,color: FunctionUtils.colorFromHex(configModel.mainColor))
                            ],
                          )
                      ),
                      if(widget.typePage.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE)==0 && admin_roles.contains(widget.userScoopRole))GestureDetector(
                          onTap: () {
                           // FunctionUtils.shareAnnonce(localAnnonce,configModel);
                            _showVisbleDialog(context,localAnnonce,configModel);
                          },
                          child:Row(
                            children: [
                              SizedBox(width: 15,),
                              Icon(Icons.remove_red_eye ,size: 20.0,color: FunctionUtils.colorFromHex(configModel.mainColor))
                            ],
                          )
                      ),
                      if(widget.userKey.compareTo("${localAnnonce.userId}")==0 && widget.typePage.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE)!=0 )Row(
                        children: [
                          SizedBox(width: 15,),
                          GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => AddAnnoncePage("${localAnnonce.apiId}",widget.biztypeId)),
                                ).then((value){
                                  if(value!=null){
                                    widget.onrefresh();
                                    if(value.compareTo("updated")==0){
                                      _displaySnackBar(context, allTranslations.text('uannonce_success'));
                                    }else{
                                      _displaySnackBar(context, allTranslations.text('annonce_success'));
                                    }
                                  }
                                });
                              },
                              child:Icon(Icons.mode_edit ,size: 20.0,color: FunctionUtils.colorFromHex(configModel.mainColor))
                          ),
                        ],
                      ),
                      if(widget.userKey.compareTo("${localAnnonce.userId}")==0 && widget.typePage.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE)!=0 )Row(
                        children: [
                          SizedBox(width: 15,),
                          GestureDetector(
                              onTap: () {
                                _showAlertDialog(context,localAnnonce,"1",configModel);
                              },
                              child:Icon(Icons.delete_outline,size: 20.0,color: Colors.red)
                          ),
                        ],
                      ),
                      if(widget.userKey.compareTo("${localAnnonce.userId}")!=0 && widget.typePage.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE)!=0 )Row(
                        children: [
                          SizedBox(width: 15,),
                          GestureDetector(
                              onTap: () {
                                if(localAnnonce.autorisation.compareTo(0)==0) {


                                  AppSharedPreferences appSharedPreferences = AppSharedPreferences();
                                  if(userConnected.id.compareTo(-1)==0) {
                                    //recuperation du menu et comparer son type
                                    appSharedPreferences.setRoute("M081","2",localAnnonce.apiId).then((value){
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => LoginPage("0")),
                                      );
                                    });

                                  }else{
                                    if(userConnected.validationUser.compareTo("1")==0) {
                                      _showAlertDialog(context, localAnnonce, "2",configModel);
                                    }else{
                                      _displaySnackBar(context, allTranslations.text('waiting_validation'));
                                    }
                                  }
                                }else{
                                  UrlLauncher.launch('tel:+${localAnnonce.contact}');
                                }
                              },
                              child:localAnnonce.autorisation.compareTo(0)==0?Icon(Icons.phonelink_lock,size: 20.0,color: Colors.blue):Icon(Icons.phone,size: 20.0,color: Colors.green)
                          ),
                        ],
                      ),
                      if(widget.userKey.compareTo("${localAnnonce.userId}")!=0 && localAnnonce.certificate.compareTo(1)==0)Row(
                        children: [
                          SizedBox(width: 15,),
                          GestureDetector(
                            onTap: () {
                              _showAlertDialog(context,localAnnonce,"4",configModel);
                            },
                            child:Image.asset("assets/img/certificate.png",width: 30,height: 30,),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        ),
      );
    },
  );

  Widget setupAlertDialoadContainer(BuildContext context, LocalAnnonceEntity annonce,ConfigModel configModel) {
    List<int> intArr = [1,2,3];
    intArr.remove(annonce.statutPrive);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[

        Text("Changer le statut ",style: TextStyle(
            color:Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.bold
        ),),
        SizedBox(height: 20.0,),
        Padding(
          padding: const EdgeInsets.all(0.1),

          child:ListView.builder(
            shrinkWrap: true,
            itemCount: 2,
            itemBuilder: (BuildContext context, int index) {
              int statut = intArr[index];
              return FlatButton(

                child:Container(
                    alignment: Alignment.centerLeft,
                  child:  Text(allTranslations.text('type_association${statut}')),
                ),

                onPressed: () {
                    Navigator.of(context).pop(null);
                    _showAlertDialogStatutPrive(context,annonce,statut,configModel);
                },
              );
            },
          )
        )

      ],
    );
  }

  _showVisbleDialog(BuildContext context,LocalAnnonceEntity annonce,ConfigModel configModel) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);

      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: setupAlertDialoadContainer(context, annonce, configModel),
      actions: [
        cancelButton,
        //continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _showAlertDialog(BuildContext context,LocalAnnonceEntity annonce,String type,ConfigModel configModel) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text( (type.compareTo("1")==0 || type.compareTo("2")==0)?allTranslations.text('cancel'):allTranslations.text('ok'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);

        if(type.compareTo("1")==0) {
          _confirmeDelete(annonce,configModel);
        }else{
          _confirmeContact(annonce,configModel);
        }

      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(
        type.compareTo("0")==0?allTranslations.text('contact_annonce_text'):
        type.compareTo("2")==0?allTranslations.text('contact_annonce_text'):
        type.compareTo("3")==0?allTranslations.text('note_annonce_text'):
        type.compareTo("4")==0?allTranslations.text('certification_expert'):
        type.compareTo("5")==0?allTranslations.text('bio_expert'):
        allTranslations.text('delete_annonce_text'),
        style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        if(type.compareTo("1")==0 || type.compareTo("2")==0)continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _showAlertDialogStatutPrive(BuildContext context,LocalAnnonceEntity annonce,int statut,ConfigModel configModel) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
          // Navigator.of(context).pop(null);
          _confimeChangeStatutPrive(annonce,configModel,statut);
      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(
        allTranslations.text('statut_annonce_text'),
        style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
       continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _confirmeDelete(LocalAnnonceEntity annonce,ConfigModel configModel) async {
    AnnonceDto _annonceDto = AnnonceDto();

    _annonceDto.etat="2";
    _annonceDto.photo="0";
    _annonceDto.apiId="${annonce.apiId}";
    _annonceDto.userId="${annonce.myApiId}";


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('adelete_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.sendAnnonce(_annonceDto).then((value) {
      if (value.isRight()) {
        value.all((a) {

          if(a.status.compareTo("ok")==0){


            _localAnnonceRepository.delete(annonce.idAnnonce,"idAppAnnonce").then((value){
              widget.onrefresh();
            });

            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('delete_annonce_success'));
            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
  }

  _confirmeContact(LocalAnnonceEntity annonce,ConfigModel configModel) async {

    _appSharedPreferences.getUserInformation().then((value) {

      ContactDto _contactDto = ContactDto();

      _contactDto.annonceId="${annonce.apiId}";
      _contactDto.userId=widget.userKey;
      _contactDto.accessToken=value.token;

      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return AlertDialog(
              backgroundColor: Colors.white,
              contentPadding: EdgeInsets.all(12),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(allTranslations.text('contact_processing')),
                  SizedBox(
                    height: 20,
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                  )
                ],
              ),
            );
          });

      Api api = ApiRepository();
      api.getContact(_contactDto).then((value) {
        if (value.isRight()) {
          value.all((a) {

            if(a.status!=null && a.status.compareTo("ok")==0){

              annonce.autorisation=1;
              _localAnnonceRepository.update(annonce,annonce.idAnnonce).then((value){
                widget.onrefresh();
              });

              Navigator.of(context).pop(null);
              Widget cancelButton = FlatButton(
                child: Text(allTranslations.text('ok'),style: new TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 14.0),
                ),
                onPressed:  () {
                  Navigator.of(context).pop(null);
                },
              );

              AlertDialog alert = AlertDialog(
                content: Text(a.notif),
                actions: [
                  cancelButton,
                ],
              );
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return alert;
                },
              );
              return true;

            }else{
              Navigator.of(context).pop(null);
              _displaySnackBar(context, allTranslations.text('error_process'));
              return false;
            }
          });
        }else{
          Navigator.of(context).pop(null);
          _displaySnackBar(context, allTranslations.text('error_process'));
          return false;
        }
      });

    });


  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget.scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _showQuickAddToCart(BuildContext context, LocalAnnonceEntity annonce ,GlobalKey<ScaffoldState> scaffold ) async {

    int qty = await showModalBottomSheet<int>(
        context: context,
        builder: (BuildContext context) {
          return AddToCartBottomSheet(
              infoAnnonce: annonce,
              scaffoldKey: scaffold
          );
        }).then((value) {
      debugPrint('666');
      widget.onrefresh();
    });

    //_addToCart(product, qty, _cartBloc);
  }

  void _showAddToCartDialog(ConfigModel configModel,CartModel cartModel,double annonce_price_final){
    bool existAnnonce = cartModel.findInCart(widget.infoAnnonce);

    if(existAnnonce==true){
      _displaySnackBar(context,allTranslations.text('already_exist_incart') );
    }else{

    String titre = widget.infoAnnonce.title!=null && widget.infoAnnonce.title.compareTo("")!=0 ? widget.infoAnnonce.title : widget.infoAnnonce.nomCategorieFilleProduit;
    String unite = widget.infoAnnonce.nomUnite;
    int maxQte = widget.infoAnnonce.stock;

    setState(() {
      _qteController.text = widget.infoAnnonce.stock.toString();
      _quantity = widget.infoAnnonce.stock;
    });


    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0,color: Colors.grey),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('add'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0,color:FunctionUtils.colorFromHex(configModel.mainColor)),
      ),
      onPressed:  () {
        _addToCart(widget.infoAnnonce, cartModel);
        widget.onrefresh();
      },
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {

        return AlertDialog(
          content: StatefulBuilder(  // You need this, notice the parameters below:
            builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                  Container(
                      alignment: Alignment.center,
                      child: Text("Ajouter ${titre} au panier",style: TextStyle(
                          color:Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                      ),)
                  ),
                    SizedBox(height: 20.0,),
                    Container(
                        alignment: Alignment.center,
                        child: Text(allTranslations.text('prix_unitaire')+" : ${FunctionUtils.separateur(annonce_price_final)} ${configModel.devise}/${unite}",style: TextStyle(
                          color:Colors.black,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                        ),)
                    ),
                  if(widget.infoAnnonce.indivisibilite.compareTo(1)!=0)Container(
                    padding: EdgeInsets.only(top: 20.0),
                    alignment: Alignment.center,
                    child: Text(allTranslations.text('message_indivisibilite'),style: TextStyle(
                        color:Colors.red,
                        fontSize: 13,
                        fontWeight: FontWeight.w500
                    ),)
                  ),

                    SizedBox(height: 10.0,),
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            IconButton(icon: Icon(Icons.remove, size: 20.0),
                              onPressed: () => {
                              if(widget.infoAnnonce.indivisibilite.compareTo(1)==0){
                                setState(() {
                                  _quantity = int.parse(_qteController.text);
                                  if(_quantity > 1)_quantity -= 1;
                                  _qteController.text = _quantity.toString();
                                })
                              }
                              },
                            ),
                            SizedBox(width: 10.0,),
                            // decrease qty button
                            Expanded(

                              child:
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.black12,
                                  // borderRadius: new BorderRadius.circular(10.0),
                                ),
                                child:   TextField(
                                  enabled: widget.infoAnnonce.indivisibilite.compareTo(1)==0?true:false,
                                  //controller: _qteController,
                                  controller: new TextEditingController.fromValue(new TextEditingValue(text: _qteController.text,selection: new TextSelection.collapsed(offset: _qteController.text.length))),

                                  keyboardType: TextInputType.number,
                                  cursorColor: Colors.black54,
                                  autofocus: true,
                                  onChanged: (text) {

                                    if(text.compareTo("")==0){
                                      setState(() {
                                        _qteController.text = "1";
                                      });
                                    }else{
                                      var value = int.parse(text);
                                      if(value < 1){
                                        setState(() {
                                          _qteController.text = "1";
                                        });
                                      }else{
                                        if(value > maxQte)value = maxQte;
                                        setState(() {
                                          _qteController.text = value.toString() ;
                                        });
                                      }
                                    }
                                  },
                                  decoration: InputDecoration(
                                    hintText:  allTranslations.text('qte_minimum'),
                                    border: new OutlineInputBorder(
                                        borderSide: new BorderSide(color: Colors.teal)
                                    ),

                                  ),
                                  style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold, color: Colors.black54),

                                ),
                              ),

                            ),

                            // Text("$_quantity", style: TextStyle(
                            //     fontSize: 20.0, fontWeight: FontWeight.bold)),
                            // current quanity
                            SizedBox(width: 10.0,),
                            IconButton(
                              icon: Icon(Icons.add, size: 20.0),
                              onPressed: () => {
                              if(widget.infoAnnonce.indivisibilite.compareTo(1)==0){
                                setState(() {
                                  _quantity = int.parse(_qteController.text);
                                  if(_quantity < maxQte)_quantity += 1;
                                  _qteController.text = _quantity.toString();
                                })
                              }
                              },
                            )
                            // increase qty button
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0,),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Checkbox(
                              value:_defaultCondition,
                              activeColor: FunctionUtils.colorFromHex(configModel.mainColor)  ,
                              onChanged:(bool newValue) {
                                setState(() {
                                  _defaultCondition=newValue;
                                });
                              }
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _defaultCondition=!_defaultCondition;
                              });
                            },
                            child:Text(allTranslations.text('propose_price'),style: TextStyle(fontSize: 14, color: FunctionUtils.colorFromHex(configModel.mainColor))),
                          )
                        ]
                    ),
                    if(_defaultCondition == true)Container(
                      height: 100.0,
                      color: Colors.red,
                      child : Column(
                        children: [
                             SizedBox(height: 20.0),
                             Divider(height: 10)
                          ],
                        )
                    )
                  ],
                ),
              );
            },
          ),
          actions: [
            cancelButton,
            continueButton,
          ],
        );
      },
    );
  }
  }

  _addToCart(LocalAnnonceEntity annonce,CartModel _cartModel) async {

    String _message = "Produit bien ajouté";
    _quantity = int.parse(_qteController.text);

    print(_quantity);
    _cartModel.addToCart(annonce, _quantity);
    _appSharedPreferences.setCart(_cartModel.toMap());


    _displaySnackBar(context,_message );
    Navigator.of(context).pop(null);
    return false;
  }

  _confimeChangeStatutPrive(LocalAnnonceEntity annonce,ConfigModel configModel, int statut) async {
    AnnonceDto _annonceDto = AnnonceDto();

    _annonceDto.etat="3";
    _annonceDto.statutPrive=statut;
    _annonceDto.photo="0";
    _annonceDto.apiId="${annonce.apiId}";
    // _annonceDto.userId="${annonce.myApiId}";


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('astatutchange_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.sendAnnonce(_annonceDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          print(a.toString());
          if(a.status.compareTo("ok")==0){

            List<AnnonceModel> allAnnonce=[];
            //allAnnonce.add(a.annonce);

            annonce.statutPrive=statut;
            _localAnnonceRepository.update(annonce,annonce.idAnnonce).then((value){
              widget.onrefresh();

              widget.onrefresh();
                Navigator.of(context).pop(null);
                Navigator.of(context).pop(null);

                _displaySnackBar(context, allTranslations.text('state_annonce_success'));
              widget.onrefresh();
            });

            return true;

          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
  }


/*_showQuickAddToCart(BuildContext context, Product product) async {
    //CartBloc _cartBloc = AppState.of(context).blocProvider.cartBloc;

    int qty = await showModalBottomSheet<int>(
        context: context,
        builder: (BuildContext context) {
          return AddToCartBottomSheet(
            key: Key(product.id),
          );
        });

    _addToCart(product, qty, _cartBloc);
  }*/





}
