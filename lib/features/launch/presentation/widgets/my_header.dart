import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/domain/repositories/local_menu_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/notification/notification_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/liste_annonce_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/launch/presentation/pages/account/account_page.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/prix_marche_page.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';

class MyHeader extends StatefulWidget {
  final String userKey;
  final String userValidation;
  final String image;
  final String textTop;
  final String textBottom;
  final double offset;
  final GlobalKey pKey;
  final GlobalKey<ScaffoldState> scaffoldKey;
  const MyHeader(
      {Key key, this.image, this.textTop, this.userKey, this.userValidation, this.textBottom, this.offset, this.pKey, this.scaffoldKey})
      : super(key: key);

  @override
  _MyHeaderState createState() => _MyHeaderState();
}


class _MyHeaderState extends State<MyHeader> {

  // final GlobalKey popupButtonKey = GlobalKey<State>();
  LocalMenuRepository _localMenuRepository = LocalMenuRepository();
  final List<PopupMenuEntry> menuEntries = [
    PopupMenuItem(
      value: 1,
      child: Text(allTranslations.text('menu_marche')),
    ),
    PopupMenuItem(
      value: 2,
      child: Text(allTranslations.text('menu_vendeurs')),
    ),/* 
    PopupMenuItem(
      value: 5,
      child: Text(allTranslations.text('menu_achat')),
    ), */
    PopupMenuItem(
      value: 3,
      child: Text(allTranslations.text('menu_mprice')),
    ),/* 
    PopupMenuItem(
      value:6,
      child: Text(allTranslations.text('menu_notification')),
    ), */
    PopupMenuItem(
      value: 4,
      child: Text(allTranslations.text('menu_account')),
    ),
  ];




  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return ClipPath(
        clipper: MyClipper(),
        child: Container(
          padding: EdgeInsets.only(left: 10, top: 0, right: 20),
          height: 350,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                FunctionUtils.colorFromHex(configModel.mainColor),
                FunctionUtils.colorFromHex(configModel.mainColor)
                //Color(0xFF11249F),
              ],
            ),
            /* image: DecorationImage(
            image: AssetImage(widget.image),
          ),*/
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[

              SizedBox(height: 20),
              Expanded(
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: (widget.offset < 0) ? 0 : widget.offset,
                      child: Image.asset(
                        widget.image,
                        width: 230,
                        fit: BoxFit.fitWidth,
                        alignment: Alignment.topCenter,
                      ),
                    ),
                    if(widget.userKey.compareTo("-1")==0)Positioned(
                      top: 25,
                      right: 40,
                      child: GestureDetector(
                        onTap: () {
                          AppSharedPreferences appSharedPreferences = AppSharedPreferences();
                          appSharedPreferences.setRoute("","1",0).then((value){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => LoginPage("0")),
                            );
                          });

                        },
                        child: Container(
                          padding: EdgeInsets.only(top:0),
                          child: SvgPicture.asset("assets/img/user.svg",width: 23,color: Colors.white,),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 30,
                      right: 0,
                      child: GestureDetector(
                        onTap: () {
                          final RenderBox popupButtonObject = widget.pKey.currentContext.findRenderObject();
                          final RenderBox overlay = Overlay.of(context).context.findRenderObject();
                          final RelativeRect position = RelativeRect.fromRect(
                              Rect.fromPoints(
                                popupButtonObject.localToGlobal(Offset.zero, ancestor: overlay),
                                popupButtonObject.localToGlobal(popupButtonObject.size.bottomRight(Offset.zero), ancestor: overlay),
                              ),
                              new Rect.fromLTWH(-100.0, -50.0, overlay.size.width, overlay.size.height)
                          );

                          // Finally, show the menu.
                          showMenu(
                            context: context,
                            elevation: 8.0, // default value
                            items: menuEntries,
                            initialValue: null,
                            position: position,
                          ).then((res) {
                            int venteBiztypeId = 1;
                            int achatBiztypeId = 2;
                            if(res==1){
                              FunctionUtils.pressedMenu(context,widget.userKey,widget.userValidation,"M001",MyAnnoncePage(DataConstantesUtils.ANNONCE_MARCHE,0,0,venteBiztypeId),widget.scaffoldKey,0);
                            }else if(res==2){
                              FunctionUtils.pressedMenu(context,widget.userKey,widget.userValidation,"M005",MyAnnoncePage(DataConstantesUtils.ANNONCE_VENDEUR,int.parse(widget.userKey),0,venteBiztypeId),widget.scaffoldKey,0);
                            }else if(res==3){
                              FunctionUtils.pressedMenu(context,widget.userKey,widget.userValidation,"M009",PrixMarchePage(),widget.scaffoldKey,0);
                            }else if(res==4){
                              FunctionUtils.pressedMenu(context,widget.userKey,widget.userValidation,"M010",AccountPage("0"),widget.scaffoldKey,0);
                            }/* else if(res==5){
                              FunctionUtils.pressedMenu(context,widget.userKey,widget.userValidation,"M005",MyAnnoncePage(DataConstantesUtils.ANNONCE_ACHAT,int.parse(widget.userKey),0,achatBiztypeId),widget.scaffoldKey,0);
                            } */
                          });
                        },
                        child: Container(
                          padding: EdgeInsets.only(top:0),
                          child: SvgPicture.asset("assets/img/menu.svg"),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 70 - widget.offset / 2,
                      left: 180,
                      child: Text(
                        "${widget.textTop} \n${widget.textBottom}",
                        style: kHeadingTextStyle.copyWith(
                          color: Colors.white,
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ],
          ),
        ),
      );
    },
  );


}


class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height - 80);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 80);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
