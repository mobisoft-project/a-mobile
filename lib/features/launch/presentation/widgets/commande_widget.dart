import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_commande_entity.dart';
import 'package:agrimobile/features/common/data/dto/commande_delete_dto.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';

import 'dart:core';
import 'package:url_launcher/url_launcher.dart';
import 'package:agrimobile/features/launch/presentation/pages/commande/liste_commande_detail_page.dart';

  class CommandeWidget extends StatefulWidget {

    CommandeWidget({
    this.infoCommande,
      this.onrefresh,
  });
      final VoidCallback onrefresh;
    LocalCommandeEntity infoCommande;

  @override
  CommandeWidgetState createState() => CommandeWidgetState();
}

class CommandeWidgetState extends State<CommandeWidget> {

  AppSharedPreferences _appSharedPreferences = AppSharedPreferences();
  //LocalAnnonceRepository _localAnnonceRepository = LocalAnnonceRepository();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      String _imageName="";
      String _serverImageName="";
      String countryDraf = "";
      String type = "";
      String phrasetype = "";

      int annonce_commande =  widget.infoCommande.amountCommande;
      double annonce_commande_final = annonce_commande * 1.0;

      return   Container(
        width: 200,
        child: GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ListeCommandeDetailPage(
                infoCommande: widget.infoCommande
              )),
            ).then((value){
                widget.onrefresh();
            });
      },
          onLongPress: () {
            _showMenu(context, configModel,widget.infoCommande);
          },
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          child:Container(
                padding: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: [
                      Text('N° :'+ widget.infoCommande.numeroCommande, style: TextStyle(color: Colors.black,fontWeight: FontWeight.w700,)),
                      SizedBox(width: 5),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Container(
                              //width: 50,
                              height: 25,
                              padding: EdgeInsets.all(5.0),
                              decoration: new BoxDecoration(
                                borderRadius: new BorderRadius.circular(20.0),
                                color: widget.infoCommande.etatCommande == 0 ? FunctionUtils.colorFromHex("AE682D"):FunctionUtils.colorFromHex(configModel.mainColor),
                              ),
                              child: Text(widget.infoCommande.etatCommande == 0 ?allTranslations.text('pending_order'):allTranslations.text('delivred_order'),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    //fontWeight: FontWeight.w700,
                                    fontSize: 12,
                                    color: Colors.white,
                                  )),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    children: [
                      Icon(Icons.payment, color: FunctionUtils.colorFromHex(configModel.mainColor),size: 15.0,),
                      SizedBox(width: 5,),
                      Text("${FunctionUtils.separateur(annonce_commande_final)} ${configModel.devise}", style: TextStyle(color: Colors.black))
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    children: [
                      Icon(Icons.calendar_today_rounded, color: FunctionUtils.colorFromHex(configModel.mainColor),size: 10.0,),
                      SizedBox(width: 5,),
                      Expanded(
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(widget.infoCommande.dateCommande, style: TextStyle(color: Colors.black)),
                          ],
                        ) ,
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                ],
              ),
            ),
        ),
        ),
      );
    },
  );

  _showAlertDialog(BuildContext context,ConfigModel configModel, LocalCommandeEntity commande) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
        _confirmeDelete(configModel, commande);
      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: Text(
        allTranslations.text('delete_commande_text'),
        style: new TextStyle( fontSize: 15.0),
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _confirmeDelete(ConfigModel configModel, LocalCommandeEntity commande) async {
    _appSharedPreferences.getUserInformation().then((user) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('cdelete_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    //remove

    print(commande.keyCommande);
    Api api = ApiRepository();
    CommandeDeleteDto deleteCommandeDto = CommandeDeleteDto();

    deleteCommandeDto.accessToken=DataConstantesUtils.API_TOKEN;
    deleteCommandeDto.userToken=user.token;
    deleteCommandeDto.keyCommande=commande.keyCommande;

    api.deleteCommande(deleteCommandeDto).then((value) {
      if (value.isRight()) {
        value.all((a) {
          if(a.status.compareTo("000")==0) {

            // delete in database
             FunctionUtils.deleteCommande(commande).then((value){
                Navigator.of(context).pop(null);
                   //widget.onrefresh();
             });

             Navigator.of(context).pop(null);
             widget.onrefresh();
            return true;
          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }
        });
      } else {
        Navigator.of(context).pop(null);
        return false;
      }
    });
    setState(() {
     // widget.allItems = cartModel.allItems;
    });
    return false;
    });
  }

  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
   // widget._scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _showMenu(BuildContext context,ConfigModel configModel,LocalCommandeEntity infoCommande) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title:  Text(allTranslations.text('choose_action')),
            children: <Widget>[
              SimpleDialogOption(
                child:  Text(allTranslations.text('delete_commande')),
                onPressed: () {
                  _showAlertDialog(context, configModel,widget.infoCommande);
                },
              ),
              if(infoCommande.etatCommande == 0)SimpleDialogOption(
                child:  Text(allTranslations.text('pay_commande')),
                onPressed: () {
                  _launchURL(infoCommande.urlPaiementCommande);
                },
              ),
            ],
          );
        });
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }



}
