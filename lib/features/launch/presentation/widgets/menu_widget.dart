import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/annonce_dto.dart';
import 'package:agrimobile/features/common/data/dto/contact_dto.dart';
import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_menu_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_menu_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/annonce/add_annonce_page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:agrimobile/features/launch/presentation/widgets/add_to_cart_bottom_sheet_widget.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';

class MenuWidget extends StatefulWidget {

  MenuWidget({
    Key key,
    this.imagepath,
    this.typeMenu,
    this.titleMenu,
    this.widgetMenu,
    this.codeMenu,
    this.iconeMenu,
    this.connectedUser,
    this.validationUser,
    this.scaffoldKey,
  });

  final int typeMenu;
  final String imagepath;
  final String titleMenu;
  final Widget widgetMenu;
  final String codeMenu;
  final String connectedUser;
  final String iconeMenu;
  final String validationUser;
  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  MenuWidgetState createState() => MenuWidgetState();
}

class MenuWidgetState extends State<MenuWidget> {

 // BuildContext context ;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }



  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {

      return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(8.0),
              bottomLeft: Radius.circular(8.0),
              bottomRight: Radius.circular(8.0),
              topRight: Radius.circular(8.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                offset: const Offset(1.1, 1.1),
                blurRadius: 10.0),
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            focusColor: Colors.transparent,
            highlightColor: Colors.transparent,
            hoverColor: Colors.transparent,
            borderRadius: const BorderRadius.all(Radius.circular(8.0)),
            splashColor: Color(0xFF2633C5).withOpacity(0.2),
            onTap: () {
              FunctionUtils.pressedMenu(context,widget.connectedUser,widget.validationUser,widget.codeMenu,widget.widgetMenu,widget.scaffoldKey,widget.typeMenu);
            },
            child: Column(
              children: <Widget>[
                Padding(
                  padding:
                  const EdgeInsets.only(top: 5, left: 16, right: 16),
                  child: CachedNetworkImage(
                    imageUrl:  DataConstantesUtils.IMAGE_MENU_URL + "" + widget.iconeMenu,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    height: 70,
                    placeholder: (context, url) => new Image.asset('assets/img/loading.gif',height:30),
                    errorWidget: (context, url, error) => new Icon(Icons.error),
                  ),
                ),
                Text(
                  widget.titleMenu,
                  maxLines: 1,
                  style: kTitleTextstyle.copyWith(
                    fontSize: 13.5,
                  ),
                )
              ],
            ),
          ),
        ),
      );
    },
  );



}
