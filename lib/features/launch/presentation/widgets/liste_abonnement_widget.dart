import 'package:agrimobile/features/common/data/function_utils.dart';
import 'package:agrimobile/core/utils/colors.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/speculation_dto.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:flutter/material.dart';
import 'package:agrimobile/features/common/domain/entities/local_speculation_entity.dart';
import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/repositories/api_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_speculation_repository.dart';
import 'dart:async';

import 'package:provider/provider.dart';

class ListeAbonnementWidget extends StatefulWidget {

  ListeAbonnementWidget({
    this.localSpeculationEntity,
    this.userToken,
    this.userId,
    this.scaffoldKey,
    this.onrefresh,
  });

  final VoidCallback onrefresh;
  List<LocalSpeculationEntity> localSpeculationEntity;
  List<String>annonceContact;
  GlobalKey<ScaffoldState> scaffoldKey;
  String userToken;
  String userId;


  @override
  ListeAbonnementWidgetState createState() => ListeAbonnementWidgetState();
}

class ListeAbonnementWidgetState extends State<ListeAbonnementWidget> {

  LocalSpeculationRepository _localSpeculationRepository = LocalSpeculationRepository();
  var div= Divider(
    thickness: 1.0,
    color: Colors.black12,
  );

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) => Consumer<ConfigModel>(
    builder: (context, configModel, child) {
      return  ListView.builder(
          itemCount: widget.localSpeculationEntity.length,
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemBuilder: (BuildContext context, int position) {

            LocalSpeculationEntity infoPrice=  widget.localSpeculationEntity[position];

            return Column(
                children: [
                  if(position==0)SizedBox(height: 10,),
                  Row(
                    children: [
                      SizedBox(width: 10,),
                      Expanded(child: Text("${infoPrice.nom}")),
                      infoPrice.isAbonne!=1?MaterialButton(
                        elevation: 0.0,
                        child: Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            decoration: BoxDecoration(
                              color: FunctionUtils.colorFromHex(configModel.mainColor),
                              borderRadius: new BorderRadius.circular(5.0),
                            ),
                            child:Text(allTranslations.text('abonner'))
                        ),
                        textColor: Colors.white,
                        onPressed: (){
                          _showAlertDialog(context,infoPrice,"1",configModel);
                        },
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      )
                          :Row(
                        children: [
                          Text(
                            " Qte >= ${infoPrice.quantite} ${infoPrice.nomUnite}",
                            style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 5,right: 5),
                            padding: EdgeInsets.only(bottom: 5),
                            decoration: BoxDecoration(
                              color: FunctionUtils.colorFromHex(configModel.cancelColor),
                              borderRadius: new BorderRadius.circular(5.0),
                            ),
                            width:40,
                            height:35,
                            child: IconButton(
                              color: Colors.white,
                              icon: Icon(Icons.close,size: 18,),
                              onPressed: () {
                                _showAlertDialog(context,infoPrice,"0",configModel);
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  div,

                ]
            );

          });
    },
  );


  _showAlertDialog(BuildContext context,LocalSpeculationEntity speculation,String type, ConfigModel configModel) {
    TextEditingController _qteController=TextEditingController();

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(allTranslations.text('cancel'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {
        Navigator.of(context).pop(null);
      },
    );
    Widget continueButton = FlatButton(
      child: Text(allTranslations.text('confirm'),style: new TextStyle(
          fontWeight: FontWeight.bold, fontSize: 14.0),
      ),
      onPressed:  () {

        if(type.compareTo("0")==0) {
          Navigator.of(context).pop(null);
          _confirmeDesabonnement(speculation,configModel);
        }else{

          if(_qteController.text.compareTo("")!=0 && _qteController.text.compareTo("0")!=0){
            Navigator.of(context).pop(null);
            _confirmeAbonnement(speculation,_qteController.text,configModel);
          }else{
            _displaySnackBar(context, allTranslations.text('empty_qte'));
          }
        }

      },
    );


    AlertDialog alert = AlertDialog(
      //  title: Text("AlertDialog"),
      content: type.compareTo("0")==0
          ?Text(allTranslations.text('speculation_desactive_text'), style: new TextStyle( fontSize: 15.0),)
          :Container(
        height: 160,
        child: Column(
          children: [
            Text(allTranslations.text('speculation_sactive_text'), style: new TextStyle( fontSize: 15.0),),
            SizedBox(height: 20,),
            Text("${speculation.nom}: ${allTranslations.text('qte_minimum')} (${speculation.nomUnite})", style: new TextStyle( fontSize: 15.0),),
            Container(
              width: MediaQuery.of(context).size.width/1.2,
              height: 45,
              margin: EdgeInsets.only(top: 12),
              padding: EdgeInsets.only(
                  top: 4,left: 16, right: 16, bottom: 4
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                      Radius.circular(50)
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        blurRadius: 5
                    )
                  ]
              ),
              child: TextField(
                controller: _qteController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  border: InputBorder.none,

                  hintText:  allTranslations.text('qte_minimum'),
                ),
              ),
            ),
          ],
        ) ,
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _confirmeAbonnement(LocalSpeculationEntity speculation,String qte,ConfigModel configModel) async {

    SpeculationDto speculationDto = SpeculationDto();

    speculationDto.accessToken=widget.userToken;
    speculationDto.idUser=widget.userId;
    speculationDto.speculationId="${speculation.apiId}";
    speculationDto.unite="${speculation.idUnite}";
    speculationDto.quantite="$qte";
    speculationDto.type_offre="1,2";



    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('abonnement_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.sabonner(speculationDto,"1").then((value) {

      if (value!=null && value.isRight()) {
        value.all((a) {

          if(a.status.compareTo("ok")==0){

            //enregistrement des speculations
            FunctionUtils.saveSpeculations(a.speculations,int.parse(widget.userId));


            Timer(Duration(seconds: 2),(){
              _getAbonnementList(int.parse(widget.userId));
              _displaySnackBar(context, allTranslations.text('abonnement_annonce_success'));
              Navigator.of(context).pop(null);
              return true;
            });


          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }

        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
  }

  _confirmeDesabonnement(LocalSpeculationEntity speculation,ConfigModel configModel) async {

    SpeculationDto speculationDto = SpeculationDto();

    speculationDto.accessToken=widget.userToken;
    speculationDto.idUser=widget.userId;
    speculationDto.speculationIds="${speculation.apiId}";


    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(12),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(allTranslations.text('desabonnement_processing')),
                SizedBox(
                  height: 20,
                ),
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(FunctionUtils.colorFromHex(configModel.mainColor)),
                )
              ],
            ),
          );
        });

    Api api = ApiRepository();
    api.sabonner(speculationDto,"2").then((value) {

      if (value!=null && value.isRight()) {

        value.all((a) {

          if(a.status.compareTo("ok")==0){

            //enregistrement des speculations
            FunctionUtils.saveSpeculations(a.speculations,int.parse(widget.userId));
            _getAbonnementList(int.parse(widget.userId));

            Timer(Duration(seconds: 2),(){
              _displaySnackBar(context, allTranslations.text('desabonnement_annonce_success'));
              Navigator.of(context).pop(null);
              return true;
            });


          }else{
            Navigator.of(context).pop(null);
            _displaySnackBar(context, allTranslations.text('error_process'));
            return false;
          }

        });
      }else{
        Navigator.of(context).pop(null);
        _displaySnackBar(context, allTranslations.text('error_process'));
        return false;
      }
    });
  }


  _getAbonnementList(int idUser){

    //recuperation des prix disponible sur le marche
    _localSpeculationRepository.allSpeculation(idUser).then((response) {
      if(response.length>0 ){

        setState(() {
          widget.localSpeculationEntity=response;
        });
        widget.onrefresh();
      }

    });

  }


  _displaySnackBar(BuildContext context, message) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    widget.scaffoldKey.currentState.showSnackBar(snackBar);
  }


}
