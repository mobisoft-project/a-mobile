import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_send_model.dart';

class ListeTransporterDto {

  String accessToken;
  int moyenTransportApiId;

  ListeTransporterDto();
  ListeTransporterDto.create({this.accessToken,this.moyenTransportApiId});

  @override
  factory ListeTransporterDto.fromMap(Map<String, dynamic> json) {

    return ListeTransporterDto.create(
      accessToken: json['access_token'],
      moyenTransportApiId: json['moyenTransportApiId'],
    );
  }


  @override
  Map<String, dynamic> toMap() {

    return {
      'access_token': accessToken,
      'moyenTransportApiId': moyenTransportApiId,
    };
  }

  @override
  String toGetMap() {
    return "access_token=$accessToken&moyenTransportApiId=$moyenTransportApiId";
  }
}


