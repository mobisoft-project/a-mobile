class MyStatisticsDto{
  String accessToken;
  String userToken;

  MyStatisticsDto();

  MyStatisticsDto.create({
    this.accessToken, this.userToken
  });

  @override
  factory MyStatisticsDto.fromMap(Map<String, dynamic> json){
    return MyStatisticsDto.create(
        accessToken: json['access_token'],
        userToken: json['user_token'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
       'user_token': userToken
    };
  }
}