import 'dart:convert';

class MeteoDto {

  String accessToken;
  String nbre;
  String latitude;
  String longitude;
  String idUser;
  String dateDebut;

  MeteoDto();

  MeteoDto.create({this.accessToken, this.nbre,this.latitude, this.longitude, this.idUser,this.dateDebut});

  @override
  factory MeteoDto.fromMap(Map<String, dynamic> json) {
    return MeteoDto.create(
      accessToken: json['access-token'],
      nbre: json['nbre'],
      latitude: json['latitude'],
      longitude: json['longitude'],
      idUser: json['idUser'],
      dateDebut: json['dateDebut'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access-token': accessToken,
      'nbre': nbre,
      'latitude': latitude,
      'longitude': longitude,
      'idUser': idUser,
      'dateDebut': dateDebut,
    };
  }

  @override
  String toGetMap() {

    return "access-token=$accessToken&nbre=$nbre&latitude=$latitude&longitude=$longitude&idUser=$idUser&dateDebut=$dateDebut";


  }



}


