import 'dart:io';

class ListeAutrePretDto {

  String accessToken;
  String userToken;

  ListeAutrePretDto();

  ListeAutrePretDto.create({this.accessToken,this.userToken});


  @override
  factory ListeAutrePretDto.fromMap(Map<String, dynamic> json) {

    return ListeAutrePretDto.create(
      accessToken: json['access_token'],
      userToken: json['user_token']
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken
    };
  }


}


