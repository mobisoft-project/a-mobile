class ConseilDto {
  String accessToken;
  String userToken;
  String conseil_ids;


  ConseilDto();

  ConseilDto.create({this.accessToken, this.userToken, this.conseil_ids});

  @override
  factory ConseilDto.fromMap(Map<String, dynamic> json) {
    return ConseilDto.create(
        accessToken: json['access_token'],
        userToken: json['user_token'],
        conseil_ids: json['conseil_ids']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken,
      'conseil_ids': conseil_ids,
    };
  }
}
