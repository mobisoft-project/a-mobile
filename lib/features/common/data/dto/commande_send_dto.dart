import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_send_model.dart';

class CommandeSendDto {

  String accessToken;
  String userToken;
  int modeLivraison;
  int moyenTransport;
  int transporterId;
  List <CommandeSendModel> infoCommande;

  CommandeSendDto();

  CommandeSendDto.create({this.accessToken, this.userToken, this.modeLivraison, this.moyenTransport, this.transporterId,this.infoCommande});

  @override
  factory CommandeSendDto.fromMap(Map<String, dynamic> json) {

    List<CommandeSendModel> commandeSendList=null;
    var listCommandeSend= json['info_commande'] as List;
    if(listCommandeSend!=null) commandeSendList = listCommandeSend.map((i) => CommandeSendModel.fromMap(i)).toList();

    return CommandeSendDto.create(
        accessToken: json['access_token'],
        userToken: json['user_token'],
        modeLivraison: json['mode_livraison'],
        moyenTransport: json['moyen_transport'],
        transporterId: json['transporter_id'],
        infoCommande: commandeSendList
    );
  }


  @override
  Map<String, dynamic> toMap() {

    return {
      'access_token': accessToken,
      'user_token': userToken,
      'mode_livraison': modeLivraison,
      'moyen_transport': moyenTransport,
      'transporter_id': transporterId,
      'info_commande':  infoCommande.map((commande) => jsonEncode(commande.toMap())).toList().toString()
    };
  }

  @override
  String toGetMap() {
    return "access_token=$accessToken&user_token=$userToken&mode_livraison=$modeLivraison&moyen_transport=$moyenTransport&transporter_id=$transporterId&info_commande=$infoCommande";
  }
}


