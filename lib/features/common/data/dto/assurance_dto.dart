import 'dart:convert';

class AssuranceDto {

  String accessToken;
  String idAssuranceinfo;
  String nbreAnnee;
  String speculationIds;
  String prixUnitaire;

  AssuranceDto();

  AssuranceDto.create({this.accessToken, this.idAssuranceinfo,this.nbreAnnee, this.prixUnitaire});

  @override
  factory AssuranceDto.fromMap(Map<String, dynamic> json) {
    return AssuranceDto.create(
      accessToken: json['access-token'],
      idAssuranceinfo: json['id_assuranceinfo'],
      nbreAnnee: json['nbre_annee'],
      prixUnitaire: json['prix_unitaire'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access-token': accessToken,
      'id_assuranceinfo': idAssuranceinfo,
      'nbre_annee': nbreAnnee,
      'prix_unitaire': prixUnitaire,
    };
  }

  @override
  String toGetMap() {

    return "access-token=$accessToken&id_assuranceinfo=$idAssuranceinfo&nbre_annee=$nbreAnnee&prix_unitaire=$prixUnitaire";

  }



}


