class ActorProfilDto {
  String accessToken;
  String code;

  ActorProfilDto();
  ActorProfilDto.create({this.accessToken, this.code});

  @override
  factory ActorProfilDto.fromMap(Map<String, dynamic> json) {
    return ActorProfilDto.create(
        accessToken: json['access-token'],
        code: json['code']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access-token': accessToken,
      'code': code
    };
  }

  @override
  String toGetMap() {
     return "access-token=$accessToken&code=$code";
  }
}
