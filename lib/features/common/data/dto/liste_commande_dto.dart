import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_send_model.dart';

class ListeCommandeDto {

  String accessToken;
  String userToken;

  ListeCommandeDto();
  ListeCommandeDto.create({this.accessToken, this.userToken});

  @override
  factory ListeCommandeDto.fromMap(Map<String, dynamic> json) {

    return ListeCommandeDto.create(
        accessToken: json['access_token'],
        userToken: json['user_token'],
    );
  }


  @override
  Map<String, dynamic> toMap() {

    return {
      'access_token': accessToken,
      'user_token': userToken,
    };
  }

  @override
  String toGetMap() {
    return "access_token=$accessToken&user_token=$userToken";
  }
}


