import 'dart:io';

class SimulationDto {

  String accessToken;
  String userToken;
  String keyEop;
  String superficie;
  String jsonScoop;
  String keyScoop;
  String statutScoop;
  String denominationProjet;
  String descriptionProjet;
  List<File>  fileProjet;


  SimulationDto();


  SimulationDto.create({this.accessToken, this.statutScoop, this.userToken, this.keyEop, this.superficie,this.jsonScoop,this.keyScoop,this.denominationProjet,this.descriptionProjet,this.fileProjet});


  @override
  factory SimulationDto.fromMap(Map<String, dynamic> json) {

    return SimulationDto.create(
      accessToken: json['access_token'],
      userToken: json['user_token'],
      keyEop: json['key_eop'],
      superficie: json['superficie'],
      jsonScoop: json['json_scoop'],
      keyScoop: json['key_scoop'],
      denominationProjet: json['denomination_projet'],
      descriptionProjet: json['description_projet'],
      fileProjet: json['file_projet'],
      statutScoop: json['statut_publication'],
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken,
      'key_eop': keyEop,
      'superficie': superficie,
      'json_scoop': jsonScoop,
      'key_scoop': keyScoop,
      'denomination_projet': denominationProjet,
      'description_projet': descriptionProjet,
      'file_projet': fileProjet,
      'statut_publication': statutScoop,
    };
  }


}


