import 'dart:convert';

class AnnonceDto {

  int id;
  String userId;
  String title;
  String picture;
  String categorieId;
  String unitesId;
  String prix;

  String lieu;
  String stock;
  String biztypeId;
  String info;
  String exp;
  String disponibilite;
  String indivisibilite;
  String moisDisponibilite;
  String apiId;
  String scoopId;
  int statutPrive;
  String etat;
  String photo;



  AnnonceDto();

  AnnonceDto.create({this.userId, this.title,this.picture, this.categorieId, this.unitesId,this.prix,
    this.lieu, this.stock,this.biztypeId,this.info, this.exp, this.indivisibilite,this.disponibilite, this.moisDisponibilite, this.apiId, this.scoopId, this.statutPrive,this.etat, this.photo, this.id});

  @override
  factory AnnonceDto.fromMap(Map<String, dynamic> json) {
    return AnnonceDto.create(
      id: json['id'],
      userId: json['userId'],
      title: json['title'],
      picture: json['picture'],
      categorieId: json['categorieId'],
      unitesId: json['unitesId'],
      prix: json['prix'],
      lieu: json['lieu'],
      stock: json['stock'],
      biztypeId: json['biztypeId'],
      info: json['info'],
      exp: json['exp'],
      disponibilite: json['disponibilite'],
      indivisibilite: json['indivisibilite'],
      moisDisponibilite: json['moisDisponibilite'],
      apiId: json['apiId'],
      scoopId: json['scoopId'],
      statutPrive: json['statutPrive'],
      etat: json['etat'],
      photo: json['photo'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    /*return {
      'userId': userId,
      'myApiId': userId,
      'title': title,
      'picture': picture,
      'categorieId': categorieId,
      'unitesId': unitesId,
      'deviseId': '2',
      'prix': prix,
      'latitude': '',
      'longitude': '',
      'lieu': lieu,
      'stock': stock,
      'biztypeId': biztypeId,
      'id': DateTime.now().millisecondsSinceEpoch,
      'time': DateTime.now().millisecondsSinceEpoch,
      'info': info,
      'exp': exp,
      'disponibilite': disponibilite,
      'type': 'annonce',
      'moisDisponibilite': moisDisponibilite,
      'apiId': apiId,
      'photo': photo,
      'etat': '1',
    };*/

   var information={
     'annonces':[{
       'userId': userId,
       'myApiId': userId,
       'title': title,
       'picture': picture,
       'categorieId': categorieId,
       'unitesId': unitesId,
       'deviseId': '2',
       'prix': prix,
       'latitude': '',
       'longitude': '',
       'lieu': lieu,
       'stock': stock,
       'biztypeId': biztypeId,
       'id': id,
       'time': int.parse("${(DateTime.now().millisecondsSinceEpoch/1000).toInt()}"),
       'info': info,
       'exp': exp,
       'disponibilite': disponibilite,
       'indivisibilite': indivisibilite,
       'type': 'annonce',
       'moisDisponibilite': moisDisponibilite,
       'scoopId': scoopId,
       'statutPrive': statutPrive,
       'apiId': apiId,
       'photo': photo,
       'etat': etat,
     }]
   };

    return {
      'datas': jsonEncode(information),
    };
  }

  @override
  Map<String, dynamic> finaltoMap($datas) {
    return {
      'datas': $datas,
    };
  }


}


