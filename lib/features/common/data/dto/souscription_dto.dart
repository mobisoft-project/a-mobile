class SouscriptionDto {
  String accessToken;
  String userToken;
  String souscription_ids;

  SouscriptionDto();

  SouscriptionDto.create({this.accessToken,this.userToken,this.souscription_ids});

  @override
  factory SouscriptionDto.fromMap(Map<String, dynamic> json) {
    return SouscriptionDto.create(
        accessToken: json['access_token'],
        userToken: json['user_token'],
        souscription_ids: json['souscription_ids']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken,
      'conseil_ids': souscription_ids,
    };
  }
}