class UserStatisticsDto{
  String accessToken;
  String userToken;

  UserStatisticsDto();

  UserStatisticsDto.create({
    this.accessToken, 
    this.userToken
  });

  @override
  factory UserStatisticsDto.fromMap(Map<String, dynamic> json){
    return UserStatisticsDto.create(
      accessToken: json['access_token'],
      userToken: json['user_token'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken
    };
  }
}