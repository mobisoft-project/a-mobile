import 'dart:io';

class AutrePretDto {

  String accessToken;
  String userToken;
  String titre;
  String description;
  String idPretBanque;
  String montant;
  List<File>  fileBanque;

  AutrePretDto();

  AutrePretDto.create({this.accessToken,this.userToken,this.titre,this.description, this.idPretBanque,this.fileBanque,this.montant});


  @override
  factory AutrePretDto.fromMap(Map<String, dynamic> json) {

    return AutrePretDto.create(
      accessToken: json['access_token'],
      userToken: json['user_token'],
      titre: json['titre'],
      description: json['description'],
      idPretBanque: json['id_pret_banque'],
      fileBanque: json['file_banque'],
      montant: json['montant'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken,
      'titre': titre,
      'description': description,
      'id_pret_banque': idPretBanque,
      'file_banque': fileBanque,
      'montant': montant,
    };
  }


}


