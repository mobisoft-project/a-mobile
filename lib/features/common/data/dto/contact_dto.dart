import 'dart:convert';

class ContactDto {

  String annonceId;
  String userId;
  String accessToken;


  ContactDto();

  ContactDto.create({this.userId, this.accessToken,this.annonceId});

  @override
  factory ContactDto.fromMap(Map<String, dynamic> json) {
    return ContactDto.create(
      accessToken: json['access-token'],
      userId: json['userId'],
      annonceId: json['annonceId'],
    );
  }
  @override
  Map<String, dynamic> toMap() {
    return {
      'email': accessToken,
      'userId': userId,
      'annonceId': annonceId
    };
  }

  @override
  String toGetMap() {
    return "access-token=$accessToken&userId=$userId&annonceId=$annonceId";
  }



}


