import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_send_model.dart';

class CommandeDeleteDto {

  String accessToken;
  String userToken;
  String keyCommande;

  CommandeDeleteDto();

  CommandeDeleteDto.create({this.accessToken, this.userToken,this.keyCommande});

  @override
  factory CommandeDeleteDto.fromMap(Map<String, dynamic> json) {

    return CommandeDeleteDto.create(
        accessToken: json['access_token'],
        userToken: json['user_token'],
        keyCommande: json['key_commande'],
    );
  }


  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken,
      'key_commande':  keyCommande
    };
  }

  @override
  String toGetMap() {
    return "access_token=$accessToken&user_token=$userToken&key_commande=$keyCommande";
  }
}


