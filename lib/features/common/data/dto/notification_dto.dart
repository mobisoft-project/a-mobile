class NotificationDto {
  String accessToken;
  String userToken;
  String notification_ids;


  NotificationDto();

  NotificationDto.create({this.accessToken, this.userToken, this.notification_ids});

  @override
  factory NotificationDto.fromMap(Map<String, dynamic> json) {
    return NotificationDto.create(
        accessToken: json['access_token'],
        userToken: json['user_token'],
        notification_ids: json['notification_ids']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken,
      'notification_ids': notification_ids,
    };
  }
}
