import 'dart:convert';

class SpeculationDto {

  String accessToken;
  String idUser;
  String speculationId;
  String speculationIds;
  String unite;
  String quantite;
  String type_offre;

  SpeculationDto();

  SpeculationDto.create({this.accessToken, this.idUser,this.speculationId, this.unite, this.quantite,this.type_offre, this.speculationIds});

  @override
  factory SpeculationDto.fromMap(Map<String, dynamic> json) {
    return SpeculationDto.create(
      accessToken: json['access-token'],
      idUser: json['idUser'],
      speculationId: json['speculationId'],
      unite: json['unite'],
      quantite: json['quantite'],
      type_offre: json['type_offre'],
      speculationIds: json['speculationIds'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access-token': accessToken,
      'idUser': idUser,
      'speculationId': speculationId,
      'unite': unite,
      'quantite': quantite,
      'type_offre': type_offre,
      'speculationIds': speculationIds,
    };
  }

  @override
  String toGetMap() {

    return "access-token=$accessToken&idUser=$idUser&speculationId=$speculationId&unite=$unite&quantite=$quantite&type_offre=$type_offre&speculationIds=$speculationIds";

  }



}


