class EopDto {

  String accessToken;
  String userToken;


  EopDto();


  EopDto.create({this.accessToken, this.userToken});


  @override
  factory EopDto.fromMap(Map<String, dynamic> json) {

    return EopDto.create(
      accessToken: json['access_token'],
      userToken: json['user_token'],
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken,
    };
  }


}


