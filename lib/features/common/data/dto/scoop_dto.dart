class ScoopDto {

  String accessToken;
  String userToken;


  ScoopDto();


  ScoopDto.create({this.accessToken, this.userToken});


  @override
  factory ScoopDto.fromMap(Map<String, dynamic> json) {

    return ScoopDto.create(
      accessToken: json['access_token'],
      userToken: json['user_token'],
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken,
    };
  }


}


