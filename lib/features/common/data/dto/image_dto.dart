import 'package:agrimobile/features/common/data/models/tampon_image_model.dart';

class ImageDto {

  String accessToken;
  String userToken;
  String keyProjet;
  int nbrePhoto;
  List<TamponImageModel>  fileProjet;


  ImageDto();


  ImageDto.create({this.accessToken, this.userToken, this.keyProjet, this.fileProjet, this.nbrePhoto});


  @override
  factory ImageDto.fromMap(Map<String, dynamic> json) {

      return ImageDto.create(
          accessToken: json['access_token'],
          userToken: json['user_token'],
          keyProjet: json['key_projet'],
          nbrePhoto: json['nbre_photo'],
          fileProjet: json['file_projet'],
      );

  }

  @override
  Map<String, dynamic> toMap() {
      return {
          'access_token': accessToken,
          'user_token': userToken,
          'key_projet': keyProjet,
          'nbre_photo': nbrePhoto,
          'file_projet': fileProjet,
          };
      }
  }


