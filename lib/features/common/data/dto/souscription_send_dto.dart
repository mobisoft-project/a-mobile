class SouscriptionSendDto {
  String accessToken;
  String userToken;
  String keyTypeSouscription;
  
  SouscriptionSendDto();

  SouscriptionSendDto.create({
    this.accessToken,
    this.userToken,
    this.keyTypeSouscription
  });

  @override
  factory SouscriptionSendDto.fromMap(Map<String, dynamic> json) {
    return SouscriptionSendDto.create(
        accessToken: json['access_token'],
        userToken: json['user_token'],
        keyTypeSouscription: json['key_type_souscription']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access_token': accessToken,
      'user_token': userToken,
      'key_type_souscription': keyTypeSouscription
    };
  }

  @override
  String toGetMap() {
    return "access_token=$accessToken&user_token=$userToken&key_type_souscription=$keyTypeSouscription";
  }
}