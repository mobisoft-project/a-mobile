import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_send_model.dart';

class ListeTypePretBanqueDto {

  String accessToken;
  int typePretApiId;
  int montant;

  ListeTypePretBanqueDto();
  ListeTypePretBanqueDto.create({this.accessToken,this.typePretApiId,this.montant});

  @override
  factory ListeTypePretBanqueDto.fromMap(Map<String, dynamic> json) {

    return ListeTypePretBanqueDto.create(
      accessToken: json['access_token'],
      typePretApiId: json['typePretApiId'],
      montant: json['montant'],
    );
  }


  @override
  Map<String, dynamic> toMap() {

    return {
      'access_token': accessToken,
      'typePretApiId': typePretApiId,
      'montant': montant,
    };
  }

  @override
  String toGetMap() {
    return "access_token=$accessToken&typePretApiId=$typePretApiId&montant=$montant";
  }
}


