import 'dart:convert';

class UpdateAssuranceDto {

  String accessToken;
  String idAssuranceinfo;

  UpdateAssuranceDto();

  UpdateAssuranceDto.create({this.accessToken, this.idAssuranceinfo});

  @override
  factory UpdateAssuranceDto.fromMap(Map<String, dynamic> json) {
    return UpdateAssuranceDto.create(
      accessToken: json['access-token'],
      idAssuranceinfo: json['id_assuranceinfo']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'access-token': accessToken,
      'id_assuranceinfo': idAssuranceinfo
    };
  }

  @override
  String toGetMap() {

    return "access-token=$accessToken&id_assuranceinfo=$idAssuranceinfo";

  }



}


