import 'package:agrimobile/core/error/Failure.dart';
import 'package:agrimobile/features/common/data/dto/actor_profil_dto.dart';
import 'package:agrimobile/features/common/data/dto/assurance_dto.dart';
import 'package:agrimobile/features/common/data/dto/commande_delete_dto.dart';
import 'package:agrimobile/features/common/data/dto/commande_send_dto.dart';
import 'package:agrimobile/features/common/data/dto/connection_dto.dart';
import 'package:agrimobile/features/common/data/dto/conseil_dto.dart';
import 'package:agrimobile/features/common/data/dto/contact_dto.dart';
import 'package:agrimobile/features/common/data/dto/eop_dto.dart';
import 'package:agrimobile/features/common/data/dto/image_dto.dart';
import 'package:agrimobile/features/common/data/dto/inscription_dto.dart';
import 'package:agrimobile/features/common/data/dto/annonce_dto.dart';
import 'package:agrimobile/features/common/data/dto/liste_commande_dto.dart';
import 'package:agrimobile/features/common/data/dto/my_statistics_dto.dart';
import 'package:agrimobile/features/common/data/dto/notification_dto.dart';
import 'package:agrimobile/features/common/data/dto/scoop_dto.dart';
import 'package:agrimobile/features/common/data/dto/simulation_dto.dart';
import 'package:agrimobile/features/common/data/dto/souscription_dto.dart';
import 'package:agrimobile/features/common/data/dto/souscription_send_dto.dart';
import 'package:agrimobile/features/common/data/dto/speculation_dto.dart';
import 'package:agrimobile/features/common/data/dto/meteo_dto.dart';
import 'package:agrimobile/features/common/data/dto/user_statistics_dto.dart';
import 'package:agrimobile/features/common/data/models/account_statistique_model.dart';
import 'package:agrimobile/features/common/data/models/actor_profil_model.dart';
import 'package:agrimobile/features/common/data/models/assurance_reponse_model.dart';
import 'package:agrimobile/features/common/data/models/commande_delete_response_model.dart';
import 'package:agrimobile/features/common/data/models/commande_response_model.dart';
import 'package:agrimobile/features/common/data/models/conseil_recuperation_model.dart';
import 'package:agrimobile/features/common/data/models/contact_model.dart';
import 'package:agrimobile/features/common/data/models/liste_commande_response_model.dart';
import 'package:agrimobile/features/common/data/models/notification_model_response.dart';
import 'package:agrimobile/features/common/data/models/price_response_model.dart';
import 'package:agrimobile/features/common/data/models/region_model.dart';
import 'package:agrimobile/features/common/data/models/app_response_model.dart';
import 'package:agrimobile/features/common/data/models/country_model.dart';
import 'package:agrimobile/features/common/data/models/register_model.dart';
import 'package:agrimobile/features/common/data/models/annonce_model.dart';
import 'package:agrimobile/features/common/data/models/annonce_response_model.dart';
import 'package:agrimobile/features/common/data/models/souscription_recuperation_model.dart';
import 'package:agrimobile/features/common/data/models/souscription_send_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_response_model.dart';
import 'package:agrimobile/features/common/data/models/meteo_response_model.dart';

import 'package:agrimobile/features/common/data/dto/update_assurance_dto.dart';
import 'package:agrimobile/features/common/data/models/update_assurance_reponse_model.dart';
import 'package:agrimobile/features/common/data/models/user_souscription_recuperation_model.dart';
import 'package:agrimobile/features/common/data/models/user_statistics_model.dart';
import 'package:dartz/dartz.dart';

import 'package:agrimobile/features/common/data/models/liste_transporter_response_model.dart';
import 'package:agrimobile/features/common/data/dto/liste_transporter_dto.dart';

import 'package:agrimobile/features/common/data/dto/autre_pret_dto.dart';
import 'package:agrimobile/features/common/data/dto/liste_autre_pret_dto.dart';

abstract class Api {

  Future<Either<Failure, RegisterModel>> registration(InscriptionDto inscriptionDto);

  Future<Either<Failure, AppResponseModel>> updatePassword(ConnectionDto connectionDto);

  Future<Either<Failure, AppResponseModel>> login(ConnectionDto connectionDto);

  Future<Either<Failure, AppResponseModel>> refreshLocation(ConnectionDto connectionDto);

  Future<Either<Failure, AppResponseModel>> updateProfil(InscriptionDto inscriptionDto);

  Future<Either<Failure, AppResponseModel>> forgetPassord(ConnectionDto connectionDto);

  Future<Either<Failure, AppResponseModel>> confirmRegistration(ConnectionDto connectionDto);

  Future<Either<Failure, AppResponseModel>> resetPassword(ConnectionDto connectionDto);

  Future<Either<Failure, List<CountryModel>>> getCountry();

  Future<Either<Failure, List<RegionModel>>> getRegion();

  Future<Either<Failure, List<AnnonceModel>>> getAnnonce(String token);

  Future<Either<Failure, AnnonceResponseModel>> sendAnnonce(AnnonceDto annonceDto);

  Future<Either<Failure, ContactModel>> getContact(ContactDto contactDto);

  Future<Either<Failure, PriceResponseModel>> getPrice(String token);

  Future<Either<Failure, SpeculationResponseModel>> sabonner(SpeculationDto speculationDto,String type);

  Future<Either<Failure, MeteoResponseModel>> askMeteo(MeteoDto meteoDto);

  Future<Either<Failure, AssuranceReponseModel>> askAbonnement(AssuranceDto assuranceDto);

  Future<Either<Failure, AppResponseModel>> getEop(EopDto eopDto);

  Future<Either<Failure, AppResponseModel>> getScoop(ScoopDto scoopDto);

  Future<Either<Failure, AppResponseModel>> sendUsimulation(SimulationDto simulationDto);

  Future<Either<Failure, AppResponseModel>> sendSsimulation(SimulationDto simulationDto);


  Future<Either<Failure, AppResponseModel>> sendUpret(SimulationDto simulationDto);

  Future<Either<Failure, AppResponseModel>> sendSpret(SimulationDto simulationDto);

  Future<Either<Failure, AppResponseModel>> sendDocument(ImageDto imageDto);

  Future<Either<Failure, AppResponseModel>> getProjet(SimulationDto simulationDto);

  Future<Either<Failure, ConseilRecuperationModel>> getConseil(ConseilDto conseilDto);

  Future<Either<Failure, ConseilRecuperationModel>> deleteConseil(ConseilDto conseilDto);


  Future<Either<Failure, ListeCommandeResponseModel>> getCommande(ListeCommandeDto listeCommandeDto);

  Future<Either<Failure, CommandeResponseModel>> sendCommande(CommandeSendDto commandeSendDto);

  Future<Either<Failure, CommandeDeleteResponseModel>> deleteCommande(CommandeDeleteDto commandeDeleteDto);

  Future<Either<Failure, AppResponseModel>> sendUserDocument(ImageDto imageDto);

  Future<Either<Failure, UpdateAssuranceReponseModel>> updateAssurance(UpdateAssuranceDto updateAssuranceDto);

  Future<Either<Failure, MyStatisticsModel>> getMyStatistics(MyStatisticsDto myStatisticsDto);

  Future<Either<Failure, UserStatisticModel>> getUserStatistics(UserStatisticsDto userStatisticsDto);

  Future<Either<Failure, NotificationRecuperationModel>> getNotification(NotificationDto notificationDto);

  Future<Either<Failure, NotificationRecuperationModel>> deleteNotification(NotificationDto notificationDto);

  Future<Either<Failure, SouscriptionRecuperationModel>> getTypeSouscription(SouscriptionDto souscriptionDto);

  Future<Either<Failure, UserSouscriptionRecuperationModel>> getUserTypeSouscription(SouscriptionDto userSouscriptionDto);
  
  Future<Either<Failure, ActorProfilModel>> getActorProfil(ActorProfilDto actorProfilDto);

  Future<Either<Failure, SouscriptionSendModel>> sendSouscription(SouscriptionSendDto souscriptionSendDto);

  Future<Either<Failure, AppResponseModel>> sendStatut(SimulationDto simulationDto);

  Future<Either<Failure, ListeTransporterResponseModel>> getTransportersListe(ListeTransporterDto listeTransporterDto);

  Future<Either<Failure, AppResponseModel>> sendAutrePret(AutrePretDto autrePretDto);

  Future<Either<Failure, AppResponseModel>> getListeAutrePret(ListeAutrePretDto listeAutrePretDto);
}

