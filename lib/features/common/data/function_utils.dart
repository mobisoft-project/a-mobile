import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'dart:ui';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/core/utils/preference.dart';
import 'package:agrimobile/features/common/data/models/account_statistique_model.dart';
import 'package:agrimobile/features/common/data/models/actor_profil_model.dart';
import 'package:agrimobile/features/common/data/models/commande_detail_model.dart';
import 'package:agrimobile/features/common/data/models/commande_model.dart';
import 'package:agrimobile/features/common/data/models/conseil_model.dart';
import 'package:agrimobile/features/common/data/models/eop_model.dart';
import 'package:agrimobile/features/common/data/models/menu_model.dart';
import 'package:agrimobile/features/common/data/models/notification_model.dart';
import 'package:agrimobile/features/common/data/models/projet_model.dart';
import 'package:agrimobile/features/common/data/models/scoop_model.dart';
import 'package:agrimobile/features/common/data/models/souscription_model.dart';
import 'package:agrimobile/features/common/data/models/user_assurance_model.dart';
import 'package:agrimobile/features/common/data/models/user_souscription_model.dart';
import 'package:agrimobile/features/common/data/models/user_statistics_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_actor_profil_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_commande_detail_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_commande_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_conseil_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_eop_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_menu_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_account_statistics_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_notification_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_pret_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_souscription_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_user_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_user_sourciption_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_user_statistics_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_account_statistics_repository.dart.dart';
import 'package:agrimobile/features/common/domain/repositories/local_actor_profil_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_commande_detail_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_commande_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_conseil_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_eop_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_menu_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_notification_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_pret_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_scoop_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_souscription_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_user_assurance_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_user_souscription_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_user_statistics_repository.dart';
import 'package:agrimobile/features/launch/presentation/pages/login/login_page.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';

import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/data/models/annonce_model.dart';
import 'package:agrimobile/features/common/data/models/region_model.dart';
import 'package:agrimobile/features/common/data/models/assurance_model.dart';
import 'package:agrimobile/features/common/data/models/biztype_model.dart';
import 'package:agrimobile/features/common/data/models/country_model.dart';
import 'package:agrimobile/features/common/data/models/devise_model.dart';
import 'package:agrimobile/features/common/data/models/meteo_model.dart';
import 'package:agrimobile/features/common/data/models/produit_model.dart';
import 'package:agrimobile/features/common/data/models/sous_produit_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_model.dart';
import 'package:agrimobile/features/common/data/models/type_assurance_model.dart';
import 'package:agrimobile/features/common/data/models/unite_model.dart';
import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_biztype_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_country_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_devise_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_meteo_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_produit_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_region_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_sous_produit_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_speculation_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_type_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_unite_entity.dart';
import 'package:agrimobile/features/common/domain/repositories/local_annonce_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_biztype_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_country_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_devise_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_meteo_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_assurance_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_produit_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_region_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_sous_produit_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_speculation_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_type_assurance_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_unite_repository.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:share/share.dart';

import 'package:agrimobile/features/common/data/models/mode_livraison_model.dart';
import 'package:agrimobile/features/common/data/models/moyen_transport_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_mode_livraison_repository.dart';
import 'package:agrimobile/features/common/domain/repositories/local_moyen_transport_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_mode_livraison_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_moyen_transport_entity.dart';

import 'package:agrimobile/features/common/data/models/type_pret_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_type_pret_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_type_pret_entity.dart';

import 'package:agrimobile/features/common/data/models/categorie_produit_model.dart';
import 'package:agrimobile/features/common/domain/repositories/local_categorie_produit_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_categorie_produit_entity.dart';

class FunctionUtils {
  static String  topicGlobal="global_wiagri";

  static Future<bool> saveMeteo(List<MeteoModel> allMeteo,int userId) async {

    if(allMeteo!=null){

      //enregistrement des meteo
      LocalMeteoRepository _localMeteoRepository = LocalMeteoRepository();
      if(allMeteo.length>0){

        int nbreTotal=allMeteo.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalMeteoEntity localMeteoEntity = LocalMeteoEntity.fromMap(allMeteo[i].toMap());
          localMeteoEntity.userId=userId;

          //verifier si cette meteo existe deja
          _localMeteoRepository.existMeteo(localMeteoEntity.apiId,localMeteoEntity.date).then((response){

            if(response.length>0){
              localMeteoEntity.idMeteo=response[0].idMeteo;
              _localMeteoRepository.update(localMeteoEntity,response[0].idMeteo);
            }else{
              localMeteoEntity.idMeteo=0;
              _localMeteoRepository.save(localMeteoEntity);
            }

          });


        }
      }

    }
  }

  static String separateur(double nbre){

    final oCcy = new NumberFormat("#,##0.00", "en_US");
    String fullConvert= "${oCcy.format(nbre)}";
   return fullConvert.replaceAll(".00", "").replaceAll(",", " ");

  }

  static Future<bool> saveAssurance(List<AssuranceModel> allAssurance) async {

    if(allAssurance!=null){

      //enregistrement des assurances
      LocalAssuranceRepository localAssuranceRepository = LocalAssuranceRepository();
      if(allAssurance.length>0){

        int nbreTotal=allAssurance.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalAssuranceEntity localAssuranceEntity = LocalAssuranceEntity.fromMap(allAssurance[i].toMap());

          //verifier si cette assurance existe deja
          localAssuranceRepository.existAssurance(localAssuranceEntity.apiId).then((response){

            if(response.length>0){
              localAssuranceEntity.idAssurance=response[0].idAssurance;
              localAssuranceRepository.update(localAssuranceEntity,response[0].idAssurance);
            }else{
              localAssuranceEntity.idAssurance=0;
              localAssuranceRepository.save(localAssuranceEntity);
            }

          });


        }
      }

    }
  }

  static Future<bool> saveTypeAssurance(List<TypeAssuranceModel> allTAssurance) async {

    if(allTAssurance!=null){

      //enregistrement des types assurances
      LocalTypeAssuranceRepository _localTypeAssuranceRepository = LocalTypeAssuranceRepository();
      if(allTAssurance.length>0){

        int nbreTotal=allTAssurance.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalTypeAssuranceEntity localTypeAssuranceEntity = LocalTypeAssuranceEntity.fromMap(allTAssurance[i].toMap());

          //verifier si ce type assurance existe deja
          _localTypeAssuranceRepository.existTypeAssurance(localTypeAssuranceEntity.apiId).then((response){

            if(response.length>0){
              localTypeAssuranceEntity.idTypeAssurance=response[0].idTypeAssurance;
              _localTypeAssuranceRepository.update(localTypeAssuranceEntity,response[0].idTypeAssurance);
            }else{
              localTypeAssuranceEntity.idTypeAssurance=0;
              _localTypeAssuranceRepository.save(localTypeAssuranceEntity);
            }

          });


        }
      }

    }
  }

  static Future<bool> saveDevise(List<DeviseModel> allDevise) async {

    if(allDevise!=null){

      //enregistrement des devises
      LocalDeviseRepository localDeviseRepository = LocalDeviseRepository();
      if(allDevise.length>0){

        int nbreTotal=allDevise.length;
        for(int i=0;i<nbreTotal;i++) {


          LocalDeviseEntity localDeviseEntity = LocalDeviseEntity.fromMap(allDevise[i].toMap());

          //verifier si cette devise existe deja
          localDeviseRepository.existDevise(localDeviseEntity.apiId).then((response){

            if(response.length>0){
              localDeviseEntity.idDevise=response[0].idDevise;
              localDeviseRepository.update(localDeviseEntity,response[0].idDevise);
            }else{
              localDeviseEntity.idDevise=0;
              localDeviseRepository.save(localDeviseEntity);
            }

          });


        }
      }

    }
  }

  static Future<bool> saveUnites(List<UniteModel> allUnite) async {

    if(allUnite!=null){

      //enregistrement des unites
      LocalUniteRepository localUniteRepository = LocalUniteRepository();
      if(allUnite.length>0){

        int nbreTotal=allUnite.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalUniteEntity localUniteEntity = LocalUniteEntity.fromMap(allUnite[i].toMap());

          //verifier si cette unite existe deja
          localUniteRepository.existUnite(localUniteEntity.apiId).then((response){

            if(response.length>0){
              localUniteEntity.idUnite=response[0].idUnite;
              localUniteRepository.update(localUniteEntity,response[0].idUnite);
            }else{
              localUniteEntity.idUnite=0;
              localUniteRepository.save(localUniteEntity);
            }

          });


        }
      }

    }
  }

  static Future<bool> saveProduits(List<ProduitModel> allProduit) async {

    if(allProduit!=null){

      //enregistrement des produits
      LocalProduitRepository localProduitRepository = LocalProduitRepository();
      if(allProduit.length>0){

        int nbreTotal=allProduit.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalProduitEntity localProduitEntity = LocalProduitEntity.fromMap(allProduit[i].toMap());

          //verifier si ce produit existe deja
          localProduitRepository.existProduit(localProduitEntity.apiId).then((response){

            if(response.length>0){
              localProduitEntity.idProduit=response[0].idProduit;
              localProduitRepository.update(localProduitEntity,response[0].idProduit);
            }else{
              localProduitEntity.idProduit=0;
              localProduitRepository.save(localProduitEntity);
            }

          });


        }
      }

    }
  }



  static Future<bool> saveBiztype(List<BiztypeModel> allBtype) async {

    if(allBtype!=null){

      //enregistrement des biztypes
      LocalBiztypeRepository localBiztypeRepository = LocalBiztypeRepository();
      if(allBtype.length>0){

        int nbreTotal=allBtype.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalBiztypeEntity localBiztypeEntity = LocalBiztypeEntity.fromMap(allBtype[i].toMap());

          //verifier si ce biztypes existe deja
          localBiztypeRepository.existBiztype(localBiztypeEntity.apiId).then((response){

            if(response.length>0){
              localBiztypeEntity.idBiztype=response[0].idBiztype;
              localBiztypeRepository.update(localBiztypeEntity,response[0].idBiztype);
            }else{
              localBiztypeEntity.idBiztype=0;
              localBiztypeRepository.save(localBiztypeEntity);
            }

          });


        }
      }

    }
  }

  static Future<bool> saveSousProduits(List<SousProduitModel> allSousProduit) async {

    if(allSousProduit!=null){

      //enregistrement des sous produits
      LocalSousProduitRepository localSousProduitRepository = LocalSousProduitRepository();
      if(allSousProduit.length>0){

        int nbreTotal=allSousProduit.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalSousProduitEntity localSousProduitEntity = LocalSousProduitEntity.fromMap(allSousProduit[i].toMap());

          //verifier si ce sous produit existe deja
          localSousProduitRepository.existSousProduit(localSousProduitEntity.apiId).then((response){

            if(response.length>0){
              localSousProduitEntity.idSousProduit=response[0].idSousProduit;
              localSousProduitRepository.update(localSousProduitEntity,response[0].idSousProduit);
            }else{
              localSousProduitEntity.idSousProduit=0;
              localSousProduitRepository.save(localSousProduitEntity);
            }

          });


        }
      }

    }
  }

  static Future<bool> saveSpeculations(List<SpeculationModel> allSpeculations,int userId) async {

    if(allSpeculations!=null){

      //enregistrement des speculations
      LocalSpeculationRepository localSpeculationRepository = LocalSpeculationRepository();
      if(allSpeculations.length>0){

        int nbreTotal=allSpeculations.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalSpeculationEntity localSpeculationEntity = LocalSpeculationEntity.fromMap(allSpeculations[i].toMap());

          localSpeculationEntity.userId=userId;
          if(allSpeculations[i].unites!=null) {
            localSpeculationEntity.idUnite = allSpeculations[i].unites[0].id;
            localSpeculationEntity.nomUnite = allSpeculations[i].unites[0].nom;
          }

          //verifier si cette speculation existe deja
          localSpeculationRepository.existSpeculation(localSpeculationEntity.apiId).then((response){

            if(response.length>0){
              localSpeculationEntity.idSpeculation=response[0].idSpeculation;
              localSpeculationRepository.update(localSpeculationEntity,response[0].idSpeculation);
            }else{
              localSpeculationEntity.idSpeculation=0;
              localSpeculationRepository.save(localSpeculationEntity);
            }

          });


        }
      }

    }
  }

  static Future<List<LocalCountryEntity>> saveCountry(List<CountryModel> allCountry) async {
    List<LocalCountryEntity> allLocalCountryEntity=[];

    if(allCountry!=null){

      //enregistrement des speculations
      LocalCountryRepository localCountryRepository = LocalCountryRepository();
      if(allCountry.length>0){

        int nbreTotal=allCountry.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalCountryEntity localCountryEntity = LocalCountryEntity.fromMap(allCountry[i].toMap());

          //verifier si cette speculation existe deja
          localCountryRepository.existCountry(localCountryEntity.apiId).then((response){

            if(response.length>0){

              localCountryEntity.idCountry=response[0].idCountry;
              localCountryRepository.update(localCountryEntity,response[0].idCountry).then((value){
                allLocalCountryEntity.add(localCountryEntity);
              });

            }
            else{

              localCountryEntity.idCountry=0;
              localCountryRepository.save(localCountryEntity).then((value){
                localCountryEntity.idCountry=value;
                allLocalCountryEntity.add(localCountryEntity);
              });

            }

          });

        }
      }

    }
    return allLocalCountryEntity ;
  }

  static  saveUserAssurance(List<UserAssuranceModel> allUserAssurance) async {

    if(allUserAssurance!=null){
      //enregistrement des speculations
      LocalUserAssuranceRepository localUserAssuranceRepository = LocalUserAssuranceRepository();
      if(allUserAssurance.length>0){
        int nbreTotal=allUserAssurance.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalUserAssuranceEntity localUassuranceEntity = LocalUserAssuranceEntity.fromMap(allUserAssurance[i].toMap());
          //verifier si cette speculation existe deja
          localUserAssuranceRepository.existUAssurance(localUassuranceEntity.idInfoUserAssurance).then((response){

            if(response.length>0){

              localUassuranceEntity.idUserAssurance=response[0].idUserAssurance;
              localUserAssuranceRepository.update(localUassuranceEntity,response[0].idUserAssurance);
            }else{

              localUassuranceEntity.idUserAssurance=0;
              localUserAssuranceRepository.save(localUassuranceEntity);
            }

          });

        }
      }

    }


  }

  static Future<List<LocalRegionEntity>> saveRegion(List<RegionModel> allRegion) async {

    List<LocalRegionEntity> allLocalRegionEntity = [];
    if(allRegion!=null) {

      //enregistrement des speculations
      LocalRegionRepository localRegionRepository = LocalRegionRepository();
      if (allRegion.length > 0) {
        int nbreTotal = allRegion.length;
        for (int i = 0; i < nbreTotal; i++) {
          LocalRegionEntity localRegionEntity = LocalRegionEntity.fromMap(
              allRegion[i].toMap());



          //verifier si cette speculation existe deja
          localRegionRepository.existRegion(localRegionEntity.apiId).then((
              response) {
            if (response.length > 0) {
              localRegionEntity.idRegion = response[0].idRegion;
              localRegionRepository.update(
                  localRegionEntity, response[0].idRegion).then((value) {
                allLocalRegionEntity.add(localRegionEntity);
              });
            }
            else {
              localRegionEntity.idRegion = 0;
              localRegionRepository.save(localRegionEntity).then((value) {
                localRegionEntity.idRegion = value;
                allLocalRegionEntity.add(localRegionEntity);
              });
            }
          });
        }
      }
    }

    return allLocalRegionEntity ;

  }

  static Future<List<LocalAnnonceEntity>> saveAnnonce(List<AnnonceModel> allAnnonce) async {

    List<LocalAnnonceEntity> allLocalAnnonceEntity = [];
    if(allAnnonce!=null) {

      //enregistrement des speculations
      LocalAnnonceRepository localAnnonceRepository = LocalAnnonceRepository();
      if (allAnnonce.length > 0) {


        int nbreTotal = allAnnonce.length;
        for (int i = 0; i < nbreTotal; i++) {
          LocalAnnonceEntity localAnnonceEntity = LocalAnnonceEntity.fromMap(
              allAnnonce[i].toMap());

          //verifier si cette annonce existe deja
          localAnnonceRepository.existAnnonce(localAnnonceEntity.apiId).then((
              response) {

            //calcul de la date expiration
            localAnnonceEntity.expireTime=localAnnonceEntity.time+(localAnnonceEntity.categorieDuree*24*60*60);

            if (response.length > 0) {
              localAnnonceEntity.idAnnonce = response[0].idAnnonce;
              localAnnonceRepository.update(localAnnonceEntity, response[0].idAnnonce).then((value) {
                allLocalAnnonceEntity.add(localAnnonceEntity);
              });
            }
            else {
              localAnnonceEntity.idAnnonce = 0;
              localAnnonceRepository.save(localAnnonceEntity).then((value) {
                localAnnonceEntity.idAnnonce = value;
                allLocalAnnonceEntity.add(localAnnonceEntity);
              });
            }
          });
        }
        return allLocalAnnonceEntity ;
      }
    }



  }

  static Future<List<LocalAnnonceEntity>> saveEop(List<EopModel> allEop,int idUser) async {


    if(allEop!=null) {

      //enregistrement des speculations
      LocalEopRepository localEopRepository = LocalEopRepository();
      if (allEop.length > 0) {

        int nbreTotal = allEop.length;
        for (int i = 0; i < nbreTotal; i++) {
          LocalEopEntity localEopEntity = LocalEopEntity.fromMap(allEop[i].toMap());

          //verifier si cette annonce existe deja
          localEopRepository.existEop(localEopEntity.keyEop).then((response) {

            //calcul de la date expiration
            localEopEntity.idUser=idUser;
            if (response.length > 0) {
              localEopEntity.idEop = response[0].idEop;
              localEopRepository.update(localEopEntity, response[0].idEop);
            }else {
              localEopEntity.idEop = 0;
              localEopRepository.save(localEopEntity);
            }
          });
        }

      }

    }



  }

  static  saveScoop(List<ScoopModel> allScoop,int idUser) async {


    if(allScoop!=null) {

      //enregistrement des speculations
      LocalScoopRepository localScoopRepository = LocalScoopRepository();
      if (allScoop.length > 0) {

        int nbreTotal = allScoop.length;
        for (int i = 0; i < nbreTotal; i++) {
          LocalScoopEntity localScoopEntity = LocalScoopEntity.fromMap(allScoop[i].toMap());

          //verifier si cette annonce existe deja
          localScoopRepository.existScoop(localScoopEntity.idAssociation).then((response) {

            //calcul de la date expiration
            localScoopEntity.idUser=idUser;
            if (response.length > 0) {
              localScoopEntity.idScoop = response[0].idScoop;
              localScoopRepository.update(localScoopEntity, response[0].idScoop);
            }else {
              localScoopEntity.idScoop = 0;
              localScoopRepository.save(localScoopEntity);
            }
          });
        }

      }

    }
  }

  static  saveProjet(List<ProjetModel> allProjet,int idUser) async {


    if(allProjet!=null) {

      //enregistrement des speculations
      LocalPretRepository localPretRepository = LocalPretRepository();
      if (allProjet.length > 0) {

        int nbreTotal = allProjet.length;
        for (int i = 0; i < nbreTotal; i++) {
          LocalPretEntity localPretEntity = LocalPretEntity.fromMap(allProjet[i].toMap());

          //verifier si cette annonce existe deja
          localPretRepository.existPret(localPretEntity.idProjet).then((response) {

            //calcul de la date expiration
            localPretEntity.idUser=idUser;
            if (response.length > 0) {
              localPretEntity.idPret = response[0].idPret;
              localPretRepository.update(localPretEntity, response[0].idPret);
            }else {
              localPretEntity.idPret = 0;
              localPretRepository.save(localPretEntity);
            }
          });
        }

      }

    }



  }

  static Future<bool> saveMenu(List<MenuModel> allMenu) async {

    if(allMenu!=null){

      //enregistrement des menu
      LocalMenuRepository localMenuRepository = LocalMenuRepository();
      if(allMenu.length>0){

        int nbreTotal=allMenu.length;
        for(int i=0;i<nbreTotal;i++) {
          print(allMenu[i].toMap());

          LocalMenuEntity localMenuEntity = LocalMenuEntity.fromMap(allMenu[i].toMap());

          //verifier si ce menu existe deja
          localMenuRepository.existMenu(localMenuEntity.codeMenu).then((response){

            if(response.length>0){
              localMenuEntity.idMenu=response[0].idMenu;
              localMenuRepository.update(localMenuEntity,response[0].idMenu);
            }else{
              localMenuEntity.idMenu=0;
              localMenuRepository.save(localMenuEntity);
            }

          });


        }
      }

    }
  }

  static Color colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    return Color(int.parse('FF$hexCode', radix: 16));
  }

  static String convertfDate(String timestamp){

    String createDate =DateFormat('yyyy-MM-dd hh:mm aaa').format(DateTime.fromMillisecondsSinceEpoch(int.parse(timestamp)*1000));
    var _alltabDate = createDate.split(' ');

    var _tabDate = _alltabDate[0].split('-');
    if (_tabDate.length == 3) {
      return "${_tabDate[2]} ${convertirMois(_tabDate[1])} ${_tabDate[0]} ${_alltabDate[1]}";
    } else{
      return "01 janvier 2019 00:00:00";
    }

  }


  static String convertPdate(String createDate){

    var _alltabDate = createDate.split(' ');
    var _tabDate = _alltabDate[0].split('-');

    if (_tabDate.length == 3) {
      return "${_tabDate[2]} ${convertirMois(_tabDate[1])} ${_tabDate[0]} ${_alltabDate[1]}";
    } else{
      return "01 janvier 2019 00:00:00";
    }

  }

  static String convertPdateWithoutHour(String createDate){

    print(createDate);

    var _alltabDate = createDate.split(' ');

    var _tabDate = _alltabDate[0].split('-');
    print(_tabDate[1]);
    if (_tabDate.length == 3) {
      return "${_tabDate[2]} ${convertirMois(_tabDate[1])} ${_tabDate[0]}";
    } else{
      return "01 janvier 2019 00:00:00";
    }

  }

  static String convertFormat(String timestamp){

    var _alltabDate = timestamp.split(' ');
    var _tabDate = _alltabDate[0].split('/');
    if (_tabDate.length == 3) {
      return "${_tabDate[2]}-${_tabDate[1]}-${_tabDate[0]}";
    } else{
      return "2020-01-01";
    }

  }

  static String disponibiliteDate(String timestamp){

    var _alltabDate = timestamp.split(' ');
    var _tabDate = _alltabDate[0].split('/');
    if (_tabDate.length == 3) {
      return "${convertirMois(_tabDate[1])} ${_tabDate[2]}";
    } else{
      return "janvier 2019";
    }

  }

  static int concatDate(String timestamp){

    var _alltabDate = timestamp.split(' ');
    var _tabDate = _alltabDate[0].split('/');
    if (_tabDate.length == 3) {
      return int.parse("${_tabDate[2]}${_tabDate[1]}${_tabDate[0]}");
    } else{
      return 0;
    }

  }

  static String convertDate(String timestamp){

    String createDate =DateFormat('yyyy-MM-dd hh:mm aaa').format(DateTime.fromMillisecondsSinceEpoch(int.parse(timestamp)*1000));
    var _alltabDate = createDate.split(' ');

    var _tabDate = _alltabDate[0].split('-');
    if (_tabDate.length == 3) {
      return "${_tabDate[2]} ${convertirMois(_tabDate[1])} ${_tabDate[0]}";
    } else{
      return "01 janvier 2019 00:00:00";
    }

  }

  static String convertirMois(String mois){


    print("++++mois+++++++");
    print(int.parse(mois));

    String convertirMois = "" ;
    switch (int.parse(mois)) {

      case 1: convertirMois=allTranslations.text('month_1');
      break;
      case 2: convertirMois=allTranslations.text('month_2');
      break;
      case 3: convertirMois=allTranslations.text('month_3');
      break;
      case 4: convertirMois=allTranslations.text('month_4');
      break;
      case 5: convertirMois=allTranslations.text('month_5');
      break;
      case 6: convertirMois=allTranslations.text('month_6');
      break;
      case 7: convertirMois=allTranslations.text('month_7');
      break;
      case 8: convertirMois=allTranslations.text('month_8');
      break;
      case 9: convertirMois=allTranslations.text('month_9');
      break;
      case 10: convertirMois=allTranslations.text('month_10');
      break;
      case 11: convertirMois=allTranslations.text('month_11');
      break;
      case 12: convertirMois=allTranslations.text('month_12');
      break;
    }
    print(convertirMois);
    return convertirMois ;
  }

  static Future <Position> getCurrentLocation() async {

    Position userposition;
    Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    await geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {

      userposition=position;
    }).catchError((e) {});

    return userposition;
  }

  static expireDate(int time, int duree){


    int todaytime=DateTime.now().millisecondsSinceEpoch;

    duree=duree*24*60*60*1000;

    if( (time*1000 + duree)>todaytime){
      int diff_jour =(time*1000 + duree)- todaytime ;
      return  "${(diff_jour/(24*60*60*1000)).toInt()} ${allTranslations.text('days')}";
    }else{
      return allTranslations.text('expired');
    }

  }

  static shareAnnonce(LocalAnnonceEntity localAnnonce,ConfigModel configModel){
    String type=" ";
    String phrasetype = "";


    if(localAnnonce.biztypeId.compareTo(1)==0) {
      type = "Offre";

      phrasetype = "${allTranslations.text('offre_immediat')}";
      if(localAnnonce.disponibilite.compareTo(1)!=0) {
        phrasetype = "${allTranslations.text('offre_ulterieur')} ${localAnnonce.moisDisponibilite}";
      }
    }else {
      type="Demande";

      phrasetype = "${allTranslations.text('demande_immediat')}";
      if(localAnnonce.disponibilite.compareTo(1)!=0) {
        phrasetype = "${allTranslations.text('demande_ulterieur')} ${localAnnonce.moisDisponibilite}";
      }

    }


    String shareContent="${type} ${localAnnonce.nomCategorieFilleProduit}. Quantité: ${localAnnonce.stock} ${localAnnonce.nomUnite}. Prix unitaire: ${localAnnonce.prix} ${configModel.devise}/${localAnnonce.nomUnite}. Lieu: ${localAnnonce.lieu}";
    shareContent+="(${phrasetype})\n\n${DataConstantesUtils.SHARE_ANNONCE_URL}${localAnnonce.reference}";
    Share.share(shareContent);
  }


  static Future<bool> saveConseils(List<ConseilsModel> allConseil, int userId) async {
    if (allConseil != null) {
      LocalConseilRepository locationConseilRepository =
      LocalConseilRepository();

      if (allConseil.length > 0) {



        int nbreTotal = allConseil.length;
        for (int i = 0; i < nbreTotal; i++) {

          print(allConseil[i].toMap());
          LocalConseilEntity localConseilEntity =LocalConseilEntity.fromMap(allConseil[i].toMap());

          locationConseilRepository
              .existConseil(localConseilEntity.apiId)
              .then((response) {
            if (response.length > 0) {
              localConseilEntity.idConseil = response[0].idConseil;
              locationConseilRepository.update(
                  localConseilEntity, response[0].idConseil);
            } else {
              localConseilEntity.idConseil = 0;
              locationConseilRepository.save(localConseilEntity);
            }
          });
        }
      }
    }
  }


  static Future<List<LocalCommandeEntity>> saveCommande(List<CommandeModel> allCommande, int type) async {

    List<LocalCommandeEntity> allLocalCommandeEntity = [];
    if(allCommande!=null) {

      //enregistrement des speculations
      LocalCommandeRepository localCommandeRepository = LocalCommandeRepository();
      if (allCommande.length > 0) {


        int nbreTotal = allCommande.length;
        for (int i = 0; i < nbreTotal; i++) {

          LocalCommandeEntity localCommandeEntity = LocalCommandeEntity.fromMap(allCommande[i].toMap());

          //verifier si cette annonce existe deja
          localCommandeRepository.existCommande(localCommandeEntity.keyCommande).then((
              response) {

            localCommandeEntity.typeCommande = type;

            if (response.length > 0) {
              localCommandeEntity.keyCommande = response[0].keyCommande;
              localCommandeRepository.update(localCommandeEntity, response[0].idCommande).then((value) {
                allLocalCommandeEntity.add(localCommandeEntity);
              });
            }
            else {
              localCommandeRepository.save(localCommandeEntity).then((value) {
                localCommandeEntity.idCommande = value;
                allLocalCommandeEntity.add(localCommandeEntity);
              });
            }
          });
        }
        return allLocalCommandeEntity ;
      }
    }
  }

  static Future<List<LocalCommandeDetailEntity>> saveDetailCommande(List<CommandeDetailModel> allCommandeDetail, String keyCommande) async {

    List<LocalCommandeDetailEntity> allLocalCommandeDetailEntity = [];
    if(allCommandeDetail!=null) {

      //enregistrement des speculations
      LocalCommandeDetailRepository localCommandeDetailRepository = LocalCommandeDetailRepository();
      if (allCommandeDetail.length > 0) {


        int nbreTotal = allCommandeDetail.length;
        for (int i = 0; i < nbreTotal; i++) {

          LocalCommandeDetailEntity localCommandeDetailEntity = LocalCommandeDetailEntity.fromMap(allCommandeDetail[i].toMap());

          //verifier si cette annonce existe deja
          localCommandeDetailRepository.existCommandeDetail(localCommandeDetailEntity, keyCommande).then((response) {

            localCommandeDetailEntity.keyCommande = keyCommande;

            if (response.length > 0) {
              localCommandeDetailEntity.keyCommande = response[0].keyCommande;
              localCommandeDetailRepository.update(localCommandeDetailEntity, response[0].idCommandeDetail).then((value) {
                allLocalCommandeDetailEntity.add(localCommandeDetailEntity);
              });
            }
            else {
              localCommandeDetailRepository.save(localCommandeDetailEntity).then((value) {
                localCommandeDetailEntity.idCommandeDetail = value;
                allLocalCommandeDetailEntity.add(localCommandeDetailEntity);
              });
            }
          });
        }
        return allLocalCommandeDetailEntity ;
      }
    }
  }

  static Future<LocalCommandeEntity> deleteCommande(LocalCommandeEntity localCommandeEntity) async {

    LocalCommandeRepository localCommandeRepository = LocalCommandeRepository();
    LocalCommandeDetailRepository localCommandeDetailRepository = LocalCommandeDetailRepository();

    localCommandeRepository.delete(localCommandeEntity.idCommande,"idAppCommande").then((value) {
      localCommandeRepository.detailsCommande(localCommandeEntity.keyCommande).then((response) {
        int nbreTotal = response.length;
        for (int i = 0; i < nbreTotal; i++) {
          localCommandeDetailRepository.delete(response[i].idCommandeDetail,"idAppCommandeDetail");
        }
      });
    });

  }

  static Future<bool> saveMyStatistics(
      int totalOffre, int activeOffre,
      int chiffreAffaire, String offreSpeculation, int userId
      ) async {
    List<MyStatisticsModel> localAccountStat = [];
    localAccountStat.add(MyStatisticsModel(
        totalOffre: totalOffre,
        activeOffre: activeOffre,
        chiffreAffaire: chiffreAffaire,
        offreSpeculation: offreSpeculation,
        idUtilisateur: userId
    ));

    if(localAccountStat != null){
      LocalMyStatisticsRepository localStatisticRepository = LocalMyStatisticsRepository();
      if(localAccountStat.length > 0){
        for (int i = 0; i < localAccountStat.length; i++) {
          LocalMyStatisticsEntity localStatEntity = LocalMyStatisticsEntity.fromMap(localAccountStat[i].toMap());
          print(localAccountStat[i].toMap());
          localStatisticRepository.existStatics(
              localStatEntity.totalOffre,localStatEntity.activeOffre,
              localStatEntity.chiffreAffaire,localStatEntity.idUtlsateur).then((val) {
            if(val.length > 0){
              localStatisticRepository.updateFromAnotherKey(
                  localStatEntity,
                  localStatEntity.totalOffre
              );
            }else{
              localStatisticRepository.save(localStatEntity);
            }
          });
        }
      }
    }
  }

  static Future<bool> saveUserStatistic(int nbreAchat, int montantAchat,
      int endLivraison, int waitingLivraison, String demandeSpeculation, int userId
      ) async {
    List<UserStatisticModel> localUserStat = [];
    localUserStat.add(UserStatisticModel(
        nbreAchat: nbreAchat,
        montantAchat: montantAchat,
        endLivraison: endLivraison,waitingLivraison: waitingLivraison,
        demandeSpeculation: demandeSpeculation,
        idUtlisateur: userId
    ));

    if(localUserStat != null){
      LocalUserStatisticRepository localStatisticRepository = LocalUserStatisticRepository();
      if(localUserStat.length > 0){
        for (int i = 0; i < localUserStat.length; i++) {
          print(localUserStat[i].toMap());
          LocalUserStatisticEntity localUserStatEntity = LocalUserStatisticEntity.fromMap(localUserStat[i].toMap());
          localStatisticRepository.userStatisticExist(
              localUserStatEntity.nbreAchat,
              localUserStatEntity.montantAchat,
              localUserStatEntity.endLivraison, localUserStatEntity.waitingLivraison, localUserStatEntity.idUtlisateur).then((val) {
            if(val.length > 0){
              localStatisticRepository.updateFromAnotherKey(
                  localUserStatEntity,
                  localUserStatEntity.nbreAchat
              );
            }else{
              localStatisticRepository.save(localUserStatEntity);
            }
          });
        }
      }
    }
  }

  static Future<bool> saveNotifications(List<NotificationModel> allNotification, int userId) async {
    if (allNotification != null) {
      LocalNotificationRepository locationNotificationRepository = LocalNotificationRepository();

      if (allNotification.length > 0) {
        int nbreTotal = allNotification.length;
        for (int i = 0; i < nbreTotal; i++) {
          allNotification[i].idUser = userId;
          LocalNotificationEntity localNotificationEntity = LocalNotificationEntity.fromMap(allNotification[i].toMap());

          locationNotificationRepository
              .existNotification(localNotificationEntity.apiId)
              .then((response) {
            if (response.length > 0) {
              localNotificationEntity.idNotification = response[0].idNotification;
              locationNotificationRepository.update(
                  localNotificationEntity, response[0].idNotification);
            } else {
              localNotificationEntity.idNotification = 0;
              locationNotificationRepository.save(localNotificationEntity);
            }
          });
        }
      }
    }
  }

  static Future<bool> saveTypeSouscription(List<SouscriptionModel> allTypeSouscription, int userId) async {
    if (allTypeSouscription != null) {
      LocalSouscriptionRepository _localTypeSouscriptionRepository = LocalSouscriptionRepository();

      if (allTypeSouscription.length > 0) {
        int nbreTotal = allTypeSouscription.length;
        for (int i = 0; i < nbreTotal; i++) {
          allTypeSouscription[i].userId = userId;
          LocalSouscriptionEntity _localSouscriptionEntity = LocalSouscriptionEntity.fromMap(allTypeSouscription[i].toMap());
          
          _localTypeSouscriptionRepository
              .existTypeSouscription(_localSouscriptionEntity.apiIdType)
              .then((response) {
            if (response.length > 0) {
              _localSouscriptionEntity.idTypeSouscription = response[0].idTypeSouscription;
              _localTypeSouscriptionRepository.update(
                  _localSouscriptionEntity, 
                  response[0].idTypeSouscription
              );
            } else {
              _localSouscriptionEntity.idTypeSouscription = 0;
              _localTypeSouscriptionRepository.save(_localSouscriptionEntity);
            }
          });
        }
      }
    }
  }

  static Future<bool> saveUserSouscription(List<UserSouscriptionModel> userSouscription, int userId) async {
    if (userSouscription != null) {
      LocalUserSouscriptionRepository _localUserSouscriptionRepository = LocalUserSouscriptionRepository();

      if (userSouscription.length > 0) {
        int nbreTotal = userSouscription.length;

        for (int i = 0; i < nbreTotal; i++) {
          userSouscription[i].idUtilisateur = userId;
          LocalUserSouscriptionEntity _localSouscriptionEntity = LocalUserSouscriptionEntity.fromMap(userSouscription[i].toMap());
          
          
          _localUserSouscriptionRepository.userSouscriptionExists(
            _localSouscriptionEntity.id
            ).then((response) {
            if (response.length > 0) {
              _localSouscriptionEntity.idUserSouscription = response[0].idUserSouscription;
              _localUserSouscriptionRepository.update(
                  _localSouscriptionEntity, response[0].idUserSouscription);
            } else {
              _localSouscriptionEntity.idUserSouscription = 0;
              _localUserSouscriptionRepository.save(_localSouscriptionEntity);
            }
          });
        }
      }
    }
  }





  static pressedMenu(BuildContext context,String connectedUser,String validationUser,String codeMenu,Widget widgetMenu,GlobalKey<ScaffoldState> scaffoldKey,int typeMenu){


    if(typeMenu.compareTo(0)==0){
      LocalMenuRepository _localMenuRepository = LocalMenuRepository();
      _localMenuRepository.existMenu(codeMenu).then((response){

        if(response!=null && response.length>0) {
          runMenu(context,connectedUser,validationUser,codeMenu,widgetMenu,scaffoldKey,response[0].typeMenu);
        }

      });
    }else{
      runMenu(context,connectedUser,validationUser,codeMenu,widgetMenu,scaffoldKey,typeMenu);
    }

  }

  static runMenu(BuildContext context,String connectedUser,String validationUser,String codeMenu,Widget widgetMenu,GlobalKey<ScaffoldState> scaffoldKey,int typeMenu){


    print(widgetMenu.key);
    AppSharedPreferences appSharedPreferences = AppSharedPreferences();
    if(connectedUser.compareTo("-1")==0) {
      //recuperation du menu et comparer son type
      if(typeMenu.compareTo(2)==0 || typeMenu.compareTo(3)==0){

        appSharedPreferences.setRoute(codeMenu,"$typeMenu",0).then((value){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => LoginPage("0")),
          );
        });

      }else{
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => widgetMenu),
        );
      }

    }else{
      if(validationUser.compareTo("1")==0) {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => widgetMenu),
        );
      }else{
        if(typeMenu.compareTo(2)==0){
          //message de waiting validation
          displaySnackBar(context, allTranslations.text('waiting_validation'),scaffoldKey);
        }else {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => widgetMenu),
          );
        }
      }
    }
  }



  static displaySnackBar(BuildContext context, message,GlobalKey<ScaffoldState> scaffoldKey) {
    if (message == null) {
      message = "Opération en cours ...";
    }
    final snackBar = SnackBar(content: Text(message));
    scaffoldKey.currentState.showSnackBar(snackBar);
  }


  static Future<bool> saveModeLivraison(List<ModeLivraisonModel> allModeLivraison) async {

    if(allModeLivraison!=null){

      //enregistrement des modes de livraison
      LocalModeLivraisonRepository localModeLivraisonRepository = LocalModeLivraisonRepository();
      if(allModeLivraison.length>0){

        int nbreTotal=allModeLivraison.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalModeLivraisonEntity localModeLivraisontEntity =  LocalModeLivraisonEntity.fromMap(allModeLivraison[i].toMap());

          //verifier si ce  mode de livraison existe deja
          localModeLivraisonRepository.existModeLivraison(localModeLivraisontEntity.apiId).then((response){

            if(response.length>0){
              localModeLivraisontEntity.idModeLivraison=response[0].idModeLivraison;
              localModeLivraisonRepository.update(localModeLivraisontEntity,response[0].idModeLivraison);
            }else{
              localModeLivraisontEntity.idModeLivraison=0;
              localModeLivraisonRepository.save(localModeLivraisontEntity);
            }
          });
        }
      }
    }
  }

  static Future<bool> saveMoyenTransport(List<MoyenTransportModel> allMoyenTransport) async {

    if(allMoyenTransport!=null){

      //enregistrement des moyens de transport
      LocalMoyenTransportRepository localMoyenTransportRepository = LocalMoyenTransportRepository();
      if(allMoyenTransport.length>0){

        int nbreTotal=allMoyenTransport.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalMoyenTransportEntity localMoyenTransportEntity =  LocalMoyenTransportEntity.fromMap(allMoyenTransport[i].toMap());

          //verifier si ce moyen de transport existe deja
          localMoyenTransportRepository.existMoyenTransport(localMoyenTransportEntity.apiId).then((response){

            if(response.length>0){
              localMoyenTransportEntity.idMoyenTransport=response[0].idMoyenTransport;
              localMoyenTransportRepository.update(localMoyenTransportEntity,response[0].idMoyenTransport);
            }else{
              localMoyenTransportEntity.idMoyenTransport=0;
              localMoyenTransportRepository.save(localMoyenTransportEntity);
            }
          });
        }
      }
    }
  }

  static Future<bool> saveTypePret(List<TypePretModel> allTypePret) async {

    if(allTypePret!=null){

      //enregistrement des moyens de transport
      LocalTypePretRepository localTypePretRepository = LocalTypePretRepository();
      if(allTypePret.length>0){

        int nbreTotal=allTypePret.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalTypePretEntity localTypePretEntity =  LocalTypePretEntity.fromMap(allTypePret[i].toMap());

          //verifier si ce moyen de transport existe deja
          localTypePretRepository.existTypePret(localTypePretEntity.apiId).then((response){

            if(response.length>0){
              localTypePretEntity.idTypePret=response[0].idTypePret;
              localTypePretRepository.update(localTypePretEntity,response[0].idTypePret);
            }else{
              localTypePretEntity.idTypePret=0;
              localTypePretRepository.save(localTypePretEntity);
            }
          });
        }
      }
    }
  }

  static Future<bool> saveCategories(List<CategorieProduitModel> allCategorie) async {

    if(allCategorie!=null){

      //enregistrement des produits
      LocalCategorieProduitRepository localCategorieProduitRepository = LocalCategorieProduitRepository();
      if(allCategorie.length>0){

        int nbreTotal=allCategorie.length;
        for(int i=0;i<nbreTotal;i++) {

          LocalCategorieProduitEntity localCategorieProduitEntity = LocalCategorieProduitEntity.fromMap(allCategorie[i].toMap());
            print(localCategorieProduitEntity.name);
          //verifier si cette categorie existe deja
          localCategorieProduitRepository.existProduit(localCategorieProduitEntity.apiId).then((response){

            if(response.length>0){
              localCategorieProduitEntity.idCategorieProduit= response[0].idCategorieProduit;
              localCategorieProduitRepository.update(localCategorieProduitEntity,response[0].idCategorieProduit);
            }else{
              localCategorieProduitEntity.idCategorieProduit=0;
              localCategorieProduitRepository.save(localCategorieProduitEntity);
            }

          });


        }
      }

    }
  }

  static Future<bool> saveActorProfil(
      String uname, String fname, String lname, String uemail, 
      int nVendeur, int nAcheteur, String ucode
    ) async {
    List<ActorProfilModel> loacalActorProfil = [];

    loacalActorProfil.add(
      ActorProfilModel(
        username: uname, 
        nom: fname, 
        prenoms: lname, 
        email: uemail, 
        noteVendeur: nVendeur, 
        noteAcheteur: nAcheteur,
        codeActeur: ucode
      )
    );

    if(loacalActorProfil != null){
      LocalActorProfilRepository localActorProfilRepository = LocalActorProfilRepository();
      if(loacalActorProfil.length>0){
        int nbreTotal = loacalActorProfil.length;
        for(int i = 0; i < nbreTotal; i++) {
          LocalActorProfilEntity localActorProfilEntity = LocalActorProfilEntity.fromMap(loacalActorProfil[i].toMap());
            print(loacalActorProfil[i].username);
            print('----ACTOR CODE---');
            print(localActorProfilEntity.actorCode);
            print('----ACTOR CODE---');
            localActorProfilRepository.actorProfil(localActorProfilEntity.actorCode).then((value) {
              print('VVVVVVVVVVVVVVVVVVV---');
              print(value.toString());
              print('VVVVVVVVVVVVVVVVVVV---');
            });
            localActorProfilRepository.existsActorProfil(localActorProfilEntity.actorCode).then((response){
              if(response.length>0){
                localActorProfilEntity.idActorProfil = response[0].idActorProfil;
                localActorProfilRepository.update(localActorProfilEntity,response[0].idActorProfil);
              }else{
                localActorProfilEntity.idActorProfil = 0;
                localActorProfilRepository.save(localActorProfilEntity);
              }
          });
        }
      }
    }
  }

  static String convertirCourtMois(String mois){


    String convertirMois = "" ;
    switch (int.parse(mois)) {

      case 1: convertirMois=allTranslations.text('month_1');
      break;
      case 2: convertirMois=allTranslations.text('month_2');
      break;
      case 3: convertirMois=allTranslations.text('month_3');
      break;
      case 4: convertirMois=allTranslations.text('month_4');
      break;
      case 5: convertirMois=allTranslations.text('month_5');
      break;
      case 6: convertirMois=allTranslations.text('month_6');
      break;
      case 7: convertirMois=allTranslations.text('month_7');
      break;
      case 8: convertirMois=allTranslations.text('month_8');
      break;
      case 9: convertirMois=allTranslations.text('month_9');
      break;
      case 10: convertirMois=allTranslations.text('month_10');
      break;
      case 11: convertirMois=allTranslations.text('month_11');
      break;
      case 12: convertirMois=allTranslations.text('month_12');
      break;
    }

    return convertirMois ;
  }

  static String convertFormatDate(String dateRecup){

    if(dateRecup!=null) {
      var alltabDate = dateRecup.split(' ');

      var tabDate = alltabDate[0].split('-');
      if (tabDate.length == 3) {
        return "${tabDate[2]} ${convertirCourtMois(tabDate[1])} ${tabDate[0]}";
      } else
        return "01 janvier 2019";
    }else
      return "01 janvier 2019";
  }


}
