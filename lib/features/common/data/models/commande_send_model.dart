import 'dart:convert';

class CommandeSendModel {

  String id;
  String qte;
  String prix;

  CommandeSendModel();

  CommandeSendModel.create({this.id, this.qte,this.prix});

  @override
  factory CommandeSendModel.fromMap(Map<String, dynamic> json) {
    return CommandeSendModel.create(
      id: json['id'],
      qte: json['qte'],
      prix: json['prix']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'qte': qte,
      'prix': prix
    };
  }

  @override
  String toGetMap() {
    return "id=$id&qte=$qte&prix=$prix";
  }

}


