import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_detail_model.dart';
import 'package:agrimobile/features/common/data/models/commande_user_model.dart';

class AutrePretModel {

  int idAutrePret;
  String keyAutrePret;
  int typePretBanqueId;
  String typePretBanqueName;
  String banqueName;
  String titre;
  String description;
  String codePret;
  String datePret;
  int etatAutrePret;
  String montantPret;
  String montantInteret;
  String montantAPayer;
  String tauxApplique;
  String duree;
  String dateDebut;
  String dateEcheance;
  String motifRejet;

  AutrePretModel();
  AutrePretModel.create({this.keyAutrePret, this.idAutrePret, this.typePretBanqueId, this.typePretBanqueName, this.banqueName,this.titre,this.description,this.codePret,this.datePret,
    this.etatAutrePret, this.montantPret, this.montantInteret,
    this.montantAPayer, this.tauxApplique, this.duree, this.dateDebut, this.dateEcheance, this.motifRejet});

  @override
  factory AutrePretModel.fromMap(Map<String, dynamic> json) {
    return AutrePretModel.create(

      idAutrePret: json['id'],
      keyAutrePret: json['key_autre_pret'],
      typePretBanqueId: json['type_pret_banqueId'],
      typePretBanqueName: json['type_pret_banqueName'],
      banqueName: json['banqueName'],
      titre: json['titre'],
      description: json['description'],
      codePret: json['codePret'],
      datePret: json['date_pret'],
      montantPret: json['montant_pret'],
      montantInteret: json['montant_interet'],
      montantAPayer: json['montant_a_payer'],
      etatAutrePret: json['etat'],
      tauxApplique: json['taux_applique'],
      duree: json['duree'],
      dateDebut: json['date_debut'],
      dateEcheance: json['date_echeance'],
      motifRejet: json['motif_rejet'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': idAutrePret,
      'key_autre_pret': keyAutrePret,
      'type_pret_banqueId': typePretBanqueId,
      'type_pret_banqueName': typePretBanqueName,
      'banqueName': banqueName,
      'titre': titre,
      'description': description,
      'codePret': codePret,
      'date_pret': datePret,
      'montant_pret': montantPret,
      'montant_interet': montantInteret,
      'montant_a_payer': montantAPayer,
      'etat': etatAutrePret,
      'taux_applique': tauxApplique,
      'duree': duree,
      'date_debut': dateDebut,
      'date_echeance': dateEcheance,
      'motif_rejet': motifRejet,
    };
  }

}