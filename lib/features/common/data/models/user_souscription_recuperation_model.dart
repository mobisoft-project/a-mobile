import 'package:agrimobile/features/common/data/models/user_souscription_model.dart';

class UserSouscriptionRecuperationModel {
  String status;
  List<UserSouscriptionModel> userSouscription;

  UserSouscriptionRecuperationModel();
  UserSouscriptionRecuperationModel.create({
    this.status,
    this.userSouscription
  });

  @override
  factory UserSouscriptionRecuperationModel.fromMap(Map<String, dynamic> json) {
    List<UserSouscriptionModel> userSouscriptionList = null;
    var userListSouscription = json['user_souscription'] as List;
    if(userListSouscription!=null) userSouscriptionList = userListSouscription.map((i) => UserSouscriptionModel.fromMap(i)).toList();

    return UserSouscriptionRecuperationModel.create(
      status: json['status'],
      userSouscription: userSouscriptionList,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'user_souscription': userSouscription,
    };
  }
}