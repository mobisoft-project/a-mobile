class SouscriptionSendModel {
  String status;
  SouscriptionInfo souscriptionInfo;

  SouscriptionSendModel();

  SouscriptionSendModel.create({
    this.status, this.souscriptionInfo
  });

  SouscriptionSendModel.fromMap(Map<String, dynamic> json) {
    status = json['status'];
    souscriptionInfo = json['souscription_info'] != null
        ? new SouscriptionInfo.fromMap(json['souscription_info'])
        : null;
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.souscriptionInfo != null) {
      data['souscription_info'] = this.souscriptionInfo.toMap();
    }
    return data;
  }
}

class SouscriptionInfo {
  String keySouscription;
  String numeroSouscription;
  String dateSouscription;
  String dateFinsouscription;
  int montant;
  int etatSouscription;
  String urlPaiement;
  UserInfo userInfo;
  TypeSouscription typeSouscription;

  SouscriptionInfo(
      {this.keySouscription,
      this.numeroSouscription,
      this.dateSouscription,
      this.dateFinsouscription,
      this.montant,
      this.etatSouscription,
      this.urlPaiement,
      this.userInfo,
      this.typeSouscription});

  SouscriptionInfo.fromMap(Map<String, dynamic> json) {
    keySouscription = json['key_souscription'];
    numeroSouscription = json['numero_souscription'];
    dateSouscription = json['date_souscription'];
    dateFinsouscription = json['date_finsouscription'];
    montant = json['montant'];
    etatSouscription = json['etat_souscription'];
    urlPaiement = json['url_paiement'];
    userInfo = json['user_info'] != null
        ? new UserInfo.fromMap(json['user_info'])
        : null;
    typeSouscription = json['type_souscription'] != null
        ? new TypeSouscription.fromMap(json['type_souscription'])
        : null;
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key_souscription'] = this.keySouscription;
    data['numero_souscription'] = this.numeroSouscription;
    data['date_souscription'] = this.dateSouscription;
    data['date_finsouscription'] = this.dateFinsouscription;
    data['montant'] = this.montant;
    data['etat_souscription'] = this.etatSouscription;
    data['url_paiement'] = this.urlPaiement;
    if (this.userInfo != null) {
      data['user_info'] = this.userInfo.toMap();
    }
    if (this.typeSouscription != null) {
      data['type_souscription'] = this.typeSouscription.toMap();
    }
    return data;
  }
}

class UserInfo {
  String nom;
  String prenom;
  String email;
  String telephone;

  UserInfo({this.nom, this.prenom, this.email, this.telephone});

  UserInfo.fromMap(Map<String, dynamic> json) {
    nom = json['nom'];
    prenom = json['prenom'];
    email = json['email'];
    telephone = json['telephone'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nom'] = this.nom;
    data['prenom'] = this.prenom;
    data['email'] = this.email;
    data['telephone'] = this.telephone;
    return data;
  }
}

class TypeSouscription {
  String keyTypeSouscription;
  String denoTypeSouscription;
  String prixSouscription;
  String dureeSouscription;

  TypeSouscription(
      {this.keyTypeSouscription,
      this.denoTypeSouscription,
      this.prixSouscription,
      this.dureeSouscription});

  TypeSouscription.fromMap(Map<String, dynamic> json) {
    keyTypeSouscription = json['key_type_souscription'];
    denoTypeSouscription = json['deno_type_souscription'];
    prixSouscription = json['prix_souscription'];
    dureeSouscription = json['duree_souscription'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['key_type_souscription'] = this.keyTypeSouscription;
    data['deno_type_souscription'] = this.denoTypeSouscription;
    data['prix_souscription'] = this.prixSouscription;
    data['duree_souscription'] = this.dureeSouscription;
    return data;
  }
}