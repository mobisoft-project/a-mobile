class NotificationModel {
  int idNotifcation;
  String title;
  String description;
  String dateCreation;
  int idAnnonce;
  int idUser;

  NotificationModel();


  NotificationModel.create({
    this.idNotifcation, this.title, this.description, 
    this.dateCreation, this.idAnnonce, this.idUser
  });


  @override
  factory NotificationModel.fromMap(Map<String, dynamic> json) {

    return NotificationModel.create(
      idNotifcation: json['id'],
      title: json['title'],
      description: json['description'],
      dateCreation: json['date_creation'],
      idAnnonce: json['id_annonce'],
      idUser: json['user_id']
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': idNotifcation,
      'title': title,
      'description': description,
      'date_creation': dateCreation,
      'id_annonce': idAnnonce,
      'user_id': idUser
    };
  }
}