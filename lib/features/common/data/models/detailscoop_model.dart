class DetailScoopModel {
  String fullName;
  String phoneNumber;
  String superficie;
  String role;
  int etat;
  int idAgriculteur;


  DetailScoopModel();


  DetailScoopModel.create({this.idAgriculteur, this.fullName, this.phoneNumber, this.superficie, this.role, this.etat});


  @override
  factory DetailScoopModel.fromMap(Map<String, dynamic> json) {

    return DetailScoopModel.create(
      fullName: json['full_name'],
      phoneNumber: json['phone_number'],
      superficie: "${json['superficie']}",
      role: json['role'],
      etat: json['etat'],
      idAgriculteur: json['id_agriculteur'],
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'full_name': fullName,
      'phone_number': phoneNumber,
      'superficie': superficie,
      'role': role,
      'etat': etat,
      'id_agriculteur': idAgriculteur,
    };
  }
}