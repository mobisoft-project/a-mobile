import 'package:agrimobile/features/common/data/models/biztype_model.dart';
import 'package:agrimobile/features/common/data/models/devise_model.dart';
import 'package:agrimobile/features/common/data/models/produit_model.dart';
import 'package:agrimobile/features/common/data/models/sous_produit_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_model.dart';
import 'package:agrimobile/features/common/data/models/unite_model.dart';

import 'meteo_model.dart';
import 'assurance_model.dart';
import 'type_assurance_model.dart';
import 'information_model.dart';

class ContactModel {
  String status;
  String message;
  String notif;
  String contact;
  int solde;

  ContactModel();


  ContactModel.create({this.status, this.message, this.notif, this.contact, this.solde});


  @override
  factory ContactModel.fromMap(Map<String, dynamic> json) {

    return ContactModel.create(
      status: json['statuts'],
      message: json['message'],
      notif: json['notif'],
      contact: json['contact'],
      solde: json['solde']
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'statuts': status,
      'message': message,
      'notif': notif,
      'contact': contact,
      'solde': solde,
    };
  }
}