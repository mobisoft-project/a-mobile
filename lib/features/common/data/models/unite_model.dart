

class UniteModel {
  int id;
  String nom;


  UniteModel();


  UniteModel.create({this.id, this.nom});
  @override
  factory UniteModel.fromMap(Map<String, dynamic> json) {

    return UniteModel.create(
        id: json['id'],
        nom: json['nom'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nom': nom,
    };
  }
}