import 'package:agrimobile/features/common/data/models/souscription_model.dart';

class SouscriptionRecuperationModel {
  String status;
  List<SouscriptionModel> allTypeSouscription;

  SouscriptionRecuperationModel();
  SouscriptionRecuperationModel.create({
    this.status,
    this.allTypeSouscription
  });

  @override
  factory SouscriptionRecuperationModel.fromMap(Map<String, dynamic> json) {
    List<SouscriptionModel> souscriptionList = null;
    var listSouscription = json['type_souscription'] as List;
    if(listSouscription!=null) souscriptionList = listSouscription.map((i) => SouscriptionModel.fromMap(i)).toList();

    return SouscriptionRecuperationModel.create(
      status: json['status'],
      allTypeSouscription: souscriptionList,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'type_souscription': allTypeSouscription,
    };
  }
}
