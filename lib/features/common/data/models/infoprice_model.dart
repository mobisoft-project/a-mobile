import 'package:agrimobile/features/common/data/models/price_model.dart';

class InfoPriceModel {
  String date;
  int timestamp;
  List<PriceModel> information;





  InfoPriceModel();

  InfoPriceModel.create({this.date, this.information, this.timestamp});

  @override
  factory InfoPriceModel.fromMap(Map<String, dynamic> json) {

    List<PriceModel> priceList=null;
    var listPrice = json['information'] as List;
    if(listPrice!=null) priceList = listPrice.map((i) => PriceModel.fromMap(i)).toList();


    return InfoPriceModel.create(
      date: json['date'],
      information: priceList,
      timestamp: json['timestamp'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'information': information,
      'date': date,
      'timestamp': timestamp,
    };
  }
}