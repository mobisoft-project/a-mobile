

class SousProduitModel {
  int id;
  String nomCategorieFille;
  int categorieFilleId;
  int statut;


  SousProduitModel();

  SousProduitModel.create({this.id, this.nomCategorieFille, this.categorieFilleId, this.statut});

  @override
  factory SousProduitModel.fromMap(Map<String, dynamic> json) {

    return SousProduitModel.create(
        id: json['id'],
        nomCategorieFille: json['nomCategorie_fille'],
        categorieFilleId: json['categoriefilleId'],
        statut: json['statut'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nomCategorie_fille': nomCategorieFille,
      'categoriefilleId': categorieFilleId,
      'statut': statut,
    };
  }
}