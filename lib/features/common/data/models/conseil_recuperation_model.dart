import 'package:agrimobile/features/common/data/models/conseil_model.dart';

class ConseilRecuperationModel {
  String status;
  String message;
  List<ConseilsModel> allConseil;


  ConseilRecuperationModel();


  ConseilRecuperationModel.create({this.status, this.message, this.allConseil});


  @override
  factory ConseilRecuperationModel.fromMap(Map<String, dynamic> json) {


    List<ConseilsModel> conseilList=null;
    var listConseil = json['all_conseil'] as List;
    if(listConseil!=null) conseilList = listConseil.map((i) => ConseilsModel.fromMap(i)).toList();

    return ConseilRecuperationModel.create(
      status: json['status'],
      message: json['message'],
      allConseil: conseilList,
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'all_conseil': allConseil,
    };
  }
}
