import 'package:agrimobile/features/common/data/models/biztype_model.dart';
import 'package:agrimobile/features/common/data/models/config_model.dart';
import 'package:agrimobile/features/common/data/models/country_model.dart';
import 'package:agrimobile/features/common/data/models/devise_model.dart';
import 'package:agrimobile/features/common/data/models/eop_model.dart';
import 'package:agrimobile/features/common/data/models/menu_model.dart';
import 'package:agrimobile/features/common/data/models/produit_model.dart';
import 'package:agrimobile/features/common/data/models/projet_model.dart';
import 'package:agrimobile/features/common/data/models/region_model.dart';
import 'package:agrimobile/features/common/data/models/scoop_model.dart';
import 'package:agrimobile/features/common/data/models/simulation_model.dart';
import 'package:agrimobile/features/common/data/models/sous_produit_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_model.dart';
import 'package:agrimobile/features/common/data/models/unite_model.dart';
import 'package:agrimobile/features/common/data/models/user_assurance_model.dart';
import 'package:agrimobile/features/common/data/models/categorie_produit_model.dart';

import 'meteo_model.dart';
import 'assurance_model.dart';
import 'type_assurance_model.dart';
import 'information_model.dart';

import 'package:agrimobile/features/common/data/models/mode_livraison_model.dart';
import 'package:agrimobile/features/common/data/models/moyen_transport_model.dart';
import 'package:agrimobile/features/common/data/models/type_pret_model.dart';
class AppResponseModel {

  String status;
  String message;
  String description;
  int code;
  int id;
  String nom;
  String prenom;
  String email;
  String username;
  String token;
  String document;
  String validate;
  int solde;
  String latitude;
  String dateCreated;
  String longitude;
  String paysAlpha;
  String totalActeurs;
  String totalOffre;
  String totalExperts;
  String paysNom;
  int noteVendeur;
  int isfournisseur;
  List<MeteoModel> meteos;
  List<MenuModel> allMenus;
  List<AssuranceModel> assurance;
  List<TypeAssuranceModel> typeAssurance;
  List<DeviseModel> devises;
  List<UniteModel> unites;
  List<ProduitModel> produits;
  List<BiztypeModel> biztypes;
  List<SousProduitModel> sousProduits;
  List<SpeculationModel> speculations;
  List<UserAssuranceModel> mesAssurances;
  List<EopModel> mesEop;
  List<ScoopModel> mesScoop;
  ConfigModel informations;
  List<SimulationModel>  simulation;
  List<ProjetModel>  allProject;
  List<RegionModel>  allRegion;
  List<CountryModel>  allCountry;
  String  allFichier;
  String  keyProjet;

  List<MoyenTransportModel> moyenTransports;
  List<ModeLivraisonModel> modeLivraisons;
  List<TypePretModel> typePrets;
  List<CategorieProduitModel> categories;






  AppResponseModel();

  AppResponseModel.create({this.status, this.message, this.code, this.id, this.nom, this.prenom, this.email,this.dateCreated,this.username,
    this.token, this.solde, this.latitude, this.longitude, this.paysAlpha,
    this.paysNom, this.noteVendeur, this.isfournisseur, this.meteos, this.assurance, this.typeAssurance, this.devises, this.unites, this.produits
    , this.biztypes, this.sousProduits, this.speculations, this.totalActeurs, this.totalOffre, this.totalExperts, this.mesAssurances, this.informations, this.mesEop, this.mesScoop, this.simulation,
    this.allFichier, this.keyProjet, this.description, this.allProject, this.document, this.validate, this.allMenus, this.allRegion, this.allCountry,this.moyenTransports, this.modeLivraisons, this.typePrets, this.categories});

  @override
  factory AppResponseModel.fromMap(Map<String, dynamic> json) {

    List<CountryModel> uCountryList=null;
    var listCountry = json['all_pays'] as List;
    if(listCountry!=null) uCountryList = listCountry.map((i) => CountryModel.fromMap(i)).toList();

    List<RegionModel> uRegionList=null;
    var listRegion = json['all_region'] as List;
    if(listRegion!=null) uRegionList = listRegion.map((i) => RegionModel.fromMap(i)).toList();

    List<UserAssuranceModel> uAssuranceList=null;
    var listUassurance = json['mes_assurances'] as List;
    if(listUassurance!=null) uAssuranceList = listUassurance.map((i) => UserAssuranceModel.fromMap(i)).toList();

    List<MenuModel> menuModel=null;
    var listMenu = json['all_menu'] as List;
    if(listMenu!=null) menuModel = listMenu.map((i) => MenuModel.fromMap(i)).toList();

    List<MeteoModel> meteoList=null;
    var listMeteo = json['meteos'] as List;
    if(listMeteo!=null) meteoList = listMeteo.map((i) => MeteoModel.fromMap(i)).toList();

    List<AssuranceModel> assuranceList=null;
    var listAssurance = json['assurance'] as List;
    if(listAssurance!=null) assuranceList = listAssurance.map((i) => AssuranceModel.fromMap(i)).toList();

    List<TypeAssuranceModel> typeAssuranceList=null;
    var listTypeAssurance = json['typeAssurance'] as List;
    if(listTypeAssurance!=null) typeAssuranceList = listTypeAssurance.map((i) => TypeAssuranceModel.fromMap(i)).toList();


    List<DeviseModel> deviseList=null;
    var listDevise = json['devises'] as List;
    if(listDevise!=null) deviseList = listDevise.map((i) => DeviseModel.fromMap(i)).toList();

    List<UniteModel> uniteList=null;
    var listUnite = json['unites'] as List;
    if(listUnite!=null) uniteList = listUnite.map((i) => UniteModel.fromMap(i)).toList();

    List<ProduitModel> produitList=null;
    var listProduit = json['produits'] as List;
    if(listProduit!=null) produitList = listProduit.map((i) => ProduitModel.fromMap(i)).toList();

    List<BiztypeModel> biztyList=null;
    var listBiztype = json['biztypes'] as List;
    if(listBiztype!=null) biztyList = listBiztype.map((i) => BiztypeModel.fromMap(i)).toList();

    List<SousProduitModel> sousProduitsList=null;
    var listsousProduits = json['sous_produits'] as List;
    if(listsousProduits!=null) sousProduitsList = listsousProduits.map((i) => SousProduitModel.fromMap(i)).toList();


    List<SpeculationModel> speculationList=null;
    var listSpeculation = json['speculations'] as List;
    if(listSpeculation!=null) speculationList = listSpeculation.map((i) => SpeculationModel.fromMap(i)).toList();


    List<EopModel> eopList=null;
    var listEop = json['all_eop'] as List;
    if(listEop!=null) eopList = listEop.map((i) => EopModel.fromMap(i)).toList();

    List<ScoopModel> scoopList=null;
    var listScoop = json['all_scoop'] as List;
    if(listScoop!=null) scoopList = listScoop.map((i) => ScoopModel.fromMap(i)).toList();

    List<SimulationModel> simulationList=null;
    var listSimulation = json['all_simulation'] as List;
    if(listSimulation!=null) simulationList = listSimulation.map((i) => SimulationModel.fromMap(i)).toList();

    List<ProjetModel> projectList=null;
    var listProject = json['all_project'] as List;
    if(listProject!=null) projectList = listProject.map((i) => ProjetModel.fromMap(i)).toList();

    List<ModeLivraisonModel> modeLivraisonList=null;
    var listModeLivraison = json['mode_livraisons'] as List;
    if(listModeLivraison!=null) modeLivraisonList = listModeLivraison.map((i) => ModeLivraisonModel.fromMap(i)).toList();


    List<MoyenTransportModel> moyenTransportList=null;
    var listMoyenTransport = json['moyen_transports'] as List;
    if(listMoyenTransport!=null) moyenTransportList = listMoyenTransport.map((i) => MoyenTransportModel.fromMap(i)).toList();

    List<TypePretModel> typePretList=null;
    var listTypePrets = json['type_prets'] as List;
    if(listTypePrets!=null) typePretList = listTypePrets.map((i) => TypePretModel.fromMap(i)).toList();

    List<CategorieProduitModel> categorieList=null;
    var listCategories = json['categories'] as List;
    if(listTypePrets!=null) categorieList = listCategories.map((i) => CategorieProduitModel.fromMap(i)).toList();

    return AppResponseModel.create(
      status: json['status'],
      message: json['message'],
      code: json['code'],
      id: json['id'],
      dateCreated: "${json['created_at']}",
      username: "${json['username']}",
      nom: json['nom'],
      prenom: json['prenoms'],
      email: json['email'],
      token: "${json['token']}",
      solde: json['solde'],
      latitude: json['latitude'],
      longitude: json['longitude'],
      paysAlpha: json['paysAlpha'],
      document: json['document'],
      validate: json['validate'],
      paysNom: json['paysNom'],
      noteVendeur: json['noteVendeur'],
      isfournisseur: json['isfournisseur'],
      totalActeurs: json['total_acteurs'],
      totalOffre: json['total_offres'],
      totalExperts: json['total_experts'],
      allFichier: json['all_fichier'],
      keyProjet: json['key_projet'],
      description: json['description'],
      informations: json['informations']!=null?ConfigModel.fromMap(json['informations']):null,
      meteos: meteoList,
      assurance: assuranceList,
      typeAssurance: typeAssuranceList,
      devises: deviseList,
      unites: uniteList,
      produits: produitList,
      biztypes:biztyList,
      sousProduits:sousProduitsList,
      speculations: speculationList,
      mesAssurances: uAssuranceList,
      mesEop: eopList,
      mesScoop: scoopList,
      simulation: simulationList,
      allProject: projectList,
      allMenus: menuModel,
      allCountry: uCountryList,
      allRegion: uRegionList,
      modeLivraisons: modeLivraisonList,
      moyenTransports: moyenTransportList,
      typePrets: typePretList,
      categories: categorieList,
    );


  }
  @override
  Map<String, dynamic> toMap() {
    return {
      'all_fichier': allFichier,
      'key_projet': keyProjet,
      'status': status,
      'message': message,
      'code': code,
      'id': id,
      'created_at': dateCreated,
      'username': username,
      'nom': nom,
      'prenoms': prenom,
      'validate': validate,
      'document': document,
      'email': email,
      'token': token,
      'solde': solde,
      'latitude': latitude,
      'longitude': longitude,
      'paysAlpha': paysAlpha,
      'noteVendeur': noteVendeur,
      'isfournisseur': isfournisseur,
      'meteos': meteos,
      'assurance': assurance,
      'typeAssurance': typeAssurance,
      'devises': devises,
      'unites': unites,
      'produits': produits,
      'biztypes': biztypes,
      'sous_produits': sousProduits,
      'speculations': speculations,
      'total_acteurs': totalActeurs,
      'total_offres': totalOffre,
      'total_experts': totalExperts,
      'mes_assurances': mesAssurances,
      'informations': informations,
      'all_eop': mesEop,
      'all_scoop': mesScoop,
      'all_simulation': simulation,
      'description': description,
      'all_projet': allProject,
      'all_menu': allMenus,
      'all_pays': allCountry,
      'all_region': allRegion,
      'mode_livraisons': modeLivraisons,
      'moyen_transports': moyenTransports,
      'type_prets': typePrets,
      'categories': categories,
    };
  }
}