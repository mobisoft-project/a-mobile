import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_response_message_model.dart';

class ListeTransporterResponseModel {

  String status;
  String message;
  List<TransporterModel> information;

  ListeTransporterResponseModel();

  ListeTransporterResponseModel.create({this.status, this.message, this.information});

  @override
  factory ListeTransporterResponseModel.fromMap(Map<String, dynamic> json) {

    List<TransporterModel> send_commandeList=null;
    var s_commande= json['information'] as List;
    if(s_commande!=null) send_commandeList = s_commande.map((i) => TransporterModel.fromMap(i)).toList();


    return ListeTransporterResponseModel.create(
      status: json['status'],
      message: json['message'],
      information: send_commandeList,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'information': information.map((commande) => jsonEncode(commande.toMap())).toList().toString() ,
    };
  }

  @override
  String toGetMap() {
    return "status=$status&message=$message";
  }



}

class TransporterModel {
  int transporterId;
  String transporterName;
  TransporterModel();

  TransporterModel.create({this.transporterId,this.transporterName});

  @override
  factory TransporterModel.fromMap(Map<String, dynamic> json) {

    return TransporterModel.create(
      transporterId: json['transporter_id'],
      transporterName: json['transporter_name'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'transporter_id': transporterId,
      'transporter_name': transporterName,
    };
  }
}




