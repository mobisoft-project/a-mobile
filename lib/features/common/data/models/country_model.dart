

class CountryModel {
  int id;
  String alpha;
  String nomFr;
  String indicatif;

  CountryModel();

  CountryModel.create({this.id, this.alpha, this.nomFr,this.indicatif});

  @override
  factory CountryModel.fromMap(Map<String, dynamic> json) {

    return CountryModel.create(
        id: json['id'],
        alpha: json['alpha2'],
        nomFr: json['nom_fr_fr'],
        indicatif: json['indicatif']
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'alpha2': alpha,
      'nom_fr_fr': nomFr,
      'indicatif': indicatif,
    };
  }
}
