class ActorProfilModel {
  String status;
  String username;
  String nom;
  String prenoms;
  String email;
  int noteVendeur;
  int noteAcheteur;
  String codeActeur;

  ActorProfilModel({
    this.username, 
    this.nom, 
    this.prenoms, 
    this.email, 
    this.noteVendeur, 
    this.noteAcheteur,
    this.codeActeur
  });


  ActorProfilModel.create({
    this.status, 
    this.username, 
    this.nom, 
    this.prenoms, 
    this.email, 
    this.noteVendeur, 
    this.noteAcheteur,
    this.codeActeur
  });


  @override
  factory ActorProfilModel.fromMap(Map<String, dynamic> json) {
    return ActorProfilModel.create(
      status: json['status'],
      username: json['username'],
      nom: json['nom'],
      prenoms: json['prenoms'],
      email: json['email'],
      noteVendeur: json['noteVendeur'],
      noteAcheteur: json['noteAcheteur'],
      codeActeur: json['actorCode']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'username': username,
      'nom': nom,
      'prenoms': prenoms,
      'email': email,
      'noteVendeur': noteVendeur,
      'noteAcheteur': noteAcheteur,
      'actorCode': codeActeur,
    };
  }
}