import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_detail_model.dart';
import 'package:agrimobile/features/common/data/models/commande_user_model.dart';

class CommandeResponseMessageModel {

  int id_commande;
  String key_commande;
  String numero_commande;
  String date_commande;
  String mode_livraison;
  String mot_cle_mode_livraison;
  String moyen_transport;
  String transporter;
  int amount_commande;
  int etat_commande;
  String url_paiement;
  CommandeUserModel user_info;
  List<CommandeDetailModel> detail_commande;

  CommandeResponseMessageModel();
  CommandeResponseMessageModel.create({this.key_commande, this.id_commande, this.numero_commande,this.date_commande,this.mode_livraison,this.mot_cle_mode_livraison,this.moyen_transport,this.transporter,this.amount_commande, this.etat_commande, this.url_paiement,this.user_info,this.detail_commande});


  @override
  factory CommandeResponseMessageModel.fromMap(Map<String, dynamic> json) {

    List<CommandeDetailModel> detail_commandeList=null;
    var listDetail= json['detail_commande'] as List;
    if(listDetail!=null) detail_commandeList = listDetail.map((i) => CommandeDetailModel.fromMap(i)).toList();

    return CommandeResponseMessageModel.create(
      key_commande: json['key_commande'],
      id_commande: json['id_commande'],
      numero_commande: json['numero_commande'],
      date_commande: json['date_commande'],
      mode_livraison: json['mode_livraison'],
      mot_cle_mode_livraison: json['mot_cle_mode_livraison'],
      moyen_transport: json['moyen_transport'],
      transporter: json['transporter'],
      amount_commande: json['amount_commande'],
      etat_commande: json['etat_commande'],
      url_paiement: json['url_paiement'],
      // user_info: json['user_info'],
      // detail_commande: json['detail_commande'],
      user_info: json['user_info']!=null?CommandeUserModel.fromMap(json['user_info']):null,
      detail_commande: detail_commandeList,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'key_commande': key_commande,
      'id_commande': id_commande,
      'numero_commande': numero_commande,
      'date_commande': date_commande,
      'amount_commande': amount_commande,
      'mode_livraison': mode_livraison,
      'mot_cle_mode_livraison': mot_cle_mode_livraison,
      'moyen_transport': moyen_transport,
      'transporter': transporter,
      'etat_commande': etat_commande,
      'url_paiement': url_paiement,
      'user_info': user_info,
      'detail_commande': detail_commande,
    };
  }

  @override
  String toGetMap() {
    return "key_commande=$key_commande&numero_commande=$numero_commande&date_commande=$date_commande&mode_livraison=$mode_livraison&mot_cle_mode_livraison=$mot_cle_mode_livraison&moyen_transport=$moyen_transport&transporter=$transporter&amount_commande=$amount_commande&etat_commande=$etat_commande&url_paiement=$url_paiement&user_info=$user_info&detail_commande=$detail_commande";
  }

}


