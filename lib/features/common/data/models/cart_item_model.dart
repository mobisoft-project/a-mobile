import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:flutter/cupertino.dart';
import 'dart:convert';

class CartItemModel{
  int qte;
  String nomCategorieFilleProduit;
  LocalAnnonceEntity annonce;

  CartItemModel();
  CartItemModel.create({this.qte,this.nomCategorieFilleProduit, this.annonce});

  @override
  Map<String, dynamic> toMap() {
    return {
      'qte': qte,
      'nomCategorieFilleProduit': nomCategorieFilleProduit,
      'annonce': annonce.toMap()
      // 'annonce': annonce
    };
  }


  @override
  factory CartItemModel.fromMap(Map<String, dynamic> json) {

    return CartItemModel.create(
      qte: json['qte'],
      nomCategorieFilleProduit: json['nomCategorieFilleProduit'],
      annonce:  json['annonce']!=null?LocalAnnonceEntity.fromMap(json['annonce']):null,
    );

  }
}