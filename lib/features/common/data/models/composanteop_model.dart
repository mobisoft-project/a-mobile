

class ComposantEopModel {

  String description;
  String etiquette;
  String unites;
  int qte;
  int coutUnitaire;
  String superficie;
  String coutTotal;


  ComposantEopModel();


  ComposantEopModel.create({this.description, this.etiquette, this.unites,this.qte, this.coutUnitaire, this.coutTotal, this.superficie});


  @override
  factory ComposantEopModel.fromMap(Map<String, dynamic> json) {

    return ComposantEopModel.create(
      description: json['description'],
      etiquette: json['etiquette'],
      unites: json['unites'],
      qte: json['qte'],
      superficie: json['superficie'],
      coutUnitaire: json['cout_unitaire'],
      coutTotal: json['cout_total'],
    );


  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'description': description,
      'etiquette': etiquette,
      'unites': unites,
      'qte': qte,
      'superficie': superficie,
      'cout_unitaire': coutUnitaire,
      'cout_total': coutTotal,
    };
  }
}