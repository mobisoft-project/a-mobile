

class TypePretModel {
  int id;
  String libelle;
  String typePretKey;
  int etat;
  int createdBy;
  String dateCreate;
  int updatedBy;
  String dateUpdate;


  TypePretModel();

  TypePretModel.create({this.id, this.libelle, this.typePretKey, this.etat, this.createdBy, this.dateCreate, this.updatedBy, this.dateUpdate});

  @override
  factory TypePretModel.fromMap(Map<String, dynamic> json) {

    return TypePretModel.create(
        id: json['id'],
        libelle: json['libelle'],
      typePretKey: json['type_pret_key'],
      etat: json['etat'],
      createdBy: json['created_by'],
      dateCreate: json['date_create'],
      updatedBy: json['updated_by'],
      dateUpdate: json['date_update'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'libelle': libelle,
      'type_pret_key': typePretKey,
      'etat': etat,
      'created_by': createdBy,
      'date_create': dateCreate,
      'updated_by': updatedBy,
      'date_update': dateUpdate,
    };
  }
}