import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_response_message_model.dart';

class CommandeResponseModel {

  String status;
  CommandeResponseMessageModel message;

  CommandeResponseModel();

  CommandeResponseModel.create({this.status, this.message});

  @override
  factory CommandeResponseModel.fromMap(Map<String, dynamic> json) {
    return CommandeResponseModel.create(
      status: json['status'],
      // message: json['message']
      message: json['message']!=null?CommandeResponseMessageModel.fromMap(json['message']):null,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message
    };
  }

  @override
  String toGetMap() {
    return "status=$status&message=$message";
  }

}




