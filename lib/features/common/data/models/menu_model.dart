

class MenuModel {
  String codeMenu;
  String nameMenu;
  String iconeMenu;
  int typeMenu;
  int visibleMenu;


  MenuModel();


  MenuModel.create({this.codeMenu, this.nameMenu, this.typeMenu, this.iconeMenu, this.visibleMenu});


  @override
  factory MenuModel.fromMap(Map<String, dynamic> json) {

    return MenuModel.create(
      codeMenu: json['code_menu'],
      nameMenu: json['name_menu'],
      typeMenu: json['type_menu'],
      iconeMenu: json['icone_menu'],
      visibleMenu: json['visible_menu'],
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'code_menu': codeMenu,
      'name_menu': nameMenu,
      'type_menu': typeMenu,
      'icone_menu': iconeMenu,
      'visible_menu': visibleMenu,
    };
  }
}
