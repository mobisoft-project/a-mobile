
class ConseilsModel {
  int idConseil;
  String typeConseil;
  String titre;
  String contenu;
  String speculation;
  String dateConseil;
  String lien;
  String fichier;

  ConseilsModel();


  ConseilsModel.create({this.idConseil, this.typeConseil, this.titre, this.contenu,
    this.speculation, this.dateConseil, this.lien, this.fichier});


  @override
  factory ConseilsModel.fromMap(Map<String, dynamic> json) {
    return ConseilsModel.create(
      idConseil: json['id_conseil'],
      typeConseil: json['type_conseil'],
      titre: json['titre'],
      contenu: json['contenu'],
      speculation: json['speculation'],
      dateConseil: json['date_conseil'],
      lien: json['lien'],
      fichier: json['fichier'],
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id_conseil': idConseil,
      'type_conseil': typeConseil,
      'titre': titre,
      'contenu': contenu,
      'speculation': speculation,
      'date_conseil': dateConseil,
      'lien': lien,
      'fichier': fichier,
    };
  }
}