import 'dart:convert';

import 'package:agrimobile/features/common/data/models/biztype_model.dart';
import 'package:agrimobile/features/common/data/models/devise_model.dart';
import 'package:agrimobile/features/common/data/models/produit_model.dart';
import 'package:agrimobile/features/common/data/models/simulation_model.dart';
import 'package:agrimobile/features/common/data/models/sous_produit_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_model.dart';
import 'package:agrimobile/features/common/data/models/unite_model.dart';

import 'meteo_model.dart';
import 'assurance_model.dart';
import 'type_assurance_model.dart';
import 'information_model.dart';

class ProjetModel {

  String idScoop;
  String idEop;
  String superficie;
  String  dateDemande;
  int  etatDemande;
  int  idServerPret;
  int  typePret;
  String  contenuDemande;
  String  titleDemande;
  String  descriptionDemande;
  String  idProjet;
  String  fichierProjet;




  ProjetModel();


  ProjetModel.create({this.idScoop, this.idEop, this.idServerPret,this.typePret, this.superficie, this.dateDemande,
    this.etatDemande, this.contenuDemande, this.titleDemande, this.idProjet, this.fichierProjet, this.descriptionDemande});


  @override
  factory ProjetModel.fromMap(Map<String, dynamic> json) {


    List<SimulationModel> simulationList=null;
    var listSimulation = json['all_simulation'] as List;
    if(listSimulation!=null) simulationList = listSimulation.map((i) => SimulationModel.fromMap(i)).toList();


    String contentInfo  =jsonEncode(simulationList.map((e) => e.toJson()).toList());


    return ProjetModel.create(
      idEop: json['idEop'],
      idScoop: json['idScoop'],
      typePret: json['typePret'],
      idServerPret: json['idServerPret'],
      superficie: json['superficie'],
      dateDemande: json['dateDemande'],
      etatDemande: json['etatDemande'],
      contenuDemande: contentInfo,
      titleDemande: json['titleDemande'],
      descriptionDemande: json['descriptionDemande'],
      idProjet: json['key_projet'],
      fichierProjet: json['all_fichier'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'idEop': idEop,
      'idScoop': idScoop,
      'typePret': typePret,
      'idServerPret': idServerPret,
      'superficie': superficie,
      'dateDemande': dateDemande,
      'etatDemande': etatDemande,
      'contenuDemande': contenuDemande,
      'all_fichier': fichierProjet,
      'key_projet': idProjet,
      'descriptionDemande': descriptionDemande,
      'titleDemande': titleDemande
    };
  }
}