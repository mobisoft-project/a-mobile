import 'package:agrimobile/features/common/data/models/biztype_model.dart';
import 'package:agrimobile/features/common/data/models/devise_model.dart';
import 'package:agrimobile/features/common/data/models/produit_model.dart';
import 'package:agrimobile/features/common/data/models/sous_produit_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_model.dart';
import 'package:agrimobile/features/common/data/models/unite_model.dart';

import 'meteo_model.dart';
import 'assurance_model.dart';
import 'type_assurance_model.dart';
import 'information_model.dart';

class ScoopModel {
  int idAssociation;
  String nameAssociation;
  String cantonAssociation;
  String adresseAssociation;
  String descriptionAssociation;
  int totalMembre;
  int etat;
  String totalSuperficie;
  String memberAssociation;
  String typeAssociation;
  String roleMember;



  ScoopModel();


  ScoopModel.create({this.idAssociation,this.roleMember,this.typeAssociation, this.etat, this.nameAssociation, this.cantonAssociation, this.adresseAssociation,this.descriptionAssociation, this.totalMembre, this.totalSuperficie,
    this.memberAssociation});


  @override
  factory ScoopModel.fromMap(Map<String, dynamic> json) {

    return ScoopModel.create(
      idAssociation: json['id_association'],
      nameAssociation: json['name_association'],
      cantonAssociation: json['canton_association'],
      adresseAssociation: json['adresse_association'],
      descriptionAssociation: json['description_association'],
      totalMembre: json['total_membre'],
      etat: json['etat_scoop'],
      totalSuperficie: json['total_superficie'],
      memberAssociation: json['member_association'],
      typeAssociation: json['statut_publication'],
      roleMember: json['role_membre'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id_association': idAssociation,
      'name_association': nameAssociation,
      'canton_association': cantonAssociation,
      'adresse_association': adresseAssociation,
      'description_association': descriptionAssociation,
      'total_membre': totalMembre,
      'etat_scoop': etat,
      'total_superficie': totalSuperficie,
      'member_association': memberAssociation,
      'statut_publication': typeAssociation,
      'role_membre': roleMember,
    };
  }
}