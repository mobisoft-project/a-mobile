import 'package:agrimobile/features/common/data/models/user_assurance_model.dart';

class UpdateAssuranceReponseModel {
  String status;
  int isvalide;
  String situation;
  String date;
  List<UserAssuranceModel> datas;

  UpdateAssuranceReponseModel();

  UpdateAssuranceReponseModel.create({this.status, this.isvalide, this.situation,this.date,this.datas});

  @override
  factory UpdateAssuranceReponseModel.fromMap(Map<String, dynamic> json) {

    List<UserAssuranceModel> uassuranceList = null;
    var listMeteo = json['datas'] as List;
    if(listMeteo!=null) uassuranceList = listMeteo.map((i) => UserAssuranceModel.fromMap(i)).toList();

    return UpdateAssuranceReponseModel.create(
        status: json['status'],
        isvalide: json['isvalide'],
        situation: json['situation'],
        date: json['date'],
        datas: uassuranceList
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'isvalide': isvalide,
      'situation': situation,
      'date': date,
      'datas': datas,
    };
  }
}
