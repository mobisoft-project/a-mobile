import 'dart:convert';

class CommandeUserModel {

  String nom;
  String prenom;
  String email;
  String telephone;

  CommandeUserModel();

  CommandeUserModel.create({this.nom, this.prenom,this.email,this.telephone});

  @override
  factory CommandeUserModel.fromMap(Map<String, dynamic> json) {
    return CommandeUserModel.create(
      nom: json['nom'],
        prenom: json['prenom'],
        email: json['email'],
      telephone: json['telephone'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'nom': nom,
      'qte': prenom,
      'email': email,
      'email': telephone
    };
  }

  @override
  String toGetMap() {
    return "nom=$nom&prenom=$prenom&email=$email&telephone=$telephone";
  }

}


