

class ModeLivraisonModel {
  int id;
  String libelle;
  String modeLivraisonKey;
  int etat;
  String motCle;


  ModeLivraisonModel();


  ModeLivraisonModel.create({this.id, this.libelle, this.modeLivraisonKey, this.etat,
      this.motCle});

  @override
  factory ModeLivraisonModel.fromMap(Map<String, dynamic> json) {

    return ModeLivraisonModel.create(
        id: json['id'],
        libelle: json['libelle'],
        modeLivraisonKey: json['mode_livraison_key'],
      etat: json['etat'],
        motCle: json['mot_cle'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'libelle': libelle,
      'mode_livraison_key': modeLivraisonKey,
      'etat': etat,
      'mot_cle': motCle,
    };
  }
}