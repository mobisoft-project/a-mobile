import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_response_message_model.dart';

class ListeCommandeResponseModel {

  String status;
  String message;
  List<CommandeResponseMessageModel> send_commande;
  List<CommandeResponseMessageModel> receive_commande;

  ListeCommandeResponseModel();

  ListeCommandeResponseModel.create({this.status, this.message, this.send_commande, this.receive_commande});

  @override
  factory ListeCommandeResponseModel.fromMap(Map<String, dynamic> json) {

    List<CommandeResponseMessageModel> send_commandeList=null;
    var s_commande= json['send_commande'] as List;
    if(s_commande!=null) send_commandeList = s_commande.map((i) => CommandeResponseMessageModel.fromMap(i)).toList();

    List<CommandeResponseMessageModel> receive_commandeList=null;
    var r_commande= json['receive_commande'] as List;
    if(r_commande!=null) receive_commandeList = r_commande.map((i) => CommandeResponseMessageModel.fromMap(i)).toList();

    return ListeCommandeResponseModel.create(
      status: json['status'],
      message: json['message'],
      send_commande: send_commandeList,
      receive_commande:receive_commandeList,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'send_commande': send_commande.map((commande) => jsonEncode(commande.toMap())).toList().toString() ,
      'receive_commande': receive_commande.map((commande) => jsonEncode(commande.toMap())).toList().toString(),
    };
  }

  @override
  String toGetMap() {
    return "status=$status&message=$message";
  }

}




