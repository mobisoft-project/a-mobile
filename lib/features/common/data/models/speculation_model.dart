

import 'package:agrimobile/features/common/data/models/unite_model.dart';

class SpeculationModel {
  int apiId;
  String nom;
  int isAbonne;
  int quantite;
  List<UniteModel> unites;

  SpeculationModel();

  SpeculationModel.create({this.apiId, this.nom, this.isAbonne, this.quantite, this.unites});

  @override
  factory SpeculationModel.fromMap(Map<String, dynamic> json) {

    List<UniteModel> uniteList=null;
    var listUnite = json['unites'] as List;
    if(listUnite!=null) uniteList = listUnite.map((i) => UniteModel.fromMap(i)).toList();

    return SpeculationModel.create(
        apiId: json['id'],
        nom: json['nom'],
        isAbonne: json['is_abonne'],
        quantite: json['quantite'],
        unites: uniteList,

    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': apiId,
      'nom': nom,
      'is_abonne': isAbonne,
      'quantite': quantite,
      'unites': unites,
    };
  }
}