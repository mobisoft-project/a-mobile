import 'package:agrimobile/features/common/data/models/biztype_model.dart';
import 'package:agrimobile/features/common/data/models/devise_model.dart';
import 'package:agrimobile/features/common/data/models/produit_model.dart';
import 'package:agrimobile/features/common/data/models/sous_produit_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_model.dart';
import 'package:agrimobile/features/common/data/models/unite_model.dart';

import 'meteo_model.dart';
import 'assurance_model.dart';
import 'type_assurance_model.dart';
import 'information_model.dart';

class RegisterModel {
  String status;
  String message;
  int code;





  RegisterModel();

  RegisterModel.create({this.status, this.message, this.code});

  @override
  factory RegisterModel.fromMap(Map<String, dynamic> json) {



    return RegisterModel.create(
      status: json['status'],
      message: json['message'],
      code: json['code']
    );

   /*
    return RegisterModel.create(
      status: json['status'],
      message: json['message'],
    );
  */
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'code': code,
    };
  }
}