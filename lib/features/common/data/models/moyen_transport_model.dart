

class MoyenTransportModel {
  int id;
  String libelle;
  int etat;
  String moyenTransportKey;


  MoyenTransportModel();


  MoyenTransportModel.create({this.id, this.libelle, this.etat, this.moyenTransportKey});

  @override
  factory MoyenTransportModel.fromMap(Map<String, dynamic> json) {

    return MoyenTransportModel.create(
        id: json['id'],
        libelle: json['libelle'],
      etat: json['etat'],
      moyenTransportKey: json['moyen_transport_key'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'libelle': libelle,
      'etat': etat,
      'moyen_transport_key': moyenTransportKey,
    };
  }
}