import 'package:agrimobile/features/common/data/models/biztype_model.dart';
import 'package:agrimobile/features/common/data/models/devise_model.dart';
import 'package:agrimobile/features/common/data/models/produit_model.dart';
import 'package:agrimobile/features/common/data/models/sous_produit_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_model.dart';
import 'package:agrimobile/features/common/data/models/unite_model.dart';

import 'meteo_model.dart';
import 'assurance_model.dart';
import 'type_assurance_model.dart';
import 'information_model.dart';

class EopModel {
  int keyEop;
  int etatEop;
  String superficie;
  String nameSite;
  String nameSpeculation;
  int interetEop;
  int dureeEop;
  String montantTotal;
  String montantInteret;
  String projetInteret;
  String montantSuperficie;
  String montantProduction;
  String descriptionEop;
  String fichierEop;
  String composantEop;



  EopModel();


  EopModel.create({this.keyEop, this.etatEop, this.superficie, this.nameSite,this.nameSpeculation, this.interetEop, this.dureeEop,
    this.montantTotal, this.montantInteret, this.projetInteret,this.montantSuperficie, this.montantProduction, this.descriptionEop, this.composantEop, this.fichierEop});


  @override
  factory EopModel.fromMap(Map<String, dynamic> json) {

    return EopModel.create(
      keyEop: json['key_eop'],
      etatEop: json['etat_eop'],
      superficie: json['superficie'],
      nameSite: json['name_site'],
      nameSpeculation: json['name_speculation'],
      interetEop: json['interet_eop'],
      dureeEop: json['duree_eop'],
      montantTotal: json['montant_total'],
      montantInteret: json['montant_interet'],
      projetInteret: json['projet_interet'],
      montantSuperficie: json['montant_surperficie'],
      montantProduction: json['montant_production'],
      descriptionEop: json['description_eop'],
      fichierEop: json['fichier_eop'],
      composantEop: json['composant_eop'],
    );




  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'key_eop': keyEop,
      'etat_eop': etatEop,
      'superficie': superficie,
      'name_site': nameSite,
      'name_speculation': nameSpeculation,
      'interet_eop': interetEop,
      'duree_eop': dureeEop,
      'montant_total': montantTotal,
      'montant_interet': montantInteret,
      'projet_interet': projetInteret,
      'montant_surperficie': montantSuperficie,
      'montant_production': montantProduction,
      'description_eop': descriptionEop,
      'fichier_eop': fichierEop,
      'composant_eop': composantEop,
    };
  }
}