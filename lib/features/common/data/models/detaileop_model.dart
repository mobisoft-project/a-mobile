

import 'package:agrimobile/features/common/data/models/composanteop_model.dart';

class DetailEopModel {
  String nomComposants;
  String montantParametre;
  List<ComposantEopModel>  detailComposant;


  DetailEopModel();


  DetailEopModel.create({this.nomComposants, this.montantParametre, this.detailComposant});


  @override
  factory DetailEopModel.fromMap(Map<String, dynamic> json) {


    List<ComposantEopModel> composantList=null;
    var listComposant = json['detail_composant'] as List;
    if(listComposant!=null) composantList = listComposant.map((i) => ComposantEopModel.fromMap(i)).toList();

    return DetailEopModel.create(
      nomComposants: json['nom_composants'],
      montantParametre: json['montant_parametre'],
      detailComposant: composantList ,
    );



  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'nom_composants': nomComposants,
      'montant_parametre': montantParametre,
      'detail_composant': detailComposant,
    };
  }
}