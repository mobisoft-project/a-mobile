

class CategorieProduitModel {
  int id;
  String name;
  int statut;


  CategorieProduitModel();


  CategorieProduitModel.create({this.id, this.name, this.statut});

  @override
  factory CategorieProduitModel.fromMap(Map<String, dynamic> json) {

    return CategorieProduitModel.create(
        id: json['id'],
        name: json['name'],
        statut: json['statut'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'statut': statut,
    };
  }
}