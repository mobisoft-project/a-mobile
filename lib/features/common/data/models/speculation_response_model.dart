import 'package:agrimobile/features/common/data/models/biztype_model.dart';
import 'package:agrimobile/features/common/data/models/devise_model.dart';
import 'package:agrimobile/features/common/data/models/produit_model.dart';
import 'package:agrimobile/features/common/data/models/sous_produit_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_model.dart';
import 'package:agrimobile/features/common/data/models/unite_model.dart';

import 'meteo_model.dart';
import 'assurance_model.dart';
import 'type_assurance_model.dart';
import 'information_model.dart';

class SpeculationResponseModel {
  String status;
  String message;
  List<SpeculationModel> speculations;





  SpeculationResponseModel();

  SpeculationResponseModel.create({this.status, this.message, this.speculations});

  @override
  factory SpeculationResponseModel.fromMap(Map<String, dynamic> json) {


    List<SpeculationModel> speculationList=null;
    var listSpeculation = json['speculations'] as List;
    if(listSpeculation!=null) speculationList = listSpeculation.map((i) => SpeculationModel.fromMap(i)).toList();

    return SpeculationResponseModel.create(
      status: json['status'],
      message: json['message'],
      speculations: speculationList,
    );


  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'speculations': speculations,
    };
  }
}