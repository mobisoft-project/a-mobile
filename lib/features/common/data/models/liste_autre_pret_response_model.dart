import 'dart:convert';

import 'package:agrimobile/features/common/data/models/liste_autre_pret_response_model.dart';

class ListeAutrePretResponseModel {

  String status;
  String message;
  List<TypePretBanqueModel> information;

  ListeAutrePretResponseModel();

  ListeAutrePretResponseModel.create({this.status, this.message, this.information});

  @override
  factory ListeAutrePretResponseModel.fromMap(Map<String, dynamic> json) {

    List<TypePretBanqueModel> send_commandeList=null;
    var s_commande= json['information'] as List;
    if(s_commande!=null) send_commandeList = s_commande.map((i) => TypePretBanqueModel.fromMap(i)).toList();


    return ListeAutrePretResponseModel.create(
      status: json['status'],
      message: json['message'],
      information: send_commandeList,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'information': information.map((commande) => jsonEncode(commande.toMap())).toList().toString() ,
    };
  }

  @override
  String toGetMap() {
    return "status=$status&message=$message";
  }



}

class TypePretBanqueModel {
  String apiId;
  String nom;
  String taux;
  String montantDemande;
  String montantInteret;
  String montantAPayer;
  String documents;
  String duree;
  bool isSelected;

  TypePretBanqueModel();

  TypePretBanqueModel.create({this.apiId,this.nom,this.taux,this.montantDemande,this.montantInteret,this.montantAPayer,this.documents,this.duree,this.isSelected});

  @override
  factory TypePretBanqueModel.fromMap(Map<String, dynamic> json) {

    return TypePretBanqueModel.create(
      apiId: json['id'],
      nom: json['nom'],
      taux: json['taux'],
      montantDemande: json['montant_demande'],
      montantInteret: json['montant_interet'],
      montantAPayer: json['montant_a_payer'],
      documents: json['documents'],
      duree: json['duree'],
      isSelected: false,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {

      "id": apiId,
      "nom": nom,
      "taux": taux,
      "montant_demande": montantDemande,
      "montant_interet": montantInteret,
      "montant_a_payer": montantAPayer,
      "documents": documents,
      "duree": duree,
      "isSelected": isSelected,
    };
  }
}




