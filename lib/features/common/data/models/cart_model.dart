import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:flutter/cupertino.dart';
import 'package:agrimobile/features/common/data/models/cart_item_model.dart';
import 'dart:convert';

class CartModel with ChangeNotifier {

  List<CartItemModel> allItems = [];

  CartModel();
  CartModel.create({this.allItems});


  double get total {
    return allItems.fold(0.0, (double currentTotal, CartItemModel nextItem) {
      return currentTotal + nextItem.annonce.prix*nextItem.qte;
    });
  }

  bool findInCart(LocalAnnonceEntity annonce) {

    debugPrint(annonce.toDatabase().toString());
    List<CartItemModel> nosuggestionList=[];

    nosuggestionList.addAll(allItems.where((element) => element.annonce.idAnnonce.compareTo(annonce.idAnnonce)==0));

    bool response = false;

    if(nosuggestionList.length >0){
      response= true;
    }

    return response;

  }

  void addToCart(LocalAnnonceEntity annonce,int qte) {

    debugPrint(annonce.toDatabase().toString());
    List<CartItemModel> suggestionList=[];
    List<CartItemModel> nosuggestionList=[];

    suggestionList.addAll(allItems.where((element) => element.annonce.idAnnonce.compareTo(annonce.idAnnonce)!=0));

    nosuggestionList.addAll(allItems.where((element) => element.annonce.idAnnonce.compareTo(annonce.idAnnonce)==0));


    int oldqte=0;

    if(nosuggestionList.length >0){
      oldqte=nosuggestionList[0].qte;
    }

    CartItemModel newItem =  CartItemModel();
    newItem.annonce = annonce;
    newItem.nomCategorieFilleProduit = annonce.nomCategorieFilleProduit;
    newItem.qte = oldqte +qte;

    suggestionList.add(newItem);

    allItems=suggestionList;
    debugPrint('add');
  }



  void decreaseQte(CartItemModel item,int qte) {

    List<CartItemModel> suggestionList=[];
    List<CartItemModel> nosuggestionList=[];

    suggestionList.addAll(allItems.where((element) => element.annonce.idAnnonce.compareTo(item.annonce.idAnnonce)!=0));

    nosuggestionList.addAll(allItems.where((element) => element.annonce.idAnnonce.compareTo(item.annonce.idAnnonce)==0));

    int oldqte=0;

    if(nosuggestionList.length >0) oldqte=nosuggestionList[0].qte;

    //decrementer la quantité
    //int final_qte = oldqte-1;
    int final_qte = oldqte-qte;

    //si la qte ==0 enlever le produit du panier sinon mettre a jour la qte
    if(final_qte < 1) {
      removeFromCart(item);
    }else{
      //assigner la quantité
      item.qte = final_qte;
      suggestionList.add(item);
      allItems=suggestionList;
    }
  }

  void removeFromCart(CartItemModel item) {
    allItems.remove(item);
    debugPrint('000');
    notifyListeners();
  }

  void clearCart() {
    allItems = [];
    notifyListeners();
  }

   List<CartItemModel>  getItems() {
    return allItems;
  }

  int getCount() {
    return allItems.length;
  }

  int itemCartTotal(CartItemModel item) {
    return item.annonce.prix * item.qte;
  }


  Map<String, dynamic> toMap() {
    List<Map> items =
    this.allItems != null ? this.allItems.map((i) => i.toMap()).toList() : null;

    return {
      'allItems': items,
    };
  }


  @override
  factory CartModel.fromMap(Map<String, dynamic> json) {

    List<CartItemModel> cartItemList=null;
    var listSpeculation = json['allItems'] as List;
    if(listSpeculation!=null) cartItemList = listSpeculation.map((i) => CartItemModel.fromMap(i)).toList();

    CartModel cart = CartModel.create(
      allItems: cartItemList,
    );
    print(cart);
    return cart;

  }

}
