class MyStatisticsModel {
  String status;
  int totalOffre;
  int activeOffre;
  int chiffreAffaire;
  String offreSpeculation;
  int idUtilisateur;

  MyStatisticsModel({
    this.totalOffre, 
    this.activeOffre, 
    this.chiffreAffaire, 
    this.offreSpeculation,
    this.idUtilisateur
  });
  
  MyStatisticsModel.create({
    this.status,
    this.totalOffre, 
    this.activeOffre, 
    this.chiffreAffaire, 
    this.offreSpeculation,
    this.idUtilisateur
  });

  @override
  factory MyStatisticsModel.fromMap(Map<String, dynamic> json){
    return MyStatisticsModel.create(
      status: json['status'],
      totalOffre: json['total_offre'],
      activeOffre: json['active_offre'],
      chiffreAffaire: json['chiffre_affaire'],
      offreSpeculation: json['offre_speculation'],
      idUtilisateur: json['id_user']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'total_offre': totalOffre,
      'active_offre': activeOffre,
      'chiffre_affaire': chiffreAffaire,
      'offre_speculation': offreSpeculation,
      'id_user': idUtilisateur
    };
  }

}