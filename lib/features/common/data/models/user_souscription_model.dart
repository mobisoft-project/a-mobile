class UserSouscriptionModel {
  int id;
  String key;
  String numeroSouscription;
  int montant;
  String datePaiement;
  String dateFinsouscription;
  int etat;
  String typeSouscription;
  int idUtilisateur;

  UserSouscriptionModel();
  UserSouscriptionModel.create(
      {this.id,
      this.key,
      this.numeroSouscription,
      this.montant,
      this.datePaiement,
      this.dateFinsouscription,
      this.etat,
      this.typeSouscription,
      this.idUtilisateur});

  @override
  factory UserSouscriptionModel.fromMap(Map<String, dynamic> json){
    return UserSouscriptionModel.create(
      id: json['id'],
      key: json['key'],
      numeroSouscription: json['numero_souscription'],
      montant: json['montant'],
      datePaiement :json['date_paiement'],
      dateFinsouscription: json['date_finsouscription'],
      etat: json['etat'],
      typeSouscription: json['type_souscription'],
      idUtilisateur: json['id_user']
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'key': key,
      'numero_souscription': numeroSouscription,
      'montant': montant,
      'date_paiement': datePaiement,
      'date_finsouscription': dateFinsouscription,
      'etat': etat,
      'type_souscription': typeSouscription,
      'id_user': idUtilisateur
    };
  }
}