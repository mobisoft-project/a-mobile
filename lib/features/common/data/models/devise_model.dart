

class DeviseModel {
  int id;
  String name;
  int createdBy;
  int modifiedBy;
  String jour;
  String heure;


  DeviseModel();


  DeviseModel.create({this.id, this.name, this.createdBy, this.modifiedBy, this.jour,
      this.heure});
  @override
  factory DeviseModel.fromMap(Map<String, dynamic> json) {

    return DeviseModel.create(
        id: json['id'],
        name: json['name'],
        createdBy: json['createdBy'],
        modifiedBy: json['modifiedBy'],
        jour: json['jour'],
        heure: json['heure']
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'createdBy': createdBy,
      'modifiedBy': modifiedBy,
      'temp_max': jour,
      'heure': heure,
    };
  }
}