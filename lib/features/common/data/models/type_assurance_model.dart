

class   TypeAssuranceModel {
  int apiId;
  int typeassuranceId;
  String typeassuranceNom;
  int assureurId;
  String assureurNom;
  int prix;
  int validite;
  int isvalide;
  String situation;
  String date;


  TypeAssuranceModel();


  TypeAssuranceModel.create({this.apiId, this.typeassuranceId,
      this.typeassuranceNom, this.assureurId, this.assureurNom, this.prix,
      this.validite, this.isvalide, this.situation, this.date});

  @override
  factory TypeAssuranceModel.fromMap(Map<String, dynamic> json) {

    return TypeAssuranceModel.create(
      apiId: json['apiId'],
      typeassuranceId: json['typeassuranceId'],
      typeassuranceNom: json['typeassuranceNom'],
      assureurId: json['assureurId'],
      assureurNom: json['assureurNom'],
      prix: json['prix'],
      validite: json['validite'],
      isvalide: json['isvalide'],
      situation: json['situation'],
      date: json['date'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'apiId': apiId,
      'typeassuranceId': typeassuranceId,
      'typeassuranceNom': typeassuranceNom,
      'assureurId': assureurId,
      'assureurNom': assureurNom,
      'prix': prix,
      'validite': validite,
      'isvalide': isvalide,
      'situation': situation,
      'date': date,
    };
  }
}
