

class AssuranceModel {
  String denomination;
  String logo;
  int statuts;
  int apiId;

  AssuranceModel();

  AssuranceModel.create({this.denomination, this.logo, this.statuts,this.apiId});

  @override
  factory AssuranceModel.fromMap(Map<String, dynamic> json) {

    return AssuranceModel.create(
        denomination: json['denomination'],
        logo: json['logo'],
        statuts: json['statuts'],
        apiId: json['apiId']
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'denomination': denomination,
      'logo': logo,
      'statuts': statuts,
      'apiId': apiId,
    };
  }
}
