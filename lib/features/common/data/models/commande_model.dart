import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_detail_model.dart';
import 'package:agrimobile/features/common/data/models/commande_user_model.dart';

class CommandeModel {

  String keyCommande;
  int idCommande;
  String numeroCommande;
  String dateCommande;
  int amountCommande;
  String modeLivraisonCommande;
  String motCleModeLivraisonCommande;
  String moyenTransportCommande;
  String transporterCommande;
  int etatCommande;
  String urlPaiementCommande;
  String NomPrenomUserCommande;
  String emailUserCommande;
  String telephoneUserCommande;

  CommandeModel();
  CommandeModel.create({this.keyCommande, this.idCommande, this.numeroCommande, this.dateCommande, this.amountCommande,this.modeLivraisonCommande,this.motCleModeLivraisonCommande,this.moyenTransportCommande,this.transporterCommande,
    this.etatCommande, this.urlPaiementCommande, this.NomPrenomUserCommande,
    this.emailUserCommande, this.telephoneUserCommande});


  @override
  factory CommandeModel.fromMap(Map<String, dynamic> json) {
    return CommandeModel.create(
      keyCommande: json['keyCommande'],
      idCommande: json['id_commande'],
      numeroCommande: json['numeroCommande'],
      dateCommande: json['dateCommande'],
      amountCommande: json['amountCommande'],
      modeLivraisonCommande: json['modeLivraisonCommande'],
      motCleModeLivraisonCommande: json['motCleModeLivraisonCommande'],
      moyenTransportCommande: json['moyenTransportCommande'],
      transporterCommande: json['transporterCommande'],
      etatCommande: json['etatCommande'],
      urlPaiementCommande: json['urlPaiementCommande'],
      NomPrenomUserCommande: json['NomPrenomUserCommande'],
      emailUserCommande: json['emailUserCommande'],
      telephoneUserCommande: json['telephoneUserCommande'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id_commande': idCommande,
      'keyCommande': keyCommande,
      'numeroCommande': numeroCommande,
      'dateCommande': dateCommande,
      'amountCommande': amountCommande,
      'modeLivraisonCommande': modeLivraisonCommande,
      'motCleModeLivraisonCommande': motCleModeLivraisonCommande,
      'moyenTransportCommande': moyenTransportCommande,
      'transporterCommande': transporterCommande,
      'etatCommande': etatCommande,
      'urlPaiementCommande': urlPaiementCommande,
      'NomPrenomUserCommande': NomPrenomUserCommande,
      'emailUserCommande': emailUserCommande,
      'telephoneUserCommande': telephoneUserCommande,
    };
  }

  @override
  String toGetMap() {
    return "keyCommande=$keyCommande&numeroCommande=$numeroCommande&dateCommande=$dateCommande&modeLivraisonCommande=$modeLivraisonCommande&motCleModeLivraisonCommande=$motCleModeLivraisonCommande&moyenTransportCommande=$moyenTransportCommande&transporterCommande=$transporterCommande&amountCommande=$amountCommande&etatCommande=$etatCommande&urlPaiementCommande=$urlPaiementCommande&NomPrenomUserCommande=$NomPrenomUserCommande&emailUserCommande=$emailUserCommande&telephoneUserCommande=$telephoneUserCommande";
  }

}