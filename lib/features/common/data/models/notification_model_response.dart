import 'package:agrimobile/features/common/data/models/notification_model.dart';

class NotificationRecuperationModel {
  String status;
  String message;
  List<NotificationModel> allNotification;

  NotificationRecuperationModel();

  NotificationRecuperationModel.create({this.status, this.message, this.allNotification});


  @override
  factory NotificationRecuperationModel.fromMap(Map<String, dynamic> json) {
    List<NotificationModel> notificationList=null;
    var listNotification = json['all_notification'] as List;
    if(listNotification!=null) notificationList = listNotification.map((i) => NotificationModel.fromMap(i)).toList();
    return NotificationRecuperationModel.create(
      status: json['status'],
      message: json['message'],
      allNotification: notificationList,
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'all_notification': allNotification,
    };
  }
}
