import 'package:agrimobile/features/common/data/models/user_assurance_model.dart';

class AssuranceReponseModel {
  String status;
  String message;
  int solde;
  List<UserAssuranceModel> assurances;

  AssuranceReponseModel();

  AssuranceReponseModel.create({this.status, this.message, this.solde,this.assurances});

  @override
  factory AssuranceReponseModel.fromMap(Map<String, dynamic> json) {

    List<UserAssuranceModel> uassuranceList = null;
    var listMeteo = json['assurances'] as List;
    if(listMeteo!=null) uassuranceList = listMeteo.map((i) => UserAssuranceModel.fromMap(i)).toList();

    return AssuranceReponseModel.create(
        status: json['status'],
        message: json['message'],
        solde: json['solde'],
        assurances: uassuranceList
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
      'solde': solde,
      'assurances': assurances,
    };
  }
}
