import 'package:agrimobile/features/common/data/models/biztype_model.dart';
import 'package:agrimobile/features/common/data/models/devise_model.dart';
import 'package:agrimobile/features/common/data/models/eop_model.dart';
import 'package:agrimobile/features/common/data/models/produit_model.dart';
import 'package:agrimobile/features/common/data/models/sous_produit_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_model.dart';
import 'package:agrimobile/features/common/data/models/unite_model.dart';

import 'meteo_model.dart';
import 'assurance_model.dart';
import 'type_assurance_model.dart';
import 'information_model.dart';

class SimulationModel {

  String name;
  int position;
  EopModel information;
  SimulationModel();


  SimulationModel.create({this.name, this.information, this.position});


  @override
  factory SimulationModel.fromMap(Map<String, dynamic> json) {

    return SimulationModel.create(
      name: json['name'],
      position: json['position'],
      information: json['information']!=null?EopModel.fromMap(json['information']):null,
    );




  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'position': position,
      'information': information,
    };
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'position': position,
      'information': information.toMap(),
    };
  }
}