

class RegionModel {
  int id;
  String nomregion;
  int paysId;

  RegionModel();

  RegionModel.create({this.id, this.nomregion, this.paysId});

  @override
  factory RegionModel.fromMap(Map<String, dynamic> json) {

    return RegionModel.create(
        id: json['id'],
        nomregion: json['nomregion'],
        paysId: json['paysId'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nomregion': nomregion,
      'paysId': paysId
    };
  }
}