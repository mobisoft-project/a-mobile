import 'dart:convert';

class CommandeDetailModel {

  String nom_produit;
  String qte_produit;
  String prix_unitaire;
  String prix_total;

  CommandeDetailModel();

  CommandeDetailModel.create({this.nom_produit, this.qte_produit,this.prix_unitaire,this.prix_total});

  @override
  factory CommandeDetailModel.fromMap(Map<String, dynamic> json) {
    return CommandeDetailModel.create(
        nom_produit: json['nom_produit'],
        qte_produit: json['qte_produit'],
        prix_unitaire: json['prix_unitaire'],
        prix_total: json['prix_total'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'nomProduitCommandeDetail': nom_produit,
      'qteProduitCommandeDetail': qte_produit,
      'prixUnitaireCommandeDetail': prix_unitaire,
      'prixTotalCommandeDetail': prix_total,
    };
  }

  @override
  String toGetMap() {
    return "nom_produit=$nom_produit&qte_produit=$qte_produit&prix_unitaire=$prix_unitaire&prix_total=$prix_total";
  }

}


