import 'dart:convert';

import 'package:agrimobile/features/common/data/models/commande_detail_model.dart';
import 'package:agrimobile/features/common/data/models/commande_user_model.dart';

class CommandeDeleteResponseModel {

  String status;
  String message;

  CommandeDeleteResponseModel();
  CommandeDeleteResponseModel.create({this.status, this.message});


  @override
  factory CommandeDeleteResponseModel.fromMap(Map<String, dynamic> json) {

    return CommandeDeleteResponseModel.create(
      status: json['status'],
      message: json['message'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': message,
    };
  }

  @override
  String toGetMap() {
    return "status=$status&message=$message";
  }

}


