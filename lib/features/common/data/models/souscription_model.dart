class SouscriptionModel{
  int idTypeSouscription;
  String keyTypeSouscription;
  String denoTypeSouscription;
  int prixSouscription;
  int dureeSouscription;
  int userId;

  SouscriptionModel();

  SouscriptionModel.create({
    this.idTypeSouscription,
    this.keyTypeSouscription,
    this.denoTypeSouscription,
    this.prixSouscription,
    this.dureeSouscription,
    this.userId
  });

  @override
  factory SouscriptionModel.fromMap(Map<String, dynamic> json){
    return SouscriptionModel.create(
      idTypeSouscription: json['id_type_souscription'],
      keyTypeSouscription: json['key_type_souscription'],
      denoTypeSouscription: json['deno_type_souscription'],
      prixSouscription: json['prix_souscription'],
      dureeSouscription: json['duree_souscription'],
      userId: json['user_id'],
    );
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id_type_souscription': idTypeSouscription,
      'key_type_souscription': keyTypeSouscription,
      'deno_type_souscription': denoTypeSouscription,
      'prix_souscription': prixSouscription,
      'duree_souscription': dureeSouscription,
      'user_id': userId
    };
  }
}