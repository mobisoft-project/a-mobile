class UserStatisticModel {
  String status;
  int nbreAchat;
  int montantAchat;
  int endLivraison;
  int waitingLivraison;
  String demandeSpeculation;
  int idUtlisateur;

  UserStatisticModel({this.nbreAchat,
      this.montantAchat,
      this.endLivraison,
      this.waitingLivraison,
      this.demandeSpeculation, this.idUtlisateur});

  UserStatisticModel.create(
      {this.status,
      this.nbreAchat,
      this.montantAchat,
      this.endLivraison,
      this.waitingLivraison,
      this.demandeSpeculation, this.idUtlisateur});

  UserStatisticModel.fromMap(Map<String, dynamic> json) {
    status = json['status'];
    nbreAchat = json['nbre_achat'];
    montantAchat = json['montant_achat'];
    endLivraison = json['end_livraison'];
    waitingLivraison = json['waiting_livraison'];
    demandeSpeculation = json['demande_speculation'];
    idUtlisateur = json['id_user'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['nbre_achat'] = this.nbreAchat;
    data['montant_achat'] = this.montantAchat;
    data['end_livraison'] = this.endLivraison;
    data['waiting_livraison'] = this.waitingLivraison;
    data['demande_speculation'] = this.demandeSpeculation;
    data['id_user'] =  this.idUtlisateur;
    return data;
  }
}