import 'package:agrimobile/features/common/data/models/annonce_model.dart';
import 'package:agrimobile/features/common/data/models/infoprice_model.dart';

class PriceResponseModel {
  String status;
  String lastDate;
  List<InfoPriceModel> datas;





  PriceResponseModel();

  PriceResponseModel.create({this.status, this.lastDate, this.datas});

  @override
  factory PriceResponseModel.fromMap(Map<String, dynamic> json) {


    List<InfoPriceModel> infoPriceList=null;
    var listInfoPrice = json['datas'] as List;
    if(listInfoPrice!=null) infoPriceList = listInfoPrice.map((i) => InfoPriceModel.fromMap(i)).toList();


    return PriceResponseModel.create(
      status: json['status'],
      lastDate: json['last_date'],
      datas: infoPriceList,
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'last_date': lastDate,
      'annonce': datas,
    };
  }
}