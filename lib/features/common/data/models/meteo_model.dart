

class MeteoModel {
  String date;
  String description;
  double tempDay;
  double tempMin;
  double tempMax;
  double tempNight;
  double tempMorn;
  double tempEve;
  double humidity;
  int apiId;



  MeteoModel();

  MeteoModel.create({this.date, this.description, this.tempDay, this.tempMin,
      this.tempMax, this.tempNight, this.tempMorn, this.tempEve, this.humidity,
      this.apiId});

  @override
  factory MeteoModel.fromMap(Map<String, dynamic> json) {

    return MeteoModel.create(
        date: json['date'],
        description: json['description'],
        tempDay: double.parse("${json['temp_day']}"),
        tempMin: double.parse("${json['temp_min']}"),
        tempMax: double.parse("${json['temp_max']}"),
        tempNight: double.parse("${json['temp_night']}"),
        tempMorn: double.parse("${json['temp_morn']}"),
        tempEve: double.parse("${json['temp_eve']}"),
        humidity: double.parse("${json['humidity']}"),
        apiId: json['apiId']
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'date': date,
      'description': description,
      'temp_day': tempDay,
      'temp_min': tempMin,
      'temp_max': tempMax,
      'temp_night': tempNight,
      'temp_morn': tempMorn,
      'temp_eve': tempEve,
      'humidity': humidity,
      'apiId': apiId
    };
  }
}