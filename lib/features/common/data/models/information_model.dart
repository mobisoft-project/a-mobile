import 'package:agrimobile/features/common/data/models/produit_model.dart';



import 'biztype_model.dart';
import 'devise_model.dart';
import 'sous_produit_model.dart';
import 'unite_model.dart';

class   InformationModel {
  List<DeviseModel> devises;
  List<UniteModel> unites;
  List<ProduitModel> produits;
  List<BiztypeModel> biztypes;
  List<SousProduitModel> sousProduits;


  InformationModel();


  InformationModel.create({this.devises, this.unites, this.produits, this.biztypes, this.sousProduits});

  @override
  factory InformationModel.fromMap(Map<String, dynamic> json) {

    List<DeviseModel> deviseList=null;
    var listDevise = json['meteos'] as List;
    if(listDevise!=null) deviseList = listDevise.map((i) => DeviseModel.fromMap(i)).toList();

    List<UniteModel> uniteList=null;
    var listUnite = json['unites'] as List;
    if(listUnite!=null) uniteList = listUnite.map((i) => UniteModel.fromMap(i)).toList();

    List<ProduitModel> produitList=null;
    var listProduit = json['produits'] as List;
    if(listProduit!=null) produitList = listProduit.map((i) => ProduitModel.fromMap(i)).toList();

    List<BiztypeModel> biztyList=null;
    var listBiztype = json['biztypes'] as List;
    if(listBiztype!=null) biztyList = listBiztype.map((i) => BiztypeModel.fromMap(i)).toList();

    List<SousProduitModel> sousProduitsList=null;
    var listsousProduits = json['sous_produits'] as List;
    if(listsousProduits!=null) sousProduitsList = listsousProduits.map((i) => SousProduitModel.fromMap(i)).toList();

    return InformationModel.create(
      devises: deviseList,
      unites: uniteList,
      produits: produitList,
      biztypes:biztyList,
      sousProduits:sousProduitsList,
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'devises': devises,
      'unites': unites,
      'produits': produits,
      'biztypes': biztypes,
      'sous_produits': sousProduits,
    };
  }
}
