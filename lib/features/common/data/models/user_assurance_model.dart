

class UserAssuranceModel {
  int idInfoUserAssurance;
  int idUser;
  int idAssureurInfo;
  int montantOperation;
  int dureeOperation;
  String dateFin;
  String dateOperation;
  int satutOperation;
  String urlPaiement;
  String keyAssurance;

  UserAssuranceModel();

  UserAssuranceModel.create({this.idInfoUserAssurance, this.idUser, this.idAssureurInfo,this.montantOperation,this.dureeOperation, this.dateFin, this.dateOperation,this.satutOperation,this.urlPaiement,this.keyAssurance});

  @override
  factory UserAssuranceModel.fromMap(Map<String, dynamic> json) {

    return UserAssuranceModel.create(
        idInfoUserAssurance: json['id_info_userassurance'],
        idUser: json['id_user'],
        idAssureurInfo: json['id_assureur_info'],
        montantOperation: json['montant_operation'],
        dureeOperation: json['duree_operation'],
        dateFin: json['date_fin'],
        dateOperation: json['date_operation'],
        urlPaiement: json['url_paiement'],
        keyAssurance: json['key_assurance'],
        satutOperation: json['satut_operation']
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id_info_userassurance': idInfoUserAssurance,
      'id_user': idUser,
      'id_assureur_info': idAssureurInfo,
      'montant_operation': montantOperation,
      'duree_operation': dureeOperation,
      'date_fin': dateFin,
      'date_operation': dateOperation,
      'key_assurance': keyAssurance,
      'url_paiement': urlPaiement,
      'satut_operation': satutOperation,
    };
  }
}