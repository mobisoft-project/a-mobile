import 'package:agrimobile/features/common/data/models/annonce_model.dart';

class AnnonceResponseModel {
  String status;
  AnnonceModel annonce;





  AnnonceResponseModel();

  AnnonceResponseModel.create({this.status, this.annonce});

  @override
  factory AnnonceResponseModel.fromMap(Map<String, dynamic> json) {

    if(json['annonce']!=null) {
      return AnnonceResponseModel.create(
        status: json['status'],
        annonce: AnnonceModel.fromMap(json['annonce']),
      );
    }else{
      return AnnonceResponseModel.create(
        status: json['status']
      );
    }

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'annonce': annonce,
    };
  }
}