

class ProduitModel {
  int id;
  String nomCategorieFille;
  int categorieId;
  String uniteMesure;
  int statut;
  int dureeJour;


  ProduitModel();


  ProduitModel.create({this.id, this.nomCategorieFille, this.categorieId,
      this.uniteMesure, this.statut, this.dureeJour});

  @override
  factory ProduitModel.fromMap(Map<String, dynamic> json) {

    return ProduitModel.create(
        id: json['id'],
        nomCategorieFille: json['nomCategorie_fille'],
        categorieId: json['categorieId'],
        uniteMesure: json['unite_mesure'],
        statut: json['statut'],
        dureeJour: json['duree_jour']
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nomCategorie_fille': nomCategorieFille,
      'categorieId': categorieId,
      'unite_mesure': uniteMesure,
      'statut': statut,
      'duree_jour': dureeJour,
    };
  }
}