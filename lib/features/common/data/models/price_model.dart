

class PriceModel {
  String nomProduit;
  String ville;
  String prix;
  int apiId;


  PriceModel();


  PriceModel.create({this.nomProduit, this.ville, this.prix, this.apiId});
  @override
  factory PriceModel.fromMap(Map<String, dynamic> json) {

    return PriceModel.create(
      nomProduit: json['nom_produit'],
      ville: json['ville'],
      prix: json['prix'],
      apiId: json['apiId'],
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'nom_produit': nomProduit,
      'ville': ville,
      'prix': prix,
      'apiId': apiId,
    };
  }
}