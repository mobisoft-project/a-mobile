import 'meteo_model.dart';

class MeteoResponseModel {
  String status;
  List<MeteoModel> meteos;



  MeteoResponseModel();

  MeteoResponseModel.create({this.status, this.meteos});

  @override
  factory MeteoResponseModel.fromMap(Map<String, dynamic> json) {


    List<MeteoModel> meteoList=null;
    if(json['status'].compareTo("ok")==0) {
      var listMeteo = json['message'] as List;
      if (listMeteo != null)
        meteoList = listMeteo.map((i) => MeteoModel.fromMap(i)).toList();
    }

    return MeteoResponseModel.create(
        status: json['status'],
        meteos: meteoList,
    );

  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'status': status,
      'message': meteos,
    };
  }
}