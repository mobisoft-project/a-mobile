import 'dart:convert';
import 'dart:io';
import 'package:agrimobile/features/common/data/dto/actor_profil_dto.dart';
import 'package:agrimobile/features/common/data/dto/assurance_dto.dart';
import 'package:agrimobile/features/common/data/dto/commande_delete_dto.dart';
import 'package:agrimobile/features/common/data/dto/commande_send_dto.dart';
import 'package:agrimobile/features/common/data/dto/connection_dto.dart';
import 'package:agrimobile/features/common/data/dto/conseil_dto.dart';
import 'package:agrimobile/features/common/data/dto/contact_dto.dart';
import 'package:agrimobile/features/common/data/dto/eop_dto.dart';
import 'package:agrimobile/features/common/data/dto/image_dto.dart';
import 'package:agrimobile/features/common/data/dto/inscription_dto.dart';
import 'package:agrimobile/features/common/data/dto/annonce_dto.dart';
import 'package:agrimobile/features/common/data/dto/liste_commande_dto.dart';
import 'package:agrimobile/features/common/data/dto/meteo_dto.dart';
import 'package:agrimobile/core/error/Failure.dart';
import 'package:agrimobile/core/error/exception.dart';
import 'package:agrimobile/core/network/network_info.dart';
import 'package:agrimobile/core/utils/core_constantes.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';
import 'package:agrimobile/features/common/data/datasources/remote/api.dart';
import 'package:agrimobile/features/common/data/dto/my_statistics_dto.dart';
import 'package:agrimobile/features/common/data/dto/notification_dto.dart';
import 'package:agrimobile/features/common/data/dto/scoop_dto.dart';
import 'package:agrimobile/features/common/data/dto/simulation_dto.dart';
import 'package:agrimobile/features/common/data/dto/souscription_dto.dart';
import 'package:agrimobile/features/common/data/dto/souscription_send_dto.dart';
import 'package:agrimobile/features/common/data/dto/speculation_dto.dart';
import 'package:agrimobile/features/common/data/dto/user_statistics_dto.dart';
import 'package:agrimobile/features/common/data/models/account_statistique_model.dart';
import 'package:agrimobile/features/common/data/models/actor_profil_model.dart';
import 'package:agrimobile/features/common/data/models/annonce_model.dart';
import 'package:agrimobile/features/common/data/models/annonce_response_model.dart';
import 'package:agrimobile/features/common/data/models/assurance_reponse_model.dart';
import 'package:agrimobile/features/common/data/models/commande_delete_response_model.dart';
import 'package:agrimobile/features/common/data/models/commande_response_model.dart';
import 'package:agrimobile/features/common/data/models/conseil_recuperation_model.dart';
import 'package:agrimobile/features/common/data/models/contact_model.dart';
import 'package:agrimobile/features/common/data/models/liste_commande_response_model.dart';
import 'package:agrimobile/features/common/data/models/notification_model_response.dart';
import 'package:agrimobile/features/common/data/models/price_response_model.dart';
import 'package:agrimobile/features/common/data/models/region_model.dart';
import 'package:agrimobile/features/common/data/models/app_response_model.dart';
import 'package:agrimobile/features/common/data/models/country_model.dart';
import 'package:agrimobile/features/common/data/models/register_model.dart';
import 'package:agrimobile/features/common/data/models/souscription_recuperation_model.dart';
import 'package:agrimobile/features/common/data/models/souscription_send_model.dart';
import 'package:agrimobile/features/common/data/models/speculation_response_model.dart';
import 'package:agrimobile/features/common/data/models/meteo_response_model.dart';
import 'package:agrimobile/features/common/data/models/tampon_image_model.dart';
import 'package:agrimobile/features/common/data/models/user_souscription_recuperation_model.dart';
import 'package:agrimobile/features/common/data/models/user_statistics_model.dart';
import 'package:dartz/dartz.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';

import 'package:agrimobile/features/common/data/dto/update_assurance_dto.dart';
import 'package:agrimobile/features/common/data/models/update_assurance_reponse_model.dart';


import 'package:agrimobile/features/common/data/models/liste_transporter_response_model.dart';
import 'package:agrimobile/features/common/data/dto/liste_transporter_dto.dart';
import 'package:agrimobile/features/common/data/dto/autre_pret_dto.dart';
import 'package:agrimobile/features/common/data/dto/liste_autre_pret_dto.dart';


class ApiRepository implements Api{

  Dio dio;
  final NetworkInfo networkInfo =
  new NetworkInfoImpl(new DataConnectionChecker());

  ApiRepository(){
    // or new Dio with a BaseOptions instance.
    BaseOptions options = new BaseOptions(
      baseUrl: "$SERVER_URL",
      connectTimeout: 50000,
      receiveTimeout: 30000,
    );
    dio = new Dio(options);
  }

  Future<Dio> getApiClient() async {
    var token = "";
    dio.interceptors.clear();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      // Do something before request is sent
      options.headers["Authorization"] = "Bearersend_message " + token;
      return options;
    },onResponse:(Response response) {
      // Do something with response data
      return response; // continue
    }, onError: (DioError error) async {
      // Do something with response error
      if (error.response?.statusCode == 403) {
        dio.interceptors.requestLock.lock();
        dio.interceptors.responseLock.lock();
        RequestOptions options = error.response.request;
        //refresh token
        //token = await user.getIdToken(refresh: true);

        options.headers["Authorization"] = "Bearer " + token;

        dio.interceptors.requestLock.unlock();
        dio.interceptors.responseLock.unlock();
        return dio.request(options.path,options: options);
      } else {
        return error;
      }
    }));
    //dio.options.baseUrl = baseUrl;
    return dio;
  }

  @override
  Future<Either<Failure, AppResponseModel>> refreshLocation(ConnectionDto connectionDto) async{

    if (await networkInfo.isConnected) {
      try {

        String fullData=connectionDto.toGetMap();

        var response = await dio.get("${DataConstantesUtils.RLOCATION_SERVER_URL}?$fullData");

        //var data = response.data;
        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> login(ConnectionDto connectionDto) async{

    if (await networkInfo.isConnected) {
      try {


        String fullData=connectionDto.toGetMap();

        print("${DataConstantesUtils.LOGIN_SERVER_URL}?$fullData");
        var response = await dio.get("${DataConstantesUtils.LOGIN_SERVER_URL}?$fullData");

        print(response);
        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> updatePassword(ConnectionDto connectionDto) async{

    if (await networkInfo.isConnected) {
      try {


        String fullData=connectionDto.toGetMap();

        var response = await dio.get("${DataConstantesUtils.UPASSWORD_SERVER_URL}?$fullData");

        //var data = response.data;
        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> updateProfil(InscriptionDto inscriptionDto) async{

    if (await networkInfo.isConnected) {
      try {


        String fullData=inscriptionDto.toGetMap();

        var response = await dio.get("${DataConstantesUtils.RPROFIL_SERVER_URL}?$fullData");

        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> confirmRegistration(ConnectionDto connectionDto) async{

    if (await networkInfo.isConnected) {
      try {

        String fullData=connectionDto.toGetMap();

        var response = await dio.get("${DataConstantesUtils.FREGISTER_SERVER_URL}?$fullData");

        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, RegisterModel>> registration(InscriptionDto inscriptionDto) async{

    if (await networkInfo.isConnected) {
      try {

        FormData formData=new FormData.fromMap(inscriptionDto.toMap());

        if(inscriptionDto.fileProjet.isNotEmpty){

          List<File> image=inscriptionDto.fileProjet;

          for(int u=0;u<image.length;u++){

            if(image[u]!=null && image[u].path.compareTo("")!=0){
              List<String> pathInfo=image[u].path.split("/");
              String  filename=pathInfo[pathInfo.length-1];

              formData.files.addAll([
                MapEntry("photo$u",MultipartFile.fromFileSync("./${image[u].path}",filename:"$filename"))
              ]);
            }

          }


        }

        print(DataConstantesUtils.REGISTER_SERVER_URL);
        print(formData.fields);

        var response = await dio.post(DataConstantesUtils.REGISTER_SERVER_URL,data:formData);

        Map responseMap = jsonDecode(response.toString());
        return Right((RegisterModel.fromMap(responseMap)));


        /*
        String fullData=inscriptionDto.toGetMap();


        var response = await dio.get("${DataConstantesUtils.REGISTER_SERVER_URL}?$fullData");


        //var data = response.data;
        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((RegisterModel.fromMap(responseMap)));
        */

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> forgetPassord(ConnectionDto connectionDto) async{

    if (await networkInfo.isConnected) {
      try {


        String fullData=connectionDto.toGetMap();

        var response = await dio.get("${DataConstantesUtils.FORGET_SERVER_URL}?$fullData");

        //var data = response.data;
        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> resetPassword(ConnectionDto connectionDto) async{

    if (await networkInfo.isConnected) {
      try {


        String fullData=connectionDto.toGetMap();
        String fullUrl="${DataConstantesUtils.RESET_SERVER_URL}?$fullData";

        var response = await dio.get(fullUrl);

        //var data = response.data;
        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<CountryModel>>> getCountry() async{

    if (await networkInfo.isConnected) {

      try {

        var response = await dio.get(DataConstantesUtils.COUNTRY_SERVER_URL);
        List<dynamic> responseMap ;

        if (response.data.runtimeType == String){
          responseMap = jsonDecode( response.toString());
        }else{
          responseMap = response.data;
        }

        List<CountryModel> countryList = responseMap.map((i) => CountryModel.fromMap(i)).toList();

        return Right((countryList));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<RegionModel>>> getRegion() async{

    if (await networkInfo.isConnected) {
      try {

        var response = await dio.get(DataConstantesUtils.REGION_SERVER_URL);

        List<dynamic> responseMap ;

        if (response.data.runtimeType == String){
          responseMap = jsonDecode( response.toString());
        }else{
          responseMap = response.data;
        }

        List<RegionModel> regionList = responseMap.map((i) => RegionModel.fromMap(i)).toList();

        return Right((regionList));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<AnnonceModel>>> getAnnonce(String token) async{

    if (await networkInfo.isConnected) {
      try {

        String url = DataConstantesUtils.ANNONCE_SERVER_URL;

        if(token.compareTo("")!=0)url+='?access-token='+token;
        print(url);

        var response = await dio.get(url);

        print(response.toString());

        List<dynamic> responseMap ;

        if (response.data.runtimeType == String){
          responseMap = jsonDecode( response.toString());
        }else{
          responseMap = response.data;
        }

        List<AnnonceModel> annonceList = responseMap.map((i) => AnnonceModel.fromMap(i)).toList();

        return Right((annonceList));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AnnonceResponseModel>> sendAnnonce(AnnonceDto annonceDto) async{

    if (await networkInfo.isConnected) {
      try {


        Map<String, dynamic> full_info=annonceDto.toMap();
        FormData formData=new FormData.fromMap(full_info);

        if(annonceDto.photo.compareTo("0")!=0){
          formData.files.addAll([
            MapEntry("photo",MultipartFile.fromFileSync("./${annonceDto.photo}",filename:"photo.jpg"))
          ]);
        }

        print(formData.fields);

        var response = await dio.post(DataConstantesUtils.PANNONCE_SERVER_URL,data:formData);

        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((AnnonceResponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, ContactModel>> getContact(ContactDto contactDto) async{

    if (await networkInfo.isConnected) {
      try {

        String fullData=contactDto.toGetMap();

        var response = await dio.get("${DataConstantesUtils.CONTACT_SERVER_URL}?$fullData");

        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((ContactModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, PriceResponseModel>> getPrice(String token) async{

    if (await networkInfo.isConnected) {
      try {


        String url="${DataConstantesUtils.PRICE_SERVER_URL}?access-token=$token";

        var response = await dio.get(url);

        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((PriceResponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> sabonnerild(SpeculationDto speculationDto,String type) async{

    if (await networkInfo.isConnected) {
      try {

        String fullData=speculationDto.toGetMap();
        String url="${DataConstantesUtils.ASPECULATION_SERVER_URL}";
        if(type.compareTo("2")==0)url="${DataConstantesUtils.DSPECULATION_SERVER_URL}";

        String fullUrl="$url?$fullData";

        print(fullUrl);
        var response = await dio.get(fullUrl);

        var data = response.toString();
        Map responseMap = jsonDecode(data);
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {

      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, MeteoResponseModel>> askMeteo(MeteoDto meteoDto) async{

    if (await networkInfo.isConnected) {
      try {

        String fullData=meteoDto.toGetMap();
        String url="${DataConstantesUtils.AMETEO_SERVER_URL}?$fullData";

        var response = await dio.get(url);
        var data = response.toString();


        Map responseMap = jsonDecode(data);
        return Right((MeteoResponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, SpeculationResponseModel>> sabonner(SpeculationDto speculationDto,String typeoperation) async{

    if (await networkInfo.isConnected) {
      try {



        String fullData=speculationDto.toGetMap();
        String url="${DataConstantesUtils.ASPECULATION_SERVER_URL}";
        if(typeoperation.compareTo("2")==0)url="${DataConstantesUtils.DSPECULATION_SERVER_URL}";

        String fullUrl="$url?$fullData";


        var response = await dio.get(fullUrl);

        Map responseMap  ;
        if (response.data.runtimeType == String){
          responseMap = jsonDecode( response.toString());
        }else{
          responseMap = response.data;
        }


        print(fullUrl);

        return Right((SpeculationResponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AssuranceReponseModel>> askAbonnement(AssuranceDto assuranceDto) async{

    if (await networkInfo.isConnected) {
      try {

        String fullData=assuranceDto.toGetMap();
        String url="${DataConstantesUtils.ASSURANCE_SERVER_URL}?$fullData";
        print(url);

        var response = await dio.get("$url");

        print(response);

        Map responseMap  ;
        if (response.data.runtimeType == String){
          responseMap = jsonDecode( response.toString());
        }else{
          responseMap = response.data;
        }

        return Right((AssuranceReponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> getEop(EopDto eopDto) async{

    if (await networkInfo.isConnected) {
      try {

        eopDto.accessToken=DataConstantesUtils.bTokenServer;

        FormData formData=new FormData.fromMap(eopDto.toMap());
        var response = await dio.post(DataConstantesUtils.eopServerUrl,data:formData);

        print(response);
        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> getScoop(ScoopDto scoopDto) async{

    if (await networkInfo.isConnected) {
      try {

        scoopDto.accessToken=DataConstantesUtils.bTokenServer;

        FormData formData=new FormData.fromMap(scoopDto.toMap());
        var response = await dio.post(DataConstantesUtils.scoopServerUrl,data:formData);

        print(response);
        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> sendUsimulation(SimulationDto simulationDto) async{

    if (await networkInfo.isConnected) {
      try {

        simulationDto.accessToken=DataConstantesUtils.bTokenServer;

        FormData formData=new FormData.fromMap(simulationDto.toMap());
        var response = await dio.post(DataConstantesUtils.usimulationServerUrl,data:formData);

        print(response);
        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> sendSsimulation(SimulationDto simulationDto) async{

    if (await networkInfo.isConnected) {
      try {

        simulationDto.accessToken=DataConstantesUtils.bTokenServer;

        FormData formData=new FormData.fromMap(simulationDto.toMap());
        var response = await dio.post(DataConstantesUtils.ssimulationServerUrl,data:formData);

        print(response);
        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> sendUpret(SimulationDto simulationDto) async{

    if (await networkInfo.isConnected) {
      try {

        simulationDto.accessToken=DataConstantesUtils.bTokenServer;

        FormData formData=new FormData.fromMap(simulationDto.toMap());

        if(simulationDto.fileProjet!=null && simulationDto.fileProjet.isNotEmpty){

          List<File> image=simulationDto.fileProjet;

          for(int u=0;u<image.length;u++){

            if(image[u]!=null && image[u].path.compareTo("")!=0){
              List<String> pathInfo=image[u].path.split("/");
              String  filename=pathInfo[pathInfo.length-1];

              formData.files.addAll([
                MapEntry("photo$u",MultipartFile.fromFileSync("./${image[u].path}",filename:"$filename"))
              ]);
            }

          }

        }


        var response = await dio.post(DataConstantesUtils.upretServerUrl,data:formData);

        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> sendSpret(SimulationDto simulationDto) async{

    if (await networkInfo.isConnected) {
      try {

        simulationDto.accessToken=DataConstantesUtils.bTokenServer;

        FormData formData=new FormData.fromMap(simulationDto.toMap());


        if(simulationDto.fileProjet!=null && simulationDto.fileProjet.isNotEmpty){

          List<File> image=simulationDto.fileProjet;

          for(int u=0;u<image.length;u++){

            if(image[u]!=null && image[u].path.compareTo("")!=0){
              List<String> pathInfo=image[u].path.split("/");
              String  filename=pathInfo[pathInfo.length-1];

              formData.files.addAll([
                MapEntry("photo$u",MultipartFile.fromFileSync("./${image[u].path}",filename:"$filename"))
              ]);
            }

          }


        }


        var response = await dio.post(DataConstantesUtils.spretServerUrl,data:formData);

        print(response);
        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));





      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> sendDocument(ImageDto imageDto) async{

    if (await networkInfo.isConnected) {
      try {

        imageDto.accessToken=DataConstantesUtils.bTokenServer;
        imageDto.nbrePhoto=imageDto.fileProjet.length;

        FormData formData=new FormData.fromMap(imageDto.toMap());


        if(imageDto.fileProjet.isNotEmpty){

          List<TamponImageModel> image=imageDto.fileProjet;

          for(int u=0;u<image.length;u++){

            if(image[u]!=null && image[u].path.compareTo("")!=0){
              List<String> pathInfo=image[u].path.split(".");
              String  filename="${image[u].titre}.${pathInfo[pathInfo.length-1]}";

              formData.files.addAll([
                MapEntry("photo$u",MultipartFile.fromFileSync("./${image[u].path}",filename:"$filename"))
              ]);
            }
          }

        }


        var response = await dio.post(DataConstantesUtils.imagepretServerUrl,data:formData);

        print(response);
        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));





      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> getProjet(SimulationDto simulationDto) async{

    if (await networkInfo.isConnected) {
      try {

        simulationDto.accessToken=DataConstantesUtils.bTokenServer;
        FormData formData=new FormData.fromMap(simulationDto.toMap());


        var response = await dio.post(DataConstantesUtils.listepretServerUrl,data:formData);

        print(response);
        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }


  @override
  Future<Either<Failure, ConseilRecuperationModel>> getConseil(ConseilDto conseilDto) async {

    if (await networkInfo.isConnected) {
      try {


        FormData formData=new FormData.fromMap(conseilDto.toMap());
        var response = await dio.post(DataConstantesUtils.CONSEIL_SERVER_URL ,data:formData);

        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((ConseilRecuperationModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, ConseilRecuperationModel>> deleteConseil(ConseilDto conseilDto) async {
    if(await networkInfo.isConnected){
      try{
        print("delete request = ${DataConstantesUtils.DELETE_CONSEIL_URL}");
        print("deleteConseil dto = ${conseilDto.toMap()}");
        FormData formData=new FormData.fromMap(conseilDto.toMap());
        var response = await dio.post(DataConstantesUtils.DELETE_CONSEIL_URL ,data:formData);

        var data = response.toString();
        print("delete response = ${data}");

        Map responseMap = jsonDecode(data);

        return Right((ConseilRecuperationModel.fromMap(responseMap)));
      }on ServerException{
        return Left(ServerFailure());
      }
    }else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }

  }

  @override
  Future<Either<Failure, CommandeResponseModel>> sendCommande(CommandeSendDto commandeSendDto) async{

    if (await networkInfo.isConnected) {
      try {

        FormData formData = new FormData.fromMap(commandeSendDto.toMap());
        print(commandeSendDto.toMap());
        print(formData.fields);

        var response = await dio.post(DataConstantesUtils.COMMANDE_SERVER_URL,data: formData);

        // String url="${DataConstantesUtils.COMMANDE_SERVER_URL}?$fullData";
        //var response = await dio.post(url);
        var data = response.toString();



        Map responseMap = jsonDecode(data);
        print(responseMap);
        return Right((CommandeResponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, ListeCommandeResponseModel>> getCommande(ListeCommandeDto listeCommandeDto) async{

    if (await networkInfo.isConnected) {
      try {

        FormData formData = new FormData.fromMap(listeCommandeDto.toMap());
        print(listeCommandeDto.toMap());
        print(formData.fields);

        var response = await dio.post(DataConstantesUtils.LISTE_COMMANDE_SERVER_URL,data: formData);
        var data = response.toString();

        Map responseMap = jsonDecode(data);
        print(responseMap);
        return Right((ListeCommandeResponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, CommandeDeleteResponseModel>> deleteCommande(CommandeDeleteDto commandeDeleteDto) async{

    if (await networkInfo.isConnected) {
      try {

        FormData formData = new FormData.fromMap(commandeDeleteDto.toMap());
        print(commandeDeleteDto.toMap());
        print(formData.fields);

        var response = await dio.post(DataConstantesUtils.DELETE_COMMANDE_SERVER_URL,data: formData);
        var data = response.toString();

        Map responseMap = jsonDecode(data);
        print(responseMap);
        return Right((CommandeDeleteResponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }



  @override
  Future<Either<Failure, AppResponseModel>> sendUserDocument(ImageDto imageDto) async{

    if (await networkInfo.isConnected) {
      try {

        imageDto.accessToken=DataConstantesUtils.bTokenServer;
        imageDto.nbrePhoto=imageDto.fileProjet.length;

        FormData formData=new FormData.fromMap(imageDto.toMap());


        if(imageDto.fileProjet.isNotEmpty){

          List<TamponImageModel> image=imageDto.fileProjet;

          for(int u=0;u<image.length;u++){

            if(image[u]!=null && image[u].path.compareTo("")!=0){
              List<String> pathInfo=image[u].path.split(".");
              String  filename="${image[u].titre}.${pathInfo[pathInfo.length-1]}";

              formData.files.addAll([
                MapEntry("photo$u",MultipartFile.fromFileSync("./${image[u].path}",filename:"$filename"))
              ]);
            }
          }

        }


        var response = await dio.post(DataConstantesUtils.UserDocument_send,data:formData);

        print(response);
        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }


  @override
  Future<Either<Failure, UpdateAssuranceReponseModel>> updateAssurance(UpdateAssuranceDto updateAssuranceDto) async{
    if (await networkInfo.isConnected) {
      try {

        FormData formData = new FormData.fromMap(updateAssuranceDto.toMap());

        String fullData=updateAssuranceDto.toGetMap();
        String url="${DataConstantesUtils.UPDATE_ASSURANCE_SERVER_URL}?$fullData";
        print(url);
        var response = await dio.get("$url");
        var data = response.toString();
        print(data);
        Map responseMap = jsonDecode(data);
        print(responseMap);
        return Right((UpdateAssuranceReponseModel.fromMap(responseMap)));
      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, MyStatisticsModel>> getMyStatistics(MyStatisticsDto myStatisticsDto) async {
    if (await networkInfo.isConnected) {
      try {
        FormData formData = new FormData.fromMap(myStatisticsDto.toMap());
        var response = await dio.post(
            DataConstantesUtils.GET_MY_STATISTICS_SERVER_URL,
            data:formData
        );
        var data = response.toString();
        Map responseMap = jsonDecode(data);
        return Right((MyStatisticsModel.fromMap(responseMap)));
      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, UserStatisticModel>> getUserStatistics(UserStatisticsDto userStatisticsDto) async {
    if (await networkInfo.isConnected) {
      try {
        FormData formData = new FormData.fromMap(userStatisticsDto.toMap());
        var response = await dio.post(
            DataConstantesUtils.GET_USER_STATISTICS,
            data:formData
        );
        var data = response.toString();
        Map responseMap = jsonDecode(data);
        return Right((UserStatisticModel.fromMap(responseMap)));
      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, ActorProfilModel>> getActorProfil(ActorProfilDto actorProfilDto) async {
    if (await networkInfo.isConnected) {
      try {
        String fullData = actorProfilDto.toGetMap();
        print('----222--22--');
        print(fullData.toString());
        print('----222--22--');

        var response = await dio.get("${DataConstantesUtils.GET_ACTOR_PROFIL}?$fullData");
        var data = response.toString();
        Map responseMap = jsonDecode(data);

        return Right((ActorProfilModel.fromMap(responseMap)));
      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, NotificationRecuperationModel>> getNotification(NotificationDto notificationDto) async {

    if (await networkInfo.isConnected) {
      try {
        FormData formData=new FormData.fromMap(notificationDto.toMap());
        var response = await dio.post(DataConstantesUtils.GET_NOTIFICATION_SERVER_URL ,data:formData);

        var data = response.toString();

        Map responseMap = jsonDecode(data);
        return Right((NotificationRecuperationModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, SouscriptionRecuperationModel>>getTypeSouscription(SouscriptionDto souscriptionDto) async {
    if (await networkInfo.isConnected) {
      try {
        FormData formData=new FormData.fromMap(souscriptionDto.toMap());
        var response = await dio.post(DataConstantesUtils.GET_SOUSCRIPTION_SERVER_URL ,data:formData);

        var data = response.toString();
        Map responseMap = jsonDecode(data);
        return Right((SouscriptionRecuperationModel.fromMap(responseMap)));
      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, UserSouscriptionRecuperationModel>>getUserTypeSouscription(SouscriptionDto userSouscriptionDto) async {
    if (await networkInfo.isConnected) {
      try {
        FormData formData=new FormData.fromMap(userSouscriptionDto.toMap());
        var response = await dio.post(DataConstantesUtils.GET_USER_SOUSCRIPTION ,data:formData);

        var data = response.toString();
        Map responseMap = jsonDecode(data);
        return Right((UserSouscriptionRecuperationModel.fromMap(responseMap)));
      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, NotificationRecuperationModel>> deleteNotification(NotificationDto notificationDto) async {
    if(await networkInfo.isConnected){
      try{
        FormData formData = new FormData.fromMap(notificationDto.toMap());
        var response = await dio.post(
            DataConstantesUtils.DELETE_NOTIFICATION_SERVER_URL,
            data:formData
        );

        var data = response.toString();
        Map responseMap = jsonDecode(data);
        return Right((NotificationRecuperationModel.fromMap(responseMap)));
      }on ServerException{
        return Left(ServerFailure());
      }
    }else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, SouscriptionSendModel>> sendSouscription(SouscriptionSendDto souscriptionSendDto) async{
    if (await networkInfo.isConnected) {
      try {
        FormData formData = new FormData.fromMap(souscriptionSendDto.toMap());
        var response = await dio.post(
            DataConstantesUtils.SOUSCRIPTION_PAIEMENT_SERVER_URL,
            data: formData
        );
        var data = response.toString();
        Map responseMap = jsonDecode(data);
        return Right((SouscriptionSendModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> sendStatut(SimulationDto simulationDto) async{

    if (await networkInfo.isConnected) {
      try {

        simulationDto.accessToken=DataConstantesUtils.bTokenServer;

        FormData formData=new FormData.fromMap(simulationDto.toMap());


        var response = await dio.post(DataConstantesUtils.scoopStatutServerUrl,data:formData);

        print(response);
        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, ListeTransporterResponseModel>> getTransportersListe(ListeTransporterDto listeTransporterDto)async{

    if (await networkInfo.isConnected) {
      try {

        FormData formData = new FormData.fromMap(listeTransporterDto.toMap());
        print(listeTransporterDto.toMap());
        print(formData.fields);

        var response = await dio.post(DataConstantesUtils.LISTE_TRANSPORTER_SERVER_URL,data: formData);
        var data = response.toString();

        Map responseMap = jsonDecode(data);
        print(responseMap);
        return Right((ListeTransporterResponseModel.fromMap(responseMap)));


      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AppResponseModel>> sendAutrePret(AutrePretDto autrePretDto) async{

    if (await networkInfo.isConnected) {
      try {

        autrePretDto.accessToken=DataConstantesUtils.bTokenServer;

        FormData formData=new FormData.fromMap(autrePretDto.toMap());

        if(autrePretDto.fileBanque!=null && autrePretDto.fileBanque.isNotEmpty){

          List<File> image=autrePretDto.fileBanque;

          for(int u=0;u<image.length;u++){

            if(image[u]!=null && image[u].path.compareTo("")!=0){
              List<String> pathInfo=image[u].path.split("/");
              String  filename=pathInfo[pathInfo.length-1];

              formData.files.addAll([
                MapEntry("photo$u",MultipartFile.fromFileSync("./${image[u].path}",filename:"$filename"))
              ]);
            }

          }

        }


        var response = await dio.post(DataConstantesUtils.SEND_AUTRE_PRET_SERVER_URL,data:formData);

        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  } 
  
  @override
  Future<Either<Failure, AppResponseModel>> getListeAutrePret(ListeAutrePretDto listeAutrePretDto) async{

    if (await networkInfo.isConnected) {
      try {

        listeAutrePretDto.accessToken=DataConstantesUtils.bTokenServer;

        FormData formData=new FormData.fromMap(listeAutrePretDto.toMap());


        var response = await dio.post(DataConstantesUtils.SEND_AUTRE_PRET_SERVER_URL,data:formData);

        Map responseMap = jsonDecode(response.toString());
        return Right((AppResponseModel.fromMap(responseMap)));

      } on ServerException{
        return Left(ServerFailure());
      }
    } else {
      try {
        return Right(null);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

}
