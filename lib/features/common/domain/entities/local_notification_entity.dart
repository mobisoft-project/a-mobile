import 'package:agrimobile/helpers/base_entity.dart';

class LocalNotificationEntity extends BaseEntity {
  int idNotification;
  int apiId;
  String title;
  String description;
  String dateCreation;
  int idAnnonce;
  int idUser;

    
  LocalNotificationEntity();

  LocalNotificationEntity.create({
    this.idNotification, this.apiId,
    this.title,this.description,this.dateCreation,this.idAnnonce,
    this.idUser
  });

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalNotificationEntity.create(
      idNotification: json['idAppNotification'],
      apiId: json['id'],
      title: json['title'],
      description: json['description'],
      dateCreation: json['date_creation'],
      idAnnonce: json['id_annonce'],
      idUser: json['user_id'],
    );
  }


  @override
  factory LocalNotificationEntity.fromMap(Map<String, dynamic> json) {
    return LocalNotificationEntity.create(
      idNotification: json['idAppNotification'],
      apiId: json['id'],
      title: json['title'],
      description: json['description'],
      dateCreation: json['date_creation'],
      idAnnonce: json['id_annonce'],
      idUser: json['user_id'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'title': title,
      'description': description,
      'date_creation': dateCreation,
      'id_annonce': idAnnonce,
      'user_id': idUser,
    };
  }
}
