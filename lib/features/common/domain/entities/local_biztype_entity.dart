import 'package:agrimobile/helpers/base_entity.dart';


class LocalBiztypeEntity extends BaseEntity {

  int idBiztype;
  int apiId;
  String name;
  int createdBy;
  int modifiedBy;
  String jour;
  String heure;


  LocalBiztypeEntity();


  LocalBiztypeEntity.create({this.idBiztype, this.apiId, this.name,
      this.createdBy, this.modifiedBy, this.jour, this.heure});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalBiztypeEntity.create(
        idBiztype: json['idAppBiztype'],
        apiId: json['id'],
        name: json['name'],
        createdBy: json['createdBy'],
        modifiedBy: json['modifiedBy'],
        jour: json['jour'],
        heure: json['heure']);
  }

  @override
  factory LocalBiztypeEntity.fromMap(Map<String, dynamic> json) {
    return LocalBiztypeEntity.create(
        idBiztype: json['idAppBiztype'],
        apiId: json['id'],
        name: json['name'],
        createdBy: json['createdBy'],
        modifiedBy: json['modifiedBy'],
        jour: json['jour'],
        heure: json['heure']);
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'name': name,
      'createdBy': createdBy,
      'modifiedBy': modifiedBy,
      'jour': jour,
      'heure': heure,
    };
  }


  @override
  String toString() => name;

  /*
  @override
  operator ==(o) => o is LocalCityEntity && o.city_denomiation == city_denomiation;

  @override
  int get hashCode => city_key.hashCode^city_denomiation.hashCode^etat.hashCode^id_country.hashCode;

   */
}