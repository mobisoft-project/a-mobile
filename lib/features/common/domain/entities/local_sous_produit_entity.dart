import 'package:agrimobile/helpers/base_entity.dart';


class LocalSousProduitEntity extends BaseEntity {
  int idSousProduit;
  int apiId;
  String nomCategorieFille;
  int categorieFilleId;
  int statut;


  LocalSousProduitEntity();


  LocalSousProduitEntity.create({this.idSousProduit, this.apiId, this.nomCategorieFille,
      this.categorieFilleId, this.statut});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalSousProduitEntity.create(
      idSousProduit: json['idAppSousProduit'],
      apiId: json['id'],
      nomCategorieFille: json['nomCategorie_fille'],
      categorieFilleId: json['categoriefilleId'],
      statut: json['statut']);
  }

  @override
  factory LocalSousProduitEntity.fromMap(Map<String, dynamic> json) {
    return LocalSousProduitEntity.create(
        idSousProduit: json['idAppSousProduit'],
        apiId: json['id'],
        nomCategorieFille: json['nomCategorie_fille'],
        categorieFilleId: json['categoriefilleId'],
        statut: json['statut']);
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'nomCategorie_fille': nomCategorieFille,
      'categoriefilleId': categorieFilleId,
      'statut': statut,
    };
  }

  @override
  String toString() => nomCategorieFille;
}