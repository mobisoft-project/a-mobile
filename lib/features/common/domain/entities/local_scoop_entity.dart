import 'package:agrimobile/helpers/base_entity.dart';


class LocalScoopEntity extends BaseEntity {
  int idScoop;
  int idUser;
  int idAssociation;
  int etat;
  String nameAssociation;
  String cantonAssociation;
  String adresseAssociation;
  String descriptionAssociation;
  int totalMembre;
  String totalSuperficie;
  String typeAssociation;
  String memberAssociation;
  String roleMember;


  LocalScoopEntity();


  LocalScoopEntity.create({this.idScoop,this.roleMember,this.typeAssociation, this.idUser, this.etat, this.idAssociation, this.nameAssociation, this.cantonAssociation, this.adresseAssociation,this.descriptionAssociation, this.totalMembre, this.totalSuperficie,
    this.memberAssociation});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalScoopEntity.create(
      idScoop: json['idAppScoop'],
      idUser: json['id_user'],
      etat: json['etat_scoop'],
      idAssociation: json['id_association'],
      nameAssociation: json['name_association'],
      cantonAssociation: json['canton_association'],
      adresseAssociation: json['adresse_association'],
      descriptionAssociation: json['description_association'],
      totalMembre: json['total_membre'],
      totalSuperficie: json['total_superficie'],
      memberAssociation: json['member_association'],
      typeAssociation: json['statut_publication'],
      roleMember: json['role_membre'],
    );
  }

  @override
  factory LocalScoopEntity.fromMap(Map<String, dynamic> json) {
    return LocalScoopEntity.create(
      idScoop: json['idAppScoop'],
      idUser: json['id_user'],
      etat: json['etat_scoop'],
      idAssociation: json['id_association'],
      nameAssociation: json['name_association'],
      cantonAssociation: json['canton_association'],
      adresseAssociation: json['adresse_association'],
      descriptionAssociation: json['description_association'],
      totalMembre: json['total_membre'],
      totalSuperficie: json['total_superficie'],
      memberAssociation: json['member_association'],
      typeAssociation: json['statut_publication'],
      roleMember: json['role_membre'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id_association': idAssociation,
      'id_user': idUser,
      'etat_scoop': etat,
      'name_association': nameAssociation,
      'canton_association': cantonAssociation,
      'adresse_association': adresseAssociation,
      'description_association': descriptionAssociation,
      'total_membre': totalMembre,
      'total_superficie': totalSuperficie,
      'member_association': memberAssociation,
      'statut_publication': typeAssociation,
      'role_membre': roleMember,
    };
  }
}