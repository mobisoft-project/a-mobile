import 'package:agrimobile/helpers/base_entity.dart';


class LocalTypePretEntity extends BaseEntity {
  int idTypePret;
  int apiId;
  String libelle;
  String typePretKey;
  int etat;
  int createdBy;
  String dateCreate;
  int updatedBy;
  String dateUpdate;


  LocalTypePretEntity();


  LocalTypePretEntity.create({this.idTypePret, this.apiId,this.libelle, this.typePretKey, this.etat, this.createdBy, this.dateCreate, this.updatedBy, this.dateUpdate});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalTypePretEntity.create(
      idTypePret: json['idAppTypePret'],
      apiId: json['id'],
      libelle: json['libelle'],
      typePretKey: json['type_pret_key'],
      etat: json['etat'],
      createdBy: json['created_by'],
      dateCreate: json['date_create'],
      updatedBy: json['updated_by'],
      dateUpdate: json['date_update'],
    );
  }



  @override
  factory LocalTypePretEntity.fromMap(Map<String, dynamic> json) {
    return LocalTypePretEntity.create(
      idTypePret: json['idAppTypePret'],
        apiId: json['id'],
        libelle: json['libelle'],
      etat: json['etat'],
      typePretKey: json['type_pret_key'],
      createdBy: json['created_by'],
      dateCreate: json['date_create'],
      updatedBy: json['updated_by'],
      dateUpdate: json['date_update'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'libelle': libelle,
      'type_pret_key': typePretKey,
      'etat': etat,
      'created_by': createdBy,
      'date_create': dateCreate,
      'updated_by': updatedBy,
      'date_update': dateUpdate,
    };
  }
}