import 'package:agrimobile/helpers/base_entity.dart';


class LocalUniteEntity extends BaseEntity {
  int idUnite;
  int apiId;
  String nom;


  LocalUniteEntity();


  LocalUniteEntity.create({this.idUnite, this.apiId, this.nom});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalUniteEntity.create(
        idUnite: json['idAppUnite'],
        apiId: json['id'],
        nom: json['nom']);
  }

  @override
  factory LocalUniteEntity.fromMap(Map<String, dynamic> json) {
    return LocalUniteEntity.create(
        idUnite: json['idAppUnite'],
        apiId: json['id'],
        nom: json['nom']);
  }

  @override

  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'nom': nom,
    };
  }

  @override
  String toString() => nom;
}