import 'package:agrimobile/features/common/data/dto/meteo_dto.dart';
import 'package:agrimobile/helpers/base_entity.dart';

class LocalConseilEntity extends BaseEntity {
  int idConseil;
  int apiId;
  String typeConseil;
  String titre;
  String contenu;
  String speculation;
  String dateConseil;
  String lien;
  String fichier;
  String userToken;

  LocalConseilEntity();

  LocalConseilEntity.create(
      {this.idConseil,
      this.apiId,
      this.typeConseil,
      this.titre,
      this.contenu,
      this.dateConseil,
      this.lien,
      this.speculation,
      this.fichier,
      this.userToken});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalConseilEntity.create(
      idConseil: json['idAppConseil'],
      apiId: json['id_conseil'],
      typeConseil: json['type_conseil'],
      titre: json['titre'],
      contenu: json['contenu'],
      dateConseil: json['date_conseil'],
      lien: json['lien'],
      speculation: json['speculation'],
      fichier: json['fichier'],
      userToken: json['userToken'],
    );
  }


  @override
  factory LocalConseilEntity.fromMap(Map<String, dynamic> json) {
    return LocalConseilEntity.create(
      idConseil: json['idAppConseil'],
      apiId: json['id_conseil'],
      typeConseil: json['type_conseil'],
      titre: json['titre'],
      contenu: json['contenu'],
      dateConseil: json['date_conseil'],
      lien: json['lien'],
      speculation: json['speculation'],
      fichier: json['fichier'],
      userToken: json['userToken'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id_conseil': apiId,
      'type_conseil': typeConseil,
      'titre': titre,
      'contenu': contenu,
      'speculation': speculation,
      'date_conseil': dateConseil,
      'lien': lien,
      'fichier': fichier,
      'userToken': userToken,
    };
  }
}
