import 'package:agrimobile/helpers/base_entity.dart';


class LocalProduitEntity extends BaseEntity {
  int idProduit;
  int apiId;
  String nomCategorieFille;
  int categorieId;
  String uniteMesure;
  int statut;
  int dureeJour;


  LocalProduitEntity();


  LocalProduitEntity.create({this.idProduit, this.apiId, this.nomCategorieFille,
      this.categorieId, this.uniteMesure, this.statut, this.dureeJour});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalProduitEntity.create(
        idProduit: json['idAppProduit'],
        apiId: json['id'],
        nomCategorieFille: json['nomCategorie_fille'],
        categorieId: json['categorieId'],
        uniteMesure: json['unite_mesure'],
        statut: json['statut'],
        dureeJour: json['duree_jour']);
  }

  @override
  factory LocalProduitEntity.fromMap(Map<String, dynamic> json) {
    return LocalProduitEntity.create(
        idProduit: json['idAppProduit'],
        apiId: json['id'],
        nomCategorieFille: json['nomCategorie_fille'],
        categorieId: json['categorieId'],
        uniteMesure: json['unite_mesure'],
        statut: json['statut'],
        dureeJour: json['duree_jour']);
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'nomCategorie_fille': nomCategorieFille,
      'categorieId': categorieId,
      'unite_mesure': uniteMesure,
      'statut': statut,
      'duree_jour': dureeJour,
    };
  }

  @override
  String toString() => nomCategorieFille;
}