import 'package:agrimobile/helpers/base_entity.dart';


class LocalCommandeEntity extends BaseEntity {
  int idCommande;
  int idServerCommande;
  String keyCommande;
  String numeroCommande;
  String dateCommande;
  int amountCommande;
  String modeLivraisonCommande;
  String motCleModeLivraisonCommande;
  String moyenTransportCommande;
  String transporterCommande;
  int etatCommande;
  int typeCommande;
  String urlPaiementCommande;
  String NomPrenomUserCommande;
  String emailUserCommande;
  String telephoneUserCommande;


  LocalCommandeEntity();


  LocalCommandeEntity.create({this.idCommande, this.idServerCommande, this.keyCommande, this.numeroCommande, this.dateCommande, this.amountCommande,this.modeLivraisonCommande,this.motCleModeLivraisonCommande,this.moyenTransportCommande,this.transporterCommande,
    this.etatCommande, this.typeCommande, this.urlPaiementCommande, this.NomPrenomUserCommande,
    this.emailUserCommande, this.telephoneUserCommande});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalCommandeEntity.create(
        idCommande: json['idAppCommande'],
      idServerCommande: json['id_commande'],
      keyCommande: json['keyCommande'],
      numeroCommande: json['numeroCommande'],
      dateCommande: json['dateCommande'],
      amountCommande: json['amountCommande'],
      modeLivraisonCommande: json['modeLivraisonCommande'],
      motCleModeLivraisonCommande: json['motCleModeLivraisonCommande'],
      moyenTransportCommande: json['moyenTransportCommande'],
      transporterCommande: json['transporterCommande'],
      etatCommande: json['etatCommande'],
      typeCommande: json['typeCommande'],
      urlPaiementCommande: json['urlPaiementCommande'],
      NomPrenomUserCommande: json['NomPrenomUserCommande'],
      emailUserCommande: json['emailUserCommande'],
      telephoneUserCommande: json['telephoneUserCommande'],
    );
  }

  @override
  factory LocalCommandeEntity.fromMap(Map<String, dynamic> json) {
    return LocalCommandeEntity.create(
        idCommande: json['idAppCommande'],
        idServerCommande: json['id_commande'],
        keyCommande: json['keyCommande'],
        numeroCommande: json['numeroCommande'],
        dateCommande: json['dateCommande'],
        amountCommande: json['amountCommande'],
        modeLivraisonCommande: json['modeLivraisonCommande'],
        motCleModeLivraisonCommande: json['motCleModeLivraisonCommande'],
        moyenTransportCommande: json['moyenTransportCommande'],
        transporterCommande: json['transporterCommande'],
        etatCommande: json['etatCommande'],
        typeCommande: json['typeCommande'],
        urlPaiementCommande: json['urlPaiementCommande'],
        NomPrenomUserCommande: json['NomPrenomUserCommande'],
      emailUserCommande: json['emailUserCommande'],
      telephoneUserCommande: json['telephoneUserCommande'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id_commande': idServerCommande,
      'keyCommande': keyCommande,
      'numeroCommande': numeroCommande,
      'dateCommande': dateCommande,
      'amountCommande': amountCommande,
      'modeLivraisonCommande': modeLivraisonCommande,
      'motCleModeLivraisonCommande': motCleModeLivraisonCommande,
      'moyenTransportCommande': moyenTransportCommande,
      'transporterCommande': transporterCommande,
      'etatCommande': etatCommande,
      'typeCommande': typeCommande,
      'urlPaiementCommande': urlPaiementCommande,
      'NomPrenomUserCommande': NomPrenomUserCommande,
      'emailUserCommande': emailUserCommande,
      'telephoneUserCommande': telephoneUserCommande,
    };
  }
}