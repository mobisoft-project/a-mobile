import 'package:agrimobile/helpers/base_entity.dart';

class LocalUserStatisticEntity extends BaseEntity{
  int idStatistic;
  int nbreAchat;
  int montantAchat;
  int endLivraison;
  int waitingLivraison;
  String demandeSpeculation;
  int idUtlisateur;

  LocalUserStatisticEntity();

  LocalUserStatisticEntity.create({
    this.idStatistic,this.nbreAchat,this.montantAchat,
    this.endLivraison, this.waitingLivraison, this.demandeSpeculation, this.idUtlisateur
  });

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalUserStatisticEntity.create(
      idStatistic: json['idAppUserStatistics'],
      nbreAchat: json['nbre_achat'],
      montantAchat: json['montant_achat'],
      endLivraison: json['end_livraison'],
      waitingLivraison: json['waiting_livraison'],
      demandeSpeculation: json['demande_speculation'],
      idUtlisateur:  json['id_user']
    );
  }

  @override
  factory LocalUserStatisticEntity.fromMap(Map<String, dynamic> json) {
    return LocalUserStatisticEntity.create(
      idStatistic: json['idAppUserStatistics'],
      nbreAchat: json['nbre_achat'],
      montantAchat: json['montant_achat'],
      endLivraison: json['end_livraison'],
      waitingLivraison: json['waiting_livraison'],
      demandeSpeculation: json['demande_speculation'],
      idUtlisateur: json['id_user']
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'nbre_achat': nbreAchat,
      'montant_achat': montantAchat,
      'end_livraison': endLivraison,
      'waiting_livraison': waitingLivraison,
      'demande_speculation': demandeSpeculation,
      'id_user': idUtlisateur
    };
  }
}