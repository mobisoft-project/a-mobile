import 'package:agrimobile/helpers/base_entity.dart';


class LocalAssuranceEntity extends BaseEntity {
  int idAssurance;
  String denomination;
  String logo;
  int statuts;
  int apiId;


  LocalAssuranceEntity();


  LocalAssuranceEntity.create({this.idAssurance, this.denomination, this.logo,
      this.statuts, this.apiId});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalAssuranceEntity.create(
        idAssurance: json['idAppAssurance'],
        denomination: json['denomination'],
        logo: json['logo'],
        statuts: json['statuts'],
        apiId: json['apiId']);
  }

  @override
  factory LocalAssuranceEntity.fromMap(Map<String, dynamic> json) {
    return LocalAssuranceEntity.create(
        idAssurance: json['idAppAssurance'],
        denomination: json['denomination'],
        logo: json['logo'],
        statuts: json['statuts'],
        apiId: json['apiId']);
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'denomination': denomination,
      'logo': logo,
      'statuts': statuts,
      'apiId': apiId,
    };
  }
}