 import 'package:agrimobile/helpers/base_entity.dart';


class LocalEopEntity extends BaseEntity {
  int idEop;
  int keyEop;
  int etatEop;
  int idUser;
  String superficie;
  String nameSite;
  String nameSpeculation;
  int interetEop;
  int dureeEop;
  String montantTotal;
  String montantInteret;
  String projetInteret;
  String montantSuperficie;
  String montantProduction;
  String descriptionEop;
  String fichierEop;
  String composantEop;


  LocalEopEntity();


  LocalEopEntity.create({this.idEop, this.etatEop, this.keyEop, this.idUser, this.superficie, this.nameSite,this.nameSpeculation, this.interetEop, this.dureeEop,
    this.montantTotal, this.montantInteret, this.projetInteret,this.montantSuperficie, this.montantProduction, this.descriptionEop, this.composantEop, this.fichierEop});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalEopEntity.create(
      idEop: json['idAppEop'],
      keyEop: json['key_eop'],
      idUser: json['id_user'],
      etatEop: json['etat_eop'],
      superficie: json['superficie'],
      nameSite: json['name_site'],
      nameSpeculation: json['name_speculation'],
      interetEop: json['interet_eop'],
      dureeEop: json['duree_eop'],
      montantTotal: json['montant_total'],
      montantInteret: json['montant_interet'],
      projetInteret: json['projet_interet'],
      montantSuperficie: json['montant_surperficie'],
      montantProduction: json['montant_production'],
      descriptionEop: json['description_eop'],
      fichierEop: json['fichier_eop'],
      composantEop: json['composant_eop'],
    );
  }

  @override
  factory LocalEopEntity.fromMap(Map<String, dynamic> json) {
    return LocalEopEntity.create(
      idEop: json['idAppEop'],
      keyEop: json['key_eop'],
      idUser: json['id_user'],
      etatEop: json['etat_eop'],
      superficie: json['superficie'],
      nameSite: json['name_site'],
      nameSpeculation: json['name_speculation'],
      interetEop: json['interet_eop'],
      dureeEop: json['duree_eop'],
      montantTotal: json['montant_total'],
      montantInteret: json['montant_interet'],
      projetInteret: json['projet_interet'],
      montantSuperficie: json['montant_surperficie'],
      montantProduction: json['montant_production'],
      descriptionEop: json['description_eop'],
      fichierEop: json['fichier_eop'],
      composantEop: json['composant_eop'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'key_eop': keyEop,
      'etat_eop': etatEop,
      'id_user': idUser,
      'superficie': superficie,
      'name_site': nameSite,
      'name_speculation': nameSpeculation,
      'interet_eop': interetEop,
      'duree_eop': dureeEop,
      'montant_total': montantTotal,
      'montant_interet': montantInteret,
      'projet_interet': projetInteret,
      'montant_surperficie': montantSuperficie,
      'montant_production': montantProduction,
      'description_eop': descriptionEop,
      'fichier_eop': fichierEop,
      'composant_eop': composantEop,
    };
  }
}