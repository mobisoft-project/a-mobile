import 'package:agrimobile/helpers/base_entity.dart';


class LocalPretEntity extends BaseEntity {
  int idPret;
  String idScoop;
  String idEop;
  String superficie;
  String  dateDemande;
  int  idUser;
  int  etatDemande;
  int  idServerPret;
  int  typePret;
  String  contenuDemande;
  String  titleDemande;
  String  descriptionDemande;
  String  idProjet;
  String  fichierProjet;


  LocalPretEntity();


  LocalPretEntity.create({this.idPret, this.idScoop, this.idEop, this.idServerPret,this.typePret, this.superficie, this.dateDemande, this.idUser,
    this.etatDemande, this.contenuDemande, this.titleDemande, this.idProjet, this.fichierProjet, this.descriptionDemande});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalPretEntity.create(
        idPret: json['idAppPret'],
        idEop: json['idEop'],
        idScoop: json['idScoop'],
        typePret: json['typePret'],
        idServerPret: json['idServerPret'],
        superficie: json['superficie'],
        dateDemande: json['dateDemande'],
        idUser: json['idUser'],
        etatDemande: json['etatDemande'],
        contenuDemande: json['contenuDemande'],
        titleDemande: json['titleDemande'],
        descriptionDemande: json['descriptionDemande'],
        idProjet: json['key_projet'],
        fichierProjet: json['all_fichier'],
    );
  }

  @override
  factory LocalPretEntity.fromMap(Map<String, dynamic> json) {
    return LocalPretEntity.create(
      idPret: json['idAppPret'],
      idScoop: json['idScoop'],
      idEop: json['idEop'],
      typePret: json['typePret'],
      idServerPret: json['idServerPret'],
      superficie: json['superficie'],
      dateDemande: json['dateDemande'],
      idUser: json['idUser'],
      etatDemande: json['etatDemande'],
      contenuDemande: json['contenuDemande'],
      titleDemande: json['titleDemande'],
      descriptionDemande: json['descriptionDemande'],
      idProjet: json['key_projet'],
      fichierProjet: json['all_fichier'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {

    return {
      'idEop': idEop,
      'idScoop': idScoop,
      'typePret': typePret,
      'idServerPret': idServerPret,
      'superficie': superficie,
      'dateDemande': dateDemande,
      'idUser': idUser,
      'etatDemande': etatDemande,
      'contenuDemande': contenuDemande,
      'all_fichier': fichierProjet,
      'key_projet': idProjet,
      'descriptionDemande': descriptionDemande,
      'titleDemande': titleDemande
    };
  }
}