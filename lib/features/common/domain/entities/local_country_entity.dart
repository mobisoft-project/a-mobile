import 'package:agrimobile/helpers/base_entity.dart';


class LocalCountryEntity extends BaseEntity {
  int idCountry;
  int apiId;
  String alpha;
  String nomFr;
  String indicatif;


  LocalCountryEntity();


  LocalCountryEntity.create({this.idCountry, this.apiId, this.alpha,
      this.nomFr, this.indicatif});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalCountryEntity.create(
        idCountry: json['idAppCountry'],
        apiId: json['id'],
        alpha: json['alpha2'],
        nomFr: json['nom_fr_fr'],
        indicatif: json['indicatif']
    );
  }



  @override
  factory LocalCountryEntity.fromMap(Map<String, dynamic> json) {
    return LocalCountryEntity.create(
        idCountry: json['idAppCountry'],
        apiId: json['id'],
        alpha: json['alpha2'],
        nomFr: json['nom_fr_fr'],
        indicatif: json['indicatif']);
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'alpha2': alpha,
      'nom_fr_fr': nomFr,
      'indicatif': indicatif,
    };
  }

  @override
  String toString() => nomFr;
}