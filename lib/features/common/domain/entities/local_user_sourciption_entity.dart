import 'package:agrimobile/helpers/base_entity.dart';

class LocalUserSouscriptionEntity extends BaseEntity{
  int idUserSouscription;
  int id;
  String key;
  String numeroSouscription;
  int montant;
  String datePaiement;
  String dateFinsouscription;
  int etat;
  String typeSouscription;
  int idUtilisateur;

  LocalUserSouscriptionEntity();
  
  LocalUserSouscriptionEntity.create({
    this.idUserSouscription,this.id,this.key,this.numeroSouscription,
    this.montant,this.datePaiement,this.dateFinsouscription,
    this.etat, this.typeSouscription, this.idUtilisateur
  });

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalUserSouscriptionEntity.create(
      idUserSouscription: json['idAppUserSouscription'],
      id : json['id'],
      key : json['key'],
      numeroSouscription : json['numero_souscription'],
      montant : json['montant'],
      datePaiement :json['date_paiement'],
      dateFinsouscription : json['date_finsouscription'],
      etat : json['etat'],
      typeSouscription : json['type_souscription'],
      idUtilisateur: json['id_user']
    );
  }

  @override
  factory LocalUserSouscriptionEntity.fromMap(Map<String, dynamic> json) {
    return LocalUserSouscriptionEntity.create(
      idUserSouscription: json['idAppUserSouscription'],
      id : json['id'],
      key : json['key'],
      numeroSouscription : json['numero_souscription'],
      montant : json['montant'],
      datePaiement :json['date_paiement'],
      dateFinsouscription : json['date_finsouscription'],
      etat : json['etat'],
      typeSouscription : json['type_souscription'],
      idUtilisateur: json['id_user']
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': id,
      'key': key,
      'numero_souscription': numeroSouscription,
      'montant': montant,
      'date_paiement': datePaiement,
      'date_finsouscription': dateFinsouscription,
      'etat': etat,
      'type_souscription': typeSouscription,
      'id_user': idUtilisateur
    };
  }

}