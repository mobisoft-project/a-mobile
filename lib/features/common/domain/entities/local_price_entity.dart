import 'package:agrimobile/helpers/base_entity.dart';


class LocalPriceEntity extends BaseEntity {
  int idPrice;
  String datePrice;
  int timestamp;
  String nomProduit;
  String ville;
  String prix;
  int apiId;


  LocalPriceEntity();


  LocalPriceEntity.create({this.idPrice, this.datePrice, this.nomProduit, this.ville, this.prix, this.apiId, this.timestamp});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalPriceEntity.create(
      idPrice: json['idAppPrice'],
      datePrice: json['datePrice'],
      nomProduit: json['nom_produit'],
      ville: json['ville'],
      prix: json['prix'],
      apiId: json['apiId'],
      timestamp: json['timestamp'],
    );
  }




  @override
  factory LocalPriceEntity.fromMap(Map<String, dynamic> json) {
    return LocalPriceEntity.create(
      idPrice: json['idAppPrice'],
      datePrice: json['datePrice'],
      nomProduit: json['nom_produit'],
      ville: json['ville'],
      prix: json['prix'],
      apiId: json['apiId'],
      timestamp: json['timestamp'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'nom_produit': nomProduit,
      'ville': ville,
      'prix': prix,
      'apiId': apiId,
      'datePrice': datePrice,
      'timestamp': timestamp,
    };
  }

  @override
  String toString() => nomProduit;
}