import 'package:agrimobile/helpers/base_entity.dart';
import 'dart:convert';

class LocalMenuEntity extends BaseEntity {
  int idMenu;
  String codeMenu;
  String nameMenu;
  String iconeMenu;
  int typeMenu;
  int visibleMenu;


  LocalMenuEntity();


  LocalMenuEntity.create({this.idMenu, this.codeMenu, this.nameMenu, this.typeMenu, this.iconeMenu, this.visibleMenu});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalMenuEntity.create(
      idMenu: json['idAppMenu'],
      codeMenu: json['code_menu'],
      nameMenu: json['name_menu'],
      typeMenu: json['type_menu'],
      iconeMenu: json['icone_menu'],
      visibleMenu: json['visible_menu'],
    );
  }

  @override
  factory LocalMenuEntity.fromMap(Map<String, dynamic> json) {
    return LocalMenuEntity.create(
      idMenu: json['idMenu'],
      codeMenu: json['code_menu'],
      nameMenu: json['name_menu'],
      typeMenu: json['type_menu'],
      iconeMenu: json['icone_menu'],
      visibleMenu: json['visible_menu'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'code_menu': codeMenu,
      'name_menu': nameMenu,
      'type_menu': typeMenu,
      'icone_menu': iconeMenu,
      'visible_menu': visibleMenu,
    };
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'idMenu': idMenu,
      'code_menu': codeMenu,
      'name_menu': nameMenu,
      'type_menu': typeMenu,
      'icone_menu': iconeMenu,
      'visible_menu': visibleMenu,
    };
  }
}
