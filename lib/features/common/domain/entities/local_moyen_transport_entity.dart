import 'package:agrimobile/helpers/base_entity.dart';


class LocalMoyenTransportEntity extends BaseEntity {
  int idMoyenTransport;
  int apiId;
  String libelle;
  int etat;
  String moyenTransportKey;


  LocalMoyenTransportEntity();


  LocalMoyenTransportEntity.create({this.idMoyenTransport, this.apiId, this.libelle, this.etat, this.moyenTransportKey});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalMoyenTransportEntity.create(
      idMoyenTransport: json['idAppMoyenTransport'],
      apiId: json['id'],
      libelle: json['libelle'],
      etat: json['etat'],
      moyenTransportKey: json['moyen_transport_key'],
    );
  }



  @override
  factory LocalMoyenTransportEntity.fromMap(Map<String, dynamic> json) {
    return LocalMoyenTransportEntity.create(
      idMoyenTransport: json['idAppMoyenTransport'],
        apiId: json['id'],
        libelle: json['libelle'],
        etat: json['etat'],
        moyenTransportKey: json['moyen_transport_key'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'libelle': libelle,
      'etat': etat,
      'moyen_transport_key': moyenTransportKey,
    };
  }
}