import 'package:agrimobile/helpers/base_entity.dart';

class LocalSouscriptionEntity extends BaseEntity {
  int idTypeSouscription;
  int apiIdType;
  String keyTypeSouscription;
  String denoTypeSouscription;
  int prixSouscription;
  int dureeSouscription;
  int userId;

  LocalSouscriptionEntity();

  LocalSouscriptionEntity.create({
    this.idTypeSouscription,
    this.apiIdType,
    this.keyTypeSouscription,
    this.denoTypeSouscription,
    this.dureeSouscription,
    this.prixSouscription,
    this.userId
  });

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalSouscriptionEntity.create(
      idTypeSouscription: json['idAppTypeSouscription'],
      apiIdType: json['id_type_souscription'],
      keyTypeSouscription: json['key_type_souscription'],
      denoTypeSouscription: json['deno_type_souscription'],
      prixSouscription: json['prix_souscription'],
      dureeSouscription: json['duree_souscription'],
      userId: json['user_id'],
    );
  }

  @override
  factory LocalSouscriptionEntity.fromMap(Map<String, dynamic> json) {
    return LocalSouscriptionEntity.create(
      idTypeSouscription: json['idAppTypeSouscription'],
      apiIdType: json['id_type_souscription'],
      keyTypeSouscription: json['key_type_souscription'],
      denoTypeSouscription: json['deno_type_souscription'],
      prixSouscription: json['prix_souscription'],
      dureeSouscription: json['duree_souscription'],
      userId: json['user_id'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id_type_souscription': apiIdType,
      'key_type_souscription': keyTypeSouscription,
      'deno_type_souscription': denoTypeSouscription,
      'prix_souscription': prixSouscription,
      'duree_souscription': dureeSouscription,
      'user_id': userId,
    };
  }
}
