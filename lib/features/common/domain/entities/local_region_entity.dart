import 'package:agrimobile/helpers/base_entity.dart';


class LocalRegionEntity extends BaseEntity {
  int idRegion;
  int apiId;
  String nomRegion;
  int paysId;


  LocalRegionEntity();


  LocalRegionEntity.create({this.idRegion, this.apiId, this.nomRegion,
      this.paysId});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalRegionEntity.create(
        idRegion: json['idAppRegion'],
        apiId: json['id'],
        nomRegion: json['nomregion'],
        paysId: json['paysId']
    );
  }


  @override
  factory LocalRegionEntity.fromMap(Map<String, dynamic> json) {
    return LocalRegionEntity.create(
        idRegion: json['idAppRegion'],
        apiId: json['id'],
        nomRegion: json['nomregion'],
        paysId: json['paysId']);
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'nomregion': nomRegion,
      'paysId': paysId,
    };
  }

  @override
  String toString() => nomRegion;
}