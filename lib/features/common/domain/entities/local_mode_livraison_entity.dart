import 'package:agrimobile/helpers/base_entity.dart';


class LocalModeLivraisonEntity extends BaseEntity {
  int idModeLivraison;
  int apiId;
  String libelle;
  String modeLivraisonKey;
  int etat;
  String motCle;


  LocalModeLivraisonEntity();


  LocalModeLivraisonEntity.create({this.idModeLivraison, this.apiId, this.libelle, this.modeLivraisonKey, this.etat,
    this.motCle});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalModeLivraisonEntity.create(
      idModeLivraison: json['idAppModeLivraison'],
        apiId: json['id'],
      libelle: json['libelle'],
      modeLivraisonKey: json['mode_livraison_key'],
      etat: json['etat'],
      motCle: json['mot_cle'],
    );
  }



  @override
  factory LocalModeLivraisonEntity.fromMap(Map<String, dynamic> json) {
    return LocalModeLivraisonEntity.create(
        idModeLivraison: json['idAppModeLivraison'],
        apiId: json['id'],
        libelle: json['libelle'],
        modeLivraisonKey: json['mode_livraison_key'],
      etat: json['etat'],
        motCle: json['mot_cle'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'libelle': libelle,
      'mode_livraison_key': modeLivraisonKey,
      'etat': etat,
      'mot_cle': motCle,
    };
  }
}