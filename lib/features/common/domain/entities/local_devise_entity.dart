import 'package:agrimobile/helpers/base_entity.dart';


class LocalDeviseEntity extends BaseEntity {
  int idDevise;
  int apiId;
  String name;
  int createdBy;
  int modifiedBy;
  String jour;
  String heure;


  LocalDeviseEntity();


  LocalDeviseEntity.create({this.idDevise, this.apiId, this.name, this.createdBy,
      this.modifiedBy, this.jour, this.heure});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalDeviseEntity.create(
        idDevise: json['idAppDevise'],
        apiId: json['id'],
        name: json['name'],
        createdBy: json['createdBy'],
        modifiedBy: json['modifiedBy'],
        jour: json['jour'],
        heure: json['heure']
    );
  }



  @override
  factory LocalDeviseEntity.fromMap(Map<String, dynamic> json) {
    return LocalDeviseEntity.create(
        idDevise: json['idAppDevise'],
        apiId: json['id'],
        name: json['name'],
        createdBy: json['createdBy'],
        modifiedBy: json['modifiedBy'],
        jour: json['jour'],
        heure: json['heure']);
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'name': name,
      'createdBy': createdBy,
      'modifiedBy': modifiedBy,
      'jour': jour,
      'heure': heure,
    };
  }
}