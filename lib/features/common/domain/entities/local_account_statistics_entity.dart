import 'package:agrimobile/helpers/base_entity.dart';

class LocalMyStatisticsEntity extends BaseEntity{
  int idUserStatistic;
  int totalOffre;
  int activeOffre;
  int chiffreAffaire;
  String offreSpeculation;
  int idUtlsateur;

  LocalMyStatisticsEntity();

  LocalMyStatisticsEntity.create(
      {this.idUserStatistic,
      this.totalOffre,
      this.activeOffre,
      this.chiffreAffaire,
      this.offreSpeculation, this.idUtlsateur});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalMyStatisticsEntity.create(
      idUserStatistic: json['idAppUserStatistic'],
      totalOffre: json['total_offre'],
      activeOffre: json['active_offre'],
      chiffreAffaire: json['chiffre_affaire'],
      offreSpeculation: json['offre_speculation'],
      idUtlsateur: json['id_user']
    );
  }

  @override
  factory LocalMyStatisticsEntity.fromMap(Map<String, dynamic> json) {
    return LocalMyStatisticsEntity.create(
      idUserStatistic: json['idAppUserStatistic'],
      totalOffre: json['total_offre'],
      activeOffre: json['active_offre'],
      chiffreAffaire: json['chiffre_affaire'],
      offreSpeculation: json['offre_speculation'],
      idUtlsateur: json['id_user']
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'total_offre': totalOffre,
      'active_offre': activeOffre,
      'chiffre_affaire': chiffreAffaire,
      'offre_speculation': offreSpeculation,
      'id_user': idUtlsateur
    };
  }


}