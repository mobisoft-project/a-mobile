import 'package:agrimobile/helpers/base_entity.dart';


class LocalCategorieProduitEntity extends BaseEntity {
  int idCategorieProduit;
  int apiId;
  String name;
  int statut;


  LocalCategorieProduitEntity();


  LocalCategorieProduitEntity.create({this.idCategorieProduit, this.apiId, this.name,this.statut});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalCategorieProduitEntity.create(
        idCategorieProduit: json['idAppCategorieProduit'],
        apiId: json['id'],
        name: json['name'],
        statut: json['statut']);
  }

  @override
  factory LocalCategorieProduitEntity.fromMap(Map<String, dynamic> json) {
    return LocalCategorieProduitEntity.create(
        idCategorieProduit: json['idAppCategorieProduit'],
        apiId: json['id'],
        name: json['name'],
        statut: json['statut']);
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'name': name,
      'statut': statut,
    };
  }

  @override
  String toString() => name;
}