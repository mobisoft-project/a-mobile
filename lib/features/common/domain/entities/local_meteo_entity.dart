import 'package:agrimobile/helpers/base_entity.dart';


class LocalMeteoEntity extends BaseEntity {
  int idMeteo;
  String date;
  String description;
  double tempDay;
  double tempMin;
  double tempMax;
  double tempNight;
  double tempMorn;
  double tempEve;
  double humidity;
  int apiId;
  int userId;


  LocalMeteoEntity();


  LocalMeteoEntity.create({this.idMeteo, this.date, this.description, this.tempDay,
      this.tempMin, this.tempMax, this.tempNight, this.tempMorn, this.tempEve,this.userId,
      this.humidity, this.apiId});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalMeteoEntity.create(
        idMeteo: json['idAppMeteo'],
        date: json['date'],
        description: json['description'],
        tempDay: json['temp_day'],
        tempMin: json['temp_min'],
        tempMax: json['temp_max'],
        tempNight: json['temp_night'],
        tempMorn: json['temp_morn'],
        tempEve: json['temp_eve'],
        humidity: json['humidity'],
        userId: json['userId'],
        apiId: json['apiId']);
  }

  @override
  factory LocalMeteoEntity.fromMap(Map<String, dynamic> json) {
    return LocalMeteoEntity.create(
        idMeteo: json['idAppMeteo'],
        date: json['date'],
        description: json['description'],
        tempDay: json['temp_day'],
        tempMin: json['temp_min'],
        tempMax: json['temp_max'],
        tempNight: json['temp_night'],
        tempMorn: json['temp_morn'],
        tempEve: json['temp_eve'],
        humidity: json['humidity'],
        userId: json['userId'],
        apiId: json['apiId']);
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'date': date,
      'description': description,
      'temp_day': tempDay,
      'temp_min': tempMin,
      'temp_max': tempMax,
      'temp_night': tempNight,
      'temp_morn': tempMorn,
      'temp_eve': tempEve,
      'humidity': humidity,
      'userId': userId,
      'apiId': apiId
    };
  }
}