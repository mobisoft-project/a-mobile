import 'package:agrimobile/helpers/base_entity.dart';


class LocalSpeculationEntity extends BaseEntity {
  int idSpeculation;
  int apiId;
  String nom;
  int isAbonne;
  int quantite;
  int idUnite;
  String nomUnite;
  int userId;





  LocalSpeculationEntity();


  LocalSpeculationEntity.create({this.idSpeculation, this.apiId, this.nom, this.isAbonne, this.quantite, this.idUnite, this.nomUnite,this.userId});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalSpeculationEntity.create(
        idSpeculation: json['idAppSpeculation'],
        apiId: json['id'],
        nom: json['nom'],
        isAbonne: json['is_abonne'],
        quantite: json['quantite'],
        idUnite: json['idUnite'],
        nomUnite: json['nomUnite'],
        userId: json['userId'],
    );
  }



  @override
  factory LocalSpeculationEntity.fromMap(Map<String, dynamic> json) {
    return LocalSpeculationEntity.create(
        idSpeculation: json['idAppSpeculation'],
        apiId: json['id'],
        nom: json['nom'],
        isAbonne: json['is_abonne'],
        quantite: json['quantite'],
        idUnite: json['idUnite'],
        nomUnite: json['nomUnite'],
        userId: json['userId'],
    );
  }




  @override
  Map<String, dynamic> toDatabase() {
    return {
      'id': apiId,
      'nom': nom,
      'is_abonne': isAbonne,
      'quantite': quantite,
      'idUnite': idUnite,
      'nomUnite': nomUnite,
      'userId': userId,
    };
  }
}