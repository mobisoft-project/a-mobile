import 'package:agrimobile/helpers/base_entity.dart';


class LocalListeCommandeEntity extends BaseEntity {
  int idCommande;
  String keyCommande;
  String numeroCommande;
  String dateCommande;
  int amountCommande;
  int etatCommande;
  int typeCommande;
  String urlPaiementCommande;
  String NomPrenomUserCommande;
  String emailUserCommande;
  String telephoneUserCommande;


  LocalListeCommandeEntity();


  LocalListeCommandeEntity.create({this.idCommande, this.keyCommande, this.numeroCommande, this.dateCommande, this.amountCommande,
    this.etatCommande, this.typeCommande, this.urlPaiementCommande, this.NomPrenomUserCommande,
    this.emailUserCommande, this.telephoneUserCommande});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalListeCommandeEntity.create(
        idCommande: json['idCommande'],
      keyCommande: json['keyCommande'],
      numeroCommande: json['numeroCommande'],
      dateCommande: json['dateCommande'],
      amountCommande: json['amountCommande'],
      etatCommande: json['etatCommande'],
      typeCommande: json['typeCommande'],
      urlPaiementCommande: json['urlPaiementCommande'],
      NomPrenomUserCommande: json['NomPrenomUserCommande'],
      emailUserCommande: json['emailUserCommande'],
      telephoneUserCommande: json['telephoneUserCommande'],
    );
  }

  @override
  factory LocalListeCommandeEntity.fromMap(Map<String, dynamic> json) {
    return LocalListeCommandeEntity.create(
        idCommande: json['idCommande'],
        keyCommande: json['keyCommande'],
        numeroCommande: json['numeroCommande'],
        dateCommande: json['dateCommande'],
        amountCommande: json['amountCommande'],
        etatCommande: json['etatCommande'],
        typeCommande: json['typeCommande'],
        urlPaiementCommande: json['urlPaiementCommande'],
        NomPrenomUserCommande: json['NomPrenomUserCommande'],
      emailUserCommande: json['emailUserCommande'],
      telephoneUserCommande: json['telephoneUserCommande'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'keyCommande': keyCommande,
      'numeroCommande': numeroCommande,
      'dateCommande': dateCommande,
      'amountCommande': amountCommande,
      'etatCommande': etatCommande,
      'typeCommande': typeCommande,
      'urlPaiementCommande': urlPaiementCommande,
      'NomPrenomUserCommande': NomPrenomUserCommande,
      'emailUserCommande': emailUserCommande,
      'telephoneUserCommande': telephoneUserCommande,
    };
  }
}