import 'package:agrimobile/helpers/base_entity.dart';


class LocalTypeAssuranceEntity extends BaseEntity {
  int idTypeAssurance;
  int apiId;
  int typeassuranceId;
  String typeassuranceNom;
  int assureurId;
  String assureurNom;
  int prix;
  int validite;
  int isvalide;
  String situation;
  String date;


  LocalTypeAssuranceEntity();

  LocalTypeAssuranceEntity.create({this.idTypeAssurance, this.apiId,
      this.typeassuranceId, this.typeassuranceNom, this.assureurId,
      this.assureurNom, this.prix, this.validite, this.isvalide, this.situation,
      this.date});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalTypeAssuranceEntity.create(
        idTypeAssurance: json['idAppTAssurance'],
        apiId: json['apiId'],
        typeassuranceId: json['typeassuranceId'],
        typeassuranceNom: json['typeassuranceNom'],
        assureurId: json['assureurId'],
        assureurNom: json['assureurNom'],
        prix: json['prix'],
        validite: json['validite'],
        isvalide: json['isvalide'],
        situation: json['situation'],
        date: json['date']
    );
  }

  @override
  factory LocalTypeAssuranceEntity.fromMap(Map<String, dynamic> json) {
    return LocalTypeAssuranceEntity.create(
        idTypeAssurance: json['idAppTAssurance'],
        apiId: json['apiId'],
        typeassuranceId: json['typeassuranceId'],
        typeassuranceNom: json['typeassuranceNom'],
        assureurId: json['assureurId'],
        assureurNom: json['assureurNom'],
        prix: json['prix'],
        validite: json['validite'],
        isvalide: json['isvalide'],
        situation: json['situation'],
        date: json['date']
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'apiId': apiId,
      'typeassuranceId': typeassuranceId,
      'typeassuranceNom': typeassuranceNom,
      'assureurId': assureurId,
      'assureurNom': assureurNom,
      'prix': prix,
      'validite': validite,
      'isvalide': isvalide,
      'situation': situation,
      'date': date,
    };
  }
}