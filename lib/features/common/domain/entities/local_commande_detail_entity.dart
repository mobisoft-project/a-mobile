import 'package:agrimobile/helpers/base_entity.dart';


class LocalCommandeDetailEntity extends BaseEntity {
  int idCommandeDetail;
  String keyCommande;
  String nomProduitCommandeDetail;
  String qteProduitCommandeDetail;
  String prixUnitaireCommandeDetail;
  String prixTotalCommandeDetail;


  LocalCommandeDetailEntity();


  LocalCommandeDetailEntity.create({this.idCommandeDetail, this.keyCommande, this.nomProduitCommandeDetail, this.qteProduitCommandeDetail,this.prixUnitaireCommandeDetail, this.prixTotalCommandeDetail});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalCommandeDetailEntity.create(
      idCommandeDetail: json['idAppCommandeDetail'],
      keyCommande: json['keyCommande'],
      nomProduitCommandeDetail: json['nomProduitCommandeDetail'],
      qteProduitCommandeDetail: json['qteProduitCommandeDetail'],
      prixUnitaireCommandeDetail: json['prixUnitaireCommandeDetail'],
      prixTotalCommandeDetail: json['prixTotalCommandeDetail']
    );
  }

  @override
  factory LocalCommandeDetailEntity.fromMap(Map<String, dynamic> json) {
    return LocalCommandeDetailEntity.create(
      idCommandeDetail: json['idAppCommandeDetail'],
        keyCommande: json['keyCommande'],
      nomProduitCommandeDetail: json['nomProduitCommandeDetail'],
      qteProduitCommandeDetail: json['qteProduitCommandeDetail'],
      prixUnitaireCommandeDetail: json['prixUnitaireCommandeDetail'],
      prixTotalCommandeDetail: json['prixTotalCommandeDetail']
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'keyCommande': keyCommande,
      'nomProduitCommandeDetail': nomProduitCommandeDetail,
      'qteProduitCommandeDetail': qteProduitCommandeDetail,
      'prixUnitaireCommandeDetail': prixUnitaireCommandeDetail,
      'prixTotalCommandeDetail': prixTotalCommandeDetail,
    };
  }
}