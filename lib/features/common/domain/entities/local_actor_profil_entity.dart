import 'package:agrimobile/helpers/base_entity.dart';

class LocalActorProfilEntity extends BaseEntity{
  int idActorProfil;
  String actorCode;
  String username;
  String nom;
  String prenoms;
  String email;
  int noteVendeur;
  int noteAcheteur;

  LocalActorProfilEntity();

  LocalActorProfilEntity.create(
      {this.idActorProfil,this.actorCode,
      this.username,this.nom,this.prenoms,
      this.email, this.noteVendeur,this.noteAcheteur});

  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalActorProfilEntity.create(
      idActorProfil: json['idAppActorsProfil'],
      actorCode: json['actor_code'],
      username: json['username'],
      nom: json['nom'],
      prenoms: json['prenoms'],
      email: json['email'],
      noteVendeur: json['note_vendeur'],
      noteAcheteur: json['note_acheteur']
    );
  }

  @override
  factory LocalActorProfilEntity.fromMap(Map<String, dynamic> json) {
    return LocalActorProfilEntity.create(
      idActorProfil: json['idAppActorsProfil'],
      actorCode: json['actor_code'],
      username: json['username'],
      nom: json['nom'],
      prenoms: json['prenoms'],
      email: json['email'],
      noteVendeur: json['note_vendeur'],
      noteAcheteur: json['note_acheteur']
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'actor_code': actorCode,
      'username': username,
      'nom': nom,
      'prenoms': prenoms,
      'email': email,
      'note_vendeur': noteVendeur,
      'note_acheteur': noteAcheteur
    };
  }


}