import 'package:agrimobile/helpers/base_entity.dart';
import 'dart:convert';

class LocalAnnonceEntity extends BaseEntity {
  int idAnnonce;
  String title;
  String picture;
  int userId;
  String reference;
  int categorieId;
  int categorieDuree;
  int unitesId;
  int deviseId;
  int prix;
  String latitude;
  String longitude;
  String lieu;
  int stock;
  String contact;
  int biztypeId;
  int time;
  String info;
  String expedition;
  String identifiant;
  int autorisation;
  String paysAlpha;
  String paysNom;
  int indivisibilite;
  int disponibilite;
  String moisDisponibilite;
  int certificate;
  int bio;
  int noteVendeur;
  String uniqueCode;

  int scoopId;
  int statutPrive;
  int myApiId;
  int apiId;
  int expireTime;

  String nomCategorieFilleProduit;
  String nomUnite;


  LocalAnnonceEntity();


  LocalAnnonceEntity.create({this.idAnnonce, this.title, this.picture, this.userId, this.reference,
    this.categorieId, this.categorieDuree, this.unitesId, this.deviseId,
    this.prix, this.latitude, this.longitude, this.lieu, this.stock,
    this.contact, this.biztypeId, this.time, this.info, this.expedition,
    this.identifiant, this.autorisation, this.paysAlpha, this.paysNom, this.indivisibilite,
    this.disponibilite, this.moisDisponibilite, this.certificate, this.bio,
    this.noteVendeur, this.uniqueCode, this.scoopId, this.statutPrive,this.myApiId, this.apiId, this.nomCategorieFilleProduit, this.nomUnite, this.expireTime});


  @override
  fromDatabase(Map<String, dynamic> json) {
    return LocalAnnonceEntity.create(
      idAnnonce: json['idAppAnnonce'],
      title: json['title'],
      picture: json['picture'],
      userId: json['userId'],
      reference: json['reference'],
      categorieId: json['categorieId'],
      categorieDuree: json['categorieDuree'],
      unitesId: json['unitesId'],
      deviseId: json['deviseId'],
      prix: json['prix'],
      latitude: "${json['latitude']}",
      longitude: "${json['longitude']}",
      lieu: json['lieu'],
      stock: json['stock'],
      contact: json['contact'],
      biztypeId: json['biztypeId'],
      time: json['time'],
      info: json['info'],
      expedition: json['exp'],
      identifiant: json['identifiant'],
      autorisation: json['autorisation'],
      paysAlpha: json['paysAlpha'],
      paysNom: json['paysNom'],
        indivisibilite: json['indivisibilite'],
      disponibilite: json['disponibilite'],
      moisDisponibilite: json['moisDisponibilite'],
      certificate: json['certificate'],
      bio: json['bio'],
      noteVendeur: json['noteVendeur'],
      uniqueCode: json['uniqueCode'],
        scoopId: json['scoopId'],
        statutPrive: json['statutPrive'],
      myApiId: json['myApiId'],
      nomCategorieFilleProduit: json['nomCategorie_fille'],
      nomUnite: json['nom'],
      apiId: json['apiId'],
      expireTime: json['expireTime']
    );
  }

  @override
  factory LocalAnnonceEntity.fromMap(Map<String, dynamic> json) {
    return LocalAnnonceEntity.create(
        idAnnonce: json['idAnnonce'],
        title: json['title'],
        picture: json['picture'],
        userId: json['userId'],
        reference: json['reference'],
        categorieId: json['categorieId'],
        categorieDuree: json['categorieDuree'],
        unitesId: json['unitesId'],
        deviseId: json['deviseId'],
        prix: json['prix'],
        latitude: "${json['latitude']}",
        longitude: "${json['longitude']}",
        lieu: json['lieu'],
        stock: json['stock'],
        contact: json['contact'],
        biztypeId: json['biztypeId'],
        time: json['time'],
        info: json['info'],
        expedition: json['exp'],
        identifiant: json['identifiant'],
        autorisation: json['autorisation'],
        paysAlpha: json['paysAlpha'],
        paysNom: json['paysNom'],
        disponibilite: json['disponibilite'],
        indivisibilite: json['indivisibilite'],
        moisDisponibilite: json['moisDisponibilite'],
        certificate: json['certificate'],
        bio: json['bio'],
        noteVendeur: json['noteVendeur'],
        uniqueCode: json['uniqueCode'],
      scoopId: json['scoopId'],
      statutPrive: json['statutPrive'],
        myApiId: json['myApiId'],
        apiId: json['apiId'],
        expireTime: json['expireTime'],
    );
  }

  @override
  Map<String, dynamic> toDatabase() {
    return {
      'title': title,
      'picture': picture,
      'userId': userId,
      'reference': reference,
      'categorieId': categorieId,
      'categorieDuree': categorieDuree,
      'unitesId': unitesId,
      'deviseId': deviseId,
      'prix': prix,
      'latitude': latitude,
      'longitude': longitude,
      'lieu': lieu,
      'stock': stock,
      'contact': contact,
      'biztypeId': biztypeId,
      'time': time,
      'info': info,
      'exp': expedition,
      'identifiant': identifiant,
      'autorisation': autorisation,
      'paysAlpha': paysAlpha,
      'paysNom': paysNom,
      'indivisibilite': indivisibilite,
      'disponibilite': disponibilite,
      'moisDisponibilite': moisDisponibilite,
      'certificate': certificate,
      'bio': bio,
      'noteVendeur': noteVendeur,
      'uniqueCode': uniqueCode,
      'scoopId': scoopId,
      'statutPrive': statutPrive,
      'myApiId': myApiId,
      'apiId': apiId,
      'expireTime': expireTime,
    };
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'idAnnonce': idAnnonce,
      'title': title,
      'picture': picture,
      'userId': userId,
      'reference': reference,
      'categorieId': categorieId,
      'categorieDuree': categorieDuree,
      'unitesId': unitesId,
      'deviseId': deviseId,
      'prix': prix,
      'latitude': latitude,
      'longitude': longitude,
      'lieu': lieu,
      'stock': stock,
      'contact': contact,
      'biztypeId': biztypeId,
      'time': time,
      'info': info,
      'exp': expedition,
      'identifiant': identifiant,
      'autorisation': autorisation,
      'paysAlpha': paysAlpha,
      'paysNom': paysNom,
      'indivisibilite': indivisibilite,
      'disponibilite': disponibilite,
      'moisDisponibilite': moisDisponibilite,
      'certificate': certificate,
      'bio': bio,
      'noteVendeur': noteVendeur,
      'uniqueCode': uniqueCode,
      'scoopId': scoopId,
      'statutPrive': statutPrive,
      'myApiId': myApiId,
      'apiId': apiId,
      'expireTime': expireTime,
    };
  }
}