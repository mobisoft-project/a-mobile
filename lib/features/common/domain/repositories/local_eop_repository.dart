import 'package:agrimobile/features/common/domain/entities/local_eop_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalEopRepository extends BaseRepository<LocalEopEntity, int> {
  LocalEopRepository() : super(TABLE_NAME_EOP);

  @override
  LocalEopEntity getEntity() {
    return new LocalEopEntity();
  }

  Future<List<LocalEopEntity>> existEop(int idServer) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_EOP where  $idServerEop=$idServer ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<LocalEopEntity> oneEop(int idUser,String keyEop) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql ='''SELECT *  FROM $TABLE_NAME_EOP where $idUserEop=$idUser and $idServerEop=$keyEop and $etatEop="1"  order by $nameSiteEop desc  ''';

    final data = await dbClient.rawQuery(sql);

    return getEntity().fromDatabase(data[0]);
  }

  Future<List<LocalEopEntity>> filterEop(int idUser,String content) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";


    sql ='''SELECT *  FROM $TABLE_NAME_EOP where $idUserEop=$idUser and $etatEop="1" and ($nameSiteEop like "%$content%" or $nameSpeculationEop like "%$content%") order by $nameSiteEop desc  ''';

    print(sql);
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

 Future<List<LocalEopEntity>> listEop(int idUser) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql ='''SELECT *  FROM $TABLE_NAME_EOP where $idUserEop=$idUser and $etatEop="1"  order by $nameSiteEop desc  ''';

    print(sql);
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}