import 'package:agrimobile/features/common/domain/entities/local_assurance_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalAssuranceRepository extends BaseRepository<LocalAssuranceEntity, int> {
  LocalAssuranceRepository() : super(TABLE_NAME_ASSURANCE);

  @override
  LocalAssuranceEntity getEntity() {
    return new LocalAssuranceEntity();
  }


  Future<List<LocalAssuranceEntity>> existAssurance(int idAssurance) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_ASSURANCE where  $apiIdAssurance=$idAssurance ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalAssuranceEntity>> allAssurance() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_ASSURANCE order by $apiIdAssurance desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}