import 'package:agrimobile/features/common/domain/entities/local_souscription_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalSouscriptionRepository extends BaseRepository<LocalSouscriptionEntity, int> {
  LocalSouscriptionRepository() : super(TABLE_NAME_TYPE_SOUSCRIPTION);

  @override
  LocalSouscriptionEntity getEntity() {
    return new LocalSouscriptionEntity();
  }

  Future<List<LocalSouscriptionEntity>> existTypeSouscription(int idTypeSouscription) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM  $TABLE_NAME_TYPE_SOUSCRIPTION where $apiIdType=$idTypeSouscription ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalSouscriptionEntity>> listTypeSouscription(int userID) async {
    var dbClient = await DatabaseHelper.database;

    String sql = '''SELECT * FROM $TABLE_NAME_TYPE_SOUSCRIPTION  where $idUsr=$userID''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }
}
