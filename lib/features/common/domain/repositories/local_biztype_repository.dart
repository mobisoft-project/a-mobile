import 'package:agrimobile/features/common/domain/entities/local_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_biztype_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalBiztypeRepository extends BaseRepository<LocalBiztypeEntity, int> {
  LocalBiztypeRepository() : super(TABLE_NAME_BIZTYPE);

  @override
  LocalBiztypeEntity getEntity() {
    return new LocalBiztypeEntity();
  }


  Future<List<LocalBiztypeEntity>> existBiztype(int idAssurance) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_BIZTYPE where  $apiIdBiztype=$idAssurance ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future <LocalBiztypeEntity> oneBiztype(String id) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_BIZTYPE where  $apiIdBiztype=$id ''';
    final data = await dbClient.rawQuery(sql);

    if(data.length>0){
      return getEntity().fromDatabase(data[0]);
    }else{
      return null ;
    }
  }

  Future<List<LocalBiztypeEntity>> allBiztype() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_BIZTYPE order by $apiIdBiztype desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


  Future<List<LocalBiztypeEntity>> filterBisy(String name) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_BIZTYPE where $nameBiztype like "%$name%" order by $nameBiztype asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}