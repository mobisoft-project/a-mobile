import 'package:agrimobile/helpers/helpers_utils.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/features/common/domain/entities/local_account_statistics_entity.dart';

class LocalMyStatisticsRepository extends BaseRepository<LocalMyStatisticsEntity, int> {
  LocalMyStatisticsRepository() : super(TABLE_NAME_ACCOUNT_STATS);

  @override
  LocalMyStatisticsEntity getEntity() {
    return new LocalMyStatisticsEntity();
  }

  Future<List<LocalMyStatisticsEntity>> existStatics(
    int totalOffre, 
    int activeOffre, 
    int chiffreAffaire, 
    int idUser) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM  $TABLE_NAME_ACCOUNT_STATS where $idUtlsateur=$idUser and $totalOffre=$totalOffre and  $activeOffre=$activeOffre and $chiffreAffaire=$chiffreAffaire ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalMyStatisticsEntity>> userStatistics(int idUser) async {
    var dbClient = await DatabaseHelper.database;

    String sql = '''SELECT * FROM $TABLE_NAME_ACCOUNT_STATS where $idUtlsateur=$idUser ''';

    final data = await dbClient.rawQuery(sql);
    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

}
