import 'package:agrimobile/features/common/domain/entities/local_actor_profil_entity.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/base_repository.dart';

class LocalActorProfilRepository extends BaseRepository<LocalActorProfilEntity, int> {
  LocalActorProfilRepository() : super(TABLE_NAME_ACTORS_PROFIL);

  @override
  LocalActorProfilEntity getEntity() {
    return new LocalActorProfilEntity();
  }

  Future<List<LocalActorProfilEntity>> existsActorProfil(String moncode) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM  $TABLE_NAME_ACTORS_PROFIL where $actorCode=$moncode''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalActorProfilEntity>> actorProfil(String moncode) async {
    var dbClient = await DatabaseHelper.database;
    String sql = '''SELECT * FROM $TABLE_NAME_ACTORS_PROFIL where $actorCode=$moncode ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

}
