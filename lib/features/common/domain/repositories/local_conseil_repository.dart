import 'package:agrimobile/features/common/domain/entities/local_conseil_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

class LocalConseilRepository extends BaseRepository<LocalConseilEntity, int> {
  LocalConseilRepository() : super(TABLE_NAME_CONSEIL);

  @override
  LocalConseilEntity getEntity() {
    return new LocalConseilEntity();
  }

  Future<List<LocalConseilEntity>> existConseil(int idConseil) async {
    var dbClient = await DatabaseHelper.database;
    final sql =
        '''SELECT * FROM  $TABLE_NAME_CONSEIL where $apiIdConseil=$idConseil ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalConseilEntity>> listConseils() async {
    var dbClient = await DatabaseHelper.database;

    String sql = '''SELECT * FROM $TABLE_NAME_CONSEIL''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }
}
