import 'package:agrimobile/features/common/domain/entities/local_pret_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalPretRepository extends BaseRepository<LocalPretEntity, int> {
  LocalPretRepository() : super(TABLE_NAME_PRET);

  @override
  LocalPretEntity getEntity() {
    return new LocalPretEntity();
  }



  Future<List<LocalPretEntity>> listPret(int idUser) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql ='''SELECT *  FROM $TABLE_NAME_PRET where $idUserPret=$idUser and $etatDemandePret IN (-1,0,1)  order by $idPret desc  ''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalPretEntity>> existPret(String idPret) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_PRET where  $idProjetPret=$idPret ''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

}



