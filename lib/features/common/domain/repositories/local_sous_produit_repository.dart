import 'package:agrimobile/features/common/domain/entities/local_sous_produit_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalSousProduitRepository extends BaseRepository<LocalSousProduitEntity, int> {
  LocalSousProduitRepository() : super(TABLE_NAME_SOUSPRODUIT);

  @override
  LocalSousProduitEntity getEntity() {
    return new LocalSousProduitEntity();
  }


  Future<List<LocalSousProduitEntity>> existSousProduit(int idSousProduit) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_SOUSPRODUIT where  $apiIdSousProduit=$idSousProduit ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future <LocalSousProduitEntity> oneSousProduit(String idProduit,String name) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_SOUSPRODUIT where $categorieFilleIdSousProduit=$idProduit and $nomCategorieFilleSousProduit="$name" ''';
    final data = await dbClient.rawQuery(sql);

    if(data.length>0){
      return getEntity().fromDatabase(data[0]);
    }else{
      return null ;
    }

  }

  Future<List<LocalSousProduitEntity>> allSousProduit() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_SOUSPRODUIT order by $nomCategorieFilleSousProduit asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalSousProduitEntity>> allSousPProduit(int idProduit) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_SOUSPRODUIT where  $categorieFilleIdSousProduit=$idProduit order by $nomCategorieFilleSousProduit asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


  Future<List<LocalSousProduitEntity>> filterSousProduit(int idProduit,String name) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_SOUSPRODUIT where  $categorieFilleIdSousProduit=$idProduit and $nomCategorieFilleSousProduit like "%$name%" order by $nomCategorieFilleSousProduit asc ''';
    print(sql);
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalSousProduitEntity>> filterSousProduitList(String produits) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    if(produits.compareTo("")==0){
      sql = '''SELECT * FROM $TABLE_NAME_SOUSPRODUIT order by $nomCategorieFilleSousProduit asc ''';
    }else{
      sql = '''SELECT * FROM $TABLE_NAME_SOUSPRODUIT where $categorieFilleIdSousProduit in ($produits) order by $nomCategorieFilleSousProduit asc ''';
    }
    print(sql);

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}