import 'dart:ffi';

import 'package:agrimobile/features/common/domain/entities/local_unite_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalUniteRepository extends BaseRepository<LocalUniteEntity, int> {
  LocalUniteRepository() : super(TABLE_NAME_UNITE);

  @override
  LocalUniteEntity getEntity() {
    return new LocalUniteEntity();
  }


  Future<List<LocalUniteEntity>> existUnite(int idUnite) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_UNITE where  $apiIdUnite=$idUnite ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future <LocalUniteEntity> oneUniteProduit(String idUnite) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_UNITE where  $apiIdUnite=$idUnite ''';
    final data = await dbClient.rawQuery(sql);

    if(data.length>0){
      return getEntity().fromDatabase(data[0]);
    }else{
      return null ;
    }
  }

  Future<List<LocalUniteEntity>> allUnite() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_UNITE order by $nomUnite asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalUniteEntity>> allUniteByid(String allId) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_UNITE where $apiIdUnite in $allId order by $nomUnite asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalUniteEntity>> filterUniteProduit(String allId,String name) async {


    if(allId.compareTo("")!=0) {
      var dbClient = await DatabaseHelper.database;
      String sql = "";
      sql =
      '''SELECT * FROM $TABLE_NAME_UNITE where $apiIdUnite in $allId and $nomUnite like "%$name%"  order by $nomUnite asc ''';
      final data = await dbClient.rawQuery(sql);

      return List.generate(data.length, (i) {
        return getEntity().fromDatabase(data[i]);
      });
    }else{
      return null ;
    }
  }




}