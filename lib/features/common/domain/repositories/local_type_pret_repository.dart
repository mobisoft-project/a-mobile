import 'package:agrimobile/features/common/domain/entities/local_type_pret_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalTypePretRepository extends BaseRepository<LocalTypePretEntity, int> {
  LocalTypePretRepository() : super(TABLE_NAME_TYPE_PRET);

  @override
  LocalTypePretEntity getEntity() {
    return new LocalTypePretEntity();
  }


  Future<List<LocalTypePretEntity>> existTypePret(int idTypePret) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_TYPE_PRET where  $apiIdTypePret=$idTypePret ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalTypePretEntity>> allTypePret() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_TYPE_PRET where $etatTypePret=1 order by $apiIdTypePret desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<LocalTypePretEntity> oneTypePret(String key) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql = '''SELECT * FROM $TABLE_NAME_TYPE_PRET where $etatTypePret=1 and $keyTypePret="$key" order by $libelleTypePret asc ''';

    final data = await dbClient.rawQuery(sql);

    return getEntity().fromDatabase(data[0]);
  }

  Future<List<LocalTypePretEntity>> filterTypePret(String name) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql = '''SELECT * FROM $TABLE_NAME_TYPE_PRET where  $libelleTypePret like "%$name%" and $etatTypePret=1 order by $libelleTypePret asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}