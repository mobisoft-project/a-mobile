import 'package:agrimobile/features/common/domain/entities/local_annonce_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';
import 'package:agrimobile/core/utils/data_constantes_utils.dart';

class LocalAnnonceRepository extends BaseRepository<LocalAnnonceEntity, int> {
  LocalAnnonceRepository() : super(TABLE_NAME_ANNONCE);

  @override
  LocalAnnonceEntity getEntity() {
    return new LocalAnnonceEntity();
  }

  Future<List<LocalAnnonceEntity>> existAnnonce(int idServer) async {
    var dbClient = await DatabaseHelper.database;
    final sql =
        '''SELECT  distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite FROM  $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_ANNONCE.$apiIdAnnonce=$idServer and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit  and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalAnnonceEntity>> allAnnonce(int nombre) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    int todaytime = (DateTime.now().millisecondsSinceEpoch / 1000).toInt();
    if (nombre == 0) {
      sql =
          '''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT where $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$statutPrive=1 order by $apiIdAnnonce desc  ''';
    } else {
      sql =
          '''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT where $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$statutPrive=1 order by $apiIdAnnonce desc  limit 0,$nombre ''';
    }

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalAnnonceEntity>> userfilterlistAnnonce(
      String categories,
      String produits,
      String sousproduits,
      double lowerValue,
      double upperValue,
      int disponibilite,
      int biztypeId,
      bool filterPrix) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    String plus = "";
    // int todaytime=(DateTime.now().millisecondsSinceEpoch/1000).toInt();
    int todaytime = 0;

    if (disponibilite != 0) {
      plus += " and $TABLE_NAME_ANNONCE.$disponibiliteAnnonce=$disponibilite";
    }

    if (categories.compareTo("") != 0) {
      plus +=" and $TABLE_NAME_CATEGORIE.$apiIdCategorieProduit = $TABLE_NAME_PRODUIT.$categorieIdProduit and $TABLE_NAME_CATEGORIE.$apiIdCategorieProduit in($categories)";
    }

    if (produits.compareTo("") != 0) {
      plus += " and $TABLE_NAME_ANNONCE.$categorieIdAnnonce in ($produits)";
    }

    if (sousproduits.compareTo("") != 0) {
      plus += " and $TABLE_NAME_ANNONCE.$titleAnnonce in ($sousproduits)";
    }

    if (filterPrix == true) plus +=
          " and $TABLE_NAME_ANNONCE.$prixAnnonce >= $lowerValue and $TABLE_NAME_ANNONCE.$prixAnnonce <= $upperValue";

    sql =
        '''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE, $TABLE_NAME_CATEGORIE where $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$statutPrive=1 and $TABLE_NAME_ANNONCE.$biztypeIdAnnonce=$biztypeId $plus  order by $apiIdAnnonce desc  ''';
    print(sql);

    final data = await dbClient.rawQuery(sql);
    print(data.length);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalAnnonceEntity>> listAnnonce(
      String typePage,String idUser, int idScoop, int type) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    //int todaytime=(DateTime.now().millisecondsSinceEpoch/1000).toInt();
    int todaytime = 0;

    print(todaytime);

    if (typePage.compareTo(DataConstantesUtils.ANNONCE_MES_CONTACTS) == 0) {
      print("ANNONCE_MES_CONTACTS");
      sql = '''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_ANNONCE.$autorisationAnnonce="1" and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$statutPrive=1 order by $apiIdAnnonce desc  ''';

    } else if (typePage.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE) == 0) {
      print("ANNONCE_SCOOP_PAGE");

        sql = '''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$scoopId=$idScoop order by $apiIdAnnonce desc  ''';

    } else if (typePage.compareTo(DataConstantesUtils.ANNONCE_MARCHE) == 0 || typePage.compareTo(DataConstantesUtils.ANNONCE_ACHAT) == 0) {
      print("ANNONCE_MARCHE--------11111111111");
     // sql ='''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$statutPrive=1 and $TABLE_NAME_ANNONCE.$biztypeIdAnnonce=$type order by $apiIdAnnonce desc  ''';
      sql ='''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$statutPrive=1 order by $apiIdAnnonce desc  ''';

    }/*  else if (typePage.compareTo(DataConstantesUtils.ANNONCE_ACHAT) == 0) {
      print("ANNONCE_ACHAT");
      sql ='''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$statutPrive=1 order by $apiIdAnnonce desc  ''';

    }  */else {
      print("ANNONCE_VENDEURS");

      sql ='''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$userIdAnnonce="${idUser}" order by $apiIdAnnonce desc  ''';
    }

    print(sql);
    final data = await dbClient.rawQuery(sql);
    print("taille: "+data.length.toString());

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


  Future<List<LocalAnnonceEntity>> listAnnonceCategories(
      String typePage,String idUser, int idScoop, int type,String categories) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    String plus = "";
    String plus_table = "";
    //int todaytime=(DateTime.now().millisecondsSinceEpoch/1000).toInt();

    int todaytime = 0;
      if(categories.compareTo("")==1){
        if(categories.compareTo("0")==1){
          plus_table=",$TABLE_NAME_CATEGORIE ";
          plus=" and $TABLE_NAME_CATEGORIE.$apiIdCategorieProduit = $TABLE_NAME_PRODUIT.$categorieIdProduit and $TABLE_NAME_CATEGORIE.$apiIdCategorieProduit in($categories)";
        }
      }

    if (typePage.compareTo(DataConstantesUtils.ANNONCE_MES_CONTACTS) == 0) {
      print("ANNONCE_MES_CONTACTS");
      sql = '''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE $plus_table where  $TABLE_NAME_ANNONCE.$autorisationAnnonce="1" and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$statutPrive=1 $plus order by $apiIdAnnonce desc  ''';

    } else if (typePage.compareTo(DataConstantesUtils.ANNONCE_SCOOP_PAGE) == 0) {
      print("ANNONCE_SCOOP_PAGE");

      sql = '''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE $plus_table where  $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$scoopId=$idScoop $plus order by $apiIdAnnonce desc  ''';

    } else if (typePage.compareTo(DataConstantesUtils.ANNONCE_MARCHE) == 0) {
      print("ANNONCE_MARCHE");
      sql ='''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE $plus_table where  $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$statutPrive=1 and $TABLE_NAME_ANNONCE.$biztypeIdAnnonce=$type $plus order by $apiIdAnnonce desc  ''';

    } else if (typePage.compareTo(DataConstantesUtils.ANNONCE_ACHAT) == 0) {
      print("ANNONCE_ACHAT");
      sql ='''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE $plus_table where  $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$statutPrive=1 and $TABLE_NAME_ANNONCE.$biztypeIdAnnonce=$type $plus order by $apiIdAnnonce desc  ''';

    } else {
      print("ANNONCE_VENDEURS");

      sql =
      '''SELECT distinct($TABLE_NAME_ANNONCE.$idAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE $plus_table where $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$userIdAnnonce="${idUser}" $plus order by $apiIdAnnonce desc  ''';
    }

    print(sql);
    final data = await dbClient.rawQuery(sql);
    print("taille: "+data.length.toString());

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalAnnonceEntity>> getMaxPrixAnnonce(
     String idUser, int idScoop, int type) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    //int todaytime=(DateTime.now().millisecondsSinceEpoch/1000).toInt();
    int todaytime = 0;

    print(todaytime);
    sql =
        '''SELECT MAX($TABLE_NAME_ANNONCE.$prixAnnonce),$TABLE_NAME_ANNONCE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_ANNONCE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where $TABLE_NAME_ANNONCE.$expireTime>=$todaytime and $TABLE_NAME_ANNONCE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit  and $TABLE_NAME_PRODUIT.$statutProduit='1' and $TABLE_NAME_ANNONCE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite and $TABLE_NAME_ANNONCE.$statutPrive=1 and $TABLE_NAME_ANNONCE.$biztypeIdAnnonce=$type order by $apiIdAnnonce desc  ''';

    print(sql);
    final data = await dbClient.rawQuery(sql);
    print(data.length);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }
}
