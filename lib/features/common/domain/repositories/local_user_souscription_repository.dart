import 'package:agrimobile/features/common/domain/entities/local_user_sourciption_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalUserSouscriptionRepository extends BaseRepository<LocalUserSouscriptionEntity, int> {
  LocalUserSouscriptionRepository() : super(TABLE_NAME_USER_SOUSCRIPTION);

  @override
  LocalUserSouscriptionEntity getEntity() {
    return new LocalUserSouscriptionEntity();
  }

  Future<List<LocalUserSouscriptionEntity>> userSouscriptionExists(int idUserSouscription) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM  $TABLE_NAME_USER_SOUSCRIPTION where $id=$idUserSouscription ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalUserSouscriptionEntity>> userListSouscription(int userId) async {
    var dbClient = await DatabaseHelper.database;

    String sql = '''SELECT * FROM $TABLE_NAME_USER_SOUSCRIPTION where $idUtilisateur=$userId ''';

    final data = await dbClient.rawQuery(sql);
    
    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }
}
