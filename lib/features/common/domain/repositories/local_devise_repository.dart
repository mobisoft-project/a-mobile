import 'package:agrimobile/features/common/domain/entities/local_devise_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalDeviseRepository extends BaseRepository<LocalDeviseEntity, int> {
  LocalDeviseRepository() : super(TABLE_NAME_DEVISE);

  @override
  LocalDeviseEntity getEntity() {
    return new LocalDeviseEntity();
  }


  Future<List<LocalDeviseEntity>> existDevise(int idDevise) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_DEVISE where  $apiIdDevise=$idDevise ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalDeviseEntity>> allDevise() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_DEVISE order by $apiIdDevise desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}