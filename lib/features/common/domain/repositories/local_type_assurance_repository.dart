import 'package:agrimobile/features/common/domain/entities/local_type_assurance_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalTypeAssuranceRepository extends BaseRepository<LocalTypeAssuranceEntity, int> {
  LocalTypeAssuranceRepository() : super(TABLE_NAME_TASSURANCE);

  @override
  LocalTypeAssuranceEntity getEntity() {
    return new LocalTypeAssuranceEntity();
  }


  Future<List<LocalTypeAssuranceEntity>> existTypeAssurance(int idTypeAssurance) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_TASSURANCE where  $apiIdTAssurance=$idTypeAssurance ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalTypeAssuranceEntity>> allTypeAssurance(String idAssurance) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_TASSURANCE where  $TABLE_NAME_TASSURANCE.$typeassuranceIdTAssurance="$idAssurance" order by $apiIdTAssurance desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}