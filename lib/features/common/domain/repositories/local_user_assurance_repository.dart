import 'package:agrimobile/features/common/domain/entities/local_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_user_assurance_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalUserAssuranceRepository extends BaseRepository<LocalUserAssuranceEntity, int> {
  LocalUserAssuranceRepository() : super(TABLE_NAME_USERASSURANCE);

  @override
  LocalUserAssuranceEntity getEntity() {
    return new LocalUserAssuranceEntity();
  }


  Future<List<LocalUserAssuranceEntity>> existUAssurance(int idUserAssurance) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_USERASSURANCE where  $idInfoUserAssurance=$idUserAssurance ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalUserAssuranceEntity>> allUAssurance(int id,int idAssureur) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_USERASSURANCE where $TABLE_NAME_USERASSURANCE.$idUser=$id and $TABLE_NAME_USERASSURANCE.$idAssureurInfo=$idAssureur  order by $idInfoUserAssurance desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}