import 'package:agrimobile/features/common/domain/entities/local_user_statistics_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalUserStatisticRepository extends BaseRepository<LocalUserStatisticEntity, int> {
  LocalUserStatisticRepository() : super(TABLE_NAME_USER_STATISTIC);

  @override
  LocalUserStatisticEntity getEntity() {
    return new LocalUserStatisticEntity();
  }

  Future<List<LocalUserStatisticEntity>> userStatisticExist(
    int nbreAchat, int montantAchat, int endLivraison, int waitingLivraison, int uId) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM  $TABLE_NAME_USER_STATISTIC where $idUtlisateur=$uId and $nbreAchat=$nbreAchat and  $montantAchat=$montantAchat and $endLivraison=$endLivraison and $waitingLivraison=$waitingLivraison ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalUserStatisticEntity>> userStatisticData(int uId) async {
    var dbClient = await DatabaseHelper.database;

    String sql = '''SELECT * FROM $TABLE_NAME_USER_STATISTIC where $idUtlisateur=$uId''';

    final data = await dbClient.rawQuery(sql);
    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

}