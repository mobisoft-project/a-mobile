import 'package:agrimobile/features/common/domain/entities/local_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_country_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalCountryRepository extends BaseRepository<LocalCountryEntity, int> {
  LocalCountryRepository() : super(TABLE_NAME_COUNTRY);

  @override
  LocalCountryEntity getEntity() {
    return new LocalCountryEntity();
  }


  Future<List<LocalCountryEntity>> existCountry(int idCountry) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_COUNTRY where  $apiIdCountry=$idCountry ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }



  Future<List<LocalCountryEntity>> filterCountry(String name) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_COUNTRY where  $nomFrCountry like "%$name%" order by $nomFrCountry asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


  Future<List<LocalCountryEntity>> allCountry() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_COUNTRY order by $nomFrCountry asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}