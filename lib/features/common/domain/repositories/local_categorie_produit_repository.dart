import 'package:agrimobile/features/common/domain/entities/local_categorie_produit_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalCategorieProduitRepository extends BaseRepository<LocalCategorieProduitEntity, int> {
  LocalCategorieProduitRepository() : super(TABLE_NAME_CATEGORIE);

  @override
  LocalCategorieProduitEntity getEntity() {
    return new LocalCategorieProduitEntity();
  }


  Future<List<LocalCategorieProduitEntity>> existProduit(int idProduit) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_CATEGORIE where  $apiIdCategorieProduit=$idProduit ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future <LocalCategorieProduitEntity> oneProduit(String idProduit) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_CATEGORIE where  $apiIdProduit=$idProduit ''';
    final data = await dbClient.rawQuery(sql);

    if(data.length>0){
      return getEntity().fromDatabase(data[0]);
    }else{
      return null ;
    }

  }

  Future<List<LocalCategorieProduitEntity>> allCategories() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_CATEGORIE where $statutCategorieProduit=1 order by $nameCategorieProduit asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }
  Future<List<LocalCategorieProduitEntity>> filterProduit(String name) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_CATEGORIE where  $nameCategorieProduit like "%$name%" and $statutCategorieProduit='1'  order by $nameCategorieProduit asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}