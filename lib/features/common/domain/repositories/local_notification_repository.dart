import 'package:agrimobile/features/common/domain/entities/local_notification_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalNotificationRepository extends BaseRepository<LocalNotificationEntity, int> {
  LocalNotificationRepository() : super(TABLE_NAME_NOTIFICATION);

  @override
  LocalNotificationEntity getEntity() {
    return new LocalNotificationEntity();
  }

  Future<List<LocalNotificationEntity>> existNotification(int idNotification) async {
    var dbClient = await DatabaseHelper.database;
    final sql ='''SELECT * FROM  $TABLE_NAME_NOTIFICATION where $TABLE_NAME_NOTIFICATION.$apiId=$idNotification ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalNotificationEntity>> listNotification(int utilisateurId) async {
    var dbClient = await DatabaseHelper.database;

    String sql = '''SELECT * FROM $TABLE_NAME_NOTIFICATION where $TABLE_NAME_NOTIFICATION.$userId=$utilisateurId ORDER BY $idNotification DESC ''';
    
    final data = await dbClient.rawQuery(sql);
    
    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }
}
