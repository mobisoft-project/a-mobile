import 'package:agrimobile/features/common/domain/entities/local_commande_detail_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalCommandeDetailRepository extends BaseRepository<LocalCommandeDetailEntity, int> {
  LocalCommandeDetailRepository() : super(TABLE_NAME_COMMANDE_DETAIL);

  @override
  LocalCommandeDetailEntity getEntity() {
    return new LocalCommandeDetailEntity();
  }

  Future<List<LocalCommandeDetailEntity>> existCommandeDetail(LocalCommandeDetailEntity localCommandeEntity, String keyCommandeDetail) async {
    var dbClient = await DatabaseHelper.database;
    String nom=localCommandeEntity.nomProduitCommandeDetail;
    String qte=localCommandeEntity.qteProduitCommandeDetail;
    String prixUnitaire=localCommandeEntity.prixUnitaireCommandeDetail;
    String prixTotal=localCommandeEntity.prixTotalCommandeDetail;

    final sql = '''SELECT  distinct($TABLE_NAME_COMMANDE_DETAIL.$idCommandeDetail),$TABLE_NAME_COMMANDE_DETAIL.* FROM  $TABLE_NAME_COMMANDE_DETAIL where  $TABLE_NAME_COMMANDE_DETAIL.$keyCommande="$keyCommandeDetail" and  $TABLE_NAME_COMMANDE_DETAIL.$nomProduitCommandeDetail="$nom" and  $TABLE_NAME_COMMANDE_DETAIL.$qteProduitCommandeDetail="$qte" and  $TABLE_NAME_COMMANDE_DETAIL.$prixUnitaireCommandeDetail="$prixUnitaire" and  $TABLE_NAME_COMMANDE_DETAIL.$prixTotalCommandeDetail="$prixTotal" ''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalCommandeDetailEntity>> allAnnonce(int nombre) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    int todaytime=(DateTime.now().millisecondsSinceEpoch/1000).toInt();
    if(nombre==0) {

      sql ='''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT where $TABLE_NAME_COMMANDE.$expireTime>=$todaytime and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit  order by $apiIdAnnonce desc  ''';

    }else{
      sql ='''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT where $TABLE_NAME_COMMANDE.$expireTime>=$todaytime and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit order by $apiIdAnnonce desc  limit 0,$nombre ''';

    }

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalCommandeDetailEntity>> listAnnonce(String idUser) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    int todaytime=(DateTime.now().millisecondsSinceEpoch/1000).toInt();

    print(todaytime);

    if(idUser.compareTo("-10")==0){

      sql ='''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_COMMANDE.$autorisationAnnonce="1" and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_COMMANDE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite  order by $apiIdAnnonce desc  ''';

    }else if(idUser.compareTo("0")==0){

      sql ='''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_COMMANDE.$expireTime>=$todaytime and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_COMMANDE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite  order by $apiIdAnnonce desc  ''';

    }else {
      sql =
      '''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where $TABLE_NAME_COMMANDE.$userIdAnnonce = $idUser and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_COMMANDE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite  order by $apiIdAnnonce desc  ''';
    }
    print(sql);
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}