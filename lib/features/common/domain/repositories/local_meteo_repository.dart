import 'package:agrimobile/features/common/domain/entities/local_meteo_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalMeteoRepository extends BaseRepository<LocalMeteoEntity, int> {
  LocalMeteoRepository() : super(TABLE_NAME_METEO);

  @override
  LocalMeteoEntity getEntity() {
    return new LocalMeteoEntity();
  }


  Future<List<LocalMeteoEntity>> existMeteo(int idMeteo,String date) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_METEO where  $apiIdMeteo=$idMeteo and  $dateMeteo="$date" ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalMeteoEntity>> allMeteo(int idUser) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_METEO where $TABLE_NAME_METEO.$userIdMeteo=$idUser group by $dateMeteo order by $idMeteo desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}