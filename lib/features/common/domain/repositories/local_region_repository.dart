import 'package:agrimobile/allTranslations.dart';
import 'package:agrimobile/features/common/domain/entities/local_assurance_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_country_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_region_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalRegionRepository extends BaseRepository<LocalRegionEntity, int> {
  LocalRegionRepository() : super(TABLE_NAME_REGION);

  @override
  LocalRegionEntity getEntity() {
    return new LocalRegionEntity();
  }


  Future<List<LocalRegionEntity>> existRegion(int idRegion) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_REGION where  $apiIdRegion=$idRegion ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalRegionEntity>> allRegion() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_REGION order by $nomRegion asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalRegionEntity>> allRegionCountry(int idPays) async {

    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_REGION where  $paysId=$idPays order by $nomRegion asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalRegionEntity>> filterRegion(String idPays,String name) async {

    if(idPays.compareTo("")==0)idPays="0";

    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_REGION where  $paysId=$idPays and  $nomRegion like "%$name%" order by $nomRegion asc ''';
    final data = await dbClient.rawQuery(sql);

    List<LocalRegionEntity> allRegion=[];

    allRegion= List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });

    if(idPays.compareTo("0")!=0) {
      LocalRegionEntity newRegion = new LocalRegionEntity();
      newRegion.apiId = 0;
      newRegion.nomRegion = "${allTranslations.text('new_region')}";
      allRegion.add(newRegion);
    }

    return allRegion ;
  }


}