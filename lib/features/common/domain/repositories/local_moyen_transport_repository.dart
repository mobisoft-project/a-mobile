import 'package:agrimobile/features/common/domain/entities/local_moyen_transport_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalMoyenTransportRepository extends BaseRepository<LocalMoyenTransportEntity, int> {
  LocalMoyenTransportRepository() : super(TABLE_NAME_MOYEN_TRANSPORT);

  @override
  LocalMoyenTransportEntity getEntity() {
    return new LocalMoyenTransportEntity();
  }


  Future<List<LocalMoyenTransportEntity>> existMoyenTransport(int idMoyenTransport) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_MOYEN_TRANSPORT where  $apiIdMoyenTransport=$idMoyenTransport ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalMoyenTransportEntity>> allMoyenTransport() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_MOYEN_TRANSPORT where $etatMoyenTransport=1 order by $apiIdMoyenTransport desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalMoyenTransportEntity>> filterMoyenTransport(String name) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql = '''SELECT * FROM $TABLE_NAME_MOYEN_TRANSPORT where  $libelleMoyenTransport like "%$name%" and $etatMoyenTransport=1 order by $apiIdModeLivraison desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}