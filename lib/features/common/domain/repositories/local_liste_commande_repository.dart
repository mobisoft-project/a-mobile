import 'package:agrimobile/features/common/domain/entities/local_commande_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalListeCommandeRepository extends BaseRepository<LocalCommandeEntity, int> {
  LocalListeCommandeRepository() : super(TABLE_NAME_COMMANDE);

  @override
  LocalCommandeEntity getEntity() {
    return new LocalCommandeEntity();
  }

  Future<List<LocalCommandeEntity>> existCommande(String key) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT  distinct($TABLE_NAME_COMMANDE.$keyCommande),$TABLE_NAME_COMMANDE.* FROM  $TABLE_NAME_COMMANDE where  $TABLE_NAME_COMMANDE.$keyCommande="$key" ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalCommandeEntity>> allCommande(int nombre) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    int todaytime=(DateTime.now().millisecondsSinceEpoch/1000).toInt();

    //commande envoyées
    if(nombre==1) {

      sql ='''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT where $TABLE_NAME_COMMANDE.$expireTime>=$todaytime and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit  order by $apiIdAnnonce desc  ''';

    }else{
      sql ='''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT where $TABLE_NAME_COMMANDE.$expireTime>=$todaytime and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit order by $apiIdAnnonce desc  limit 0,$nombre ''';

    }

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalCommandeEntity>> listAnnonce(String idUser) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    int todaytime=(DateTime.now().millisecondsSinceEpoch/1000).toInt();

    print(todaytime);

    if(idUser.compareTo("-10")==0){

      sql ='''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_COMMANDE.$autorisationAnnonce="1" and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_COMMANDE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite  order by $apiIdAnnonce desc  ''';

    }else if(idUser.compareTo("0")==0){

      sql ='''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where  $TABLE_NAME_COMMANDE.$expireTime>=$todaytime and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_COMMANDE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite  order by $apiIdAnnonce desc  ''';

    }else {
      sql =
      '''SELECT distinct($TABLE_NAME_COMMANDE.$idAnnonce),$TABLE_NAME_COMMANDE.*, $TABLE_NAME_PRODUIT.$nomCategorieFilleProduit, $TABLE_NAME_UNITE.$nomUnite  FROM $TABLE_NAME_COMMANDE,$TABLE_NAME_PRODUIT,$TABLE_NAME_UNITE where $TABLE_NAME_COMMANDE.$userIdAnnonce = $idUser and $TABLE_NAME_COMMANDE.$categorieIdAnnonce=$TABLE_NAME_PRODUIT.$apiIdProduit and $TABLE_NAME_COMMANDE.$unitesIdAnnonce=$TABLE_NAME_UNITE.$apiIdUnite  order by $apiIdAnnonce desc  ''';
    }
    print(sql);
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}