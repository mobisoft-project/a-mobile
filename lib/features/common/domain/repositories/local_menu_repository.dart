import 'package:agrimobile/features/common/domain/entities/local_menu_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalMenuRepository extends BaseRepository<LocalMenuEntity, int> {
  LocalMenuRepository() : super(TABLE_NAME_MENU);

  @override
  LocalMenuEntity getEntity() {
    return new LocalMenuEntity();
  }

  Future<List<LocalMenuEntity>> existMenu(String code) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_MENU where  $codeMenu="$code" ''';
    final data = await dbClient.rawQuery(sql);
    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}