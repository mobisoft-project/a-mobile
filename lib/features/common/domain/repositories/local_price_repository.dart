import 'package:agrimobile/features/common/domain/entities/local_price_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalPriceRepository extends BaseRepository<LocalPriceEntity, int> {
  LocalPriceRepository() : super(TABLE_NAME_PRICE);

  @override
  LocalPriceEntity getEntity() {
    return new LocalPriceEntity();
  }

  Future<List<LocalPriceEntity>> existPrice(int idServer) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT  * FROM  $TABLE_NAME_PRICE  where  $TABLE_NAME_PRICE.$apiIdPrice=$idServer''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


  Future<List<LocalPriceEntity>> listFilterSpeculations(String produit) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    String sql1 = '''SELECT MAX($TABLE_NAME_PRICE.$timestampPrice) as $timestampPrice FROM $TABLE_NAME_PRICE ''';

    if(produit.compareTo("")==0){
      sql = '''SELECT Distinct($TABLE_NAME_PRICE.$nomProduitPrice) as $nomProduitPrice,$timestampPrice FROM  $TABLE_NAME_PRICE  where   $TABLE_NAME_PRICE.$timestampPrice in ($sql1) and $TABLE_NAME_PRICE.$villePrice!="" order by $TABLE_NAME_PRICE.$nomProduitPrice asc''';

    }else{
      sql = '''SELECT Distinct($TABLE_NAME_PRICE.$nomProduitPrice) as $nomProduitPrice,$timestampPrice FROM  $TABLE_NAME_PRICE  where  $TABLE_NAME_PRICE.$nomProduitPrice like "%$produit%" and  $TABLE_NAME_PRICE.$timestampPrice in ($sql1) and $TABLE_NAME_PRICE.$villePrice!="" order by $TABLE_NAME_PRICE.$nomProduitPrice asc''';

    }

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalPriceEntity>> listFilterSpeculationsCity(String produit,String city) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    String sql1 = '''SELECT MAX($TABLE_NAME_PRICE.$timestampPrice) as $timestampPrice FROM $TABLE_NAME_PRICE ''';

    if(city.compareTo("")==0){
      sql = '''SELECT Distinct($TABLE_NAME_PRICE.$villePrice) as $villePrice,$timestampPrice FROM  $TABLE_NAME_PRICE  where  $TABLE_NAME_PRICE.$nomProduitPrice = "$produit" and  $TABLE_NAME_PRICE.$timestampPrice in ($sql1) and $TABLE_NAME_PRICE.$villePrice!="" order by $TABLE_NAME_PRICE.$villePrice asc''';
    }else{
      sql = '''SELECT Distinct($TABLE_NAME_PRICE.$villePrice) as $villePrice,$timestampPrice FROM  $TABLE_NAME_PRICE  where  $TABLE_NAME_PRICE.$nomProduitPrice = "$produit" and $TABLE_NAME_PRICE.$villePrice like "%$city%" and  $TABLE_NAME_PRICE.$timestampPrice in ($sql1) and $TABLE_NAME_PRICE.$villePrice!="" order by $TABLE_NAME_PRICE.$villePrice asc''';
    }
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalPriceEntity>> listSpeculations(String produit) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";


    String sql1 = '''SELECT MAX($TABLE_NAME_PRICE.$timestampPrice) as $timestampPrice FROM $TABLE_NAME_PRICE ''';

    if(produit.compareTo("")==0){
      sql = '''SELECT Distinct($TABLE_NAME_PRICE.$nomProduitPrice) as $nomProduitPrice,$timestampPrice FROM  $TABLE_NAME_PRICE  where   $TABLE_NAME_PRICE.$timestampPrice in ($sql1)  and $TABLE_NAME_PRICE.$villePrice!="" order by $TABLE_NAME_PRICE.$nomProduitPrice asc''';

    }else{
      sql = '''SELECT Distinct($TABLE_NAME_PRICE.$nomProduitPrice) as $nomProduitPrice,$timestampPrice FROM  $TABLE_NAME_PRICE  where  $TABLE_NAME_PRICE.$nomProduitPrice ="$produit" and  $TABLE_NAME_PRICE.$timestampPrice in ($sql1) and $TABLE_NAME_PRICE.$villePrice!="" order by $TABLE_NAME_PRICE.$nomProduitPrice asc''';

    }

    final data = await dbClient.rawQuery(sql);


    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalPriceEntity>> listPrice(String nom,int time,String ville ) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    String plus = "";
    if(ville.compareTo("")!=0){
      plus = ' and $TABLE_NAME_PRICE.$villePrice="$ville"';
    }

    sql = '''SELECT DISTINCT($villePrice),* FROM  $TABLE_NAME_PRICE  where  $TABLE_NAME_PRICE.$timestampPrice =$time  $plus and $TABLE_NAME_PRICE.$villePrice!="" and $TABLE_NAME_PRICE.$nomProduitPrice="$nom"  order by $TABLE_NAME_PRICE.$nomProduitPrice asc,$TABLE_NAME_PRICE.$villePrice asc ''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}