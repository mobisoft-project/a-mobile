import 'package:agrimobile/features/common/domain/entities/local_mode_livraison_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalModeLivraisonRepository extends BaseRepository<LocalModeLivraisonEntity, int> {
  LocalModeLivraisonRepository() : super(TABLE_NAME_MODE_LIVRAISON);

  @override
  LocalModeLivraisonEntity getEntity() {
    return new LocalModeLivraisonEntity();
  }


  Future<List<LocalModeLivraisonEntity>> existModeLivraison(int idModeLivraison) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_MODE_LIVRAISON where  $apiIdModeLivraison=$idModeLivraison ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalModeLivraisonEntity>> allModeLivraison() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_MODE_LIVRAISON where $etatModeLivraison=1 order by $apiIdModeLivraison desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalModeLivraisonEntity>> filterModeLivraison(String name) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql = '''SELECT * FROM $TABLE_NAME_MODE_LIVRAISON where  $libelleModeLivraison like "%$name%" and $etatModeLivraison=1 order by $apiIdModeLivraison desc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}