import 'package:agrimobile/features/common/domain/entities/local_produit_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalProduitRepository extends BaseRepository<LocalProduitEntity, int> {
  LocalProduitRepository() : super(TABLE_NAME_PRODUIT);

  @override
  LocalProduitEntity getEntity() {
    return new LocalProduitEntity();
  }


  Future<List<LocalProduitEntity>> existProduit(int idProduit) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_PRODUIT where  $apiIdProduit=$idProduit ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future <LocalProduitEntity> oneProduit(String idProduit) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_PRODUIT where  $apiIdProduit=$idProduit ''';
    final data = await dbClient.rawQuery(sql);

    if(data.length>0){
      return getEntity().fromDatabase(data[0]);
    }else{
      return null ;
    }

  }

  Future<List<LocalProduitEntity>> allProduit() async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_PRODUIT order by $nomCategorieFilleProduit asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }
  Future<List<LocalProduitEntity>> filterProduit(String name) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_PRODUIT where  $nomCategorieFilleProduit like "%$name%" and $statutProduit='1'  order by $nomCategorieFilleProduit asc ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalProduitEntity>> filterCategorieProduitList(String categories) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    if(categories.compareTo("")==0){
      sql = '''SELECT * FROM $TABLE_NAME_PRODUIT order by $nomCategorieFilleProduit asc ''';
    }else{
      sql = '''SELECT * FROM $TABLE_NAME_PRODUIT where $categorieIdProduit in ($categories) order by $nomCategorieFilleProduit asc ''';
    }
    print(sql);

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}