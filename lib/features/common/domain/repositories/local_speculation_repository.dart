import 'package:agrimobile/features/common/domain/entities/local_speculation_entity.dart';
import 'package:agrimobile/features/common/domain/entities/local_unite_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalSpeculationRepository extends BaseRepository<LocalSpeculationEntity, int> {
  LocalSpeculationRepository() : super(TABLE_NAME_SPECULATION);

  @override
  LocalSpeculationEntity getEntity() {
    return new LocalSpeculationEntity();
  }


  Future<List<LocalSpeculationEntity>> existSpeculation(int idSpeculation) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_SPECULATION where  $apiIdSpeculation=$idSpeculation ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalSpeculationEntity>> allSpeculation(int userId) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";
    sql = '''SELECT * FROM $TABLE_NAME_SPECULATION where $userIdSpeculation=$userId order by $nomSpeculation asc ''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }


}