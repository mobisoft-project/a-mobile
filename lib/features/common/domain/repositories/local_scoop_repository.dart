import 'package:agrimobile/features/common/domain/entities/local_scoop_entity.dart';
import 'package:agrimobile/helpers/base_repository.dart';
import 'package:agrimobile/helpers/database_helper.dart';
import 'package:agrimobile/helpers/helpers_utils.dart';

class LocalScoopRepository extends BaseRepository<LocalScoopEntity, int> {
  LocalScoopRepository() : super(TABLE_NAME_SCOOP);

  @override
  LocalScoopEntity getEntity() {
    return new LocalScoopEntity();
  }

  Future<List<LocalScoopEntity>> existScoop(int idServer) async {
    var dbClient = await DatabaseHelper.database;
    final sql = '''SELECT * FROM $TABLE_NAME_SCOOP where  $idServerScoop=$idServer ''';
    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalScoopEntity>> listScoop(int idUser) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql ='''SELECT *  FROM $TABLE_NAME_SCOOP where $idUserScoop=$idUser and $etatScoop="1"  order by $nameScoop desc  ''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalScoopEntity>> filterScoopRight (int idUser,String content) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql ='''SELECT *  FROM $TABLE_NAME_SCOOP where $idUserScoop=$idUser and $etatScoop="1" and $memberRole in ("3","4") and ($nameScoop like "%$content%" or $cantonScoop like "%$content%")  order by $nameScoop desc  ''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<List<LocalScoopEntity>> filterScoop(int idUser,String content) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql ='''SELECT *  FROM $TABLE_NAME_SCOOP where $idUserScoop=$idUser and $etatScoop="1" and ($nameScoop like "%$content%" or $cantonScoop like "%$content%")  order by $nameScoop desc  ''';

    final data = await dbClient.rawQuery(sql);

    return List.generate(data.length, (i) {
      return getEntity().fromDatabase(data[i]);
    });
  }

  Future<LocalScoopEntity> oneScoop(int idUser,String keyScoop) async {
    var dbClient = await DatabaseHelper.database;
    String sql = "";

    sql ='''SELECT *  FROM $TABLE_NAME_SCOOP where $idUserScoop=$idUser and $idServerScoop=$keyScoop and $etatScoop="1"  order by $nameScoop desc  ''';

    final data = await dbClient.rawQuery(sql);

    return getEntity().fromDatabase(data[0]);
  }


}